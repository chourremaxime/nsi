Chaque caractère de la chaîne est **indexé**, c'est-à-dire **repéré par un nombre** (un numéro), ce qui permet
d'avoir un accès rapide au caractère désiré :

| Indice       |   | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 |   |
|--------------|---|---|---|---|---|---|---|---|---|---|---|----|----|----|----|----|----|---|
| `ma_phrase=` | " | V | o | i | c | i |   | u | n | e |   | p  | h  | r  | a  | s  | e  | " |

Attention ! Ce numéro débute par 0. On l'appelle **indice**.

!!! methode "Récupérer une lettre depuis un indice"

    Soit `s` une chaîne de caractères, et `i` un indice. `s[i]` est la valeur du caractère à l'indice `i` dans `s`.

Par exemple :

```python
lettre = ma_phrase[4]
print(lettre)  # i
print(ma_phrase[0])  # V
```

## Tranchage (sans danger)

Mettons la main à la pâte pour découvrir un fabuleux et complexe outil : le tranchage *(slice en anglais)*.

Le *slice* permet de récupérer un sous-ensemble d'une chaîne de caractères. Il existe trois paramètres pour le *slice* :

| Paramètre             | Valeur par défaut |
|-----------------------|-------------------|
| Indice de début       | 0                 |
| Indice de fin (exclu) | Dernier + 1       |
| Pas                   | 1                 |

!!! methode "Tranchage"

    La syntaxe consiste à séparer les paramètres avec des `:` en respectant l'ordre du tableau ci-dessus, et à les
    écrire comme les indices (entre crochets) : `#!python chaine[debut:fin:pas]`.

Si on omet un paramètre, sa valeur par défaut sera prise en compte : `#!python chaine[:4:2]` est équivalent à
`#!python chaine[0:4:2]`.

Si on omet le pas, on peut enlever les `:` entre l'indice de fin et le pas : `#!python chaine[1:3]` est équivalent à
`#!python chaine[1:3:]` et à `#!python chaine[1:3:1]`.

```python
chaine = "abcde"
print(chaine[1:3])  # bc
print(chaine[:2])  # ab
print(chaine[4:2:-1])  # ed
```

À l'instar de l'instruction `#!python range()`, le *slice* accepte aussi les pas négatifs.

!!! example "Exercice"

    Anticiper les résultats des instructions suivantes, puis les tester en Python :

    ```python
    ma_phrase[7]
    ma_phrase[18]
    ma_phrase[-1]
    ma_phrase[-5]
    ma_phrase[3:11]
    ma_phrase[:11]
    ma_phrase[3:]
    ma_phrase[::2]
    ma_phrase[len(ma_phrase)]
    ```