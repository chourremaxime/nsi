## Rappels

Pensez à enregistrer vos fichiers sur le réseau afin de ne pas les perdre ! Vous pouvez aussi les stocker sur une
clef USB pour y retravailler à la maison.

Commencez par créer l'arborescence de dossiers suivants :

```
Documents/
└── NSI/
    ├── Cours
    └── Exercices/
        ├── Introduction/
        ├── Conditions/
        ├── Boucles/
        └── Chaines/
            ├── exercice1.py
            ├── exercice2.py
            └── ...
```

Pensez à ajouter des commentaires dans vos fichiers lors de la programmation ! Il sera ainsi plus facile de revenir
dessus plus-tard.

## Exercice n°1 : Compter les lettres

Écrire un script qui demande à l'utilisateur de saisir un texte puis qui **compte et affiche** le nombre d'occurrences
du caractère ***e*** dans le texte saisi.

Dans l'exemple d'affichage ci-dessous, l'utilisateur a entré le texte : *Ceci est un exemple*. 

```
Entrez un texte : Ceci est un exemple
Il y a 5 "e" dans ce texte.
```

??? question "Une piste ?"

    Il y a deux façon de parcourir chaque caractère d'une chaîne :
    
    - Ou bien on parcourt chaque caractère à l'aide de l'opérateur d'inclusion `#!python in`.
    - Ou bien on parcourt la liste des index possibles, de `0` à `#!python len(chaine) - 1` à l'aide de la
    fonction `#!python range()`.

??? question "Un algorithme ?"

    - On demande à l'utilisateur d'entrer un texte ;
    - On parcourt chaque caractère du texte ;
    - Lorsque le caractère est un *e*, on compte 1 de plus ;
    - À la fin du parcours, on affiche le résultat.

## Exercice n°2 : Rajouter des espaces

On veut écrire un programme dans lequel l'utilisateur entre un texte, puis ce programme affiche ce texte en
insérant un espace entre chaque caractère.

Dans l'exemple d'affichage ci-dessous, l'utilisateur a entré le texte : *Ceci est un exemple*.

```
Entrez un texte : Ceci est un exemple
Réponse : C e c i   e s t   u n   e x e m p l e
```

**Amélioration** : Faites en sorte de n'avoir qu'un seul espace au lieu de trois *(cas d'un caractère étant un espace)*.

??? question "Une piste ?"
    
    Comme on ne peut pas modifier le contenu d'une chaîne de caractère, le plus simple est de partir d'une chaîne vide :
    `#!python texte = ""` que l'on remplit *au fur et à mesure* et qui sera la réponse fournie par le programme.

??? question "Un algorithme ?"

    - On demande à l'utilisateur d'entrer un texte ;
    - On définit une chaîne vide ;
    - On parcourt chaque caractère du texte ;
    - Chaque caractère est stocké dans la chaîne puis suivi d'un espace ;
    - À la fin du parcours, on affiche la chaîne (qui n'est plus vide !).

## Exercice n°3 : Inverser un texte

Écrire un script qui demande un texte à l'utilisateur, puis qui renvoie une copie inversée de ce texte.

Dans l'exemple d'affichage ci-dessous, l'utilisateur a entré le texte : *Ceci est un exemple*.

```
Entrez un texte : Ceci est un exemple
Réponse : elpmexe nu tse iceC
```

**Amélioration** : Il faudra ensuite que le programme indique si le texte saisi est un palindrome (un texte qui
peut se lire dans les deux sens, comme *radar* ou *s.o.s*).

??? question "Une piste ?"

    C'est le même principe que l'exercice n°2, il faut par contre reconstruire la chaîne *à l'envers*.

??? question "Une info ?"

    En Python:
    
    - la concaténation 'Cou'+'Rage' donne 'CouRage' ;
    - mais la concaténation 'Rage'+'Cou' donne 'RageCou'.