Une chaîne de caractère ne peut pas être modifiée, il faudra donc ***tricher*** si on souhaite la modifier. :

- On crée une chaîne vide. Par exemple : `chaine = """`
- On parcourt les caractères de la chaîne à modifier et on place les caractères à conserver dans la chaîne vide.
- On affiche la chaîne qui n'est plus vide...

## Parcourir une chaîne

On peut définir une chaîne :

```python
chaine = "Pour un test"
```

Pour parcourir les caractères d'une chaîne, on utilise une boucle **for**, il existe 2 méthodes pour cela :

```python
#méthode 1 : la variable i prend pour valeurs chaque indice
for i in range(len(chaine)):
   print(chaine[i])
```

```python
#méthode 2 : la variable carac prend pour valeurs chaque caractère
for carac in chaine:
   print(carac)
```

## Modifier une chaîne

Voici un example de programme qui lit le texte entré par un utilisateur et qui affiche le même texte dans lequel
tous les « e » ont été remplacés par des « X ».

```python
message = input('Entrer un texte : ')
reponse = ""           # On initialise une chaîne vide
for carac in message:  # Pour chaque caractère du message
   if carac == "e":    # Si le caractère est 'e'
      reponse += "X"   # On ajoute 'X' à la réponse
   else:               # Sinon
      reponse += carac # On ajoute le caractère en cours 
print("Texte modifié :", reponse)
```