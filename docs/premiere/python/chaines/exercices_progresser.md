## Rappels

Pensez à enregistrer vos fichiers sur le réseau afin de ne pas les perdre ! Vous pouvez aussi les stocker sur une
clef USB pour y retravailler à la maison.

Commencez par créer l'arborescence de dossiers suivants :

```
Documents/
└── NSI/
    ├── Cours
    └── Exercices/
        ├── Introduction/
        ├── Conditions/
        ├── Boucles/
        └── Chaines/
            ├── ...
            └── Bonus/
                ├── exercice1.py
                ├── exercice2.py
                └── ...
```

Pensez à ajouter des commentaires dans vos fichiers lors de la programmation ! Il sera ainsi plus facile de revenir
dessus plus-tard.

## Exercice n°1 : Construction de chaînes

Dans un conte américain, huit petits canetons s’appellent respectivement : Jack, Kack, Lack, Mack, Nack, Oack, Pack
et Qack.

Écrire un script qui génère tous ces noms à partir des deux chaînes de caractères suivantes :

`debuts = "JKLMNOPQ"` et `fin = "ack"`

??? question "Une piste ?"

    Utiliser une boucle for.

## Exercice n°2

Écrire un programme qui :

1. demande à l'utilisateur d'entrer un mot,
2. puis un entier ayant autant de chiffres que de nombre de lettres du mot
3. puis affiche des lignes de caractères sur le modèle de l'exemple ci-dessous.

Si l'utilisateur entre le mot `"python"` puis `213142`. Le programme devra afficher

```
pp
y
ttt
h
oooo
nn
```

Les chiffres de l'entier entré donneront donc le nombre de répétitions de la lettre ayant le même rang dans le mot.

**Amélioration** : Faites en sorte que le programme vérifie que l'entier est conforme.

??? question "Une astuce ?"

    A-t-on besoin de transformer le nombre donné en entier ?