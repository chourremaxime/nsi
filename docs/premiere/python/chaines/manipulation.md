## Concaténation

Concaténer signifie *mettre ensemble deux éléments*. Ici, cela revient à coller deux chaînes de caractères.

!!! methode "Syntaxe"

    ```python
    chaine_1 + chaine_2
    ```

Par exemple :

```python
morceau1 = 'Je vais'
morceau2 = ' manger.'
print(morceau1 + morceau2)  # Je vais manger.
```

Ou encore :

```python
morceau1 = 'Je vais'
morceau2 = ' manger.'
morceau3 = morceau1 + morceau2

print(morceau3)  # Je vais manger.
```

## Longueur de chaîne

Permet de connaître le nombre de caractères dans une chaîne de caractères.

!!! methode "Syntaxe"

    ```python
    len(chaine)
    ```

Par exemple :

```python
print(len(morceau1))  # 7
```

## Opérations sur les chaînes

On peut utiliser l'opérateur `+` pour concaténer des chaînes, mais peut-on faire de même avec d'autres opérateurs ?

Oui ! Mais uniquement avec l'opérateur `*` et un entier pour répéter les chaînes :

```python
morceau4 = morceau2 * 3
print(morceau4)  #  manger. manger. manger.
```

## Caractères spéciaux dans une chaîne

Il existe quelques caractères qui réalisent une action spéciale :

- `\n` est le caractère de passage à la ligne
- `\'` permet d'afficher une apostrophe, `\"` un guillemet *(dans le cas où ces caractères servent à délimiter la
  chaîne)*
- `\t` permet d'afficher une tabulation