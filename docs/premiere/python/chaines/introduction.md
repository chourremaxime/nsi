Une chaîne de caractères (de type string : `str`) est une **suite** de lettres, de chiffres et/ou de caractères
spéciaux. En bref, une chaîne de caractères est une **suite de caractères**.

Pour être reconnue, une chaîne de caractères peut-être...

**...délimitée par des apostrophes :**

```python
ma_phrase = 'Voici une phrase'
```

**...délimitée par des guillemets :**

```python
ma_phrase = "Voici une phrase"
```

**...délimitée par des triples guillemets qui autorisent le passage à la ligne lors de l'affichage :**

```python
ma_phrase = """Voici une phrase plus longue
qui montre le saut à la ligne."""
```