Dans ce qui suit, le crayon est stocké dans la variable `c` avec `c = Pen()`.

Il faut donc remplacer `c` par le nom de la variable de son propre programme.

!!! syntaxe "Méthodes de comportement du crayon"

    | Méthode (paramètres)  | Description                                                                                                                                                                                                                                                                                                                                                                                                                           |
    |-----------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | `c.shape(forme)`      | Forme possible : `"classic"` ou `"turtle"` (type string)                                                                                                                                                                                                                                                                                                                                                                              |
    | `c.speed(n)`          | Règle la vitesse de la tortue à une valeur entière comprise entre 0 et 10 inclus. Si aucun argument n'est donné, renvoie la vitesse actuelle.<br>Vitesse du tracé : de `n = 1` (lent) à `n = 10` (rapide) ; on peut aussi utiliser une chaîne de caractères :<ul><li>`"fastest"` : vitesse 0,</li><li>`"fast"` : vitesse 10,</li><li>`"normal"` : vitesse 6,</li><li>`"slow"` : vitesse 3,</li><li>`"slowest"` : vitesse 1.</li></ul> |
    | `c.width(n)`          | Épaisseur du trait : de `n = 0` (fin) à `n = 10` (épais)                                                                                                                                                                                                                                                                                                                                                                              |
    | `c.color(couleur)`    | Couleur du crayon : `"green"` , `"red"` , ... (type string)                                                                                                                                                                                                                                                                                                                                                                           |
    | `c.up()`              | Relever le crayon (pour le déplacer sans dessiner)                                                                                                                                                                                                                                                                                                                                                                                    |
    | `c.down()`            | Abaisser le crayon (pour recommencer à dessiner)                                                                                                                                                                                                                                                                                                                                                                                      |
    | `c.setheading(angle)` | Tourne le crayon dans la direction indiquée par l'angle (en degrés)                                                                                                                                                                                                                                                                                                                                                                   |                                                                                                                                                                                                                                                                                                                                                                  |
    | `c.write(texte)`      | Le texte, de type string, est écrit, avec la couleur courante, à la position actuelle du crayon                                                                                                                                                                                                                                                                                                                                       |
    | `c.begin_fill()`      | Commence à remplir la zone que va dessiner le crayon                                                                                                                                                                                                                                                                                                                                                                                  |
    | `c.end_fill()`        | Termine le remplissage de la zone                                                                                                                                                                                                                                                                                                                                                                                                     |

!!! syntaxe "Méthodes de tracés du crayon"

    | Méthode (paramètres)  | Description                                                                                                              |
    |-----------------------|--------------------------------------------------------------------------------------------------------------------------|
    | `c.forward(dist)`     | Avancer d'une longueur `dist` (en pixels) donnée                                                                         |
    | `c.backward(dist)`    | Reculer d'une longueur `dist` (en pixels) donnée                                                                         |
    | `c.left(angle)`       | Tourne à gauche d'un angle donné (en degrés)                                                                             |
    | `c.right(angle)`      | Tourne à droite d'un angle donné (en degrés)                                                                             |
    | `c.circle(x, [y])`    | Trace un cercle de rayon `x`.<br>Possibilité d'arc de cercle avec une valeur d'angle `y` (sans crochets pour l'utiliser) |
    | `c.dot(x, [couleur])` | Disque de diamètre `x` , centré à l'endroit où se trouve le crayon (couleur intérieure en option, type string)           |
    | `c.goto(x, y)`        | Déplacer le crayon jusqu'aux coordonnées `(x, y)`                                                                        |

!!! syntaxe "Méthodes pour récupérer des informations sur le crayon"

    | Méthode (paramètres) | Description                                                        |
    |----------------------|--------------------------------------------------------------------|
    | `c.pos()`            | Renvoie sous forme d'un couple les coordonnées actuelles du crayon |
    | `c.heading()`        | Renvoie l'orientation (en degrés) actuelle du crayon               |

!!! syntaxe "Fonctions pour modifier la fenêtre (ne s'applique pas au crayon)"

    | Fonction (paramètres) | Description                                                                             |
    |-----------------------|-----------------------------------------------------------------------------------------|
    | `clear()`             | Efface le dessin, le crayon reste à sa place                                            |
    | `reset()`             | Ré-initialise la page (dessin effacé, crayon à l'origine)                               |
    | `hideturtle()`        | Permet de masquer le crayon                                                             |
    | `exitonclick()`       | Permet de sortir du script en cliquant sur la fenêtre.<br>**À écrire en fin de script** |

!!! syntaxe "Intéraction avec l'utilisateur"

    Les instructions ci-dessous affichent une **fenêtre popup** dont le nom est `Nom_Fenetre` avec un `Message` écrit
    à destination de l'utilisateur. L'utilisateur saisit une valeur dans le champ de texte, valeur qui est stockée
    dans la variable `a`.
    
    | Instruction                               | Description            |
    |-------------------------------------------|------------------------|
    | `a = textinput("Nom_fenetre", "Message")` | `a` est de type string |
    | `a = numinput("Nom_fenetre", "Message")`  | `a` est de type float  |