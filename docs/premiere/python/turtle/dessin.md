![Repère du module Turtle](repere_turtle.png){ align=right ; width=30% }

Le module graphique **turtle** permet de piloter un _crayon_ permettant de tracer dynamiquement des figures
géométriques.

Les dessins sont réalisés dans un **repère orthonormé** virtuel centré sur la fenêtre d'affichage. L'unité des axes
est le **pixel**. Le repère n'est pas visible à l'écran.

La forme par défaut du crayon de tracé est une **flèche orientée**, placée au départ à l'origine du repère. Le crayon
est situé à la pointe, la flèche montre le sens du tracé en cours ou à venir.

!!! abstract "Des objets et des méthodes"

    Le crayon utilisé pour les tracés est un objet informatique. Pour manipuler cet objet, il faut lui appliquer une
    méthode selon la syntaxe :

    ```python
    objet.methode(param1, param2)
    ```

    Les méthodes utilisées avec turtle doivent donc être appliquée à un crayon stocké dans une variable. Par exemple
    dans `stylo` grâce à l'instruction :

    ```python
    stylo = Pen()
    ```

!!! example "Exemple 1 : le stylo avance..."

    ```python
    from turtle import *  # Importation du module
    stylo = Pen()         # Crayon "stocké" dans la variable stylo
    stylo.forward(100)    # Le crayon avance de 100 pixels
    exitonclick()         # La fenêtre se ferme en cliquant dessus
    ```

!!! example "Exemple 2 : le stylo tourne..."

    ```python
    from turtle import *      # Importation du module
    stylo = Pen()             # Crayon "stocké" dans la variable stylo
    stylo.forward(100)        # Le crayon avance de 100 pixels
    stylo.left(60)            # Le crayon tourne sur lui-même de 60° à gauche
    stylo.forward(100)        # Le crayon avance de 100 pixels
    exitonclick()             # La fenêtre se ferme en cliquant dessus
    ```

!!! example "Exemple 3 : le style trace une spirale"

    ```python
    from turtle import *         # Importation du module
    stylo = Pen()                # Crayon "stocké" dans la variable stylo
    stylo.pencolor("blue")       # On dessine en bleu...
    n = int(input("Valeur : "))  # L'utilisateur entre un nombre
    n = int(n)                   # numinput renvoie un flottant...
    for i in range(n):           # On répète n fois
       stylo.forward(i)          # Le crayon avance de i pixels
       stylo.right(90)           # Tourne sur lui-même de 90° à droite
    exitonclick()                # La fenêtre se ferme en cliquant dessus
    ```