[//]: # (Ajouter des schémas et des exemples sur les fonctions à coder)

## Consignes projet

Les projets sont à rendre en ligne, sur la plateforme Elea. On n'utilisera pas la messagerie pour le rendu !

!!! warning "Fondamental"

    La qualité du code est toujours évaluée, on prendra soin de rajouter des commentaires utiles pour le relecture,
    notamment au niveau des fonctions Python.

!!! danger "Attention"

    Tout projet non rendu à l'heure donnée le jour J sera pénalisé de **-6 points pour 24 heures** de retard et de
    **-12 pts pour 48 heures**. La note de zéro sera attribuée pour un projet non rendu ou rendu après 48 heures.
    
    **Aucun plagiat ou travail de groupe *(sauf exception signalée)* n'est autorisé**, ce sera immédiatement la note
    minimale aux questions.
    
    Il est **strictement interdit d'utiliser une intelligence artificielle** quelconque pour la réalisation des projets
    *(sauf mention explicite)*.
    
    La qualité du code est importante !
    
    Bon travail !

## Fonctions à coder

Vous devez programmer, **dans un unique fichier nommé `projet_turtle.py`** les fonctions ci-dessous.

Ce fichier devra contenir à la fois le code des fonctions et un jeu de tests (plusieurs appels de chaque fonction avec
différentes valeurs affectées aux paramètres). Ce jeu de tests sera préférentiellement aléatoire...

??? example "Exemple et modèle de fichier à rendre"

    ```python
    # Tous les import nécessaires viennent ici
    from turtle import *
    from random import *
    
    
    # Toutes les fonctions viennent ici
    def carre(xA, yA, AB, stylo):
        stylo.up()
        stylo.goto(xA, yA)
        stylo.down()
        
        for i in range(4):
            stylo.forward(AB)
            stylo.right(90)
    
    def carre_t(...):
        ...
        
    
    # Tous les tests s'écrivent dessous
    if __name__ == "__main__":
        stylo = Pen() # On crée notre stylo pour nos fonctions
        
        # Test carre()
        carre(100,0,50,stylo)
        carre(-200,-50,25, stylo)
        
        # Test carre_t()
        ...
    ```

!!! info "Remarque"

    Vous pouvez, bien entendu, réutiliser les fonctions que vous avez définies. Il est même **conseillé de réutiliser
    les fonctions**, par exemple pour faire `carre_t` en utilisant la fonction `carre`.

Toutes les fonctions prendront en dernier paramètre un stylo afin de fonctionner. C'est à partir de ce stylo que
les dessins pourront se faire.

**Les noms de fonctions sont à respecter !**

### Les carrés

| Nom de la fonction                                                                              | Arguments                                  | Description                                                                                                               |
|-------------------------------------------------------------------------------------------------|--------------------------------------------|---------------------------------------------------------------------------------------------------------------------------|
| `carre(xA, yA, AB, stylo)`                                                                      | 3 nombres                                  | Trace un carré ABCD, (AB) étant dans la direction de la tortue, connaissant les coordonnées de A et la longueur AB.       |
| `carre_t(xA, yA, AB, angle, stylo)`                                                             | 4 nombres                                  | Trace un carré ABCD connaissant les coordonnées de A, la longueur AB et l’angle que fait (AB) avec à l'axe des abscisses. |
| `carre_plein(xA, yA, AB, couleur, stylo)`<br>`carre_plein_t(xA, yA, AB, angle, couleur, stylo)` | 3 nombres, 1 string<br>4 nombres, 1 string | Idem fonctions précédentes, sauf que le carré est colorié en couleur donnée.                                              |

### Les rectangles

| Nom de la fonction                                                                                              | Arguments                                  | Description                                                                                                                                   |
|-----------------------------------------------------------------------------------------------------------------|--------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------|
| `rectangle(xA, yA, AB, BC, stylo)`                                                                              | 4 nombres                                  | Trace un rectangle ABCD, (AB) étant dans la direction de la tortue, connaissant les coordonnées de A, la longueur AB et la longueur BC.       |
| `rectangle_t(xA, yA, AB, BC, angle, stylo)`                                                                     | 5 nombres                                  | Trace un rectangle ABCD connaissant les coordonnées de A, la longueur AB, la longueur BC et l'angle que fait (AB) avec à l'axe des abscisses. |
| `rectangle_plein(xA, yA, AB, BC, couleur, stylo)`<br>`rectangle_plein_t(xA, yA, AB, BC, angle, couleur, stylo)` | 4 nombres, 1 string<br>5 nombres, 1 string | Idem fonctions précédentes, sauf que le rectangle est colorié en couleur donnée.                                                              |
| `carre_bis(xA, yA, AB, stylo)`<br>`carre_bis_t(xA, yA, AB, angle, stylo)`                                       | 3 nombres<br>4 nombres                     | Donne les mêmes résultats que les fonctions carre mais sont définies à l'aide des fonctions `rectangle` uniquement.                           |

### Escalier

| Nom de la fonction                      | Arguments | Description                                                                                                                                                                                                                                                                                                                                                |
|-----------------------------------------|-----------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `escalier(xD, yD, xA, yA, p, h, stylo)` | 6 nombres | Trace un escalier connaissant les coordonnées de la position de départ, celle de l'arrivée, la profondeur et la hauteur d'une marche.<br>La dernière marche peut avoir une profondeur et une hauteur différentes.<br>Pensez aussi aux escaliers qui descendent.<br>On pourra utiliser astucieusement une fonction marche permettant le tracé d'une marche. |

### Dés

| Nom de la fonction              | Arguments | Description                                                                                                                                                                                                     |
|---------------------------------|-----------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `face(numero, longueur, stylo)` | 2 nombres | Trace la face d'un dé cubique connaissant le numéro de cette face (entre 1 et 6) et la longueur de l'arête du dé.<br>Le numéro de la face sera représenté par le nombre correspondant de petits carrés remplis. |
| `de(longueur, stylo)`           | 1 nombre  | Simule le lancer d'un dé à 6 face en traçant la face obtenue au hasard connaissant la longueur de l'arête du dé.<br>Le numéro de la face obtenue sera également affiché dans la console.                        |

## Bonus artistique

Uniquement si vous avez réussi à coder l'ensemble des fonctions précédentes (sinon ce bonus ne sera pas évalué),
réaliser la plus belle figure artistique en ré-exploitant votre travail précédent.