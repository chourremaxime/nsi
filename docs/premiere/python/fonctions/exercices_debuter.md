## Rappels

Pensez à enregistrer vos fichiers sur le réseau afin de ne pas les perdre ! Vous pouvez aussi les stocker sur une
clef USB pour y retravailler à la maison.

Commencez par créer l'arborescence de dossiers suivants :

```
Documents/
└── NSI/
    ├── Cours
    └── Exercices/
        ├── Introduction/
        ├── Conditions/
        ├── Boucles/
        ├── Chaines/
        └── Fonctions/
            ├── exercice1.py
            ├── exercice2.py
            └── ...
```

Pensez à ajouter des commentaires dans vos fichiers lors de la programmation ! Il sera ainsi plus facile de revenir
dessus plus-tard.

## Exercice n°1 : Compter *(encore)* les lettres

Cet exercice a déjà été [traité précédemment](../chaines/exercices_debuter.md#exercice-n1-compter-les-lettres). On va le
généraliser en utilisant une fonction.

Définir une fonction `compter()` qui prend en paramètres une chaîne de caractères et un caractère. Cette fonction doit
compter le nombre d'occurrences *(le nombre de fois où apparaît)* du caractère dans la chaîne.

Voici un exemple d'affichage où le programme demande à l'utilisateur d'entrer une phrase *(ci-dessous
`"Ceci est un exemple."`)* et un caractère *(ci-dessous `"e"`)* puis il retourne le nombre d'occurrences du caractère
dans la phrase.

```
Entrez un phrase : Ceci est un exemple.
Entrez un caractère : e
Il y a 5 "e" dans la phrase.
```

Ce programme doit évidemment faire appel à la fonction `compter()`.

??? question "Une piste ?"

    Les paramètres à indiquer dans la défintion sont :

    - `phrase` : La phrase à analyser
    - `lettre` : Le caractère à rechercher

    Ensuite, la fonction doit utiliser seulement ces deux variables, une variable `compteur` pour compter
    le nombre d'occurrences et une variable `caractere` qui stockera tous les caractères de la phrase, un à un *(grâce
    à une boucle `for`)*. C'est cette variable `compteur` qui sera renvoyée.

## Exercice n°2 : Le plus petit...

1.  Compléter la définition de la fonction `mini()` qui renvoie le plus petit des deux nombres entre u et v entrés en
    paramètres.
2.  Le module `random` va permettre de tester cette fonction.<br>
    Dans le *Programme principal*, affecter aux variables `x` et `y` des valeurs entières aléatoires comprises entre 0
    et 100.
3.  Afficher la valeur stockée dans `x`, celle stockée dans `y` puis, en utilisant la fonction `mini()`, afficher la
    plus petite de ces deux valeurs.
4.  Améliorer ensuite le programme principal pour que trois variables `x`, `y` et `z` prennent une valeur entière
    aléatoire entre 1 et 100, après que la plus petite valeur est affichée à l'aide de la fonction `mini()`.

??? info "Utilisation de `random`"

    Le module **random** donne accès à des fonctions qui renvoient des nombres aléatoires. Il existe deux fonctions
    importantes :
    
    - La fonction `#!python random()` qui renvoie un nombre décimal aléatoire entre 0 et 1 (exclu).
    - La fonction `#!python randint(nb1, nb2)` qui renvoie un nombre entier aléatoire entre `nb1` et `nb2` inclus.

    Voici un exemple d'usage de ces fonctions :

    ```python
    from random import *        # Pour accéder aux fonctions du module

    a = random()                # Nombre décimal aléatoire entre 0 et 1 (exclu)
    b = randint(3, 12)          # Entier aléatoire entre 3 et 12 inclus

    print(f"La valeur de a est {a}")
    print(f"La valeur de b est {b}")
    ```

```python
# Importation des modules
from random import *

# Définition des Fonctions
def mini(u, v):
    """
    Ceci est un commentaire sur plusieurs lignes qui permet de détailler le fonctionnement d'une fonction.
    Il est appelé docstring.
    
    Entrées : deux nombres u et v
    Sortie : retourne la plus petite valeur entre u et v
    """
    

##----- Programme Principal -----##
x = 
y = 
```