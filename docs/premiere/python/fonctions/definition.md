## Introduction

Nous avons vu en début de cours sur Python que les fonctions permettent de réaliser des actions, leur but étant d'éviter
de réécrire les mêmes instructions dès qu'elles seront nécessaires.

Ce cours vise à découvrir comment créer ses propres fonctions, c'est-à-dire comment rédiger la **définition** d'une
fonction.

## Définition d'une fonction

Dans les langages de programmation, une fonction est une instruction **isolée du reste du programme**, qui possède un
**nom**, et qui peut être appelée par ce nom à n'importe quel endroit du programme et autant de fois que l'on veut.
Cette fonction possède un **corps** *(un bloc d'instruction)* qui utilise des **paramètres**.

En quelque sorte, une fonction est un **sous-programme**. Les programmes complexes deviennent ainsi plus courts et
plus lisibles, en les séparant en fonctions.

!!! methode "Syntaxe"

    ```python
    def nom_fonction():
        instructionA
        instructionB
    ```

Le mot clef `#!python def` est obligatoire. Il permet d'indiquer à Python qu'on souhaite **définir** une fonction en
y rédigeant sa **définition**.

!!! example "Bonjour"

    On souhaite rédiger une fonction qui dit bonjour. On va donc écrire le code suivant :

    ```python
    def bonjour():
        print("Bonjour !")
        print("J'espère que vous passez une bonne journée.")
    ```

## Paramètres d'une fonction

Habituellement, une fonction est définie avec des **paramètres**. Il s'agit de variables qui vont s'adapter à un cas
précis. Si on reprend l'exemple des mathématiques, la fonction $f(x) = 4x + 2$, alors x est un paramètre dont la valeur
varie quand on calcule $f(x)$, par exemple $f(4)$ vaudra $18$ mais $f(1,5)$ vaudra $8$.

!!! methode "Syntaxe"

    ```python
    def nom_fonction(param1, param2):
        instructionA
        instructionB
    ```

On peut utiliser plusieurs **paramètres**, que l'on séparera avec des virgules. Lorsqu'on appellera cette fonction,
on utilisera autant d'**arguments** que de paramètres. Un argument, c'est tout simplement la valeur que vaudra
la fonction lors de son appel. Pour $f(4)$, on dit que $4$ est l'argument de la fonction.

!!! example "Table de multiplication"

    Reprenons l'exercice de la table de multiplication. On souhaite afficher une table de multiplication, mais
    on souhaite pouvoir changer facilement la valeur de la table de multiplication.

    ```python
    # Fonction nommée table et présentant un paramètre : n
    def table(n):
        for k in range(1, 11) :
            print(f"{n} x {k} = {n*k}")
        print() # Pour laisser une ligne vide en fin de table

    table(8) # Appel de la fonction table avec l'argument 8
    table(6) # Appel de la fonction table avec l'argument 6
    ```

    Voici un autre exemple où on ajoute un paramètre qui indique quand terminer la table :

    ```python
    # Fonction nommée table et présentant deux paramètres : n et fin
    def table(n, fin):
        for k in range(1, fin + 1) :
            print(f"{n} x {k} = {n*k}")
        print() # Pour laisser une ligne vide en fin de table

    table(8, 12) # Appel de la fonction table avec les arguments 8 et 12
    table(6, 7) # Appel de la fonction table avec les argument 6 et 7
    ```