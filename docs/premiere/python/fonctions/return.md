Pour les puristes, les fonctions que nous avons décrites jusqu'à présent ne sont pas tout à fait des fonctions au
sens strict, mais plus exactement des **procédures**.

Une *vraie* fonction *(au sens strict)* doit en effet **renvoyer une valeur** lorsqu'elle se termine.

Une *vraie* fonction peut s'utiliser à la droite du signe `=` dans des expressions telles que `y = sin(a)`. On
comprend aisément que dans cette expression, la fonction `sin()` **renvoie une valeur** (le sinus de l'argument `a`)
qui est directement **affectée à la variable `y`**.

!!! methode "Syntaxe"

    ```python
    def nom_fonction(param1, param2):
        instructionA
        instructionB
        return une_valeur
    ```

Le mot clef `#!python return` permet alors d'indiquer une valeur à **renvoyer** à la fin de la fonction. C'est cette
valeur qui sera affectée *(enregistrée)* dans la variable précisée lors de l'appel de la fonction.

!!! example "Cube"

    Voici un exemple de fonction qui calcule le cube d'une valeur :

    ```python
    # Fonction qui à un paramètre et renvoie une valeur.
    def cube(x):
        return x * x * x
    ```

    La fonction calcule la valeur `x * x * x` puis renvoie le résultat de ce calcul. On peut l'utiliser de la façon
    suivante :

    ```python
    nb = 7
    y = cube(nb)
    print(f"Le cube de {nb} est {y}")
    ```

!!! note "Le rôle de découpe en unités logiques"

    Un rôle important des fonctions est de découper votre programme en **petites unités logiques** faciles à comprendre.
    L'usage des fonctions, par ce rôle, **facilite la conception d'un programme** mais aussi sa relecture, sa
    réutilisation, les corrections...