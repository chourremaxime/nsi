## Rappels

Pensez à enregistrer vos fichiers sur le réseau afin de ne pas les perdre ! Vous pouvez aussi les stocker sur une
clef USB pour y retravailler à la maison.

Commencez par créer l'arborescence de dossiers suivants :

```
Documents/
└── NSI/
    ├── Cours
    └── Exercices/
        ├── Introduction/
        ├── Conditions/
        ├── Boucles/
        ├── Chaines/
        └── Fonctions/
            ├── ...
            └── Bonus
                ├── exercice1.py
                ├── exercice2.py
                └── ...
```

Pensez à ajouter des commentaires dans vos fichiers lors de la programmation ! Il sera ainsi plus facile de revenir
dessus plus-tard.

## Exercice n°1 : Login / Mot de passe

L'utilisateur entre son nom et son prénom. Dans l'exemple d'affichage ci-dessous, l'utilisateur a entré `bernard`
puis `alain` après les messages incitatifs :

```
Quel est votre nom ? bernard
Quel est votre prénom ? alain
Votre login est : a.bernard
Votre nom masqué est : b***d
```

Le programme doit utiliser une fonction `login()` qui prend pour **paramètres** deux chaînes de caractères et qui
**renvoie** la 1ère lettre de la 1ère chaîne et la 2nde chaîne complète, séparées par un point
(cf. l'affichage ci-dessus).

Le programme doit aussi utiliser une fonction `masquer()` qui prend pour **paramètre** une chaîne de caractères et qui
**renvoie** la 1ère et la dernière lettre de cette chaîne de caractères, séparées par trois étoiles : `***`. 

??? question "Une piste ?"

    La fonction `login()` nécessite deux paramètres qui sont des chaînes de caractères. On peut les nommer `ch1` et
    `ch2` par exemple.

??? question "Un schéma ?"

    Dans la fonction `login(ch1, ch2)`,

    - on extrait le 1er caractère avec `ch1[0]` ;
    - puis on renvoie une concaténation des caractères grâce à l'opérateur `+`.


## Exercice n°2 : Suite de Robinson

Quelle est la logique de passage d'une ligne à l'autre dans ce qui suit ?

``` 
0
10
1110
3110
132110
13123110
23124110
1413223110
1423224110
2413323110
```

1. En première ligne, on commence par la chaîne `"0"`.
2. En seconde ligne, on écrit ce qu'on voit en première ligne : un `"0"`, ce qui donne `"10"`.
3. En troisième ligne, on lit ce que l'on voit dans la ligne précédente : trois `"1"` et un `"0"`,
ce qui donne `"1110"`.
4. ...et ainsi de suite en indiquant au même endroit le nombre total d'occurrences d'un chiffre donné dans la chaîne.

Les chiffres sont écrits dans l'ordre décroissant et on n'indique pas l'effectif d'un chiffre absent.

**Écrire** un programme qui **demande** un entier `n > 0` à l'utilisateur puis que affiche `n` lignes du triangle
ci-dessus. Ce programme devra être décomposé en deux fonctions :

- `compteCarac()` : fonction prenant en entrée une chaîne de caractères `ch` et un caractère `car` et retournant le
nombre d'occurrences de `car` dans `ch`.
- `ligneSuivante()` : fonction prenant en entrée une chaîne de caractères `ch` (constituée de chiffres) et retournant
la ligne constituant le terme suivant dans la suite de Robinson.
