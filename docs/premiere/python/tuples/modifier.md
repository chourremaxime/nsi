***Il n'est pas possible de modifier un tuple.***

Plus précisément, on dit qu'un tuple est une **structure non mutable**.

!!! abstract "Objet immuable"
    Un objet est dit **immuable** ou **non mutable** si sa valeur (ou son état) ne peut pas être modifiée
    après sa création.

!!! note "Objet muutable"
    L'inverse d'un objet immuable est un objet **mutable**. Cela signifie qu'il est possible de modifier la valeur
    d'un objet mutable.

Les tuples sont donc immuables. Ce n'est pas le cas de toutes les séquences en Python, dont une que nous verrons plus
tard.

## Mais comment faire alors ?

Si l'on souhaite modifier un tuple, il faudra donc créer une copie personnalisée en utilisant le tranchage vu dans la
précédente partie.

!!! abstract "Concaténation de tuples"
    Pour créer un tuple, on peut utiliser la syntaxe classique avec (val1, val2, ...) mais ça devient vite compliqué
    si le tuple est grand. 

    On va donc utiliser la **concaténation**, c'est-à-dire fusionner deux tuples pour en créer un autre, en utilisant
    `+`.
    
    Ainsi, `#!python (1, 3, 7) + (4, 2)` donne `#!python (1, 3, 7, 4, 2)`.
    
    Attention, si on fait `#!python (1, 3, 7) + 4` alors on va obtenir une erreur. En effet, `4` n'est pas un tuple !

Supposons que l'on a un tuple qui contient des prénoms (donc des *strings*). On souhaite modifier le troisième prénom.

```python
t = ("Julien", "Adrien", "Joëlle", "Dominique", "Adèle")
u = t[:2] + ("Laure",) + t[3:]
print(t)  # ('Julien', 'Adrien', 'Joëlle', 'Dominique', 'Adèle')
print(u)  # ('Julien', 'Adrien', 'Laure', 'Dominique', 'Adèle')
```

Ainsi, `t` n'a pas été modifié et `u` est une copie modifiée de `t`.