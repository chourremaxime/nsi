## Rappels utiles

Ces exercices doivent être utilisés pour vous entraîner à programmer. Ils sont généralement accompagnés d'aide et de
leur solution pour vous permettre de progresser.

Avant de vous précipiter sur ces solutions dès la première difficulté, n'oubliez pas les conseils suivants :

- Avez-vous bien fait un schéma au brouillon pour visualiser le problème posé ?
- Avez-vous essayé de rédiger un algorithme en français, avec vos propres mots, avant de vous lancer dans la 
  programmation sur machine ?
- Avez-vous utilisé des affichages intermédiaires, des `#!python print()`, pour visualiser au fur et à mesure le 
  contenu des variables ?
- Avez-vous testé le programme avec les propositions de tests donnés dans l'exercice ?
- Avez-vous testé le programme avec de nouveaux tests, différents de ceux proposés ?

!!! info "Tests du code et *main*"

    Le `main` est le code principal d'un programme. Il ne sera exécuté que si le fichier courant est directement 
    exécuté. Par exemple, si un fichier contenant un `main` est importé, ce code ne sera pas exécuté.

    Voici un exemple de ce qu'il faut faire :

    ```python
    # Rédigez ici vos fonctions
    
    if __name__ == "__main__":
        # Ces lignes permettent d'exécuter les tests présents dans les docstrings
        import doctest
        doctest.testmod()

        # Rédigez ici votre code principal (tests, affichages ...)
    ```

    Le `doctest` permet de tester les fonctions en utilisant les tests rédigés dans le `docstring`, c'est-à-dire le
    commentaire situé dans la définition de la fonction.

## Exercice n°1 : Fonction parité

### Question 1

Complétez la définition de la fonction `#!python parite()` qui prend pour paramètre un tuple `t` et renvoie un tuple de 
même longueur contenant un 0 lorsque l'élément correspondant de `t` est pair, un 1 sinon.

```python
def parite(t):
    """
    t – tuple, n-uplet d’entiers naturels
    Sortie: tuple – n-uplet de même longueur que t contenant :
            0 si l’élément de même rang dans t est pair
            1 si l’élément de même rang dans t est impair
    >>> parite( (3, 2, 6) )
    (1, 0, 0)
    """
```

??? question "Une piste"

    Il va falloir renvoyer et donc construire un nouveau tuple.

??? question "Tester la parité"

    Avec `#!python a % 2`, on peut savoir si un nombre `a` est pair. 

??? question "Créer un tuple de longueur 1"

    Un tuple de longueur 1 s'écrit de la sorte : `#!python (valeur, )`

??? bug "Solution"

    ```{.python .no-copy}
    def parite(t):
        """
        t – tuple, n-uplet d’entiers naturels
        Sortie: tuple – n-uplet de même longueur que t contenant :
                0 si l’élément de même rang dans t est pair
                1 si l’élément de même rang dans t est impair
        >>> parite( (3, 2, 6) )
        (1, 0, 0)
        """
        reponse = ()                # Création d’un tuple « vide »
        for valeur in t:
            reponse = reponse + (valeur % 2, )
        return reponse
    ```

### Question 2

On souhaite tester la fonction parité avec un tuple contenant des valeurs aléatoires. Complétez la fonction `#!python 
alea()` qui prend pour paramètre un entier `n` positif et renvoie un tuple `t` qui contient des valeurs aléatoires entre
0 et 99. Vous pourrez utiliser une boucle `#!python for` qui crée un tuple par concaténation. Ne pas oublier 
d'importer le module `#!python random` qui donne accès à `#!python randint()`.

```python
def alea(n):
    """
    n - entier
    Sortie: tuple – n-uplet contenant n valeurs aléatoires entre 0 et 99
    > alea( 4 )
    (31, 71, 98, 63)
    """
```

??? question "Une piste"

     C'est similaire à la question 1, sauf qu'on n'ajoute pas la même chose dans le tuple.

??? bug "Solution"

    ```{.python .no-copy}
    from random import randint
    
    def alea(n):
        """
        n - entier
        Sortie: tuple – n-uplet contenant n valeurs aléatoires entre 0 et 99
        >>> alea( 4 )
        (31, 71, 98, 63)
        """
        generation = ()                # Création d’un tuple « vide »
        for i in range(n):
            generation = generation + (randint(0,100), )
        return generation
    ```

### Question 3

Regroupez les deux codes précédents dans un unique programme afin de tester la fonction `#!python parite()` dans le
`main` du programme à l'aide de cinq tuples générés aléatoirement.

??? bug "Solution"

    ```{.python .no-copy}
    if __name__ == "__main__":
        import doctest
        doctest.testmod()
        
        for i in range(5):
            t = alea(10)  # 10 est choisi arbitrairement
            print(t)
            u = parite(t)
            print(u)
            print()
    ```

## Exercice n°2 : Fonctions sur le temps

### Question 1

Copiez/collez et **complétez** le corps de la fonction `#!python temps_secondes()` qui renvoie le temps en secondes,
correspondant au cumul de `h` heures, `m` minutes et `s` secondes.

```python
def temps_seconde(h, m, s):
    """
    h, m, s – int, triplet d’entiers positifs
    Sortie: int – temps en secondes égal à h heures,
            m minutes et s secondes
    >>> temps_seconde(1, 2, 3)
    3723
    >>> temps_seconde(2, 0, 1)
    7201
    """
```

??? question "Une piste ?"

    Un simple return doit suffire. N'oubliez pas de tester !

??? bug "Solution"

    ```{.python .no-copy}
    def temps_seconde(h, m, s):
        """
        h, m, s – int, triplet d’entiers positifs
        Sortie: int – temps en secondes égal à h heures,
                m minutes et s secondes
        >>> temps_seconde(1, 2, 3)
        3723
        >>> temps_seconde(2, 0, 1)
        7201
        """
        return h * 3600 + m * 60 + s
    ```

### Question 2

Copiez/collez et **complétez** le corps de la fonction `#!python sec_to_heure()` qui prend pour paramètre un nombre
entier de secondes et qui renvoie la valeur correspondante sous forme d'un triplet `(heures, minutes, secondes)`.

```python
def sec_to_heure(s):
    """
    s – int
    Sortie: triplet – conversion de s secondes en un
            triplet (heure, minute, seconde)
    >>> sec_to_heure(3723)
    (1, 2, 3)
    >>> sec_to_heure(7201)
    (2, 0, 1)
    """
```

??? question "Une piste ?"

     Pour cela, on utilisera astucieusement la division euclidienne et le modulo.

??? bug "Solution"

    ```{.python .no-copy}
    def sec_to_heure(s):
        """
        s – int
        Sortie: triplet – conversion de s secondes en un
                triplet (heure, minute, seconde)
        >>> sec_to_heure(3723)
        (1, 2, 3)
        >>> sec_to_heure(7201)
        (2, 0, 1)
        """
        h = s // 3600
        reste = s % 3600
        m = reste // 60
        s = reste % 60
        return h, m, s
    ```

??? bug "Autre solution"

    ```{.python .no-copy}
    def sec_to_heure(s):
        """
        s – int
        Sortie: triplet – conversion de s secondes en un
                triplet (heure, minute, seconde)
        >>> sec_to_heure(3723)
        (1, 2, 3)
        >>> sec_to_heure(7201)
        (2, 0, 1)
        """
        h = s // 3600
        m = (s % 3600) // 60
        s = (s % 3600) % 60
        return h, m, s
    ```

## Exercice n°3 : Fonction coïncidence

Copiez/collez et **complétez** le corps de la fonction `#!python coincidence()` en respectant ses spécifications.

```python
def coincidence(t):
    """
    t – tuple, n-uplet d'entiers positifs ou nuls
    Sortie: int – nombre de valeurs égales à leur indice
    >>> coincidence( (3, 2, 6) )
    0  # Aucun indice
    >>> coincidence( (3, 1, 6) )
    1  # Indice 1
    >>> coincidence( (0, 1, 6, 3) )
    3  # Indices 0, 1 et 3
    """
```

??? question "Une piste"

     Parcourir le tuple et comparer l'élément parcouru avec son indice.

??? bug "Solution"

    ```{.python .no-copy}
    def coincidence(t):
        """
        t – tuple, n-uplet d'entiers positifs ou nuls
        Sortie: int – nombre de valeurs égales à leur indice
        >>> coincidence( (3, 2, 6) )
        0
        >>> coincidence( (3, 1, 6) )
        1
        >>> coincidence( (0, 1, 6, 3) )
        3
        """
        nb_coincidences = 0
        for i in range(len(t)):
            if t[i] == i:
                nb_coincidences = nb_coincidences+1
        return nb_coincidences
    ```