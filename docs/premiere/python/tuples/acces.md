Pour accéder aux données d'un tuple, on peut utiliser la fonction `#!python print` pour afficher le tuple en entier.

```python
>>> t = (1, 4, 7)
>>> print(t)
(1, 4, 7)
```

Cependant, cette méthode n’est pas idéale si l'on souhaite récupérer un élément spécifique du tuple. Pour cela, on
utilise l'**indexation**, qui est applicable à toutes les séquences en Python, y compris les tuples.

## Accès à un élément par son indice

Lorsqu'on parle de la position d'un élément dans un tuple, on parle d'indice.

**L'indice *(index en anglais)* d'un élément dans une séquence** est un entier qui indique quelle est la position de 
cet élément dans la séquence.

Le premier élément d'une séquence est d'**indice 0**. [En savoir plus.](https://en.wikipedia.
org/wiki/Zero-based_numbering)

!!! methode "Accès à un élément par son indice"
    
    On utilise donc des crochets contre le nom du tuple avec à l'intérieur l'indice de l'élément à récupérer.
    
    ```python
    >>> t = (1, 4, 7)
    >>> print(t[0])
    1
    >>> print(t[1])
    4
    >>> print(t[2])
    7
    ```

Étant donné que les objets de notre tuple sont des entiers, on peut réaliser des opérations avec eux.

```python
>>> t = (1, 4, 7)
>>> print(t[0] + t[1] * t[2])
29
```

### Indices négatifs

Mais que se passe-t-il si on utilise des indices négatifs ?

```python
>>> t = (1, 4, 7)
>>> print(t[-1])
7
>>> print(t[-2])
4
>>> print(t[-3])
1
```

On se retrouve avec nos éléments mais en partant de la fin du tuple.

Ainsi, **pour toute séquence en Python**, l'élément d'indice -1 correspond au dernier élément (-2 à l'avant-dernier, 
etc.).

### Erreur possible

!!! danger "Index out of range ~ IndexError"
    
    Il arrive que votre code plante même si toute la syntaxe est correcte (même dans les codes des profs).
    
    Pas de panique ! Si cette erreur apparaît, cela signifie que vous avez entré un indice trop élevé (ou trop faible)
    et que donc aucun élément n'a cet indice dans la séquence.
    
    ```python
    >>> t = (1, 4, 7)
    >>> print(t[3])
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    IndexError: tuple index out of range
    ```
    
    On trouve ici rapidement l'erreur : il n'y a pas d'élément d'indice 3. Cet indice correspond au quatrième élément,
    or nous avons que trois éléments dans le tuple.

### Tranchage (sans danger)

Mettons la main à la pâte pour découvrir un fabuleux et complexe outil : le tranchage *(slice en anglais)*.

Le *slice* permet de récupèrer un sous-ensemble d'éléments d'une séquence. Il existe trois paramètres pour le *slice* :

| Paramètre             | Valeur par défaut |
|-----------------------|-------------------|
| Indice de début       | 0                 |
| Indice de fin (exclu) | Dernier + 1       |
| Pas                   | 1                 |

!!! methode "Tranchage"
    
    La syntaxe consiste à séparer les paramètres avec des `:` en respectant l'ordre du tableau, et à les écrire
    comme les indices (entre crochets) : `#!python t[debut:fin:pas]`.

Si on omet un paramètre, sa valeur par défaut sera prise en compte : `#!python t[:4:2]` est équivalent à 
`#!python t[0:4:2]`.

Si on omet le pas, on peut enlever les `:` entre l'indice de fin et le pas : `#!python t[1:3]` est équivalent à
`#!python t[1:3:]` et à `#!python t[1:3:1]`.

```python
>>> t = ("a", "b", "c", "d", "e")
>>> print(t[1:3])
('b', 'c')
>>> print(t[:2])
('a', 'b')
>>> print(t[4:2:-1])
('e', 'd')
```

À l'instar de l'instruction `#!python range()`, le *slice* accepte aussi les pas négatifs.

Par ailleurs, « Sans danger » signifie ici que le tranchage ne va pas modifier la séquence sur laquelle l'opération se
fait. On va simplement en créer une nouvelle.

```python
>>> t = (1, 4, 7)
>>> u = t[1:]
>>> print(t, u)
(1, 4, 7) (4, 7)
```

## Accéder aux éléments d'un tuple par affectation de variables

En Python, il est possible d'accéder aux éléments d'un tuple en les affectant à des variables. Cette méthode est
particulièrement utile lorsqu'on doit travailler avec des tuples de petite taille.

```python
t = (1, 2, 3)

# Affectation des éléments du tuple à des variables
a, b, c = t

print(a)  # Affiche 1
print(b)  # Affiche 2
print(c)  # Affiche 3
```

Dans cet exemple, chaque variable reçoit la valeur de l’élément correspondant dans le tuple. La variable `a` reçoit 
la première valeur, `b` la deuxième, et ainsi de suite. Cette technique est connue sous le nom de **déballage de 
tuple**, ou *tuple unpacking* en anglais.

---

Mais si le tranchage ne modifie pas la séquence, comment peut-on faire pour modifier les éléments de la séquence, et 
en l'occurrence des tuples ?