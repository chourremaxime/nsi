## Rappels utiles

Ces exercices doivent être utilisés pour vous entraîner à programmer. Ils sont généralement accompagnés d'aide et de
leur solution pour vous permettre de progresser.

Avant de vous précipiter sur ces solutions dès la première difficulté, n'oubliez pas les conseils suivants :

- Avez-vous bien fait un schéma au brouillon pour visualiser le problème posé ?
- Avez-vous essayé de rédiger un algorithme en français, avec vos propres mots, avant de vous lancer dans la 
  programmation sur machine ?
- Avez-vous utilisé des affichages intermédiaires, des `#!python print()`, pour visualiser au fur et à mesure le 
  contenu des variables ?
- Avez-vous testé le programme avec les propositions de tests donnés dans l'exercice ?
- Avez-vous testé le programme avec de nouveaux tests, différents de ceux proposés ?

!!! info "Tests du code et *main*"

    Le `main` est le code principal d'un programme. Il ne sera exécuté que si le fichier courant est directement 
    exécuté. Par exemple, si un fichier contenant un `main` est importé, ce code ne sera pas exécuté.

    Voici un exemple de ce qu'il faut faire :

    ```python
    # Rédigez ici vos fonctions
    
    if __name__ == "__main__":
        # Ces lignes permettent d'exécuter les tests présents dans les docstrings
        import doctest
        doctest.testmod()

        # Rédigez ici votre code principal (tests, affichages ...)
    ```

    Le `doctest` permet de tester les fonctions en utilisant les tests rédigés dans le `docstring`, c'est-à-dire le
    commentaire situé dans la définition de la fonction.

## Exercice n°1 : Tuple en entier

Copiez/collez et complétez le corps de la fonction `#!python tuple_to_int()` en respectant ses spécifications.

```python
def tuple_to_int(t):
    """
    t – tuple, n-uplet d'entiers positifs ou nuls
    Sortie: int – nombre formé à partir des valeurs de t
    >>> tuple_to_int( (2, 8) )
    28
    >>> tuple_to_int( (9, 12, 7) )
    9127
    >>> tuple_to_int( (0, 91, 0, 2) )
    9102
    """
```

## Exercice n°2 : Bibliothèque de géométrie 2D

L'objectif est simple et pas très compliqué : créer une **petite bibliothèque de fonctions** permettant de réaliser les
calculs usuels en géométrie repérée en dimension 2 (dans un repère orthonormal).

Il faut donc définir et tester des fonctions qui permettent de :

- déterminer les coordonnées du milieu d'un segment ;
- déterminer les coordonnées d'un vecteur ;
- déterminer les coordonnées d'une somme de vecteurs ;
- ...

Tout cela en utilisant intensivement des tuples !

**L'essentiel des formules mathématiques** à utiliser se trouvent sur cette
[page](https://www.maths-cours.fr/cours/vecteurs-coordonnees/).

## Exercice n°3 : Tuple Manipulation in Python

L'exercice suivant n'est pas compliqué, mais est en anglais et est à lire **sans utiliser de traducteur**. Cet
exercice a été réalisé avec ChatGPT et peut contenir des erreurs. Pensez à être vigilant et à avoir un avis
critique à la fin de l'exercice.

!!! info "Prompt"

    Write an exercise with question that asks to create functions about tuples in python. The questions are
    not independent and you can't use dictionaries or lists in the exercises.

Try to implement these functions and test them with different inputs. Happy coding! 🐍

### 1. Create a function `#!python create_tuple(n)`:

Input: An integer `n`.

Output: A tuple containing integers from 1 to `n`.

Example: `#!python create_tuple(5)` should return `#!python (1, 2, 3, 4, 5)`.

```python
def create_tuple(n):
    # Your code here
    pass
```

### 2. Create a function `#!python get_tuple_length(t)`:

Input: A tuple `t`.

Output: The length of the tuple.

Example: `#!python get_tuple_length((1, 2, 3, 4, 5))` should return `5`.

```python
def get_tuple_length(t):
    # Your code here
    pass
```

### 3. Create a function `#!python get_tuple_element(t, i)`:

Input: A tuple `t` and an index `i`.

Output: The i-th element of the tuple.

Example: `#!python get_tuple_element((1, 2, 3, 4, 5), 2)` should return `3`.

```python
def get_tuple_element(t, i):
    # Your code here
    pass
```

### 4. Create a function `#!python process_tuple(n, i)`:

Input: Two integers `n` and `i`.

Output: The i-th element of the tuple created by `#!python create_tuple(n)`.

This function should use `#!python create_tuple(n)` and `#!python get_tuple_element(t, i)` to generate the output.

Example: `#!python process_tuple(5, 2)` should return `3`.

```python
def process_tuple(n, i):
    # Your code here
    pass
```


