Voyons désormais diverses opérations utiles pour les tuples. Nous avons déjà vu la concaténation dans
le cours sur la modification de tuples.

## Répétition de tuples

Comme la concaténation utilise le symbole `+`, il peut être intéressant de se demander si les autres opérateurs
fonctionnent aussi avec les tuples.

Spoiler alert : Seule la multiplication avec un entier fonctionne : `<tuple> * <int>`

!!! abstract "Répétition de tuples"

    Lorsqu'on multiplie un tuple par un entier, Python va créer un nouveau tuple en ajoutant tous les éléments du
    premier tuple autant de fois que l'entier donné.
    
    ```python
    >>> t = (4, 1, 8)
    >>> u = t * 3
    >>> print(u)
    (4, 1, 8, 4, 1, 8, 4, 1, 8)
    ```
    
    ```python
    >>> t = (4, 1, 8)
    >>> u = t * 0
    >>> print(u)
    ()
    ```

## Existence d'un élément

Les tuples peuvent stocker des valeurs, et pour savoir si une valeur est dans un tuple, il existe deux méthodes :

- Parcourir le tuple et vérifier un par un si l'élément parcouru est l'élément qui nous intéresse
- Utiliser le mot clef `#!python in`

!!! abstract "Existence d'un élément"

    En Python, le mot clef `#!python in` permet de vérifier si un élément appartient à un objet ou non.
    
    Il s'utilise de la façon suivante : **`#!python valeur in objet`**. Cette instruction vaudra `#!python True`
    si `objet` contient `valeur`, `#!python False` sinon.
    
    À l'inverse, on utilise `#!python valeur not in objet` pour vérifier qu'un élément n'est pas présent dans un objet.
    
    ```python
    t = (4, 1, 8)
    print(4 in t)  # Affiche True
    print(7 in t)  # Affiche False
    
    if 4 in t:
        print("Le tuple contient 4")
    else:
        print("Le tuple ne contient pas 4")
    ```

## Fonctions & Méthodes

Il existe diverses fonctions en Python qui utilisent les tuples. On connait déjà `#!python len()` pour récupérer le
nombre d'éléments d'un tuple.

!!! info "Méthodes"

    Une méthode en Python, c'est une fonction qui s'applique à un objet, avec une syntaxe différente :
    
    - Fonction : `#!python maFonction(valeur)`
    - Méthode : `#!python valeur.maMéthode()`
    
    Nous verrons en détail comment créer des méthodes en classe de terminale. Vous devez toutefois savoir déjà utiliser
    les méthodes.

!!! abstract "Deux méthodes pour les tuples"
    
    Il existe deux méthodes qui s'appliquent à un tuple :
    
    - `#!python count()` : Renvoie le nombre d'occurrences d'un élément dans un tuple.
    - `#!python index()` : Renvoie l'indice d'un élément dans un tuple.
    
    ```python
    t = (2, 5, 2, 3, 1, 7, 2)
    print(t.count(2))  # Affiche 3
    print(t.index(7))  # Affiche 5
    ```