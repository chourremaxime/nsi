## Séquence

!!! abstract "Séquence"
    En Python, une **séquence** est un ensemble d'objets *(aussi appelé collection)* qui sont ordonnées. Les séquences
    permettent d'accéder à un élément via leur numéro de **position** dans la séquence.

Un **objet** en Python est n'importe quel type de donnée. Par exemple, les entiers, les chaînes de caractères et les
fonctions sont des objets.

*On parle aussi d'éléments.*

Il existe deux principaux types de séquences en Python : les tuples et les listes.

## Tuples

!!! abstract "Tuple"
    En Python, un **tuple** *(ou n-uplet)* est une séquence qui contient $n$ objets. On peut créer un tuple en listant
    les objets, séparés par des virgules, **entre parenthèses**.

```python
t = (3, 8, "car", 2.4)  # 4-uplet / quadruplet
u = (1, 9)  # 2-uplet / couple
v = 3, 9, 2.8  # 3-uplet / triplet
```

Dans cet exemple, nous avons créé trois tuples de tailles différentes. Comme on peut le voir, il est possible de
créer un tuple sans utiliser de parenthèses, mais dans la pratique on **utilisera majoritairement des parenthèses**.
On peut vérifier la création des tuples grâce à la fonction `#!python type()`.

```python
t = ()
u = (1)
v = (1, 2)

print(type(t))  # <class 'tuple'>
print(type(u))  # <class 'int'>
print(type(v))  # <class 'tuple'>
```

Il semble impossible de créer un tuple contenant un seul objet car la parenthèse est interprétée comme une parenthèse
mathématique.

Pour créer un tuple de taille 1, il faudra rajouter une virgule après l'objet pour indiquer qu'il est stocké dans un
tuple :

```python
u = (1,)
print(type(u))  # <class 'tuple'>
```
!!! warning "Types des éléments des tuples"
    Bien qu’il soit possible de stocker des objets de types différents dans un même tuple, il est préférable d’éviter
    cette pratique. On préférera des tuples contenant des objets du même type.

    ```python
    t = (1, 7, 2, 9, 17)  # OUI
    u = (3, 5.2, "mots")  # NON
    ```

## Fonctionnalité utile

Les tuples sont très utiles pour renvoyer plusieurs valeurs dans une fonction. En effet, l'instruction 
`#!python return` en Python ne permet de renvoyer qu'un seul objet. On peut donc renvoyer un tuple contenant plusieurs 
objets.

```python
def fonction_x(n):
    ...
    return (1, 4, 7)  # Avec parenthèses

def fonction_y(n):
    ...
    return 1, 4, 7  # Sans parenthèses, plus souvent utilisé
```

---

Maintenant que nous avons appris à créer des tuples, nous devons apprendre à lire les données qu'ils contiennent,
et plus généralement, à lire les données contenues dans les séquences.