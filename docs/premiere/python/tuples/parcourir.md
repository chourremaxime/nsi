On cherche désormais à parcourir un tuple ; c'est-à-dire à récupérer chaque élément un par un afin de réaliser une
certaine action sur cet élément.

Par exemple, on souhaite afficher tous les nombres pairs d'un tuple (qui contient des nombres entiers).

```python
t = (1,3,2,7,4,8,5)

if t[0] % 2 == 0:
    print(t[0])
if t[1] % 2 == 0:
    print(t[0])
if t[2] % 2 == 0:
    print(t[0])
if t[3] % 2 == 0:
    print(t[0])
if t[4] % 2 == 0:
    print(t[0])
if t[5] % 2 == 0:
    print(t[0])
if t[6] % 2 == 0:
    print(t[0])
```

Le problème, c'est que c'est très fatiguant, surtout si le tuple est très grand ! Comment résoudre ce problème ?

## Parcourir un tuple grâce à son indice

On peut remarquer dans le code précédent qu'une seule valeur change à chaque instruction `#!python if` : l'indice.

Heureusement, les boucles `#!python for` peuvent nous aider à parcourir notre tuple.

```python
t = (1,3,2,7,4,8,5)

for i in range(7):  # i prend les valeurs 0 à 6
    if t[i] % 2 == 0:
        print(t[i])
```

Ce code fonctionne à merveille ! Mais il ne fonctionne que pour les septuplets (7-uplet, tuples de 7 éléments).

Heureusement, il existe la bonne vieille fonction `#!python len()` qui est utilisée avec les *strings* pour connaître
leur taille, qui fonctionne également avec les tuples !

!!! abstract "Parcours via l'indice"

    ```python
    t = ( ... )  # t est un tuple quelconque
    
    for i in range(len(t)):
        print(t[i])  # Réaliser ici l'opération choisie pour le tuple
    ```

Ce qui donne dans notre exemple le code suivant :

```python
t = (1,3,2,7,4,8,5)

for i in range(len(t)):
    if t[i] % 2 == 0:
        print(t[i])
```

## Parcourir n'importe quelle séquence

Toutes les séquences en Python ont une particularité : elles sont **itérables**.

Cela signifie que l'on peut parcourir tous ses éléments un par un.

La boucle `#!python for` en Python permet de reconnaître si un élément est itérable, et ainsi parcourir ses éléments.
Le variant de la boucle prendra pour valeur chaque élément de la séquence.

!!! abstract "Parcours via l'itération"

    ```python
    t = ( ... )  # t est un tuple quelconque
    
    for v in t:
        print(v)  # Réaliser ici l'opération choisie pour le tuple
    ```
    
    `v` (choisi arbitrairement) prendra donc pour valeur tous les éléments du tuple

Ce qui donne dans notre exemple le code suivant :

```python
t = (1,3,2,7,4,8,5)

for nombre in t:
    if nombre % 2 == 0:
        print(nombre)
```

!!! info "Vous connaissez déjà une séquence"

    Il existe une séquence que vous utilisez depuis toujours en Python !

    Il s'agit de `#!python range()`. En effet, cette instruction va permettre à la boucle `#!python for` de prendre
    plusieurs valeurs. Il s'agit donc d'une séquence.

??? plus-loin "Afficher les valeurs d'une séquence"

    Pour afficher les valeurs d'une séquence, on peut utiliser `*` dans un `#!python print` :

    ```python
    >>> t = (1, 5, 3, 9)
    >>> print(*t)
    1 5 3 9
    >>> print(*range(2,9,3))
    2 5
    ```

    [En savoir plus](https://peps.python.org/pep-0448/)

## Avantages de chaque méthode

La **méthode par indice** permet de **connaître l'indice** de l'élément.

La **méthode par itérable** permet de **plus rapidement** rédiger un programme et est **plus synthétique**.