## Affichage

C'est le moment de tester quelques lignes de Python ! Essayez d'exécuter le code ci-dessous :

```python
print("Bienvenue")
```

1. Que se passe-t-il lorsque ce code est exécuté ?
2. Essayez d'exécuter ce code en ajoutant un dièse `#` au début de la ligne. Que se passe-t-il ?
3. Que se passe-t-il si on enlève les guillemets ?

!!! info Commentaires
    Pour commenter votre code, vous utiliserez toujours le dièse `#` en Python. Tout le texte suivant le dièse sur la
    ligne sera ignoré par le compilateur.

??? bug "Réponses"
    
    1. Le texte est affiché dans la console Python, sans les guillemets.
    2. Rien ne se passe, le code est mis en commentaire donc Python ignore la ligne.
    3. Une erreur apparait car la variable Bienvenue n'existe pas. Sans guillemets, Python interprète du texte comme
    étant une variable.

Essayez désormais le code ci-dessous. Que remarquez-vous ?

```python
print(42)
```

??? bug "Réponse"

    Un texte qui ne comporte que des chiffres sera considéré comme un nombre entier `int`. Python peut donc afficher
    la valeur de ce nombre.

## Variables

Le symbole `=` permet d'**affecter** une valeur à une variable. Lors de la première affectation, on parle aussi
d'**initialisation**.

Voici un exemple que vous pouvez exécuter :

```python
Bonjour = "Salut"
print(Bonjour)
```

1. Modifier ce programme en affectant `7` (sans guillemets) à la variable `Bonjour` puis exécuter ce programme.
2. De même en affectant `7,4` à la variable `Bonjour`.
3. De même en affectant `7.4` à la variable `Bonjour`.

Qu'avez-vous remarqué ?

??? bug "Réponse"

    Le type d'une variable sera automatiquement défini par son affectation. Dans le cas initial, la varaible `Bonjour`
    est de type `str`.

    Pour rédiger un nombre flottant `float` (nombre réel), on doit utiliser le point `.` et non pas la virgule `,` pour
    désigner la virgule de notre nombre.

    Pour rédiger une chaîne de caractères `str`, il est impératif d'utiliser des guillemets `"`.

Comme expliqué dans le cours, il est possible de transformer une valeur d'un type en un autre type. Dans l'exemple
ci-dessous, l'entier `7` va être transformé en flottant `7.0` puis en chaîne de caractères `"7.0"`. Exécutez le code,
et expliquez pourquoi la dernière opération ne fonctionne pas.

```python
n = 7          # n est un entier (type int)

n = float(n)   # Transformation de n en flottant (type float)
print(n)

n = str(n)     # Transformation de n en chaîne de caractère (type str)
print(n)

n = int(n)     # Transformation si possible de n en entier
print(n)
```

??? bug "Réponse"

    La transformation de `"7.0"` en entier ne fonctionne pas car le caractère `.` est dédié aux flottants. Il faudrait
    avoir `"7"` pour réaliser cette tranformation.

Qu'en est-il pour le code ci-dessous ?

```python
n = 7.4         # n est un flottant (type float)

n = int(n)      # Transformation de n en entier (type int)
print(n)

n = str(n)      # Transformation de n en chaîne de caractère (type str)
print(n)

n = int(n)      # Transformation si possible de n en entier
print(n)
```

??? bug "Réponse"

    Il fonctionne parfaitement, car la traduction `float` -> `int` est possible même avedc des nombres à virgule
    (contrairement à `str` -> `int` qui nécessite un nombre entier initialement).

!!! info "Connaître le type"

    Pour connaître le type d'une variable, on peut utiliser la fonction `#!python type()` qui renvoie le type d'une
    variable. Par exemple :

    ```python
    x = 3.14
    print(type(x))
    ```

## Intéraction

Nous avons vu en cours que pour récupérer les données saisies par l'utilisateur, on utilise la fonction `#!python
input()`. On va ensuite affecter la donnée saisie par l'utilisateur dans une variable afin d'*enregistrer* sa valeur.
Un message sous forme de chaîne de caractères (pour rappel, encadré par des guillemets `"`) peut être écrit
entre les parenthèses de `#!python input()`.

Par exemple, essayez le code suivant :

```python
nom = input("Quel est votre nom ?")
print("Bonjour")                      # Affichage d'un message
print(nom)                            # Affichage de la valeur contenue dans la variable nom
```

Vous devez entrer un texte dans la console Python, puis appuyer sur entrée.

!!! warning "Attention !"

    La fonction `#!python input()` récupère **forcément** des valeurs de type `str`, ce qui peut être gênant dans de
    nombreux programmes. En utilisant les fonctions int() ou float(), on force le changement de type.

    ```python
    xA = input("Donner l'abscisse de A : ")         # xA est de type str
    print(type(xA))
    
    xB = int(input("Donner l'abscisse de B : "))    # xB sera de type int
    print(type(xB))
    ```

### Exercice : Calcul du carré

Rédigez un code Python qui demande un nombre entier à un utilisateur et affiche son carré.

??? question "Comment calculer le carré d'un nombre ?"

    Si votre nombre est dans une variable nommée `n`, vous pouvez le multipliez par lui-même `n * n` (`n` fois `n`
    ou utiliser l'opérateur de la puissance `n ** 2` (`n` puissance 2).

??? bug "Solution"

    ```{.python .no-copy}
    nombre = int(input("Donner un nombre entier : "))
    print(nombre ** 2)
    ```

## Affichage simplifié

Afin de faciliter l'affichage, il existe les *f-strings*. Cela consiste à écrire le message à afficher, mais à
spécifier des variables qui viendront se mettre au milieu du message. Voici un exemple :

```python
prenom = input("Quel est votre prénom ? ")
age = int(input("Et quel est votre âge ? "))

print(f"Bonjour {prenom} ! Vous avez déjà {age} ans ! Le temps passe si vite !")
```

!!! methode "F-strings"

    En plaçant un `f` devant une chaîne de caractères `str`, toutes les parties entre `{}` seront considérées comme
    du code qui sera exécuté. On peut donc y placer une variable qui sera affichée.

### Exercice : Somme de deux nombres

Rédigez un code Python qui demande deux nombres (entiers ou flottants) à un utilisateur et affiche son addition.

La syntaxe d'affichage doit être sous la forme `4.2 + 7.0 = 11.2` (si `4.2` et `7` sont les nombres entrés par
l'utilisateur)

??? question "Entier ou flottant ?"

    Comme chaque nombre entier peut être écrit sous la forme d'un nombre flottant (en ajoutant `.0` à la fin), on
    peut directement supposé que le texte entré par un utilisateur est sous la forme d'un flottant.

    On utilisera donc `#!python float()` pour convertir le résultat obtenu avec `#!python input()`.

??? bug "Solution"

    ```{.python .no-copy}
    nombre1 = float(input("Entrez un premier nombre : "))
    nombre2 = float(input("Entrez un deuxième nombre : "))

    print(f"{nombre1} + {nombre2} = {nombre1 + nombre2}")
    ```