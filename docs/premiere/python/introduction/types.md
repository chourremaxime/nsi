## Types

En Python, les données stockées sont définies par un type (en plus de leur valeur). Il en existe une multitude,
mais nous allons voir les principales :

| Type             | Description                                         | Exemple                                          |
|------------------|-----------------------------------------------------|--------------------------------------------------|
| `#!python bool`  | Une donnée qui est soit vraie soit fausse (Booléen) | `#!python True`                                  |
| `#!python int`   | Nombres entiers                                     | `#!python 42` `#!python -32` `#!python 0`        |
| `#!python float` | Nombres réels                                       | `#!python 42.23` `#!python 3.14` `#!python -1.0` |
| `#!python str`   | Chaînes de caractères *(string)*                    | `#!python "Hello World"` `#!python "Bonjour"`    |

Les types ci-dessus sont des types de base. Pour créer notre propre type, il faudra attendre l'année de terminale.

La fonction `#!python print()` vue précédemment permet d'afficher ces différentes valeurs. Par exemple :

```python
print(42)
print("Bonjour, j'ai", 16, "ans !")
```

Ce type précise également les opérations possibles sur les données.

!!! info Objets
    À noter que les données que nous manipulons sont appelées **Objets**. On parle donc du type de l'objet.

## Opérations

On peut ensuite réaliser des opérations simples avec ces objets. Voyons celles adaptées aux nombres, c'est-à-dire 
aux entiers `int` et aux réels `float`.

| Opération                           | Symbole Python    | Type du résultat avec deux `#!python int` | Type du résultat avec au moins un `#!python float` |
|-------------------------------------|-------------------|-------------------------------------------|----------------------------------------------------|
| Addition                            | `#!python a + b`  | `#!python int`                            | `#!python float`                                   |
| Soustraction                        | `#!python a - b`  | `#!python int`                            | `#!python float`                                   |
| Multiplication                      | `#!python a * b`  | `#!python int`                            | `#!python float`                                   |
| Puissance $a^b$                     | `#!python a ** b` | `#!python int`                            | `#!python float`                                   |
| Division                            | `#!python a / b`  | `#!python float`                          | `#!python float`                                   |
| Quotient de la division euclidienne | `#!python a // b` | `#!python int`                            | `#!python float`                                   |
| Reste de la division euclidienne    | `#!python a % b`  | `#!python int`                            | `#!python float`                                   |

Lorsqu'on enchaîne plusieurs opérations à la suite, l'ordre de priorité est identique à celui en mathématiques. On
utilisera alors des parenthéses `#!python (` et `#!python )` pour contrôler les priorités, toujours comme
en mathématiques.

```python
a = 3
b = 6
c = 3.5

print(a + b * c) # (1)!
print((a + b) * c) # (2)!
```

1.  Cette instruction affiche `#!python 24.0` dans la console Python (la multiplication est prioritaire).
2.  Cette instruction affiche `#!python 31.5` dans la console Python (l'addition est prioritaire grâce aux parenthèses).

## Changer de type

Il existe des fonctions qui permettent de convertir une valeur d'un type vers un autre. C'est particulièrement utile
pour obtenir des nombres à partir de chaînes de caractères. En effet, si on essaye d'effectuer l'opération suivante
`#!python "4" + 2`, cela ne fonctionnera pas car l'addition d'un texte et d'un nombre n'a pas de sens.

Voici donc les fonctions de conversion existantes :

| Type souhaité    | Fonction            | Explications                                           |
|------------------|---------------------|--------------------------------------------------------|
| `#!python int`   | `#!python int(x)`   | Transforme un texte ou un nombre réel en nombre entier |
| `#!python float` | `#!python float(x)` | Transforme un texte ou un nombre réel en nombre réel   |
| `#!python str`   | `#!python str(x)`   | `#!python "Hello World"` `#!python "Bonjour"`          |

