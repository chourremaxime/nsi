## La mémoire

![Barettes de RAM](ram.png){ align=right ; width=50%}

Dans l'introduction aux programmes, nous avons vu que certaines instructions permettaient de stocker des **données**
dans la **mémoire**. C'est notamment utile lorsqu'on souhaite conserver le résultat d'une opération pour plus tard.

La mémoire, plus précisément la **mémoire RAM**, permet de stocker des données. *Chaque information stockée peut être
retrouvée grâce à une adresse, mais ce procédé sera étudié en classe de terminale.*

## Les données

Les données stockées dans la mémoire sont appelées ***variable***. Ce sont de petites informations temporaires qui
permettent au programme de retenir des informations. Comme leur nom l'indique, elles sont variables, c'est-à-dire
que leur valeur peut changer au cours du temps.

Les variables sont **volatiles**, autrement dit, elles peuvent être effacées lors de la fin du programme. *Si on
souhaite sauvegarder des données à long terme, on utilisera le disque dur et les fichiers. Nous étudierons ce cas plus
tard.*

## Les variables

!!! abstract "Variables"
    Les variables sont constituées de deux éléments :
    
    1. une **valeur** : c'est la donnée que stocke la variable
    2. un **nom** : c'est ce qui permet de retrouver la valeur dans la mémoire sans retenir son adresse

La syntaxe Python pour créer (ou modifier) une variable est la suivante :

```python
jour = 14
mois = "juillet"
annee = 2024
```

On peut ensuite afficher ces variables grâce à la **fonction** `#!python print()`

```python
>>> print(jour)
14
>>> print(jour, mois, annee)
14 juillet 2024
```

On remarque plusieurs choses :

- On utilise le signe `=` pour **affecter** une valeur à une variable.
- La fonction `#!python print()` permet d'**afficher** des messages dans le terminal Python.

!!! info "Nom de variable"

    Le nom d'une variable **doit** respecter les règles suivantes :
    - Commencer avec une lettre ou `_`
    - Contenir uniquement des caractères alphanumériques et `_`.
    - Ne pas être un mot clef Python (`if`, `else`...)

    Le nom d'une variable **devrait** respecter les règles suivantes :
    - Être cohérent (pour éviter de s'y perdre).
    - Contenir seulement des mots en minuscule séparés par des `_`