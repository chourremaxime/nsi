Le mot **fonction** doit vous évoquer les fonctions en mathématiques. Pour rappel une fonction mathématique est une
expression qui permet d'associer une image à une variable. Par exemple, si nous disposons de la fonction $y=f(x)=3x+2$, 
alors cela nous permet d'obtenir la valeur de $y$ pour n'importe quelle valeur de $x$. Ainsi, si on calcule $y=f(3)$, 
ainsi, on obtient $y=11$.

On peut donc voir une fonction comme étant une boîte qui dispose d'une entrée et d'une sortie :
```mermaid
graph LR
    fonction["`Fonction
    f`"]
    entree["`Entrée
    x`"]
    sortie["`Sortie
    y = f(x)`"]
    
    entree --> fonction --> sortie
    
    style entree fill:#FFFFFF00, stroke:#FFFFFF00;
    style sortie fill:#FFFFFF00, stroke:#FFFFFF00;
```

## Vocabulaire

En programmation, les fonctions réalisent un travail identique, mais disposent de leur propre vocabulaire.

!!! abstract "Argument"

    Une valeur présente en entrée de la fonction est appelée **argument**. Il peut y en avoir plusieurs.

!!! abstract "Retour"

    Une valeur présente en sortie de la fonction est appelée **retour** de fonction. Il n'en existe qu'une.

!!! abstract "Définition"

    Le contenu qui décrit le fonctionnement de la fonction est appelé **définition** de la fonction.

!!! abstract "Appel"

    L'instruction qui permet d'obtenir le retour d'une fonction avec un ou plusieurs arguments est appelé **appel** de 
    fonction.

## Comment appeler une fonction ?

Appeler une fonction consiste à exécuter le contenu de sa définition, en fonction des arguments que l'on aura donnés 
à cet appel de fonction. En Python, il faut utiliser le nom de la fonction, suivi de parenthèses contenant les 
arguments, séparés par des virgules. Par exemple :

```python
f(3)
ma_fonction(4, "Oui")
```

Lors de l'exécution de ce code Python, les instructions présentent dans la définition de la fonction vont être 
exécutées, et un résultat sera retourné. Pour stocker ce résultat dans une variable, on utilise la syntaxe suivante :

```python
y = f(3)
mon_retour = ma_fonction(4, "Oui")
```

## Comment définir une fonction ?

Oula ! Pas trop vite ! Rédiger la définition d'une fonction nécessite de savoir déjà maîtriser la programmation ! 
Nous reviendrons sur ce point plus tard.