![Logo Python](python.png){ align=left ; width=25%}

Le langage de programmation Python a commencé à être **développé en 1989**, par le néerlandais Guido van Rossum, il est
à présent reconnu, avec humour, comme « Dictateur bienveillant à vie ». Ce langage est libre et multiplateforme,
c'est-à-dire disponible sur plusieurs systèmes d'exploitation (Windows, Linux, Mac...).

!!! info "Versions de Python"

    Il existe deux versions principales du langage Python :
    
    - **Python 2**, version plus supportée depuis le 1er janvier 2020, ne sera plus utilisée.
    - **Python 3**, actuellement à la version 3.12, la version 3.X est installée sur les ordinateurs du lycée.

C'est donc cette dernière version, Python 3, qui sera utilisée cette année.

> **Attention**, un programme écrit en Python 3 ne sera pas forcément compatible avec Python 2.

Python est utilisé comme langage de programmation dans l'**enseignement supérieur**, notamment en France. Depuis 2013,
il y est enseigné, en même temps que [Scilab](http://www.scilab.org/fr/), à tous les étudiants de classes préparatoires
scientifiques dans le cadre du tronc commun (informatique pour tous). Les premières épreuves de concours portant sur le
langage Python sont celles de la session 2015.

De **nombreuses entreprises** utilisent Python, comme Google, Dropbox, la NASA ou encore le secteur bancaire comme en
témoigne
cet [article](https://www.developpez.com/actu/277305/Athena-de-JPMorgan-a-35-millions-de-lignes-de-code-Python-et-ne-sera-pas-mis-a-jour-vers-Python-3-a-temps-selon-eFinancialCareers/).