## Rappels

Voici des exercices supplémentaires pour progresser. Ils ne seront pas généralement corrigés en classe, mais rien
ne vous empêche de les traiter, et même de demander de l'aide à votre enseignant.

Pour rappel, pensez à enregistrer vos fichiers sur le réseau afin de ne pas les perdre ! Vous pouvez aussi les stocker
sur une clef USB pour y retravailler à la maison.

L'arborescence proposée reste identique :

```
Documents/
└── NSI/
    ├── Cours/
    └── Exercices/
        └── Introduction/
            ├── ...
            └── Bonus/
                ├── exercice1.py
                ├── exercice2.py
                └── ...
```

Pensez à ajouter des commentaires dans vos fichiers lors de la programmation ! Il sera ainsi plus facile de revenir
dessus plus-tard.

## Exercice n°1 : Échange de deux variables

Le programme de cet exercice demande un peu de bon sens et de *fair-play*.

L'utilisateur entre deux valeurs affectées aux variables `a` et `b`. Dans l'exemple d'affichage ci-dessous,
l'utilisateur a entré `7` pour la valeur de `a` et `2` pour la valeur de `b`.

```
Entrer la valeur de a : 7
Entrer la valeur de b : 2
A present, a = 2 et b = 7
```

Écrire un programme qui permet l'échange de ces valeurs, c'est-à-dire :

- Si on ajoute l'instruction `#!python print(a)` alors le programme affichera `2` ;
- Et si on ajoute l'instruction `#!python print(b)` alors le programme affichera `7`.

??? question "Une piste ?"

    Avec un verre de grenadine dans la main droite et un verre de menthe à l'eau dans la main gauche, comment faire*
    pour échanger les liquides de verre sans que les liquides ne se mélangent ?

??? question "Une info ?"

    Difficile d'échanger deux liquides sans un intermédiaire...

??? question "Une analyse ?"

    On introduit une 3ème variable notée `c`. Alors
        
        - on place la valeur de `a` dans `c` (`a` et `c` ont pour valeur `2`);
        - on place la valeur de `b` dans `a` (`a` et `b` ont pour valeur `7`);
        - on place la valeur de `c` dans `b` (`b` et `c` ont pour valeur `2`);

??? bug "Solution"

    ```{.python .no-copy}
    a = input("Entrer la valeur de a : ")
    b = input("Entrer la valeur de b : ")
    c = a                                    # La grenadine va dans le 3eme verre
    a = b                                    # La menthe va dans le verre de la grenadine
    b = c                                    # On vide le 3eme verre dans l'ancien verre de la menthe
    
    print(f"A present, a = {a} et b = {b}")
    ```

??? bug "Astuce"

    En fait, il y a plus simple car Python autorise l'affectation parallèle :

    ```{.python .no-copy}
    a = input("Entrer la valeur de a : ")
    b = input("Entrer la valeur de b : ")
    a, b = b, a                            # a et b echangent de valeur en "parallele"
    
    print(f"A present, a = {a} et b = {b}")
    ```

## Exercice n°2 : Échange de trois variables

Modifier le programme précédent pour échanger cette fois-ci les valeurs de trois variables (celle de a va dans b ;
celle de b va dans c et celle de c va dans a).

```
Entrer la valeur de a : 7
Entrer la valeur de b : 2
Entrer la valeur de c : 4
A present, a = 4 et b = 7 et c = 2
```

??? question "Une piste ?"

    Difficile d'échanger des liquides sans intermédiaire...

??? question "Une autre piste ?"

    On introduit une variable supplémentaire et on reprend le raisonnement précédent.
    
    On peut aussi essayer l'affectation en parallèle !

??? bug "Solution"

    ```{.python .no-copy}
    a = input("Entrer la valeur de a : ")
    b = input("Entrer la valeur de b : ")
    c = input("Entrer la valeur de c : ")
    d = a
    a = c
    c = b
    b = d
    
    print(f"A present, a = {a}, b = {b} et c = {c}")
    ```

??? bug "Astuce"

    L'affectation parallèle marche encore :

    ```{.python .no-copy}
    a = input("Entrer la valeur de a : ")
    b = input("Entrer la valeur de b : ")
    c = input("Entrer la valeur de c : ")
    a, b, c = b, c, a
    
    print(f"A present, a = {a}, b = {b} et c = {c}")
    ```