Voici vos premiers exercices sérieux. Pensez à enregistrer vos fichiers sur le réseau afin
de ne pas les perdre ! Vous pouvez aussi les stocker sur une clef USB pour y retravailler à la maison.

Commencez par créer l'arborescence de dossiers suivants :

```
Documents/
└── NSI/
    ├── Cours
    └── Exercices/
        └── Introduction/
            ├── exercice1.py
            ├── exercice2.py
            └── ...
```

Pensez à ajouter des commentaires dans vos fichiers lors de la programmation ! Il sera ainsi plus facile de revenir
dessus plus-tard.

## Exercice 1

L'utilisateur entre une valeur. Dans l'exemple d'affichage ci-dessous, l'utilisateur a entré la valeur `19` après
l'affichage du message.

```
Entrer un nombre : 19
Le carre du nombre 19 est 361
```

Écrire un programme qui affiche le carré du nombre fourni par l'utilisateur.

??? question "Une piste ?"

    Le carré d'un nombre est égal à ce nombre multiplié par lui-même...

??? question "Une autre piste ?"

    Ne pas oublier d'utiliser une variable à laquelle on affecte la valeur saisie par l'utilisateur.

??? question "Encore une piste ?"

    Attention à la fonction `#!python input()` qui renvoie une chaîne de caractère, pas un entier...

    Il y a donc une transformation de type à effectuer.

??? bug "Solution"

    ```{.python .no-copy}
    n = int(input('Entrer un nombre :'))
    print(f"Le carre du nombre {n} est {n**2}")
    ```

## Exercice 2

L'utilisateur entre deux valeurs. Dans l'exemple d'affichage ci-dessous, l'utilisateur a entré les valeurs 
`12` et `15` après les messages.

```
Entrer un nombre : 12
Entrer un autre nombre : 15
Leur somme est 27
Leur difference est -3
Leur produit est 180
```

Écrire le programme correspondant.

??? question "Une piste ?"

    Les résultats affichés doivent s'adapter aux nombres saisis par l'utilisateur.

??? question "Une autre piste ?"

    Ne pas oublier d'affecter les valeurs saisies par l'utilisateur à des variables.

??? question "Encore une piste ?"

    Encore piégé par la fonction `#!python input()` ?

??? bug "Solution"

    ```{.python .no-copy}
    a = int(input('Entrer un nombre : '))
    b = int(input('Entrer un autre nombre : '))
    print("Leur somme est {a+b}")
    print("Leur difference est {a-b}")
    print("Leur produit est {a*b}")
    ```

## Exercice 3

Traduire les instructions ci-contre en Python et implémenter le script dans un fichier.

| Ordre      | Instructions                                            |
|------------|---------------------------------------------------------|
| Entrées    | Lire la valeur de $r$<br/>$pi$ prend la valeur $3.1416$ |
| Traitement | $s$ prend la valeur $pi \times r^2$                     |
| Sorties    | Afficher $s$<br/>Afficher le type de $s$                |

Exécuter ce script en choisissant le nombre `100`.

Recommencer en choisissant le nombre `10`.

Les affichages obtenus sont-ils cohérents ?

??? question "Une piste ?"

    Chacune des instructions se traduit par une seule ligne en Python.

??? bug "Solution"

    ```{.python .no-copy}
    # Entrées
    r = int(input('Entrer un entier : '))
    pi = 3.1416
    
    # Traitement
    s = pi * r **2
    
    # Sorties
    print("Valeur de s : {s}')
    print("Type de s : {type(s)}")
    ```

??? bug "Explication des résultats"

    Voici les affichages en choisissant le nombre `100` :
    
    ```
    >>> (executing file "exercice3.py")
    Entrer un entier : 100
    Valeur de s : 31416.0
    Type de s : <class 'float'>
    ```
    
    Dans cet exemple, `r` est un entier, `pi` est un flottant donc `s` est un flottant par *multiplication des types*.
    Le résultat correspond à ce qui était attendu.
    
    Voici les affichages en choisissant le nombre `10` :
    
    ```
    >>> (executing file "exercice3.py")
    Entrer un entier : 10
    Valeur de s : 314.15999999999997
    Type de s : <class 'float'>
    ```
    
    Dans cet exemple, `r**2` donne `100`. Le résultat attendu pour `s` devrait donc être `314.16`. On n'obtient pas ce
    résultat à cause de la représentation des flottants dans la machine.
    
    En conclusion, **il faut être vraiment attentif avec l'utilisation des flottants, car le calcul n'est pas
    effectué en valeur exacte...** 