Lorsque l'on crée un programme, on doit pouvoir le rendre intéractif. En effet, il y a de nombreux cas où votre
programme doit afficher un message différent selon ce qu'a pu rentrer l'utilisateur en entrée.

Par exemple, on peut créer un programme qui demande le prénom de son utilisateur et qui affiche ce prénom (programme
très basique, et qui semble inutile au premier abord).

## Demander une valeur

On a une variable `age` qui va contenir l'âge de l'utilisateur. Pour connaître l'âge, il faut utiliser la
fonction `#!python input()` qui permet de demander une valeur. Cette fonction prend un argument facultatif, qui est le 
message à afficher avant de demander une valeur à l'utilisateur.

```python
age = input("Quel est votre âge ?")
prenom = input("Entrez votre prénom :")
message = input()
```

!!! methode "Demander une valeur"
    
    ```python
    variable = input("Message à afficher")
    ```

## Afficher une valeur

Enfin, on peut afficher une valeur avec la fonction `#!python print()`. Cette fonction prend autant d'arguments que 
nécessaire. Elle va afficher la valeur de tous les arguments, séparés par des espaces, puis fera un retour à la ligne.

```python
age = input("Quel est votre âge ?")
prenom = input("Entrez votre prénom :")
print("Bonjour", prenom, ", vous avez", age, "ans !")
```

Cela peut vite devenir fastidieux lorsqu'on a plusieurs variables à afficher dans une seule ligne avec print. C'est 
pour cela que depuis la version 3.6 de Python, il est possible d'utiliser des **f-strings**. Les f-strings 
permettent de créer un texte contenant des variables placées entre accolades.

```python
age = input("Quel est votre âge ?")
prenom = input("Entrez votre prénom :")
print(f"Bonjour {prenom}, vous avez {age} ans !")
```

!!! methode "Afficher une valeur"

    ```python
    print(variable)
    print(f"Message avec {variable}.")
    ```
