## Comment installer Python ?

Avant toute chose, il est nécessaire d'**installer Python**. Pour vérifier si Python est installé, on peut écrire dans
un terminal la commande `python -V` ou `python3 -V`. Si le programme est installé, sa version apparaît dans le terminal.
Sinon, un message d'erreur apparaît.

!!! methode "Installation de Python"

    === "Windows"
    
        [Installer Python 3.12 :material-download:](ms-windows-store://pdp/?ProductId=9ncvdn91xzqp){ .md-button }
    
        Le bouton ci-dessus vous permet d'ouvrir directement le Microsoft Store, et de vous afficher la page
        ***Python 3.12***. Installez cette application pour installer Python.
    
        Attention, vous devrez installer les nouvelles versions majeures de Python manuellement.
    
    === "Linux"
    
        [Installer Python 3 :material-download:](apt://python3){ .md-button }
    
        Le bouton ci-dessus revient à exécuter le code ci-dessous :
    
        ```bash
        sudo apt install python3 -y
        ```
    
        Cela devrait installer Python ou le mettre à jour s'il est déjà installé.

    === "Mac OS"

        [Télécharger Python 3 :material-download:](https://www.python.org/ftp/python/3.12.6/python-3.12.6-macos11.pkg){ .md-button }

        Le bouton ci-dessus permet de télécharger l'installateur de Python. Il faut ensuite l'exécuter et suivre le processus
        d'installation.

## Comment écrire un programme en Python ?

### Via le terminal

![Terminal Python](terminal.png){ align=right ; width=50% }

La première façon basique pour écrire un programme en Python (et l'exécuter) est d'utiliser un **terminal** (ou invite
de commande sous Windows). En entrant `python` ou `python3`, le terminal devient un terminal Python. Les symboles `>>>`
nous indiquent qu'on peut entrer une instruction en Python.

Essayons par exemple ceci, obtenu **en tapant `python` dans un terminal** :

```py
Python 3.12.4 (main, Jun 7 2024, 06:33:07) [GCC 14.1.1 20240522] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> print("Hello World !")
```

En appuyant sur `Entrée`, le code s'exécute et affiche `Hello World !`.

Pour quitter, on utilisera `Ctrl + D` ou on écrira `exit()`.

!!! failure "À éviter"

    L'utilisation du terminal ne peut se faire que sur des petits tests. Vous ne pourrez pas sauvegarder ce que vous
    avez écrit dans le terminal ! Il est impératif de privilégier une autre méthode.

### Avec un éditeur de texte

Python peut aussi lire le contenu d'un **fichier texte** (à ne pas confondre avec un document LibreOffice ou Word).
Ouvrez un éditeur de texte (Gedit, Bloc-notes) et entrez le même code qu'au-dessus, c'est-à-dire `print("Hello World")`.

Le fichier texte doit ensuite être enregistré avec l'extension `.py` et non `.txt`. Pour l'exécuter, on inscrira dans un
terminal `python3 nom_fichier.py` après s'être placé dans le bon dossier.

!!! tip "Afficher les extensions de fichier"

    Sous Windows, les extensions de fichier sont masquées par défaut.

    Pour les afficher, ouvrez l'explorateur de fichiers, puis accédez à ses options ***Options des dossiers***. Dans 
    l'onglet ***Affichage***, vous trouverez une ligne ***Masquer les extensions des fichiers dont le type est 
    connu***. Désactivez ce réglage et enregistrez.

### Avec un IDE (recommandé)

Les méthodes vues précédemment nécessitent l'utilisation du terminal. Cela peut vite devenir compliqué, surtout
lorsqu'on débute. La majorité des développeurs utilisent un éditeur spécialisé, plus connu sous le nom d'**IDE**
(Integrated Development Environment).

Ce type de logiciel propose, en plus d'un éditeur de texte, une intégration aux langages de programmation. Cela permet
donc de rédiger un code Python à l'aide de propositions de mots-clefs, puis de l'exécuter en un clic.

Au lycée, nous utiliserons [Pyzo](https://pyzo.org/start.html). Lors du premier démarrage, il détectera automatiquement
votre installation de Python. Validez son choix en cliquant sur **use this environment**.

!!! methode "Installation de Pyzo"

    === "Windows"
    
        [Télécharger Pyzo :material-download:](https://github.com/pyzo/pyzo/releases/download/v4.16.0/pyzo-4.16.0-win64.exe){ .md-button }
    
        Le bouton ci-dessus permet de télécharger l'installateur de Pyzo. Il faut ensuite l'exécuter et suivre le processus
        d'installation.
    
    === "Linux"
    
        [Installer Pyzo :material-download:](apt://pyzo){ .md-button }
    
        Le bouton ci-dessus revient à exécuter le code ci-dessous :
    
        ```bash
        sudo apt install pyzo -y
        ```

    === "Mac OS"

        [Télécharger Pyzo pour puce Apple Silicon (ARM64) :material-download:](https://github.com/pyzo/pyzo/releases/download/v4.16.0/pyzo-4.16.0-macos_arm64.dmg){ .md-button }
        [Télécharger Pyzo pour puce Intel (x86_64) :material-download:](https://github.com/pyzo/pyzo/releases/download/v4.16.0/pyzo-4.16.0-macos_x86_64.dmg){ .md-button }

        Le bouton ci-dessus permet de télécharger l'installateur de Pyzo. Il faut ensuite l'exécuter et suivre le processus
        d'installation.

![Écran d'accueil de Pyzo lors du premier démarrage](pyzo_premier_demarrage.png)

Afin de nous simplifier l'utilisation de ce logiciel, vous pouvez fermer les onglets ***structure du programme*** et
***explorateur de fichiers***.

!!! info "Interface"

    La **partie à gauche** du logiciel correspond à l'éditeur. C'est ici que vous pourrez naviguer entre vos fichiers via
    les différents onglets et écrire les différentes instructions pour votre programme.
    
    La **partie à droite** du logiciel correspond au terminal Python. Lors d'un appui sur :material-keyboard-f5:, le
    code contenu dans le fichier à gauche sera exécuté dans le terminal. Il est aussi possible d'interagir avec le
    terminal en y inscrivant des instructions.

    *Selon les ordinateurs, il peut être nécessaire d'appuyer sur les deux touches **fn** et :material-keyboard-f5:.

N'oubliez pas d'enregistrer vos fichiers régulièrement !