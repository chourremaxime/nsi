Des fois, on va chercher à réaliser une même action plusieurs fois. Par exemple, affiche plusieurs fois un certain
message ou réaliser un calcul plusieurs fois.

Il existe deux types de boucles en Python :

- La boucle POUR ou FOR, où on connait à l'avance le nombre total de répétitions à faire.
- La boucle TANT QUE ou WHILE où on ne connait pas le nombre de répétitions qui va dépendre d'une condition vérifiée
  à chaque tour de boucle.

Nous étudierons ces deux types de boucle en Python.