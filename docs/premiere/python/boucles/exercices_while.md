## Rappels

Pensez à enregistrer vos fichiers sur le réseau afin de ne pas les perdre ! Vous pouvez aussi les stocker sur une
clef USB pour y retravailler à la maison.

Commencez par créer l'arborescence de dossiers suivants :

```
Documents/
└── NSI/
    ├── Cours
    └── Exercices/
        ├── Introduction/
        ├── Conditions/
        └── Boucles/
            └── While/
                ├── exercice1.py
                ├── exercice2.py
                └── ...
```

Pensez à ajouter des commentaires dans vos fichiers lors de la programmation ! Il sera ainsi plus facile de revenir
dessus plus-tard.

## Exercice n°1 : Carrés et cubes

Écrire un programme, avec la boucle `#!python while`, permettant d’afficher les carrés et les cubes des 20 premiers
entiers naturels non nuls.

??? question "Une piste ?"

    Quelle sera la condition de la boucle ? Il sera nécessaire de règler une variable **avant** la boucle.

??? question "Une aide ?"

    Il faut stocker les entiers naturels dans une variable : `x` par exemple. Le premier entier est 1, et les
    suivants sont donc `x + 1`. Cela devrait vous aider à trouver la condition de boucle.

??? bug "Solution"

    ```{.python .no-copy}
    x = 1
    while x <= 20:
        print(x, x*x, x**3)
        x += 1
    ```

## Exercice n°2 : Monopoly

On veut écrire une simulation du jeu de monopoly. Un joueur peut rejouer s'il fait un double.

Mais attention ! S'il faut trois doubles de suite, il doit aller en prison.

Le programme à concevoir doit :

- **Simuler le lancer** aléatoire de deux dés à 6 faces ;
- **Afficher la valeur** de chaque face ainsi que le nombre de cases à parcourir ;
- **Relancer les dés** dans le cas d'un double ;
- **Recommencer...** et envoyer le joueur en prison s'il y a trois doubles de suite.

Un exemple d'affichage avec envoi en prison :

```
Les faces sont 5 et 5 : Avancez de 10 cases.
Il y a un double, rejouez.
Les faces sont 2 et 2 : Avancez de 4 cases.
Il y a un double, rejouez.
Les faces sont 6 et 6 : Avancez de 12 cases.
Oups ! Encore un double, allez en prison !
```

!!! info "Obtenir un nombre aléatoire"

    Le module **random** donne accès à des fonctions qui renvoient des nombres aléatoires. Il existe deux fonctions
    importantes :
    
    - La fonction `#!python random()` qui renvoie un nombre décimal aléatoire entre 0 et 1 (exclu).
    - La fonction `#!python randint(nb1, nb2)` qui renvoie un nombre entier aléatoire entre `nb1` et `nb2` inclus.

    Voici un exemple d'usage de ces fonctions :

    ```python
    from random import *        # Pour accéder aux fonctions du module

    a = random()                # Nombre décimal aléatoire entre 0 et 1 (exclu)
    b = randint(3, 12)          # Entier aléatoire entre 3 et 12 inclus

    print(f"La valeur de a est {a}")
    print(f"La valeur de b est {b}")
    ```

??? question "Une piste ?"

    Il faudrait essayer d'utiliser un maximum de variables. Et ces variables doivent avoir une dénomination explicite !

??? question "Une autre piste ?"

    Le descriptif du programme à concevoir détaille bien le travail à réaliser. Prenez le temps de faire un brouillon...

??? bug "Solution"

    ```{.python .no-copy}
    # Importation des modules
    from random import *
    
    # Initialisation des variables
    double = 0                      # Nombre de doubles
    de1 = randint(1, 6)
    de2 = randint(1, 6)
    
    # Programme principal
    print(f"Les faces sont {de1} et {de2} : Avancez de {de1 + de2} cases.")
    
    while (de1 == de2) and (double < 2):
        double += 1                    # Une seule indentation : on reste dans le while...
        if double < 2:
            print("Il y a un double, rejouez.")
            de1 = randint(1, 6)
            de2 = randint(1, 6)
            print(f"Les faces sont {de1} et {de2} : Avancez de {de1 + de2} cases.")
        else:
            print("Oups ! Encore un double, allez en prison !")
    ```

## Exercice n°3 : Un algorithme bien connu

Écrire un programme qui résout l'équation du second degré $a x^2 + bx + c = 0$.

L'utilisateur entre les valeurs des coefficients `a`, `b` et `c` (en obligeant cet utilisateur à entrer une valeur non
nulle pour a) et le programme affiche la valeur du discriminant et les éventuelles solutions (données sous forme
approchée).

Dans l'exemple d'affichage ci-dessous, l'utilisateur a entré les valeurs `1`, `-3` et `2`.

```
Resolution de ax^2 + bx + c = 0.
Valeur de a : 1
Valeur de b : -3
Valeur de c : 2
Le discriminant est 1.0.
Il y a deux solutions : 1.0 et 2.0.
```

!!! info "Calculer une racine carrée"

    Le module **math** donne accès à des fonctions autour des mathématiques. Voici un exemple d'usage :

    ```python
    from math import *        # Pour accéder aux fonctions du module
    
    x = 16
    y = sqrt(x)
    print(f"La racine carrée de {x} est {y}")
    ```

??? question "Où est la boucle while ?"

    La boucle `while` va permettre de vérfier que l'utilisateur entre une valeur non nulle pour `a`.

??? question "Un conseil ?"

    Si vous avez oublié ce qu'est une équation du second degré, consultez votre cours de mathématiques.

??? question "Une aide ?"

    Vous devez d'abord obtenir les valeurs de `a`, `b` et `c`. Pour celle de `a`, vérifiez à l'aide d'une boucle que
    la valeur est différente de 0.

    Ensuite, calculez le delta de l'équation : $\Delta = b^2 - 4ac$ et rajoutez des exécutions conditionnelles 
    *(if/else)* en fonction de la valeur de $\Delta$ :

    - Si $\Delta < 0$, il n'y a pas de solutions.
    - Si $\Delta = 0$, il y a une unique solution $x=\frac{-b}{2a}$
    - Si $\Delta > 0$, il y a deux solutions : $x_1=\frac{-b-\sqrt{\Delta}}{2a}$ et $x_2=\frac{-b+\sqrt{\Delta}}{2a}$

??? bug "Solution"

    ```{.python .no-copy}
    # Importation des modules
    from math import *
    
    # Initialisation du programme et des variables
    print("Resolution de ax^2 + bx + c = 0 :")
    
    a = float(input("Valeur de a : "))
    
    while a == 0:
        print("a doit être différent de zéro !")
        a = float(input("Valeur de a : "))
    
    b = float(input("Valeur de b : "))
    c = float(input("Valeur de c : "))
    
    # Programme de calcul
    delta = b**2 - 4*a*c
    print(f"Le discriminant est {delta}")
    
    if delta < 0:
        print("Il n'y a pas de solution.")
    elif delta == 0:
        x = -b/(2*a)
        print("Il y a une solution : {x}.")
    else:
        x1 = (-b-sqrt(delta))/(2*a)
        x2 = (-b+sqrt(delta))/(2*a)
        print("Il y a deux solutions : {x1} et {x2}.")
    ```