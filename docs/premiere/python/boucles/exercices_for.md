## Rappels

Pensez à enregistrer vos fichiers sur le réseau afin de ne pas les perdre ! Vous pouvez aussi les stocker sur une
clef USB pour y retravailler à la maison.

Commencez par créer l'arborescence de dossiers suivants :

```
Documents/
└── NSI/
    ├── Cours
    └── Exercices/
        ├── Introduction/
        ├── Conditions/
        └── Boucles/
            ├── While/
            └── For/
                ├── exercice1.py
                ├── exercice2.py
                └── ...
```

Pensez à ajouter des commentaires dans vos fichiers lors de la programmation ! Il sera ainsi plus facile de revenir
dessus plus-tard.

## Exercice n°1 : Application directe du cours

Écrire un programme qui affiche *(comme ci-dessous)* la table de multiplication de 5, depuis **1 * 5** jusqu'à
**20 * 5**.

```
1 * 5 = 5
2 * 5 = 10
etc...
```

??? question "Une piste ?"

    Quelle est la variable entre les différentes lignes attendues ? Ce sera la variable qu'on affectera à la
    boucle `#!python for`.

??? question "Une astuce ?"

    Ne pas oublier les f-strings qui permettent de mettre en forme l'affichage...

## Exercice n°2 : Somme des entiers

L'utilisateur entre un entier naturel. Dans l'exemple d'affichage ci-dessous, l'utilisateur a entré la valeur
`10` après le message incitatif.

```
Entrer un entier positif : 10
La somme des entiers de 0 à 10 est 55
```

Écrire un programme qui calcule et affiche la somme des entiers jusqu'à l'entier `n` saisi par l'utilisateur. 

??? question "Une piste ?"

    Quelles sont les deux valeurs à stocker ?

??? question "Une autre piste ?"

    L'utilisateur saisit une valeur stockée dans la variable n. On additionne 1 avec 2 avec 3 avec ... *(jusqu'à)*
    avec n.

??? question "Un détail ?"

    Il faut retourner la valeur de la somme. Au tout début du programme, cette somme vaut forcément 0
    car l'utilisateur n'a pas encore entré la valeur de `n`. On ajoute ensuite dans la somme, au fur et à mesure,
    un nouvel entier successeur du précédent jusqu'à `n`.

## Exercice n°3 : Somme des entiers pairs

Modifier le script de l'exercice précédent pour calculer et afficher la somme des entiers pairs de 0 à `n`.

??? question "Une piste ?"

    Une unique contrainte a été ajoutée. Laquelle ?

??? question "Une autre piste ?"

    Qu'est-ce qu'un entier pair ?

    Réponse : un entier multiple du nombre 2...

??? question "Une astuce ?"

    Il faut seulement modifier la fonction `#!python range()`

## Exercice n°4 : Moyenne

Écrire un programme qui demande combien d'élèves il y a dans une classe, demande la note de chaque
élève et calcule sa moyenne.

??? question "Une piste ?"

    Il faut d'abord interroger l'utilisateur avec la méthode bien connue. Puis, il faut réutiliser ce nombre pour
    notre boucle qui va demander les notes. Enfin, on calcule la moyenne, en utilisant la somme des notes...

## Exercice n°5 : Un peu de mathématiques...

La suite $(u_n)$ est définie par $u_0 = 1$ et, pour tout entier naturel $n$, par $u_{n+1} = 2 u_n+3$.

Écrire un programme qui :

- demande à l'utilisateur un entier naturel `n`,
- puis affiche la valeur de $u_n$.

Ci-dessous, l'utilisateur a entré la valeur `10` après le message incitatif.

```
Entrer un entier positif : 10
u_10 = 4093
```

??? question "Une piste ?"

    Le *numéro* du tour de boucle ne sert pas à faire de calcul.

??? question "Une autre piste ?"

    Regarder son cours de maths pour avoir une idée de l'algorithme.