La boucle ***while*** ou **tant que** est une boucle qui va exécuter les instructions qui lui sont fournies, tant qu'une
condition donnée est respectée.

La condition doit être de type booléen ([voir le dernier cours](../conditions/booleen.md)). Lorsque cette condition
vaut `#!python True`, les instructions fournies vont s'exécuter une fois. Une fois les instructions terminées, la
condition est à nouveau évaluée. Si elle vaut désormais `#!python False`, alors la boucle *tant que* est terminée.

```mermaid
flowchart TD
    entree["Début"]
    while{"`Condition
    VRAI ?`"}
    instr["`Instruction A
    Instruction B
    ...`"]
    fin["Fin ou suite"]
    entree --> while
    while -- Oui --> instr
    while -- Non --> fin
    instr --> while
    style entree fill:#FFFFFF00, stroke:#FFFFFF00;
```

!!! abstract "Condition d'arrêt"

    La **condition d'arrêt** est la condition à respecter pour que la boucle *while* termine. Autrement dit, il s'agit
    du négatif de la condition donnée dans la boucle.

De manière générale, la boucle *while* s'utilise lorsqu'on ne connaît pas à l'avance le nombre de répétitions à
effectuer dans un programme.

Avec une telle boucle, il faut s'assurer que la condition d'exécution deviendra fausse.

!!! methode "Syntaxe"

    ```python
    while condition:
        instructionA
        instructionB
    ```

    Tout comme les exécutions conditionnelles *(if / else)*, l'indentation et les deux-points `:` sont importants !

??? question "Comment indenter ?"

    En utilisant la touche Tab (elle contient les symboles :material-keyboard-tab: et
    :material-keyboard-tab-reverse:). Pour revenir en arrière, on utilisera Shift :material-apple-keyboard-shift: et
    Tab :material-keyboard-tab: en même temps.

    On peut aussi mettre quatre espaces :material-keyboard-space:.

## Exemple

On souhaite connaître le plus petit entier $n$ tel que la somme $S=1+2+3+\text{...}+n$ soit strictement supérieure à
$100\ 000$.

On s'intéresse d'abord aux variables que va traiter notre programme. Naturellement, `n` et `S` sont les deux variables
que va utiliser notre programme.

À chaque tour de boucle, on va devoir comparer la valeur de `S` à 100000. Si elle est inférieure ou égale à 100000,
alors on augmente `n` et on recalcule `S`.

Les valeurs initiales seront donc `#!python n = 1` et `#!python S = 1` puisque $S$ commence à 1.

On souhaite donc augmenter la valeur de `n` tant que la valeur de `S` ne dépasse pas 100000.

```python
n = 1
S = 1

while S <= 100000:  # S vaut à cet instant 1 + 2 + ... + n
    n = n + 1  # On augmente de 1 la valeur de n
    S = S + n  # Comme S vaut déjà 1 + 2 + ... + (n-1), alors on ajoute simplement n
    
print(f"Le plus petit entier n tel que S=1+2+3+...+n soit strictement supérieur à 100000 est n={n}.")
```

!!! hint "Astuce"

    Pour simplifier votre code Python, vous pouvez remplacer l'instruction `#!python x = x + y` par `#!python x += y`
    pour tous types prenant en charge l'opérateur `+`. `y` n'est pas nécessairement une variable.

    Cela donne le code suivant :

    ```python
    n = 1
    S = 1
    
    while S <= 100000:
        n += 1
        S += n
        
    print(f"Le plus petit entier n tel que S=1+2+3+...+n soit strictement supérieur à 100000 est n={n}.")
    ```