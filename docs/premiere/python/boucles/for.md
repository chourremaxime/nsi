La boucle ***for*** ou **pour** est une boucle qui va exécuter les instructions qui lui sont fournies, en un nombre
donné de fois.

Il s'agit d'une version simplifiée de la boucle *while* lorsque le nombre de répétitions est connu d'avance.

Pour distinguer les différentes instructions répétées de la boucle *for*, une variable sera utilisée. Cette variable
vaudra alors plusieurs valeurs données avant le tout premier passage dans la boucle *for*.

Intéressons-nous à la syntaxe de la boucle *for* en Python pour comprendre cette variable.

!!! methode "Syntaxe"

    ```python
    for variable in sequence:
        instructionA
        instructionB
    ```

    Tout comme les exécutions conditionnelles *(if / else)*, l'indentation et les deux-points `:` sont importants !

??? question "Comment indenter ?"

    En utilisant la touche Tab (elle contient les symboles :material-keyboard-tab: et
    :material-keyboard-tab-reverse:). Pour revenir en arrière, on utilisera Shift :material-apple-keyboard-shift: et
    Tab :material-keyboard-tab: en même temps.

    On peut aussi mettre quatre espaces :material-keyboard-space:.

Mais qu'est-ce qu'une séquence ? C'est un ensemble ordonné d'éléments. On peut donc savoir combien il y a d'éléments,
et ainsi combien de fois vont se faire les instructions.

## Fonction range()

La fonction `#!python range(debut, fin, pas)` nous permet d'obtenir une séquence de nombres entiers. Pour cela, on
doit lui donner trois valeurs :

- `debut` : Valeur de début de la séquence. Si non précisé, cette valeur vaudra 0.
- `fin` : Valeur de fin de la séquence. Cette valeur sera exclue du résultat final. Elle est obligatoire !
- `pas` : Écart entre les valeurs de la séquence. Si non précisé, cette valeur vaudra 1. Il est nécessaire d'indiquer
  une valeur de début pour personnaliser l'écart.

Voici quelques exemples de séquences :

- `#!python range(4)` : `0 1 2 3`
- `#!python range(3, 8)` : `3 4 5 6 7`
- `#!python range(-5, 1)` : `-5 -4 -3 -2 -1 0`
- `#!python range(0)` : ` `
- `#!python range(0, 10, 2)` : `0 2 4 6 8`
- `#!python range(-5, 3, 3)` : `-5 -2 1`
- `#!python range(4, 0, -1)` : `4 3 2 1`
- `#!python range(6, -1, -1)` : `6 5 4 3 2 1 0`

Pour afficher une séquence avec `#!python range()` en Python, il faut écrire `#!python print(*range( ... ))`.

## Exemple

On souhaite afficher la table de 8 pour réviser les mathématiques. On souhaite afficher $1 \times 8$, $2 \times 8$, ...
jusqu'à $10 \times 8$.

La valeur qui varie ici est le premier nombre de la table de multiplication. Comme il varie de 1 à 10, on va devoir
utiliser la séquence `#!python range(1, 11)`.

Ce qui donne en Python :

```python
for i in range(1, 11):
    print(f"{i} * 8 = {i * 8}")
    # OU : print(i, "* 8 =", i * 8)
```

!!! info "Nom des variables"

    Il est d'usage de nommer la variable d'une boucle *for* en `i`. Cela peut-être réalisé lorsqu'on n'a pas de nom
    approprié pour la variable.