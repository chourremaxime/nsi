## Rappels

Pensez à enregistrer vos fichiers sur le réseau afin de ne pas les perdre ! Vous pouvez aussi les stocker sur une
clef USB pour y retravailler à la maison.

Commencez par créer l'arborescence de dossiers suivants :

```
Documents/
└── NSI/
    ├── Cours
    └── Exercices/
        ├── Introduction/
        └── Conditions/
            ├── exercice1.py
            ├── exercice2.py
            └── ...
```

Pensez à ajouter des commentaires dans vos fichiers lors de la programmation ! Il sera ainsi plus facile de revenir
dessus plus-tard.

## Exercice n°1 : Comparaisons

L'utilisateur entre deux valeurs affectées aux variables `a` et `b`. Dans l'exemple d'affichage ci-dessous,
l'utilisateur a entré `12` pour la valeur de `a` et `9` pour la valeur de `b`.

```
Entrez un nombre : 12
Entrez un autre nombre : 9
12 est plus grand que 9
```

Écrire un programme qui compare ces deux valeurs fournies par l'utilisateur puis qui indique laquelle
est plus grande que l'autre.

??? question "Une piste ?"

    Les symboles de comparaison pour les tests entre nombres en Python sont :
    
    - `x == y` : (x est-il égal à y ?) ;
    - `x != y` : (x est-il différent de y ?) ;
    - `x > y` : (x est-il supérieur à y ?) ;
    - `x < y` : (x est-il inférieur à y ?) ;
    - `x >= y` : (x est-il supérieur ou égal à y ?) ;
    - `x <= y` : (x est-il inférieur ou égal à y ?).

??? question "Un conseil ?"

    Attention à `#!python input()` qui renvoie une valeur de type `#!python str`...

??? question "Une autre piste ?"

    Deux nombres sont stockés dans deux variables. Si le 1er nombre est supérieur au 2nd alors... 

??? bug "Solution"

    ```{.python .no-copy}
    a = int(input("Entrer un nombre : "))
    b = int(input("Entrer un autre nombre : "))
    if a > b :
        print(a, "est plus grand que", b)
    elif a < b :
        print(b, "est plus grand que", a)
    else:
        print(a, "et", b, "sont égaux")
    ```

## Exercice n°2

En reprenant astucieusement l'exercice précédent, écrire un programme qui classe **dans l'ordre croissant**
la valeur de trois nombres fournis par l'utilisateur.

Ci-dessous, l'utilisateur a entré les valeurs `12`, `9` et `17` dans cet ordre. 

```
Entrez un nombre : 12
Entrez un deuxieme nombre : 9
Entrez un troisieme nombre : 17
On a 9 < 12 < 17
```

??? bug "Solution"

    ```{.python .no-copy}
    a = int(input("Entrez un nombre : "))
    b = int(input("Entrez un deuxieme nombre : "))
    c = int(input("Entrez un troisieme nombre : "))
    if a < b :
        if b < c :
            print("On a", a, "<", b, "<", c)
        elif a < c:
            print("On a", a, "<", c, "<", b)
        else:
            print("On a", c, "<", a, "<", b)
    else:
        if a < c :
            print("On a", b, "<", a, "<", c)
        elif b < c:
            print("On a", b, "<", c, "<", a)
        else:
            print("On a", c, "<", b, "<", a)
    ```

## Exercice n°3

On rappelle que les années bissextiles sont les années divisibles par 4, à l'exception de celles qui sont divisibles
par 100 sans l'être par 400. Par exemple :

- 2000 est une année bissextile (2000 est divisible par 4. 2000 est divisible par 100 mais aussi par 400 : il ne fait
  donc pas partie des exceptions).
- 2012 est divisible par 4 mais pas par 100 : 2012 est bissextile.
- 1900 est divisible par 4, par 100 mais pas par 400 : 1900 n'est pas bissextile.

Écrire un programme qui demande à l'utilisateur un entier naturel non nul puis qui affiche «annee bissextile»
ou «non bissextile» suivant les cas.

??? question "Une piste ?"

    Un entier n est divisible par un entier d si le reste de la division de n par d est nul. En langage Python,
    cela signifie que `#!python n % d` doit être égal à 0... 

??? bug "Solution"

    ```{.python .no-copy}
    n = int(input("Entrez un entier positif : "))
    if n % 4 != 0 :                                       # Pas divisible par 4
        print("L'annee", n, "n'est pas bissextile")
    elif (n % 100 == 0) and (n % 400 != 0):               # divisible par 100 et pas par 400
        print("L'annee", n, "n'est pas bissextile")
    else :
        print("L'annee", n, "est bissextile")
    ```

## Exercice n°4

Écrire un programme Python qui demande une note (un nombre entre 0 et 20) à l'utilisateur en entrée.
Si la note n'est pas entre 0 et 20, le progamme affiche un message d'erreur. Sinon, il affiche une
classification basée sur cette note.

Voici les classifications :

- Si la note est supérieure ou égale à 16, afficher "Très bien".
- Sinon, si la note est supérieure ou égale à 14, afficher "Bien".
- Sinon, si la note est supérieure ou égale à 12, afficher "Assez bien".
- Sinon, si la note est supérieure ou égale à 10, afficher "Passable".
- Sinon, afficher "Insuffisant".

??? question "Une piste ?"

    Commencer par demander à l’utilisateur d’entrer une note. On peut utiliser la fonction `#!python input()` pour cela.
    Ne pas oublier de convertir la note en un nombre entier ou réel.

??? question "Une autre piste ?"

    Il faut utiliser une série de déclarations `#!python if` et `#!python elif` pour vérifier la note et afficher
    la classification appropriée. On peut commencer par vérifier si la note est supérieure ou égale à 16,
    puis passer aux autres conditions.