Pour rappel, le booléen a été vu lors de la séance d'introduction. Il s'agit d'une donnée qui ne peut prendre
que deux valeurs : `#!python True` (vrai) ou `#!python False` (faux).

On utilise ce type de variable pour indiquer le résultat d'un test, ou pour paramétrer une fonction. Dans ce
cours, nous verrons comment l'utiliser pour tester différentes choses en Python.

!!! methode "Créer un booléen"

    Pour créer un booléen en Python (type `#!python bool`) et l'affecter à une variable, on peut écrire
    directement la valeur de ce booléen :

    ```python
    x = True
    y = False
    ```

## Opérations entre booléens

Dès lors qu'on possède plusieurs booléens, on peut être intéressé de réaliser des calculs avec. Par exemple,
savoir si on a un premier test ET un deuxième test réussis.

Les opérateurs `#!python and` et `#!python or` s'appliquent sur deux booléens, tandis que l'opérateur
`#!python not` ne s'applique qu'à un seul booléen.

!!! abstract "Opérateur and"

    L'opérateur `#!python and` donnera `#!python True` seulement si les deux booléens fournis valent également
    `#!python True`
    
    ```python
    print(True and True)  # True
    print(False and True)  # False
    ```

!!! abstract "Opérateur or"

    L'opérateur `#!python or` donnera `#!python True` si l'un des deux booléens fournis vaut `#!python True`
    
    ```python
    print(True or True)  # True
    print(False or True)  # True
    print(False or False)  # False
    ```

!!! abstract "Opérateur not"

    L'opérateur `#!python not` donnera le booléen inverse de celui fourni.
    
    ```python
    print(not True)  # False
    print(not False)  # True
    ```