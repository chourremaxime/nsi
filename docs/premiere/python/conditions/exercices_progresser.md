## Rappels

Voici des exercices supplémentaires pour progresser. Ils ne seront pas généralement corrigés en classe, mais rien
ne vous empêche de les traiter, et même de demander de l'aide à votre enseignant.

Pour rappel, pensez à enregistrer vos fichiers sur le réseau afin de ne pas les perdre ! Vous pouvez aussi les stocker
sur une clef USB pour y retravailler à la maison.

L'arborescence proposée reste identique :

```
Documents/
└── NSI/
    ├── Cours/
    └── Exercices/
        ├── Introduction/
        └── Conditions/
            ├── ...
            └── Bonus/
                ├── exercice1.py
                ├── exercice2.py
                └── ...
```

Pensez à ajouter des commentaires dans vos fichiers lors de la programmation ! Il sera ainsi plus facile de revenir
dessus plus-tard.

## Exercice n°1 : Affichage

Écrire un programme qui :

- demande un entier positif `n` à l'utilisateur,
- affiche une chaîne de caractères sur plusieurs lignes suivant le modèle ci-dessous.

Pour n pair, l'affichage sera comme suit :

```
**
****
******
```

La dernière ligne comportant `n` étoiles.

Pour n impair, l'affichage sera comme suit :

```
*****
***
*
```

La première ligne comportant `n` étoiles.

## Exercice n°2 : Nombre premier

Écrire un programme Python qui demande à l’utilisateur d’entrer un nombre et détermine si ce nombre est premier ou non.

Un nombre est dit premier s’il n’a que deux diviseurs : 1 et lui-même.

??? question "Une piste ?"

    Il faut utiliser une boucle `#!python for` pour itérer sur une plage de nombres de 2 au nombre donné.

??? question "Une autre piste ?"

    Dans chaque itération, il faut utiliser une déclaration `#!python if` pour vérifier si le nombre donné est
    divisible par le nombre actuel dans la plage.

??? bug "Solution"

    ```{.python .no-copy}
    # Demande à l'utilisateur d'entrer un nombre
    nombre = int(input("Entrez un nombre : "))
    
    # Supposons d'abord que le nombre est premier
    est_premier = True
    
    # Vérifions tous les diviseurs potentiels, de 2 au nombre donné
    for i in range(2, nombre):
        # Si le nombre est divisible par i, alors il n'est pas premier
        if nombre % i == 0:
            est_premier = False
    
    # Affiche si le nombre est premier ou non
    if est_premier:
        print(nombre, "est un nombre premier.")
    else:
        print(nombre, "n'est pas un nombre premier.")
    ```