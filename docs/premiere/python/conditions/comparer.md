Intéressons-nous aux tests. Tous les tests, une fois réalisés, auront une valeur : `#!python True` ou
`#!python False`.

## Égalité

On peut vérifier l'égalité du contenu de deux variables (leur valeur) grâce à l'opérateur `==`. Pour
vérifier que deux variables sont différentes, on utilisera l'opérateur `!=`.

!!! methode "Syntaxe"

    Soit x et y deux variables quelconques :

    ```python
    x == y # x est-il égal à y ?
    x != y # x est-il différent de y ?
    ```

## Inégalité

On peut vérifier les inégalités suivantes entre deux variables en Python :

- Supérieur `>`
- Inférieur `<`
- Supérieur ou égal `>=`
- Inférieur ou égal `<=`

!!! methode "Syntaxe"

    Soit x et y deux variables quelconques :

    ```python
    x > y  # x est-il supérieur à y ?
    x < y  # x est-il inférieur à y ?
    x >= y # x est-il supérieur ou égal à y ?
    x <= y # x est-il inférieur ou égal à y ?
    ```

## Inclusion

On peut vérifier qu'une valeur est incluse dans une autre. Pour l'instant, nous ne connaissons que les
chaînes de caractères `#!python str`. On utilisera l'opérateur `#!python in`

!!! methode "Syntaxe"

    Soit x et y deux variables de type `#!python str` :

    ```python
    x in y  # x est-il inclus dans y ?
    ```

## Exemples

```python
a = 3
b = 2.3

print(a == b)  # False
print(a != b)  # True

print(a <= b)  # False
print(type(a <= b))  # <class 'bool'>
```

```python
c = "Jean"
d = "Kévin"

print(c == d)  # False
```

```python
e = "jour"
f = "Bonjour les amis !"

print(e in f)  # True
```