Les exécutions conditionnelles permettent d'effectuer différentes actions selon certaines conditions
préalables, analysées par le programme grâce à des tests d'égalité, d'inégalité ou d'appartenance.

```mermaid
flowchart TD
    entree["Début"]
    si{"`Condition
    VRAI ?`"}
    instrA["Instruction A"]
    instrB["Instruction B"]
    fin["Fin ou suite"]
    entree --> si
    si -- Oui --> instrA
    si -- Non --> instrB
    instrA --> fin
    instrB --> fin
    style entree fill:#FFFFFF00, stroke:#FFFFFF00;
```

Ce bloc en forme de losange est appelé *Si ... Alors ... Sinon ...*

!!! methode "if / else"

    ```python
    if condition:
        instructionA
    else:
        instructionB
    # Fin ou suite
    ```
    
    Notons que :

    - `#!python condition` est un test qui donne un booléen en fin de test.
    - L'instruction `#!python else` n'est pas obligatoire
    - Après `#!python if condition` et `#!python else`, il faut mettre deux points `:`
    - Il est nécessaire d'**indenter** les instructions internes *(quatre espaces)*.

??? question "Comment indenter ?"

    En utilisant la touche Tab (elle contient les symboles :material-keyboard-tab: et
    :material-keyboard-tab-reverse:). Pour revenir en arrière, on utilisera Shift :material-apple-keyboard-shift: et
    Tab :material-keyboard-tab: en même temps.

    On peut aussi mettre quatre espaces :material-keyboard-space:.

Lorsqu'il y a plus de deux cas à envisager, on peut imbriquer les *if...else...* mais le code devient difficile à lire.

```mermaid
flowchart TD
    entree["Début"]
    si1{"`Test 1
    VRAI ?`"}
    si2{"`Test 2
    VRAI ?`"}
    instrA["Instruction A"]
    instrB["Instruction B"]
    instrC["Instruction C"]
    fin["Fin ou suite"]
    entree --> si1
    si1 -- Oui --> instrA
    si1 -- Non --> si2
    si2 -- Oui --> instrB
    si2 -- Non --> instrC
    instrA --> fin
    instrB --> fin
    instrC --> fin
    style entree fill:#FFFFFF00, stroke:#FFFFFF00;
```

Pour éviter ces imbrications, on utilisera `#!python elif` qui est la contraction de *else if*.

!!! methode "elif"

    ```python
    if condition:
        instructionA
    elif condition:
        instructionB
    else:
        instructionC
    # Fin ou suite
    ```
    
    Notons que :

    - Il peut y a voir plusieurs imbrications de `#!python elif`

Voici un exemple d'utilisation où on demande l'âge de l'utilisateur et on affiche
un message personnalisé selon son âge :

```python
age = int(input("Entrez votre âge :"))

if age < 0 or age > 130:
    print("C'est impossible !")
elif age >= 0 and age < 18:
    print("Vous êtes mineur")
else:
    print("Vous êtes majeur")
```