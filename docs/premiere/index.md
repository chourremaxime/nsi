# Première NSI

Vous êtes au bon endroit pour consulter les cours de Première NSI !

## Programme

Au programme cette année :

- [ ] Histoire de l'informatique.
    - [ ] Événements clés de l'histoire de l'informatique.
- [ ] Représentation des données : types et valeurs de base.
    - [x] Écriture d'un entier positif dans une base b ⩾ 2.
    - [x] Représentation binaire d'un entier relatif.
    - [ ] Représentation approximative des nombres réels : notion de nombre flottant.
    - [x] Valeurs booléennes : 0, 1. Opérateurs booléens : and, or, not. Expressions booléennes.
    - [ ] Représentation d’un texte en machine.
- [ ] Représentation des données : types construits
    - [ ] p-uplets.
    - [ ] Tableau indexé, tableau donné en compréhension.
    - [ ] Dictionnaires par clés et valeurs.
- [ ] Traitement de données en tables.
    - [ ] Indexation de tables.
    - [ ] Recherche dans une table.
    - [ ] Tri d’une table.
    - [ ] Fusion de tables.
- [ ] Interactions entre l’homme et la machine sur le Web.
    - [ ] Modalités de l’interaction entre l’homme et la machine.
    - [ ] Événements.
    - [ ] Interaction avec l’utilisateur dans une page Web.
    - [ ] Interaction client-serveur.
    - [ ] Requêtes HTTP, réponses du serveur.
    - [ ] Formulaire d’une page Web.
- [ ] Architectures matérielles et systèmes d’exploitation.
    - [ ] Modèle d’architecture séquentielle (von Neumann).
    - [ ] Transmission de données dans un réseau.
    - [ ] Protocoles de communication.
    - [ ] Architecture d’un réseau.
    - [ ] Systèmes d’exploitation
    - [ ] Périphériques d’entrée et de sortie.
    - [ ] Interface Homme-Machine (IHM).
- [ ] Langages et programmation.
    - [x] Constructions élémentaires.
    - [ ] Diversité et unité des langages de programmation.
    - [ ] Spécification.
    - [ ] Mise au point de programmes.
    - [ ] Utilisation de bibliothèques.
- [ ] Algorithmique.
    - [ ] Parcours séquentiel d’un tableau.
    - [ ] Tris par insertion, par sélection.
    - [ ] Algorithme des k plus proches voisins.
    - [ ] Recherche dichotomique dans un tableau trié.
    - [ ] Algorithmes gloutons.