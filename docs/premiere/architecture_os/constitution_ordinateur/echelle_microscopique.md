## Introduction

!!! quote
    Au commencement était le transistor, puis nous créâmes les portes booléennes et, à la fin de la journée,
    les ordinateurs.

Dans cette partie, nous explorerons la **structure interne** des ordinateurs à l'**échelle microscopique**. Nous
partirons du **fonctionnement du transistor**, puis nous construirons des **circuits** de base comme les **portes
logiques** NON et ET.

Ces circuits serviront à concevoir toutes les autres **fonctions booléennes**, que nous aborderons plus en détail
dans un autre cours.

## Le transistor

Comme de nombreux systèmes complexes, un ordinateur peut être décrit à différentes échelles. À son niveau le plus
fondamental, il est constitué d’un **assemblage de transistors**.

Un **transistor** est un composant électronique doté de trois connexions : le **drain**, la **source** et la **grille**.
La résistance entre le drain et la source peut être soit très grande, soit très petite, selon la tension appliquée entre
la grille et la source.

- Lorsque cette tension est **inférieure à un certain seuil**, la résistance est très grande : on dit que **le
  transistor est bloqué**.
- Lorsque la tension **dépasse ce seuil**, la résistance devient très faible : on dit que **le transistor est passant**.

!!! plus-loin "Principe de fonctionnement d'un transistor MOSFET"

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/-l6QgYoYcko" title="Visionneur vidéo YouTube" frameborder="0" allow="clipboard-write; encrypted-media; picture-in-picture" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

### Le circuit NON

![Transistor](transistor1.png){ align=right }

Avec un transistor, une résistance et un générateur dont la tension est supérieure au seuil de basculement du
transistor, on peut construire le circuit ci-contre.

Si on applique entre le point A et le point O une tension inférieure au seuil de basculement du transistor, celui-ci
est bloqué et le circuit est équivalent au circuit ci-dessous, si bien que la tension entre les points B et O est égale
à la tension d'alimentation. Elle est donc supérieure au seuil de basculement.

![Transistor](transistor2.png)

Si, en revanche, on applique entre les points A et O une tension supérieure au seuil de basculement du transistor,
celui-ci est passant et le circuit est équivalent au circuit ci-dessous, si bien que la tension entre les points B et O
est nulle. Elle est donc inférieure au seuil de basculement.

![Transistor](transistor3.png)

Si on décide qu'une tension inférieure au seuil de basculement représente le bit 0 et qu'une tension supérieure à ce
seuil représente le bit 1, les deux remarques précédentes se reformulent ainsi : si on donne au circuit le bit 0 en A,
il donne le bit 1 en B ; si on lui donne le bit 1 en A, il donne le bit 0 en B.

Autrement dit, ce circuit calcule une fonction booléenne : la **fonction NON**.

### Le circuit OU

![Transistors](transistor4.png){ align=right }

Le circuit ci-contre est construit selon les mêmes principes, mais il a deux entrées A et B.

Si on donne aux deux entrées A et B le bit 0, les deux transistors dans la partie gauche du circuit sont bloqués,
si bien que la tension entre les points C et O est égale à la tension d'alimentation, supérieure au seuil de
basculement. Le transistor de droite est donc passant et la tension entre les points D et O est nulle ; autrement dit
le point D est dans l'état 0.

Si on donne à l'une ou l'autre des entrées A et B le bit 1, au moins l'un des deux transistors dans la partie gauche
du circuit est passant, si bien que la tension entre les points C et O est nulle. Le transistor de droite est donc
bloqué et la tension entre D et O est égale à la tension d'alimentation. Le point D est par conséquent dans l'état 1.

La table de ce circuit est donc :

| A | B | D |
|---|---|---|
| 0 | 0 | 0 |
| 0 | 1 | 1 |
| 1 | 0 | 1 |
| 1 | 1 | 1 |

où l'on reconnaît la table de **fonction OU**.

!!! plus-loin "Schématisation"

On peut schématiser ces circuits de manière plus succincte en remplaçant le morceau de dessin représentant le
transistor et la résistance encadrés dans la figure de gauche par un simple rectangle (à droite) et en
remplaçant de même le morceau de dessin représentant les trois transistors et les deux résistances encadrés
dans la figure de gauche par un rectangle (à droite).

Simplification Fonction NON

![Transistor](transistor5.png){ width=300px } ![Transistor](transistor6.png){ width=300px }

Simplification Fonction OU

![Transistor](transistor7.png){ width=300px } ![Transistor](transistor8.png){ width=300px }

## Organisation générale d'un ordinateur

C'est John Von Neumann qui a élaboré en 1946 un modèle de traitement de l'information. Il est étonnamment stable
depuis et court toujours :

![Modèle de Von Neumann](von-neumann.svg)
/// caption
<a href="https://commons.wikimedia.org/wiki/File:Von_Neumann_architecture_fr.svg">Schega</a>, <a href="http://creativecommons.org/licenses/by-sa/3.0/">CC BY-SA 3.0</a>, via Wikimedia Commons
///

Les ordinateurs construits avec l’architecture de Von Neumann sont constitués de quatre composants :

1. l’**unité arithmétique et logique** (UAL) ou unité de traitement, qui effectue les opérations de base ;
2. l’**unité de contrôle**, qui est chargée du séquençage des opérations ;
3. la **mémoire**, qui contient à la fois les données et le programme qui indique à l’unité de contrôle quels calculs
   faire sur ces données. La mémoire se divise en mémoire vive (programmes et données en cours de fonctionnement) et
   mémoire de masse (programmes et données de base de la machine) ;
4. les dispositifs **d’entrées-sorties**, qui permettent de communiquer avec le monde extérieur.