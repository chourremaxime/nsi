## Origine du mot _Ordinateur_

Un ordinateur est une machine composée de **circuits électroniques**, capable de manipuler des données sous **forme
binaire**, c'est-à-dire en **bits**.

![Jacques Perret](jacques_perret.jpg){ align=right }

Le terme **_ordinateur_** a été introduit par **IBM France**. En 1955, François Girard, responsable du service promotion
d'IBM France, souhaitait remplacer le terme _calculateur_, traduction littérale du mot anglais _computer_. Il consulta
alors Jacques Perret, agrégé de lettres et professeur de philologie latine à la Sorbonne.

Le 16 avril 1955, Jacques Perret proposa le mot _ordinateur_. Il expliqua qu'il s'inspirait d’un adjectif du
dictionnaire Littré signifiant « celui qui met de l’ordre », une **référence aux dieux** organisant le monde dans la
mythologie. Ce concept de mise en ordre correspondait parfaitement aux fonctions d’un ordinateur.

![Courrier](courrier.png)
/// caption
Courrier de Jacquet Perret à François Girard
///

## Les types d'ordinateurs

Toute machine capable de **traiter des informations sous forme binaire** peut être qualifiée d'ordinateur. Cependant,
le terme _ordinateur_ est souvent associé à l'ordinateur personnel _(PC, pour Personal Computer)_, qui est le type
d'ordinateur le plus répandu sur le marché. Pourtant, il existe de nombreux autres types d'ordinateurs.

Ce cours, bien qu’il adopte une approche générale, se concentre principalement sur les ordinateurs de type PC. Ces
ordinateurs, également appelés _compatibles IBM_, tirent leur nom de l’entreprise IBM, qui **a conçu les premiers
modèles** de ce type et a dominé le marché jusqu'en 1987. À cette époque, IBM contrôlait les **standards techniques**,
que d'autres fabricants adoptaient pour leurs propres produits.