Cette technologie est basée sur un écran composé de deux plaques transparentes entre lesquelles il y a une fine
couche de liquide dans laquelle il y a des molécules (cristaux) qui ont la propriété de s'orienter lorsqu'elles 
sont soumises à du courant électrique. L'avantage majeur de ce type d'écran est son encombrement réduit, d'où son
utilisation sur les ordinateurs portables et les écrans plats.

![Ecran](ecrans_lcd_ubuntu.png)