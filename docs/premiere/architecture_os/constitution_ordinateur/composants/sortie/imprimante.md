À jet d'encre ou laser, ce sont les deux technologies les plus utilisées actuellement. Elles peuvent être USB, 
réseau RJ45 ou Wifi.

![Imprimante laser](imprimante.png)

## Technologie à jet d'encre et à bulles d'encre

La technologie des **imprimantes à jet d'encre** _(Bubble jet printers)_ a été originalement inventée par Canon,
elle repose sur le principe qu'un fluide chauffé produit des bulles.

Le chercheur qui a découvert ce principe avait mis accidentellement en contact une seringue remplie d'encre et
un fer à souder, cela créa une bulle dans la seringue qui fit jaillir de l'encre de la seringue. Les têtes des
imprimantes actuelles sont composées de nombreuses buses _(jusqu'à 256)_, équivalentes à plusieurs seringues, 
chauffées entre 300 et 400°C plusieurs fois par seconde. Chaque buse produit une bulle minuscule qui fait
s'éjecter une gouttelette extrêmement fine. Le vide engendré par la baisse de pression aspire une nouvelle goutte.

![Bubble](bubble.png)

On distingue généralement deux technologies :

1. Les imprimantes à jet d'encre, utilisant des buses possédant leur propre élément chauffant intégré. La
   technologie utilisée est ainsi thermique.
2. Les imprimantes à bulles d'encre, utilisant des buses possédant une technologie piézo-électrique. Chaque
   buse est associé à un quartz piézo-électrique, qui, excité sur sa fréquence de résonance, se déforme et éjecte 
   la goutte d'encre.

## Technologie laser

L'**imprimante laser** permet d'obtenir des tirages papier de qualité, à faible coût et avec une vitesse d'impression
élevée. Autrefois réservées aux usages professionnels ou semi-professionnels, ces imprimantes ont connu une baisse
significative des prix les faisant, de nos jours, rentrer dans les foyers.

L'imprimante laser utilise une technologie proche de celle utilisée dans les photocopieurs. Une imprimante laser est
ainsi principalement constituée d'un tambour photosensible _(en anglais drum)_ qui, chargé électro-statiquement, est
capable d'attirer l'encre afin de former un motif qui sera déposé sur la feuille de papier.

Le fonctionnement global est le suivant : un ioniseur de papier charge les feuilles positivement. Le laser charge le 
tambour positivement en certains points grâce à un miroir pivotant. Ainsi, l'encre sous forme de poudre _(toner)_, 
chargée négativement, se dépose sur les parties du toner ayant été préalablement chargées par le laser. En tournant, 
le tambour dépose l'encre sur le papier. Un fil chauffant (appelé coronaire) permet enfin de fixer l'encre sur le
papier.

![Laser](laser.png)

Ainsi, l'imprimante laser, n'ayant pas de tête mécanique, est rapide et peu bruyante. On distingue en fait deux
technologies pour les imprimantes laser : carrousel _(quatre passages)_ ou tandem _(monopasse)_.

1. **carrousel** : Avec la technologie carrousel, l'imprimante effectue quatre passages pour imprimer un document
   (un par couleur primaire et un pour le noir, ce qui fait que l'impression est en théorie quatre fois moins rapide 
   en couleur qu'en noir).
2. **tandem** : Une imprimante laser exploitant la technologie tandem dépose chaque couleur en un seul passage,
   les toners étant disposés parallèlement. Les sorties sont aussi rapides en noir qu'en couleur. Cette technologie
   a toutefois un prix de revient plus élevé, la mécanique étant plus complexe. Elle est donc réservée en principe 
   aux imprimantes laser couleur de milieu ou de haut de gamme.