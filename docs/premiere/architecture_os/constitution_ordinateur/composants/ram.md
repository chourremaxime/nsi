![RAM](ram.jpg){ align=right }

La **mémoire vive** _(RAM pour Random Access Memory)_ permet de **stocker des informations** pendant tout le **temps de
fonctionnement** de l'ordinateur, son contenu est par contre **détruit dès lors que l'ordinateur est éteint ou
redémarré**, contrairement à une mémoire de masse telle que le disque dur, capable de garder les informations même
lorsqu'il est hors tension. On parle de **volatilité** pour désigner ce phénomène.

Pourquoi alors utiliser de la mémoire vive même si les disques durs reviennent moins chers à capacité égale ? La réponse
est que **la mémoire vive est extrêmement rapide** par comparaison aux périphériques de stockage de masse tels que le
disque dur. Elle possède en effet un **temps de réponse de l'ordre de quelques dizaines de nanosecondes** _(environ 70
pour la DRAM, 60 pour la RAM EDO, et 10 pour la SDRAM voire 6 ns sur les SDRam DDR)_ contre quelques millisecondes pour
le disque dur.

La mémoire vive se présente sous la forme de **barrettes** qui se branchent sur les connecteurs de la carte mère.

![Slot](slot_ram.png){ align=left }

Elle se positionne dans des slots _(fentes)_ prévues à cet effet. Il n'est pas rare que les mémoires s'installent
2 par 2, il faut pour cela lire la documentation de la carte mère.