![HDD](disque_dur_inter.jpg){ align=right }

Le **disque dur** est l'organe de l'ordinateur servant à **conserver les données de manière permanente**,
contrairement à la mémoire vive, qui s'efface à chaque redémarrage de l'ordinateur, c'est la raison pour laquelle on
parle parfois de **mémoire de masse** pour désigner les disques durs. Le disque dur est relié à la carte-mère par
l'intermédiaire d'un contrôleur de disque dur faisant l'interface entre le processeur et le disque dur.

Le contrôleur de disque dur gère les disques qui lui sont reliés, interprète les commandes envoyées par le processeur
et les achemine au disque concerné. On distingue généralement les interfaces suivantes :

- IDE
- SCSI
- Serial ATA

Avec l'apparition de la norme USB, des boîtiers externes permettant de connecter un disque dur sur un port USB ont
fait leur apparition, rendant le disque dur facile à installer et permettant de rajouter de la capacité de stockage
pour faire des sauvegardes. On parle ainsi de disque dur externe par opposition aux disques durs internes branchés
directement sur la carte mère, mais il s'agit bien des mêmes disques, si ce n'est qu'ils sont connectés à l'ordinateur
par l'intermédiaire d'un boîtier branché sur un port USB.

## Structure

![Vue HDD](disque_dur_vue.png){ align=right }

Un disque dur est constitué non pas d'un seul disque, mais de **plusieurs disques rigides** 
_(en anglais "hard disk" signifie disque dur)_ en métal, en verre ou en céramique, **empilés** à une très faible
distance les uns des autres et appelés plateaux _(en anglais "platters")_.

Les disques tournent très rapidement autour d'un axe _(à plusieurs milliers de tours par minute actuellement 7200 )_
dans le sens inverse des aiguilles d'une montre. Un ordinateur fonctionne de manière binaire, c'est-à-dire que les
données sont stockées sous forme de 0 et de 1 _(appelés bits)_. Il existe sur les disques durs des millions de ces bits,
stockés très proches les uns des autres sur une fine couche magnétique de quelques microns d'épaisseur, elle-même
recouverte d'un film protecteur.

![HDD Cylindres](disque_dur_cylindre.png){ align=left }

La lecture et l'écriture se font grâce à des **têtes de lecture** _(en anglais "heads")_ situées de part et d'autre de
chacun des plateaux. Ces têtes sont des **électro-aimants** qui se baissent et se soulèvent pour pouvoir lire 
l'information ou l'écrire. Les têtes ne sont qu'à quelques microns de la surface, séparées par une couche d'air 
provoquée par la rotation des disques qui crée un vent d'environ 250 km/h ! De plus ces têtes sont mobiles latéralement
afin de pouvoir balayer l'ensemble de la surface du disque.

Cependant, les têtes sont liées entre elles et une tête seulement peut lire ou écrire à un moment donné. On parle donc 
de **cylindre** pour désigner l'ensemble des données stockées verticalement sur la totalité des disques.
L'ensemble de cette mécanique de précision est contenue dans un boîtier totalement hermétique, car la moindre 
particule peut détériorer la surface du disque. Vous pouvez ainsi voir sur un disque des opercules permettant 
l'étanchéité, et la mention _Warranty void if removed_ qui signifie littéralement _la garantie expire si retiré_ 
car seuls les constructeurs de disques durs peuvent les ouvrir _(dans des salles blanches, exemptes de particules)_.

## Fonctionnement

![HDD Piste](disque_dur_piste.png){ align=right }

Les têtes de lecture/écriture sont dites _inductives_, c'est-à-dire qu'elles sont capables de générer un **champ 
magnétique**. C'est notamment le cas lors de l'écriture : les têtes, en créant des champs positifs ou négatifs,
viennent polariser la surface du disque en une très petite zone, ce qui se traduira lors du passage en lecture par 
des changements de polarité induisant un courant dans la tête de lecture, qui sera ensuite transformé par un 
convertisseur analogique numérique _(CAN)_ en 0 et en 1 compréhensibles par l'ordinateur.

![HDD Secteurs](disque_dur_secteur.png){ align=left }

Les têtes commencent à inscrire des données à la périphérie du disque (piste 0), puis avancent vers le centre. 
Les données sont organisées en cercles concentriques appelés « pistes », créées par le formatage de bas niveau.
Les pistes sont séparées en quartiers _(entre deux rayons)_ que l'on appelle secteurs, contenant les données _(au 
minimum 512 octets par secteur en général)_.

On appelle **cylindre** l'ensemble des données situées sur une même piste sur des plateaux différents _(c'est-à-dire 
à la verticale les unes des autres)_ car cela forme dans l'espace un "cylindre" de données.
Cylindre de données

![Cyclindre de données](disque_dur_cylindr.png)

On appelle enfin **cluster** _(ou en français unité d'allocation)_ la zone minimale que peut occuper un fichier
sur le disque.

En effet, le système d'exploitation exploite des blocs qui sont en fait plusieurs secteurs (entre 1 et 16 secteurs). 
Un fichier minuscule devra donc occuper plusieurs secteurs (un cluster), ce qui provoque des espaces perdus qui devront
être réorganisés par un _nettoyage logiciel_ des disques durs. Sur les anciens disques durs, l'adressage se faisait 
ainsi de manière physique en définissant la position de la donnée par les coordonnées cylindre / tête / secteur 
_(en anglais CHS pour Cylinder / Head / Sector)_.

!!! plus-loin "Une vidéo pour tout comprendre !"
    
    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/wteUW2sL7bc?si=Vj29UsKUmqiV8VUo" title="YouTube video player" frameborder="0" allow="clipboard-write; encrypted-media; picture-in-picture" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

!!! plus-loin "SSD : le remplaçant !"
    Un SSD, de l'anglais _solid-state drive_ _(« disque électronique » au Québec)_, est un matériel informatique
    permettant le stockage de données sur de la mémoire flash.

    La mémoire flash est une mémoire de masse à semi-conducteurs ré-inscriptible, c'est-à-dire une mémoire possédant 
    les caractéristiques d'une mémoire vive mais dont les données ne disparaissent pas lors d'une mise hors tension.
    Ainsi, la mémoire flash stocke les bits de données dans des cellules de mémoire, mais les données sont conservées
    en mémoire lorsque l'alimentation électrique est coupée.

    Le SSD est bien plus solide qu'un disque dur, et surtout il surpasse les performances d'un disque classique, 
    sans compter qu'il ne chauffe pas et qu'il est silencieux !

    Les SSD sont majoritairement branchés sur le port SATA III qui bride les performances autour de 600 Mo/s. 
    Ainsi est né la technologie NVMe _(Non Volatile Memory Express)_ qui peut être branché sur le port PCI Express, 
    déjà utilisé pour les cartes graphiques. Le NVMe permet d'accéder à la mémoire flash du SSD avec des temps d'accès
    plus faibles et des performances en lecture / écriture plus hautes.

    Sur le tableau ci-dessous, on recence les dernières technologies avec les moyens de connexion, le protocole 
    utilisé, la technologie mais aussi la forme du SSD (2,5 pouces ou M2) :

    ![Types de SSD](types_ssd.png)