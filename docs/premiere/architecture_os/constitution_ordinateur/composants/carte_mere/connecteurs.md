![Carte fille](carte_fille.png){ align=right }

Les **connecteurs d'extension** _(en anglais slots)_ sont des réceptacles généralement situés sur la carte mère,
pouvant accueillir des **cartes d'extension** _(ces cartes sont utilisées pour ajouter des fonctionnalités ou
augmenter la performance d'un micro-ordinateur, par exemple une carte graphique peut être ajoutée à un ordinateur
pour améliorer les performances de l'affichage 3D sur l'écran)_.

Ces ports peuvent être des **ports ISA** _(vieille interface)_, **PCI** _(Peripheral Component Interconnect)_, **AGP**
ou, plus récent, le **PCI Express**, qui existe sous forme de PCI-e x1, x4, x8, et x16, ce port étant souvent occupé
par une **carte graphique**.

On appelle ces cartes d'extension des _cartes filles_.

La carte mère possède un certain nombre de connecteurs d'**entrées-sorties** regroupés sur le _panneau arrière_.
Il existe plusieurs sortes de connecteurs :

- Les **ports USB** _(Universal Serial Bus)_ pour la connexion de périphériques récents : USB 2, 3 et à présent USB
  Type C.
- Les **ports séries** pour la connexion de vieux périphériques _(plutôt utilisés à présent dans l'industrie)_.
- Les **ports parallèles** pour la connexion de vieilles imprimantes, disparus depuis 2010.
- Les **connecteurs RJ45** pour la connexion filaire à un réseau informatique.
- Les **connecteurs vidéo analogique VGA et numérique DVI**, pour la connexion d'un écran d'ordinateur, remplacés à
  présent par le **HDMI** et **DisplayPort**.
- Les **connecteurs audio analogiques** _(jack 3.5 mm)_ **et audio numériques** _(SPDIF)_, pour la connexion
  d'appareils audio comme des haut-parleurs ou un microphone, identifiés par un code couleur.
- Les **connecteurs audio/video HDMI ou DisplayPort** pour la connexion avec un téléviseur HD, supportant la
  protection des contenus numériques haute définition _(HDCP)_.
- Les **connecteurs Parallel ATA** ou Serial ATA I _(1,5 Gb/s)_ ou II _(3 Gb/s)_, voire III _(6 Gb/s)_ pour la
  connexion de périphériques de stockage comme les disques durs, Solid-state drive et disques optiques.
- Les **connecteurs e-Sata** _(en connectique externe)_ pour la connexion de périphériques de stockage externe à
  haut débit
- Les **connecteurs FireWire IEEE 1394**...

![Connecteurs](connecteurs.png)