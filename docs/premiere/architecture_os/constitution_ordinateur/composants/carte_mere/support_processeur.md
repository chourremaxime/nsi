![Socket](socket.png){ align=right }

La carte mère possède un **emplacement** _(parfois plusieurs dans le cas de cartes mères multi-processeurs)_ **pour
accueillir le processeur**, appelé **support de processeur**. On distingue deux catégories de supports :

1. Slot (en français fente) : il s'agit d'un **connecteur rectangulaire** dans lequel on enfiche le processeur
   **verticalement**.
2. Socket (en français embase) : il s'agit d'un **connecteur carré** possédant un grand nombre de **petits
   connecteurs** sur lequel le processeur vient directement s'enficher.

!!! note "Remarque"
   Au sein de ces deux grandes familles, il existe différentes versions du support, selon le type de processeur. Il est
   essentiel, quel que soit le support, de **brancher délicatement le processeur** afin de ne tordre aucune de ses
   broches _(il en compte plusieurs centaines)_.

   Afin de faciliter son insertion, un support appelé **ZIF** _(Zero Insertion Force, traduisez force d'insertion
   nulle)_ a été créé. Les supports ZIF possèdent une petite manette, qui, lorsqu'elle est levée, permet l'insertion du
   processeur sans aucune pression et, lorsqu'elle est rabaissée, maintient le processeur sur son support.
   
   Le processeur possède généralement un **détrompeur**, matérialisé par un coin tronqué ou une marque de couleur,
   devant être aligné avec la marque correspondante sur le support.