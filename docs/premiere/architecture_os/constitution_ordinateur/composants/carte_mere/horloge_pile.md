![Exemples de quartz](quartz_1.png){ align=right }

L'**horloge temps réel** _(notée RTC, pour Real Time Clock)_ est un circuit chargé de la **synchronisation des signaux
du système**. Elle est constituée d'un cristal, appelé aussi _quartz_ qui, en vibrant, donne des impulsions _(appelés
tops d'horloge)_ afin de cadencer le système. On appelle **fréquence de l'horloge** _(exprimée en MHz)_ le nombre de
vibrations du cristal par seconde, c'est-à-dire le nombre de tops d'horloge émis par seconde. Plus la fréquence est
élevée, plus le système peut traiter d'informations.

Lorsque l'ordinateur est mis hors tension, l'alimentation cesse de fournir du courant à la carte mère. Or, lorsque
l'ordinateur est rebranché, **le système est toujours à l'heure**. Un circuit électronique, appelé circuit **CMOS**
_(Complementary Metal-Oxyde Semiconductor, parfois appelé BIOS CMOS)_, **conserve en effet certaines informations** sur
le système, telles que l'heure, la date système et quelques paramètres essentiels du système.

Le circuit CMOS est **continuellement alimenté par une pile** _(au format pile bouton)_ ou une batterie située sur la
carte mère. Ainsi, les informations sur le matériel installé dans l'ordinateur _(comme le nombre de pistes, de secteurs
de chaque disque dur)_ sont **conservées dans le CMOS**. Dans la mesure où le CMOS est une mémoire lente, certains
systèmes recopient parfois le contenu du CMOS dans la RAM (mémoire rapide), le terme de _memory shadow_ est employé pour
décrire ce processus de copie en mémoire vive.

!!! note "Bon à savoir !"
    Lorsque l'heure du système est régulièrement réinitialisée, ou que l'horloge prend du retard, il suffit généralement
    d'en changer la pile !