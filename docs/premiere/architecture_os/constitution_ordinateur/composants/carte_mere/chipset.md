![Chipset AMD](chipset_amd.png){ align=right }

Le chipset _(traduisez jeu de composants, jeu de circuits ou jeu de puces)_ est un circuit électronique chargé de
**coordonner les échanges de données** entre les divers composants de l'ordinateur (processeur, mémoire...). Dans la
mesure où le chipset est intégré à la carte mère, il est important de choisir une carte mère intégrant un chipset récent
afin de maximiser les possibilités d'évolutivité de l'ordinateur.

![Chipset Intel](chipset_intel.png){ align=left }

Les chipsets des cartes mères actuelles **intègrent généralement une puce graphique** et **presque toujours une puce
audio**, ce qui signifie qu'il n'est pas nécessaire d'installer une carte graphique ou une carte son additionnelle. Ces
cartes intégrées sont maintenant **presque toujours de bonne qualité** et il n'est en général pas nécessaire de les
désactiver dans le BIOS, même si on veut installer des cartes d'extension de meilleure qualité dans les emplacements
prévus à cet effet ; le simple fait de mettre en place une carte d'extension est souvent suffisant pour désactiver la
carte intégrée.