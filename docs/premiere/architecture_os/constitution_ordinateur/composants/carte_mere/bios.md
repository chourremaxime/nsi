![Exemple BIOS](bios.png){ align=right }

Le **BIOS** _(Basic Input/Output System)_ est le programme basique servant d'**interface entre le système
d'exploitation et la carte mère**. Le BIOS est stocké dans une **ROM** _(mémoire morte, c'est-à-dire une mémoire en
lecture seule)_, ainsi, il utilise les données contenues dans le CMOS pour connaître la configuration matérielle du
système.

Il est possible de **configurer le BIOS grâce à une interface** _(nommée BIOS setup, traduisez configuration du BIOS)_
accessible au démarrage de l'ordinateur par simple pression d'une touche (généralement la touche Suppr, Echap, F2...).

En réalité le setup du BIOS sert uniquement d'**interface pour la configuration**, les données sont stockées dans
le CMOS.

Le BIOS est à présent remplacé par le standard **UEFI**, signifiant en français _Interface micrologicielle extensible
unifiée_ définit une **interface entre le micrologiciel** _(firmware)_ **et le système d'exploitation** _(OS)_ d'un
ordinateur.

L'UEFI fait suite à l'**EFI** _(Extensible Firmware Interface)_, conçue par Intel pour les processeurs Itanium, dont
voici le fonctionnement synthétique :

![Fonctionnement EFI](fonctionnement_efi.png)

Plus d'informations sur la [page Wikipédia UEFI](https://fr.wikipedia.org/wiki/UEFI).