![Exemple de carte mère](carte_mere.png){ align=right }

L'élément constitutif principal de l'ordinateur est la **carte mère** _(en anglais mainboard ou motherboard)_. La carte
mère est le socle permettant la **connexion de l'ensemble des éléments** essentiels de l'ordinateur. Elle supporte le
**processeur**, centre névralgique du système. C'est lui qui effectue tous les **traitements d'informations**.

Comme son nom l'indique, la carte mère est une **carte maîtresse**, prenant la forme d'un grand circuit imprimé
possédant notamment des connecteurs pour les cartes d'extension, les barrettes de mémoires, le processeur, etc.