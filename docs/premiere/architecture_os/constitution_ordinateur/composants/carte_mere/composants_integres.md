La carte mère contient un certain nombre d'éléments embarqués, c'est-à-dire intégrés sur son circuit imprimé :

- Le support de processeur et le processeur
- Le chipset, circuit qui contrôle la majorité des ressources (interface de bus du processeur, mémoire cache et mémoire
  vive, slots d'extension…),
- L'horloge et la pile du CMOS,
- Le BIOS,
- Le bus système et les bus d'extension,
- Les mémoires RAM

En outre, les cartes mères récentes embarquent généralement un certain nombre de périphériques multimédia et réseau
pouvant être désactivés :

- carte réseau intégrée ;
- carte graphique intégrée ;
- carte son intégrée ;
- contrôleurs de disques durs évolués...

![Carte mère vue de dessus](carte_mere_dessus.png)
/// caption
Carte mère vue de dessus
///