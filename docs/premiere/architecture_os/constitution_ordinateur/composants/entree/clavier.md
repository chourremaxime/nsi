![Clavier](clavier.png){ align=right }

Le **clavier** _(en anglais keyboard)_ permet, à la manière des machines à écrire, de **saisir des caractères**
_(lettres, chiffres, symboles ...)_, il s'agit donc d'un périphérique d'entrée essentiel pour l'ordinateur, car c'est
grâce à lui qu'il nous est possible d'**envoyer des commandes**.

## Types de clavier

Il existe 2 grands types de claviers :

- **Azerty**<br>Le terme azerty _(en rapport avec les 6 premières touches alphabétiques du clavier)_ désigne un type
  de clavier, équipant la quasi-totalité des ordinateurs en France et en Belgique. Il s'agit de la déclinaison pour 
  les pays francophones du clavier QWERTY.
- **Qwerty**<br>Le clavier qwerty a été conçu en 1868 à Milwaukee par Christopher Latham Sholes en répartissant aux
  opposées du clavier les touches correspondant aux paires de lettres les plus utilisées dans la langue anglaise 
  afin d'empêcher les tiges _(portant les caractères)_ des machines à écrire de l'époque de se croiser et de se
  coincer. Ce clavier a été vendu à l'entreprise Remington en 1873.<br>Le clavier Qwerty _(et par extension le
  clavier Azerty)_ a donc été conçu dans une optique purement technique, à l'encontre de l'ergonomie et de
  l'efficacité. La légende veut que la disposition des touches sur la première ligne du clavier Qwerty a été
  motivée par les vendeurs de machines à écrire de l'époque de telle manière à ce que toutes les touches nécessaires
  à l'écriture de typewriter _(machine à écrire en anglais)_ s'y trouvent lorsqu'ils faisaient des démonstrations !

!!! plus-loin "Le clavier BEPO"
    Le bépo est une disposition de symboles et de caractères conçue pour les claviers d'ordinateur afin de 
    faciliter la saisie du français, des éléments typographiques, sans oublier les symboles de programmation. 
    Elle donne accès à de nombreux caractères d'autres langues.

    Plus d'informations sur les nombreux avantages pour nous les français :
    [https://bepo.fr/wiki/Pr%C3%A9sentation](https://bepo.fr/wiki/Pr%C3%A9sentation)

## Fonctionnement d'un clavier

À chaque pression d'une touche du clavier, un signal spécifique est transmis à l'ordinateur. Le clavier utilise
en effet un réseau matriciel permettant d'identifier chaque touche grâce à une ligne et une colonne. Lorsqu'une 
touche est pressée, un contact électrique s'établit entre la ligne et la colonne. Les signaux électriques sont 
transmis à un micro-contrôleur, qui envoie un code _(BCD, ASCII ou Unicode)_ à l'ordinateur décrivant le caractère 
correspondant à la touche.