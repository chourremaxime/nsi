Il existe plusieurs types de **souris**, classifiés selon la technologie de positionnement d'une part, selon la
transmission des données à l'unité centrale d'autre part.

On distingue ainsi plusieurs grandes familles de souris :

- Les **souris mécaniques**, dont le fonctionnement est basé sur une boule (en plastique ou en caoutchouc) encastrée
  dans un châssis (en plastique) transmettant le mouvement à deux rouleaux.
- Les **souris opto-mécaniques**, dont le fonctionnement est similaire à celui des souris mécaniques, si ce n'est que 
  le mouvement de la boule est détecté par des capteurs optiques.
- Les **souris optiques**, capables de déterminer le mouvement par analyse visuelle de la surface sur laquelle elles 
  glissent.

## Souris mécanique

La **souris mécanique** comporte une **bille** sur laquelle **tournent deux rouleaux**. Ces rouleaux comportent chacun un
**disque cranté** qui tourne entre une **photodiode** et une LED _(Diode électroluminescente)_ laissant passer la
lumière par séquence. Lorsque la lumière passe, la photodiode renvoie un bit (1), lorsqu'elle rencontre un obstacle, la 
photodiode renvoie un bit nul (0). À l'aide de ces informations, l'ordinateur peut connaître la position du curseur, 
voire sa vitesse.

![Souris à boule](souris_boule.gif)

!!! hint "Astuce..."
    À force de l'utiliser, de la poussière se dépose sur les rouleaux de la souris, empêchant celle-ci de tourner 
    correctement et provoquant des réactions curieuses de la part du curseur. Pour y remédier, il suffit d'ouvrir 
    la cage contenant la bille et de nettoyer les rouleaux (avec une brosse à dents de récupération par exemple).

    À noter que ces souris ont totalement disparu du marché ! Mais reste intéressante pour leur fonctionnement.

## Souris optique

La **souris optique** possède un fonctionnement basé sur l'**analyse de la surface** sur laquelle elle se déplace.
Ainsi une souris optique est constituée d'une LED, d'un **système d'acquisition d'images** _(IAS)_ et d'un **processeur 
de signaux numériques** _(DSP)_.

La LED est chargée d'éclairer la surface afin de permettre au système IAS d'acquérir l'image de la surface. Le DSP,
par analyse des caractéristiques microscopiques de la surface, détermine le mouvement horizontal et vertical.

Les souris optiques fonctionnent sur toutes surfaces non parfaitement lisses ou bien possédant des dégradés de couleur. 
Les avantages principaux de ce type de dispositif de pointage par rapport aux souris mécaniques sont notamment une
**précision accrue** ainsi qu'un salissement moindre.

## Souris sans-fil

Les **souris sans fil** (en anglais cordless mouse) sont les plus courantes, car elles peuvent être utilisées sans 
être physiquement reliées à l'ordinateur, ce qui procure une sensation de liberté.

Il existe également plusieurs catégories de souris sans-fil, selon la technologie utilisée :

- **souris infrarouges** _(en anglais IR pour infrared)_ : ces souris sont utilisées en vis-à-vis avec un récepteur 
  infrarouge connecté à l'ordinateur. La portée de ce type de dispositif est de quelques mètres au plus, en vision 
  directe, au même titre que la télécommande d'un téléviseur.
- **souris hertzienne** : ces souris sont utilisées avec un récepteur hertzien, généralement propriétaire au 
  constructeur. La portée de ce type de dispositif est d'une dizaine de mètres au plus, sans nécessairement avoir 
  une ligne visuelle avec l'ordinateur. Ce type de dispositif peut notamment être pratique pour les personnes 
  connectant leur ordinateur à leur téléviseur, situé dans une autre pièce.
- **souris Bluetooth** : ces souris sont utilisées avec un récepteur Bluetooth connecté à l'ordinateur. La portée de 
  ce type de dispositif est équivalente aux technologies hertziennes propriétaires.