![Son PCI](carte_son_pci.png){ align=right }

La **carte son interne ou externe** _(en anglais audio card ou sound card)_ est l'élément de l'ordinateur permettant
de gérer les entrées-sorties sonores de l'ordinateur.

Il s'agit généralement d'un contrôleur pouvant s'insérer dans un emplacement PCI mais de plus en plus de **cartes
mères possèdent une carte son intégrée**.

Les cartes son additionnelles sont généralement utilisées par les professionnels du son !

![Carte son](carte_son.jpg){ width=300px } ![Carte son](263854.jpg){ width=300px }

## Éléments d'une carte son

- Le **processeur** spécialisé, appelé DSP _(digital signal processor)_ chargé de tous les traitements numériques du 
  son _(écho, réverbération, vibrato chorus, tremolo, effets 3D, etc.)_ ;
- Le **convertisseur digital / analogique** appelé DAC _(digital to analog converter)_ permettant de convertir les 
  données audio de l'ordinateur en signal analogique vers un système de restitution sonore _(enceintes, 
  amplificateur, etc.)_ ;
- Le **convertisseur analogique / numérique** appelé ADC

## Les connecteurs

**Les connecteurs d'entrées-sorties externes** :

- Une ou deux sorties ligne au format jack standard 3.5 mm _(notée Line Out ou bien Speaker output ou SPK, signifiant 
  « hauts parleurs » en anglais)_, habituellement de couleur vert clair ;
- Une entrée ligne _(Line in)_ ;
- Une entrée microphone _(notée parfois Mic)_, généralement au format jack 3.5 mm et de couleur rose ;
- Une sortie numérique SPDIF _(Sony Philips Digital Interface)_, noté également S/PDIF ou S-PDIF
- Une interface MIDI, généralement de couleur or _(ocre)_ permettant de connecter des instruments de musique et 
  pouvant faire office de port de jeu

**Les connecteurs d'entrées-sorties internes** :

- Connecteur CD-ROM / DVD-ROM, possédant un connecteur noir, permettant de connecter la carte son à la sortie audio 
  analogique du CD-ROM à l'aide d'un câble CD Audio ;
- Entrée auxiliaire _(AUX-In)_ possédant un connecteur blanc, permettant de connecter des sources audio internes telles
  qu'une carte tuner TV ;
- Connecteur pour répondeur téléphonique _(TAD, Telephone Answering Devices)_ possédant un connecteur vert ;