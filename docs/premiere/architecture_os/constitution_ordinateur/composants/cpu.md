## Présentation

Il est **à la base de tous les calculs**, c'est le _cerveau_ de l'ordinateur.

Le processeur _(CPU, pour Central Processing Unit, soit Unité Centrale de Traitement)_ est le cerveau de l'ordinateur.
Il permet de **manipuler des informations numériques**, c'est-à-dire des informations codées sous forme binaire,
et d'**exécuter les instructions** stockées en mémoire.

Le premier microprocesseur _(Intel 4004)_ a été inventé en 1971. Il s'agissait d'une unité de calcul de 4 bits, cadencé
à 108 kHz. Depuis, la puissance des microprocesseurs a augmenté exponentiellement.

Quels sont donc ces petits morceaux de silicium qui dirigent nos ordinateurs ?

## Fonctionnement

Le processeur est un **circuit électronique** cadencé au rythme d'une horloge interne, grâce à un cristal de
[quartz](carte_mere/horloge_pile.md) qui, soumis à un courant électrique, envoie des impulsions, appelées _top_.

La **fréquence d'horloge** _(appelée également cycle, correspondant au nombre d'impulsions par seconde)_ s'exprime en
Hertz _(Hz)_. Ainsi, un ordinateur à 200 MHz possède une horloge envoyant 200 000 000 de battements par seconde. La
fréquence d'horloge est généralement un multiple de la fréquence du système (FSB, Front-Side Bus), c'est-à-dire un
multiple de la fréquence de la carte mère.

**À chaque top d'horloge**, le processeur exécute une action, correspondant à une instruction ou une partie
d'instruction. L'indicateur appelé **CPI** _(Cycles Par Instruction)_ permet de représenter le nombre moyen de cycles
d'horloge nécessaire à l'exécution d'une instruction sur un microprocesseur. La puissance du processeur peut ainsi
être caractérisée par le **nombre d'instructions qu'il est capable de traiter par seconde**. L'unité utilisée est le
**MIPS** _(Millions d'Instructions Par Seconde)_ correspondant à la fréquence du processeur que divise le CPI.
La fréquence d'horloge caractérise (grossièrement) le nombre d'opérations qu'il peut effectuer en une seconde. Elle
est actuellement de plusieurs GHz _(1 GHz permet 1 milliard d'opérations par seconde)_.

## Dissipateur

![Venti-rad](radiateur.png){ align=right }

Dans la mesure où le processeur **rayonne thermiquement**, il est nécessaire d'en **dissiper la chaleur** pour éviter
que ses circuits ne fondent. C'est la raison pour laquelle il est généralement surmonté d'un **dissipateur thermique**
_(appelé parfois refroidisseur ou radiateur)_, **composé d'un métal** ayant une bonne conduction thermique
_(cuivre ou aluminium)_, chargé d'augmenter la **surface d'échange thermique** du microprocesseur.

Le dissipateur thermique comporte une **base en contact avec le processeur** et des ailettes afin d'augmenter la
surface d'échange thermique. Un **ventilateur** accompagne généralement le dissipateur pour améliorer la circulation de
l'air autour du dissipateur et améliorer l'échange de chaleur.

Le terme _ventirad_ est ainsi parfois utilisé pour désigner l'ensemble Ventilateur + Radiateur. C'est le ventilateur du
boîtier qui est chargé d'**extraire l'air chaud du boîtier** et permettre à l'air frais provenant de l'extérieur d'y
entrer. Pour éviter les bruits liés au ventilateur et améliorer la dissipation de chaleur, il est également possible
d'utiliser un **système de refroidissement à eau** _(dit watercooling)_.