![ROM](rom.png){ align=right }

La **mémoire morte** permet de stocker des **données nécessaires au démarrage de l'ordinateur** :

- appelée **ROM** _(Read Only Memory, c'est donc une **mémoire en lecture seule**)_,
- **ne s'efface pas** lors de la mise hors tension du système,
- contient les **éléments essentiels au démarrage de l'ordinateur**.

Les mémoires mortes sont utilisées, entre autres, pour stocker :

- les informations nécessaires au démarrage d'un ordinateur (BIOS, instructions de démarrage, microcode)
- des tables de constantes ou des tables de facteurs de conversion

Le temps d'accès à la mémoire morte est de l'ordre de grandeur de 150 nanosecondes comparativement à un temps d'accès
d'environ 10 nanosecondes pour la mémoire vive. Pour accélérer le traitement des informations, les données stockées
dans la mémoire morte sont généralement copiées dans une mémoire vive avant d'être traitées.
On appelle cette opération le **shadowing**.

!!! warning "Attention"
    Les disques durs, disquettes, CDROM, etc... sont des périphériques de stockage et sont considérés comme des
    mémoires secondaires. Elles nécessitent un contrôleur de disque.

!!! plus-loin
    Avec l'évolution des technologies, la définition du terme mémoire morte a été élargie pour inclure les mémoires
    non volatiles dont le contenu est fixé lors de leur fabrication, qui peuvent être lues plusieurs fois par
    l'utilisateur et qui peuvent être modifiées par un utilisateur expérimenté.

    Ces mémoires sont les UVPROM, les PROM, les EPROM et les EEPROM. Seules les mémoires mortes de première
    génération sont vraiment mortes et vraiment read only. Par contre, dans le vocabulaire informatique, la
    définition des termes mémoire morte _(en français)_ et read only memory _(en anglais)_ a été élargie pour inclure
    les autres types, bien que ces mémoires ne soient ni mortes, ni read only.