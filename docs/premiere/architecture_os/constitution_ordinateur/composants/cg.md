![CG](cg.png){ align=right }

La **carte graphique** _(en anglais "graphic adapter")_, parfois appelée carte vidéo ou accélérateur graphique, est
l'élément de l'ordinateur chargé de **convertir les données numériques à afficher en données graphiques** exploitables
par un périphérique d'affichage. Le rôle de la carte graphique était initialement l'envoi de pixels graphique à un
écran, ainsi qu'un ensemble de manipulation graphiques simples :

- déplacement des blocs (curseur de la souris par exemple)
- tracé de lignes
- tracé de polygones...

Les cartes graphiques récentes sont désormais équipées de **processeurs spécialisés dans le calcul de scènes graphiques
complexes** en 3D !

## Le GPU

Un **processeur graphique** _(appelé GPU, pour Graphical Processing Unit)_, constituant le cœur de la carte graphique
est chargé de traiter les images en fonction de la résolution et de la profondeur de codage sélectionnée. Le GPU est
ainsi un **processeur spécialisé possédant des instructions évoluées de traitement de l'image**, notamment de la 3D. En
raison de la température que peut atteindre le processeur graphique, il est parfois surmonté d'un radiateur et d'un
ventilateur.

## Interface

Il s'agit du **type de bus** utilisé pour connecter la carte graphique à la carte mère. Le bus AGP est ainsi
spécialement prévu pour accepter des débits important de données, nécessaire pour l'affichage de séquences vidéo ou 3D.
Le bus PCI Express possède de meilleures performances que le bus AGP et l'a supplanté.

## Connectiques

### VGA

L'interface VGA standard : Les cartes graphiques sont la plupart du temps équipées d'un connecteur VGA 15 broches
_(Mini Sub-D, composé de 3 séries de 5 broches)_, généralement de couleur bleue, permettant notamment la connexion
d'un moniteur.

### DisplayPort

DisplayPort ou simplement DP est une interface numérique pour écran mise en place par le consortium Video Electronics
Standards Association _([VESA](https://fr.wikipedia.org/wiki/Video_Electronics_Standards_Association))_.

Il existe différentes normes et deux connecteurs sont connus :

![DP](displayport.png)

Plus d'informations sur la [page Wikipedia](https://fr.wikipedia.org/wiki/DisplayPort).

### HDMI

L'interface HDMI _(High-Definition Multimedia Interface)_ rassemble sur un même connecteur à la fois les signaux
vidéo et audio. Ceux-ci sont transmis numériquement et peuvent être cryptés _(protection du contenu contre la copie)_.
Elle permet d'interconnecter une source audio/vidéo — tel qu'un lecteur HD DVD ou Blu-ray, un ordinateur, une console
de jeu ou un téléviseur HD.

Elle vise donc à remplacer les câbles Péritel, coaxiaux, S-Video, et supporte aussi bien la vidéo standard que la haute
définition. Elle se base sur l'interface DVI qu'elle étend largement. Il existe en effet plusieurs versions de la norme
HDMI (1.0, 1.1, 1.2, 1.3...) en fonction des besoins et possibilités de l'appareil à connecter. La version 1.3 permet
ainsi de connecter des appareils de très haute définition (3 840 x 2 400), jusqu'à 8 voix audio peuvent être utilisées.

Le connecteur HDMI type A dispose de 19 broches et est utilisé dans la plupart des cas.

Il existe un connecteur étendu disposant de 29 broches réservé aux appareils très haute définition, et des connecteurs
de type C (19 boches) et D au format réduit pour les ordinateurs portables.

![HDMI](hdmi_type.jpg)