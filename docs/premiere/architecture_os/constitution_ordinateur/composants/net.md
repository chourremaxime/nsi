![Carte réseau](carte_reseau.png){ align=right }

Pour se connecter aux réseaux locaux ou WEB, l'ordinateur possède soit une **carte réseau filaire**, soit une **carte
réseau sans fil** _(WI-FI)_.

La carte réseau constitue l'interface entre l'ordinateur et le câble du réseau. Elle prépare pour le câble réseau
les données émises par l'ordinateur, les transfère vers un autre ordinateur et contrôle le flux de données entre
l'ordinateur et le câble. Elle traduit aussi les données venant du câble et les traduit en octets afin que l'Unité
Centrale de l'ordinateur les comprenne.

La carte réseau possède généralement 2 témoins lumineux (LED) :

1. La LED verte correspond à l'alimentation de la carte
2. La LED orange ou rouge selon le débit (10 / 100 / 1000 Mb/s) indique une activité du réseau (envoi ou réception
   de données).

![Carte Wi-Fi](carte_wifi.png)

!!! note "Remarque"
    La carte Wifi réalise les mêmes opérations que la carte réseau, mais transmet les informations via un 
    protocole sans-fil.