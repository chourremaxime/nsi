Comprendre le **vocabulaire informatique** est souvent l'un des plus grands défis pour les personnes qui
souhaitent acheter un ordinateur personnel.

Contrairement à un téléviseur, dont les critères de choix sont relativement simples, choisir un ordinateur implique
de sélectionner **plusieurs composants distincts** et de connaître leurs spécificités.

L'objectif de cette section n'est pas de détailler toutes les abréviations ou termes techniques, souvent propres aux
fabricants, mais de présenter les **principaux composants d'un ordinateur, leur fonctionnement et leurs caractéristiques
essentielles**.

![Vue explosée d'un ordinateur](vue_explose_pc.svg)
/// caption
<a href="https://commons.wikimedia.org/wiki/File:Personal_computer,_exploded_5,_unlabeled.svg">Gustavb</a>, <a href="http://creativecommons.org/licenses/by-sa/3.0/">CC BY-SA 3.0</a>, via Wikimedia Commons
///