L’**apprentissage supervisé** est un domaine de l’intelligence artificielle où la machine apprend avec l'aide de
l'utilisateur. Parmi les méthodes d’apprentissage supervisé, l’algorithme des **k plus proches voisins** (k-PPV ou
k-NN en anglais) se distingue par sa simplicité et son efficacité.

L’algorithme k-NN a été initialement proposé par Evelyn Fix et Joseph Hodges en **1951**. Ils ont utilisé cette méthode
pour la **classification de plantes** en fonction de leurs caractéristiques.

Depuis lors, l’algorithme a été largement étudié et appliqué dans divers domaines tels que la reconnaissance de formes,
la vision par ordinateur et la bioinformatique.

## Algorithmes d'apprentissage

Avec les algorithmes classiques vus jusqu'à présent, on peut résoudre efficacement des problèmes précis (tri d'une
liste par exemple).

La différence avec les algorithmes d'apprentissage est que ces algorithmes **donnent une réponse qui n'est pas
nécessairement la bonne**.

Il faut donc utiliser ce type d'algorithme lorsqu'un algorithme classique ne peut pas résoudre le problème donné,
ou difficilement.

!!! info "Quels problèmes sont concernés ?"

    - Ceux dont le calcul prendrait trop de temps (ex : un coup au jeu du go)
    - Ceux dont les données sont incomplètes ou imprécises (ex : trouver la meilleure publicité pour un internaute, publicité ciblée)
    - Ceux dont le problème est imprécis (ex : traduire une phrase d'une langue à une autre)