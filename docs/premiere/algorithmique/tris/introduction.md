Les algorithmes de tri permettent de **trier des séquences d'éléments**. Pour rappel, une séquence est un ensemble
ordonné d'éléments *(une liste est une séquence)*.

On verra au fur et à mesure des deux ans de NSI qu'une liste triée **permet de simplifier** beaucoup d'algorithmes !

Un exemple simple d'usage du tri consiste à comparer si deux listes contiennent les mêmes éléments.

## C'est quoi un tri ?

Lorsqu'on dit qu'une liste est triée, cela signifie que ses éléments sont **ordonnés** dans un ordre précis. Par
défaut, l'ordre de tri est l'**ordre croissant**, mais on peut modifier cet ordre.

!!! example "Exemple de tri"

    Soit une liste de valeurs : `#!python x = [3, 7, 4, 2, 8]`.

    La liste `#!python y = [2, 3, 4, 7, 8]` correspond à la liste `x` triée.

Et on peut trier sur toutes les caractéristiques possibles !

!!! example "Autres tris"

    Soit la liste des élèves d'une classe de NSI :
    `#!python eleves = ["Edouard", "Fatima", "Alice", "Clément", "Dominique", "Bob"]` et la liste des notes du dernier
    exemen associées : `#!python notes = [7.5, 12.0, 14.5, 6.75, 19.25, 10.5]`.

    La liste `#!python eleves_crois = ["Alice", "Bob", "Clement", "Dominique", "Edouard", "Fatima"]` correspond à la
    liste `eleves` triée dans l'ordre croissant (ordre par défaut).
    
    La liste `#!python eleves_note = ["Dominique", "Alice", "Fatima", "Bob", "Edouard", "Clement"]` correspond à la
    liste `eleves` triée dans l'ordre décroissant des notes au dernier examen.