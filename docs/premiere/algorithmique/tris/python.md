Après avoir bien maîtrisé les deux algorithmes de tri, on peut apprendre à trier des algorithmes avec les
fonctions déjà intégrées dans Python.

Soit `l` une liste.

- La fonction `#!python sorted(l)` renvoie une nouvelle liste triée avec les éléments de `l`.
  La liste `l` n'est donc pas modifiée.
    - Cette fonction peut prendre en paramètre `key` une fonction qui indique quelle fonction appliquer sur chaque
      élément pour connaître sa valeur de tri.
    - Cette fonction peut prendre en paramètre `reverse` un booléen indiquant si le tri doit être fait à l'envers. 
- La méthode `#!python l.sort()` qui ne renvoie rien, mais modifie directement l.
    - Cette méthode peut prendre les mêmes paramètres que la fonction `#!python sorted()`.

## Exemples

```python
noms = ["Edouard", "Fatima", "Alice", "Clément", "Dominique", "Bob"]

# Avec la fonction sorted(), la liste noms n'est pas modifiée
tri = sorted(noms)
print(noms)
print(tri)

# Avec la méthode .sort(), la liste noms est modifiée.
noms.sort()
print(noms)

# Ici on veut trier non pas par ordre alphabétique mais par nombre de caractères, obtenables via la fonction len()
noms.sort(key=len)
print(noms)

# Enfin, on trie les noms par ordre alphabétique décroissant (de Z à A)
noms.sort(reverse=True)
print(noms)
```