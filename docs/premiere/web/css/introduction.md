Le CSS _(Cascading Style Sheets)_ est le langage de mise en forme et de mise en page des fichiers `.html`.

- **Mise en forme** : souligner du texte, le mettre en gras, en rouge, encadrer...
- **Mise en page** : disposer les blocs les uns par rapport aux autres dans la page.

Ce second point est plus difficile que le premier. Écrire le fichier `.css` d'une mise en page commence par **établir
sur un brouillon** un plan du visuel final désiré. **Le CSS doit donc être pensé au préalable**, et ce, dès l'écriture
des pages `.html` :

![Croquis](../html/croquis.png)

Ainsi, pour décaler le contenu de la page vers la droite afin d'avoir un menu sur la gauche, il est nécessaire que
l'ensemble de ce contenu soit délimité par des
[balises universelles `<div>`](../html/balises_generiques.md#balises-universelles-div-et-span).

## Où écrire le code CSS ?

### Dans chaque balise du code HTML _(fortement déconseillé)_

Le défaut principal est la difficulté de maintenance si l'on veut avoir un site au rendu homogène.

**Ne parlons plus de cette méthode.**

### Avec une balise `<style>` dans le `<head>` de la page `.html` _(pas recommandé)_

Cette méthode peut être utile lorsqu'une page d'un site est un peu à part des autres et nécessite une mise en page
particulière. C'est très rare.

### Dans une feuille `.css` _(à privilégier)_

![Arborescence](../arborescence/arborescence_vertical.png){ align=right ; width=30% }

Avoir une feuille `.css` _(un fichier)_ commune permet d'harmoniser l'ensemble du site web : il suffit de changer un
seul paramètre _(par exemple sur la mise en forme des balises `<strong>`)_ dans la feuille `.css` pour modifier chaque
page du site.

Les pages `.html` accèdent à la feuille de style `.css` grâce à un **lien hypertext** déclaré entre les balises
`<head>`. Le [lien relatif](../arborescence/liens_relatifs.md) obéit aux mêmes règles que pour les images ou pour les
ancres, seule la balise diffère.

Avec un site doté de l'arborescence ci-contre, la page `accueil.html` accède à la feuille `style_perso.css` grâce à
l'instruction de la ligne 3 ci-dessous :

```py linenums="1" hl_lines="3"
<head>
    ...
    <link href="style_du_site/style_perso.css" rel="stylesheet" type="text/css" />
    ...
</head>
```

On pourra visiter le site [cssgarden](http://www.csszengarden.com/tr/francais/) afin de voir comment le même contenu
peut être mis en forme de manières radicalement différentes _(avec un peu d'imagination et d'efforts...)_.

## Vocabulaire

Dans une feuille `.css`, on écrit des **règles de style**. Par exemple _(entre `/*` et `*/` il y a des commentaires)_ :

```css
h1 {                               /* On modifie le style des titres */
    color: red;                       /* Le texte sera écrit en rouge */
    font-variant: small-caps;       /* Le texte sera affiché en majuscules */
}
    
strong {                           /* On modifie le style des mots importants */
    background-color: gold;           /* Le texte sera écrit sur un fond doré */
    text-decoration: underline;       /* Le texte sera souligné */
}
```

Pour appliquer une **règle de style** dans une feuille `.css`, il faut :

- débuter par un **sélecteur** qui précise à **quelle balise HTML** les règles de style s'appliquent ;
- ouvrir une accolade ;
- déclarer les **propriétés** de style à modifier selon les **valeurs** indiquées _(attention aux deux-points et au
  point-virgule)_ ;
- refermer l'accolade.

```css
selecteur {
    propriete: valeur;
}
```

## Documentation des propriétés

**Personne** ne connaît toutes les propriétés par cœur, il faut savoir se référer à la documentation régulièrement,
ne jamais hésiter à rechercher de l'aide sur internet. À ce titre, le [site MDN](https://developer.mozilla.org/fr/) est
une référence intéressante.

_Travail originel CC-BY-NC-SA : Nicolas Buyle-Bodin, Jean-Manuel Meny et Mcikaël Bordonaro. Autres modifications par
Cédric Frayssinet et Maxime Chourré_