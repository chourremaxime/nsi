## Cours

En CSS, il existe de très nombreux pseudo-éléments. Dans les exercices précédents, les pseudo-éléments
`:first-of-type`, `:last-of-type` et `:nth-of-type` ont permis d'identifier des éléments en fonction de leur _numéro_
dans un conteneur. Le pseudo-élément `:hover` permet, quant à lui, de modifier l'affichage de l'élément ciblé au
passage de la souris.

### Bac à sable : Survol d'un élément

<p id="boiteIllustrative" onmouseover="definirHover()" onmouseout="definirNormal()" style="border-color: black; border-width: 0px; border-style: solid; font-weight: normal; text-decoration: none;">
Ci-contre, on peut choisir quels seront l'image d'arrière-plan
de cette boîte ainsi que l'apparence du texte, l'épaisseur et la
couleur de la bordure lors du survol de la boîte par la souris.
</p>

<form id="choixAttributsHover">
    <h3 style="margin-top: 0;">Style au survol du paragraphe :</h3>
    <label>Image d'arrière-plan :</label>
    <select name="choixArrierePlan" id="choixArrierePlan" onchange="affichageHover()">
        <option value="">Aucune</option>
        <option value="Fond_Cerise.jpg">Cerise</option>
        <option value="Fond_Dauphin.jpg">Dauphin</option>
        <option value="Fond_Epee.png">Épée</option>
        <option value="Fond_Triangles.jpg">Triangles</option>
    </select>
    <br>
    <label>Couleur bordure :</label>
    <input type="color" name="choixCouleur" id="choixCouleur" onchange="affichageHover()">
    <br>
    <label>Épaisseur bordure :</label>
    <input type="range" name="choixEpaisseur" id="choixEpaisseur" min="0" max="20" value="1" step="1" onchange="affichageHover() ; rangevalue.value=value">
    <output id="rangevalue">1</output>
    <br>
    <label for="choixGraisse">Style texte : </label>
    <select id="choixGraisse" name="choixGraisse" onchange="affichageHover()">
        <option value="normal">normal</option>
        <option value="bold">gras</option>
        <option value="lighter">fin</option>
    </select>
    <br>
    <label for="choixDecoration">Decoration texte : </label>
    <select id="choixDecoration" name="choixDecoration" onchange="affichageHover()">
        <option value="none">normal</option>
        <option value="underline">souligné</option>
        <option value="overline">surligné</option>
        <option value="line-through">barré</option>
    </select>
</form>

```css title="Code CSS"
p:hover {      /* Mise en forme lors du survol */
    background-image: url(); 
    border: 1px solid #000000; 
    font-weight: normal; 
    text-decoration: none; 
}
```

## Exercices

### Exercice n°1

<iframe src="../resultat_survol_ex1.html"></iframe>

Les styles CSS permettent de mettre des effets _whaou_ avec peu de lignes. Modifier le code ci-dessous afin d'obtenir
le même effet que le survol de l'image ci-dessus.

[Télécharger l'image](rire.jpg){ .md-button ; download="rire.jpg" }

```html
<!-- Partie html à ne pas modifier -->
<img id="rire" src="rire.jpg" alt="stromae" >
```

```css
/* Partie css modifiable */
#rire {
    width: 80px;
    height: 80px;
    margin: 0 auto;
    padding: 5px;
    border: 1px dashed #163c99;
    border-radius: 4px;
}

#rire:hover {
    width: 90px;
}
```

??? question "Une piste ?"

    Dans les cas suivants, `X` doit être remplacé par un nombre :
    
    - Le paramètre `transform: rotate(Xdeg);` permet d'afficher un conteneur en lui faisant subir une rotation de
      `X` degrés autour de son centre.
    - Le paramètre `transition: Xs;` applique les modifications de style en `X` secondes au lieu de les appliquer
      instantanément.

### Exercice n°2

<iframe src="../resultat_survol_ex2.html"></iframe>

Modifier le code CSS suivant afin d'obtenir l'affichage ci-dessus. Passer la souris sur ce paragraphe pour comprendre
l'effet réalisé.

```html
<!-- Partie html à ne pas modifier -->
<p> 
    <span id="banane">J'apprends, je suis content !</span>
    <img id="rire" src="rire.jpg" alt="stromae" >
</p>
```

```css
/* Partie css modifiable */
#banane {                  /* Le texte */
    color: darkgreen;
    font-weight: bold;
}

#rire {                    /* L'image */
    width: 80px;
    height: 80px;
    margin: 0 auto;
    border-radius: 4px;
}

p {                        /* Le paragraphe */
    width: 100px;
    height: 80px;
    border: 2px solid darkgreen;
    text-align: center;
}
```

??? question "Une piste ?"

    - La propriété `display: none;` permet de ne pas afficher un conteneur à l'écran, `display: block;` _transforme_
      l'élément en contenur de type bloc.
    - Pour affecter un élément inclut dans un conteneur il suffit de déclarer les sélecteurs en les séparant par un
      espace. Par exemple, `p strong {...}` affecte uniquement la mise en forme des éléments `strong` contenus dans
      des paragraphes `p` (et pas ceux inclus dans des listes ou autre...).

### Exercice n°3 : Son propre menu

<iframe src="../resultat_survol_ex3.html" width="600px"></iframe>

Cet exercice est assez difficile, mais il montre tout le potentiel contenu à l'intérieur d'une relation HTML/CSS
bien pensée. La liste ci-dessous est destinée à être un menu avec un sous-niveau.

Apporter les modifications nécessaires pour que le rendu soit similaire à celui ci-dessus.

```html
<!-- Partie html à ne pas modifier -->
<ul class="horizontal">
    <li> <a href="#">Menu 1</a> 
        <ul class="vertical">
            <li> <a href="#">Lien 1.1</a> 
            <li> <a href="#">Lien 1.2</a> 
            <li> <a href="#">Lien 1.3</a> 
        </ul>
    </li>
    <li> <a href="#">Menu 2</a> 
        <ul class="vertical">
            <li> <a href="#">Lien 2.1</a> 
            <li> <a href="#">Lien 2.2</a> 
            <li> <a href="#">Lien 2.3</a> 
        </ul>
    </li>
    <li> <a href="#">Lien 3</a> </li>
    <li> <a href="#">Menu 4</a> 
        <ul class="vertical">
            <li> <a href="#">Lien 4.1</a> 
            <li> <a href="#">Lien 4.2</a> 
            <li> <a href="#">Lien 4.3</a> 
            <li> <a href="#">Lien 4.4</a> 
        </ul>
    </li>
    <li> <a href="#">Lien 5</a> </li>
</ul>
```

```css
/* Partie css modifiable */
ul {
    list-style-type: none;        /* disparition des puces */
    margin: 0;
}

li {
    width: 100px;
    margin: 0;
    padding: 3px;                /* espace intérieur */
    border: 1px solid orange;
    height: 20px;
    line-height: 20px;            /* centrage vertical */
    background-color: white;
}

a {
    text-decoration: none;
    color: orange;
}
```

??? question "Une piste ?"

    Il est nécessaire de pouvoir positionner des blocs enfants par rapport à leur parent.
    
    La documentation officielle se trouve [ici](https://developer.mozilla.org/fr/docs/Web/CSS/position). 