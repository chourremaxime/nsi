## Cours

Par défaut, les éléments HTML sont de type **en-ligne** _(inline)_ ou de type **bloc** _(block)_. C'est la propriété
CSS [display](https://developer.mozilla.org/fr/docs/Web/CSS/display) qui permet de changer le type d'une boîte afin
de modifier la manière dont elle s'affiche.

### `display: inline;`

Par défaut, les éléments de texte _(`<em>`, `<strong>`, `<code>`, etc.)_, `<img>`, `<span>`, etc. sont des boîtes de
type `inline`. **Elles seront affichées les unes à côté des autres.**

!!! warning "Boites en-ligne"

    Les boîtes de type en-ligne ne peuvent contenir que d'autres boîtes de type en-ligne. Ces boîtes ont des marges
    internes et externes nulles par défaut.

### `display: block;`

Par défaut, `<p>`, `<article>`, `<div>`, etc. sont des boîtes de type **block**. Elles seront affichées les unes en
dessous des autres, alignées sur le bord gauche.

!!! warning "Boites en-ligne"

    Les boîtes de type bloc peuvent contenir d'autres boîtes de type bloc et/ou des boîtes de type en-ligne. Par
    défaut, la plupart des éléments bloc possèdent des marges internes et externes non nulles, parfois **différentes
    selon les navigateurs**.

### `display: inline-block;`

Les boîtes auxquelles on attribue cette propriété auront les avantages des deux types précédents. Elles seront :

- affichées les unes à côtés des autres ;
- alignées sur leur bord inférieur ;
- réduite à la largeur minimum pour leur contenu (elles ne prennent pas toute la largeur de leur conteneur).

On peut spécifier les dimensions _(hauteur et largeur)_ et les marges verticales des boîtes `inline-block`.

### `display: none;`

Une boîte de propriété `display: none;` ne sera pas affichée, ainsi que toute boîte qu'elle contiendrait. Les autres
conteneurs se placent dans le flux d'affichage comme si cette boîte n'existait plus.

Cette propriété s'utilise par exemple pour les menus : les sous-menus sont par défaut en `display: none;` et passent
en `display: block;` lorsqu'on les survole.

!!! danger "Attention !"

    Il ne faut pas confondre une déclaration `display: none;` avec une déclaration `visibility: hidden;`. Pour
    cette seconde déclaration, le contenu de la boîte est invisible, mais occupe le même espace dans la page comme
    si cette boîte était visible.

## Exercices

## Exercice n°1 : Tester

Les paragraphes `<p>` sont de type block par défaut dans les navigateurs. Modifier leur type afin d'observer
les différences avec un affichage en-ligne.

```html
<!-- Partie html à ne pas modifier -->
<p>Algorithmique</p>
<p>Programmation</p>
<p>Réseaux</p>
<p>Web</p>
<p>Problèmes sociétaux</p>
```

```css
/* Partie css modifiable */
p{
    border: 1px solid orange;
}
```

## Exercices n°2 : Faire un menu

Définir le Css pour que la liste, destinée à un menu, ait l'apparence ci-dessous :

![Affichage](ex2_affichage.png)

```html
<!-- Partie html à ne pas modifier -->
<ol>
    <li> <a href="#">Lien 1</a> </li>
    <li> <a href="#">Lien 2</a> </li>
    <li> <a href="#">Lien 3</a> </li>
    <li> <a href="#">Lien 4</a> </li>
    <li> <a href="#">Lien 5</a> </li>
    <li> <a href="#">Lien 6</a> </li>
    <li> <a href="#">Lien 7</a> </li>
</ol>
```

```css
/* Partie css modifiable */

```