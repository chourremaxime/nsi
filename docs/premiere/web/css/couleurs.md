De nombreux éléments d'une page web peuvent être mis en couleur. Par exemple :

- la propriété `color` modifie la couleur du texte ;
- la propriété `background-color` modifie la couleur d'arrière-plan ;
- la propriété `border-color` modifie la couleur des bordures...

Les valeurs prisent par ces propriétés peuvent être :

- une couleur **nommée**. On trouvera 500 couleurs nommées en
  [cliquant ici](https://developer.mozilla.org/en-US/docs/Web/CSS/named-color) ;
- une couleur représentée par un **codage héxadécimal** (c'est le plus utilisé) ;
- ou une couleur représentée par un **codage RGB** sous forme `rgb(r, v, b)`.

Pour obtenir ces représentations héxadécimales ou RGB, on peut utiliser un _analyseur de couleur_.

[Just Color Picker](http://annystudio.com/software/colorpicker/) convient aux systèmes d'exploitation Windows et MacOS ;
[GColor2](https://doc.ubuntu-fr.org/gcolor2) convient au système d'exploitation Linux.

## Bac à sable : Des couleurs partout

<div id="zoneIllustrative"><p id="boiteIllustrative">
Ci-contre, on peut choisir la couleur de l'arrière-plan de cette boîte, la couleur du texte de base et celle
<span id="important">des mots importants</span> à mettre en valeur.
</p></div>

<form id="choixAttributs">
    <label for="FondParagraphe">Arrière-plan des paragraphes :</label>
    <input type="color" id="FondParagraphe" name="FondParagraphe" value="#ffffff" onchange="definirCouleur()">
    <br>
    <label for="CouleurParagraphe">Texte dans les paragraphes :</label>
    <input type="color" id="CouleurParagraphe" name="CouleurParagraphe" onchange="definirCouleur()">
    <br>
    <label for="CouleurImportant">Mots importants :</label>
    <input type="color" id="CouleurImportant" name="CouleurImportant" onchange="definirCouleur()">
</form>

```css title="Code CSS"
p {                                 /* Mise en forme des paragraphes */
    background-color: #ffffff; 
    color: #000000; 
} 

strong {                            /* Mise en forme des mots importants */
    color: #000000; 
}
```

Les exercices de CSS qui suivront comportent deux fenêtres :

- La première contient un extrait de fichier `.html` : à modifier **uniquement** lorsque l'énoncé le précise. _C'est à
  vous de copier / coller cet extrait dans un fichier `.html` que vous aurez fraichement créé en ajoutant les balises
  nécessaires `<body>`, `<html>`..._
- La deuxième contient une feuille `.css` agissant sur la mise en forme du fichier `.html`. **Cette feuille est
  modifiable.** _C'est à vous de copier / coller cette feuille dans un fichier `.css` et de lier ce fichier à votre
  fichier `.html`._

## Exercice n°1

Compléter le code Css ci-dessous pour que le paragraphe soit écrit en blanc sur fond noir et que le texte balisé par
`<strong>` soit écrit en rouge sur fond jaune.

```html
<!-- Partie html à ne pas modifier -->
<p>
    Les paragraphes doivent être écrits en <strong>blanc sur fond noir</strong>.
</p>
<p>
    Mettre <strong>en rouge</strong> le texte balisé par les balises <code>strong</code>. 
    Le fond de ce texte rouge sera <strong>jaune</strong>.
</p>
```

```css
/* Partie css modifiable */
p {
    color: green;
    background-color: lightgray;
}
```

_Travail originel CC-BY-NC-SA : Nicolas Buyle-Bodin, Jean-Manuel Meny et Mcikaël Bordonaro. Autres modifications par
Cédric Frayssinet et Maxime Chourré_