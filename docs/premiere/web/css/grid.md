## Cours

La propriété css `grid` est une propriété permettant de placer dans la page assez facilement les éléments HTML.

Nous avons vu précédemment [`display`](affichage.md), `grid` arrive en complément et on pourra aussi trouver sur le
web d'autres propriétés, telles que :

- [flexbox](https://developer.mozilla.org/fr/docs/Web/CSS/CSS_flexible_box_layout/Basic_concepts_of_flexbox)
- [float](https://developer.mozilla.org/fr/docs/Web/CSS/float)

### Grid

Analyser le code ci-dessous, et notamment la partie CSS (intégrée exceptionnellement au code HTML). Modifier les
valeurs `1fr 3fr;` qui apparaissent.

```html
<!DOCTYPE html>
<html lang="fr">

<head>
<meta charset="UTF-8">
<title>Grid</title>
<style>
    body {
        width:60%;
        margin:0 auto;
    }
    div { padding: 4px; }
    div.conteneur {
         border: 3px solid black;
         display: grid;
         grid-template-columns: 1fr 3fr;
     }

    div.item {
         border: 2px dashed pink;
     }
</style>
</head>

<body>

<div class="conteneur">
    <div class="item">
        Case 1
    </div> 
    <div class="item">
        Case 2
        <p class="sousitem"> bla bla</p>
    </div>
    <div class="item">
        Case 3
    </div>
</div>

</body>
</html>
```

!!! success "Analyse"

    L'élément `div` de classe nommée `conteneur` est déclaré en `display: grid;` : ses **fils directs** (c'est-à-dire
    ici les éléments `div` de classe nommée `item`) seront affichés suivant un principe de grille, ils constitueront
    les **cellules de la grille**. Les **descendants suivants** par contre (ici par exemple les éléments de type `p`,
    petits-fils du `div` conteneur) se placent dans la cellule.

    Pour l'élément `div` de classe `conteneur`, on a déclaré `grid-template-columns: 1fr 3fr;`. Cela signifie que
    chaque ligne de la grille sera constituée de deux cellules, la première occupant 1/(1+3) = 25% de la largeur
    du conteneur, la seconde occupant les 75% restant. Si le `div` `conteneur` a par exemple 6 fils directs,
    on verra 3 lignes de deux cellules. Tester cela dans l'exemple.

### Une mise en page classique

Voici un exemple simple d'utilisation de `display: grid;` pour une mise en page classique :

- Analyser ce code HTML qui contient, de manière exceptionnelle, du code CSS à l'intérieur.
- Réaliser quelques modifications pour bien comprendre.

```html
<!DOCTYPE html>
<html lang="fr">

<head>
<meta charset="UTF-8">
<title>Grid</title>
<style>
    header {
        text-align: center;
        background-color: rgb(255,255,0);
    }
    footer {
        text-align: center;
        background-color: rgb(0,255,255);
    }

    .conteneur { /* contiendra les éléments à disposer en "grille" */
        display: grid;
        grid-template-columns: 20% 80%;
        margin-bottom: 1em; /* marge sous la boîte */

    }
    nav {
        border: 1px dashed red; /* bordure pour mieux situer l'élément dans cet exemple */
        padding: 1em;
    }
    main {
        border: 1px solid green; /* bordure pour mieux situer l'élément dans cet exemple */
        padding: 1em;
    }
</style>
</head>

<body>

<header>
    <h1>Titre de page</h1>
    <h2>sous-titre</h2>
</header>


<div class="conteneur">

    <nav>
        liens de navigation du site
    </nav>

    <main>
        <section>
            section 1
        </section>

        <section>
            section 2
        </section>
    </main>

</div><!-- fin de la partie "grille" -->


<footer>
    Éléments de pied de page
</footer>

</body>
</html>
```

### Emboîter des grilles

Une cellule de grille peut elle-même être une grille. Visualiser par exemple le résultat du code ci-dessous :

```html
<!DOCTYPE html>
<html lang="fr">

<head>
<meta charset="UTF-8">
<title>Grid</title>
<style>
    header{
        text-align: center;
        background-color: rgb(255,255,0);
    }

    footer {
        text-align: center;
        background-color: rgb(0,255,255);
    }

    .conteneur { /* contiendra les éléments à disposer en "grille" */
        display: grid;
        grid-template-columns: 1fr 6fr;
        margin-bottom: 1em; /* marge sous la boîte */
    }

    nav {

        border: 1px dashed red; /* bordure pour mieux situer l'élément dans cet exemple */
        padding: 1em;
    }
    main {

        border: 1px solid green; /* bordure pour mieux situer l'élément dans cet exemple */
        padding: 1em;
    }

    .grilleContenu { /* une  grille à l'intérieur de la première grille pour la partie main */
        display: grid;
        grid-template-columns: 50% 50%;
        grid-template-rows: 100px 100px;
    }

    .no {
        background-color: cornsilk;
    }
    .ne {
        background-color: lavender;
    }
    .se {
        background-color: khaki;
    }
    .so {
        background-color:WhiteSmoke;
    }
</style>
</head>

<body>

<header>
    <h1>Titre de page</h1>
    <h2>sous-titre</h2>
</header>


<div class="conteneur">

    <nav>
        liens de navigation du site
    </nav>

    <main class="grilleContenu">

        <section class="no">
            Ligne 1, colonne 1
        </section>

        <section class="ne">
            Ligne 1, colonne 2
        </section>

        <section class="so">
            Ligne 2, colonne 1
        </section>

        <section class="se">
            Ligne 2, colonne 2
        </section>

    </main>

</div><!-- fin de la partie "grille" -->


<footer>
    Éléments de pied de page  
</footer>

</body>

</html>
```

### Nommer les emplacements

Pour affiner le placement des fils dans la grille, on peut nommer les emplacements. Le nom de l'emplacement se
retrouvera dans le css du fils. Si plusieurs emplacements (sur une forme rectangulaire) portent la même lettre,
un même fils occupera plusieurs cellules.

Exemple - Visualiser le rendu du code ci-dessous :

```html
<!DOCTYPE html>
<html lang="fr">

<head>
<meta charset="UTF-8">
<title>Grid</title>
<style>
    header {
        text-align: center;
        background-color: rgb(255,255,0);
    }


    footer{
        text-align: center;
        background-color: rgb(0,255,255);
    }

    .conteneur { /* contiendra les éléments nav et main en "grille" */
        display: grid;
        grid-template-columns: 20% 80%;
        margin-bottom: 1em; /* marge sous la boîte */
        grid-template-areas: "gauche droite";
    }

    nav {
        grid-area: gauche; /* placement en colonne 1 */
        border: 1px dashed red; /* bordure pour mieux situer l'élément dans cet exemple */
        padding: 1em;
    }
    main {
        grid-area: droite; /* placement en colonne 2 */
        border: 1px solid green; /* bordure pour mieux situer l'élément dans cet exemple */
        padding: 1em;
    }

    .grilleContenu { /* la partie main a ses fils disposés en grille également. */
        display: grid;
        grid-template-columns: 1fr 1fr 1fr;
        grid-template-areas: "T T T"   
                             "A B B"
                             "A C D"; /* structure de la grille */
        grid-gap: 2px;
    }

   .monTitre { 
     grid-area: T; /* l'élément de class monTitre occupera les cellules T */
     background-color: yellow;
     text-align: center;
   }

    .no { 
        grid-area: A; /* l'élément de class no occupera les cellules marquées A */
        background-color: green;
        color:orange;
    }
    .ne {
        grid-area: B; 
        background-color: lavender;
    }

    .so {
        grid-area: C; 
        background-color: orange;
    }

    .se {
        grid-area: D; 
        background-color: khaki;
    }
</style>
</head>

<body>

<header>
    <h1>Titre de page</h1>

</header>


<div class="conteneur">

    <nav>
        liens de navigation du site
    </nav>

    <main class="grilleContenu">

        <h2 class="monTitre">Le coin Geluck</h2>
        <section class="no">
            Boire du café empêche de dormir. Et dormir empêche de boire du café. 
        </section>

        <section class="se">
            Il est plus facile de jouer au mikado avec des spaghettis crus qu'avec des spaghettis cuits.
        </section>

        <section class="so">
            Quand je dis que ma richesse est intérieure je veux dire que mon argent est dans un coffre.
        </section>

        <section class="ne">
            Les imbéciles pensent que tous les noirs se ressemblent. Je connais un noir qui trouve, lui, que tous les imbéciles se ressemblent.
        </section>
    </main>

</div><!-- fin de la partie "grille" -->


<footer>
    Éléments de pied de page
</footer>

</body>

</html>
```

### Des compléments sur Grid

Il existe de nombreuses ressources sur le Web pour en savoir plus sur les possibilités de `display: grid;` :

- [alsacreation](https://www.alsacreations.com/article/lire/1388-css3-grid-layout.html)
- [MDN](https://developer.mozilla.org/fr/docs/Web/CSS/CSS_grid_layout)
- [w3schools](https://www.w3schools.com/css/css_grid.asp)
- [une série d'exemples pouvant constituer un modèle pour une mise en page](https://gridbyexample.com/examples/)

## Exercices

### Exercice n°1 : Synthèse Grid

On vous fournit le code suivant :

```html
<!DOCTYPE html>
<html lang="fr">

<head>
<meta charset="UTF-8">
<title>Grid</title>
</head>

<body>

<header>
    <h1>Un super titre</h1>
    <h2> Un joli sous-titre</h2>
</header>


<div class="grillePrincipale">

    <nav>
        Ici on navigue.
        <ul>
            <li><a href="#">lien 1</a></li>
            <li><a href="#">lien 2</a></li>
            <li><a href="#">lien 3</a></li>
            <li><a href="#">lien 4</a></li>
        </ul>
    </nav>

    <main>

        <article>
            Raymond Devos
        </article>

        <article>
            L'autre jour, au café, je commande un demi. J'en bois la moitié. Il ne m'en restait plus.
        </article>

        <article>
            Je suis adroit de la main gauche et je suis gauche de la main droite.
        </article>

        <article>   
            Se coucher tard nuit.
        </article>

        <article>
            Quand un homme ne dit rien alors que tout le monde parle, on n'entend plus que lui !
        </article>

        <article>
            Si l'on peut trouver moins que rien, c'est que rien vaut déjà quelque chose.
        </article>

        <article>
            Ne rien faire, ça peut se dire. Ca ne peut pas se faire !
        </article>

    </main>

    <aside>
        Ici, plein de remarques complémentaires super captivantes.
    </aside>
</div>

<footer>
C'est le pied !
</footer>

</body>         
</html>
```

Le but est de reproduire cette mise en page suivante à l'aide d'un fichier CSS que vous créerez et que vous lierez au
fichier HTML.

![Mise en page Grid](misenpage_grid.png)