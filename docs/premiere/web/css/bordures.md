Chaque élément HTML est contenu dans une boîte. Cette boîte possède des bordures modifiables grâce à trois propriétés :

- `border-width` pour l'épaisseur ;
- `border-style` pour la forme ;
- `border-color` pour la couleur.

On peut aussi utiliser la **propriété raccourci** `border` qui permet de préciser les trois propriétés précédentes en
une seule déclaration. On trouvera les valeurs possibles pour ces propriétés sur
[cette page de MDN](https://developer.mozilla.org/fr/docs/Web/CSS/border).

# Bac à sable : Des bordures autour des boîtes

Dans ce bac à sable, les dimensions sont en pixels.

<p id="boiteIllustrative">
Ci-dessous, on peut choisir la bordure qui encadre le paragraphe, c'est-à-dire son épaisseur, sa couleur et
son apparence.
</p>

<form id="choixAttributsBordures">
    <label for="choixEpaisseur">Épaisseur : </label>
    <input type="range" name="choixEpaisseur" id="choixEpaisseur" min="0" max="50" value="0" step="1" onchange="definirBordure() ; rangevalue.value=value">
    <output id="rangevalue">10</output>
    <br>
    <label for="choixStyle">Apparence : </label>
    <select name="choixStyle" id="choixStyle" onchange="definirBordure()">
        <option value="none">aucune</option>
        <option value="hidden">masquée</option>
        <option value="solid">trait</option>
        <option value="dashed">pointillée</option>
        <option value="dotted">point par point</option>
        <option value="double">double</option>
        <option value="groove">sous-épaisseur</option>
        <option value="ridge">sur-épaisseur</option>
        <option value="inset">sous-élevée</option>
        <option value="outset">sur-elevée</option>
    </select>
    <br>
    <label for="choixCouleur">Couleur : </label>
    <input type="color" id="choixCouleur" name="choixCouleur" onchange="definirBordure()">
</form>

```css title="Code CSS"
p {                                 /* Mise en forme des paragraphes */
    border-width: 0px;
    border-style: none;
    border-color: #000000;
}
```

```css title="Raccourci possible en CSS"
p {
    border: 0px none #000000;
}
```

## Bac à sable : Identifier les bordures

Une bordure est en fait un rectangle constitué de quatre côtés, chacun est identifié par sa position.

<p id="boiteIllustrative2">
Ci-dessous, on peut choisir l'apparence de chaque côté. Faites des tests avec des côtés épais visibles et
d'autres masqués.
</p>

<form id="choixAttributsIdentifier">
    <label>Bordure haute :</label><br>
    <input type="number" id="choixEpaisseurHaut" name="choixEpaisseurHaut" value="0" min="0" max="1000" onchange="definirBordure2()">
    <select name="choixStyleHaut" id="choixStyleHaut" onchange="definirBordure2()">
        <option value="none">aucune</option>
        <option value="hidden">masquée</option>
        <option value="solid">trait</option>
        <option value="dashed">pointillée</option>
        <option value="dotted">point par point</option>
        <option value="double">double</option>
        <option value="groove">sous-épaisseur</option>
        <option value="ridge">sur-épaisseur</option>
        <option value="inset">sous-élevée</option>
        <option value="outset">sur-elevée</option>
    </select>
    <input type="color" id="choixCouleurHaut" name="choixCouleurHaut" onchange="definirBordure2()">
    <br>
    <label>Bordure droite :</label><br>
    <input type="number" id="choixEpaisseurDroit" name="choixEpaisseurDroit" value="0" min="0" max="1000" onchange="definirBordure2()">
    <select name="choixStyleDroit" id="choixStyleDroit" onchange="definirBordure2()">
        <option value="none">aucune</option>
        <option value="hidden">masquée</option>
        <option value="solid">trait</option>
        <option value="dashed">pointillée</option>
        <option value="dotted">point par point</option>
        <option value="double">double</option>
        <option value="groove">sous-épaisseur</option>
        <option value="ridge">sur-épaisseur</option>
        <option value="inset">sous-élevée</option>
        <option value="outset">sur-elevée</option>
    </select>
    <input type="color" id="choixCouleurDroit" name="choixCouleurDroit" onchange="definirBordure2()">
    <br>
    <label>Bordure basse :</label><br>
    <input type="number" id="choixEpaisseurBas" name="choixEpaisseurBas" value="0" min="0" max="1000" onchange="definirBordure2()">
    <select name="choixStyleBas" id="choixStyleBas" onchange="definirBordure2()">
        <option value="none">aucune</option>
        <option value="hidden">masquée</option>
        <option value="solid">trait</option>
        <option value="dashed">pointillée</option>
        <option value="dotted">point par point</option>
        <option value="double">double</option>
        <option value="groove">sous-épaisseur</option>
        <option value="ridge">sur-épaisseur</option>
        <option value="inset">sous-élevée</option>
        <option value="outset">sur-elevée</option>
    </select>
    <input type="color" id="choixCouleurBas" name="choixCouleurBas" onchange="definirBordure2()">
    <br>
    <label>Bordure gauche :</label><br>
    <input type="number" id="choixEpaisseurGauche" name="choixEpaisseurGauche" value="0" min="0" max="1000" onchange="definirBordure2()">
    <select name="choixStyleGauche" id="choixStyleGauche" onchange="definirBordure2()">
        <option value="none">aucune</option>
        <option value="hidden">masquée</option>
        <option value="solid">trait</option>
        <option value="dashed">pointillée</option>
        <option value="dotted">point par point</option>
        <option value="double">double</option>
        <option value="groove">sous-épaisseur</option>
        <option value="ridge">sur-épaisseur</option>
        <option value="inset">sous-élevée</option>
        <option value="outset">sur-elevée</option>
    </select>
    <input type="color" id="choixCouleurGauche" name="choixCouleurGauche" onchange="definirBordure2()">
</form>

```css title="Code CSS"
p {                                     /* Mise en forme des paragraphes */
    border-top : 0px none #000000; 
    border-right : 0px none #000000; 
    border-bottom : 0px none #000000; 
    border-left : 0px none #000000; 
}
```

## Bac à sable : Arrondir les angles

La propriété `border-radius` permet d'arrondir les coins des bordures. Dans ce bac à sable, les dimensions sont
en pixels.

<p id="boiteIllustrative3">
Ci-contre, on peut choisir l'épaisseur, l'apparence et la couleur de la bordure qui encadre le paragraphe, ainsi que
l'arrondit des angles <em>(des coins)</em>.
</p>

<form id="choixAttributsAngles">
    <label>Style de la bordure :</label><br>
    <input type="number" id="choixEpaisseurArrondi" name="choixEpaisseurArrondi" value="0" min="0" max="1000" onchange="definirBordure3()">
    <select name="choixStyleArrondi" id="choixStyleArrondi" onchange="definirBordure3()">
        <option value="none">aucune</option>
        <option value="hidden">masquée</option>
        <option value="solid">trait</option>
        <option value="dashed">pointillée</option>
        <option value="dotted">point par point</option>
        <option value="double">double</option>
        <option value="groove">sous-épaisseur</option>
        <option value="ridge">sur-épaisseur</option>
        <option value="inset">sous-élevée</option>
        <option value="outset">sur-elevée</option>
    </select>
    <input type="color" id="choixCouleurArrondi" name="choixCouleurArrondi" onchange="definirBordure3()">
    <br>
    <label for="choixBTLR">Angle haut-gauche : </label>
    <input type="number" id="choixBTLR" name="choixBTLR" value="0" min="0" max="1000" onchange="definirBordure3()">
    <br>
    <label for="choixBTRR">Angle haut-droit : </label>
    <input type="number" id="choixBTRR" name="choixBTRR" value="0" min="0" max="1000" onchange="definirBordure3()">
    <br>
    <label for="choixBBRR">Angle bas-droit : </label>
    <input type="number" id="choixBBRR" name="choixBBRR" value="0" min="0" max="1000" onchange="definirBordure3()">    
    <br>
    <label for="choixBBLR">Angle bas-gauche : </label>
    <input type="number" id="choixBBLR" name="choixBBLR" value="0" min="0" max="1000" onchange="definirBordure3()">    
</form>

```css title="Code CSS"
p {                                 /* Mise en forme des paragraphes */
    border: 0px none #000000;       /* Épaisseur - Apparence - Couleur */
    border-radius: 0px 0px 0px 0px; /* Haut-Gauche Haut-Droit Bas-Droit Bas-Gauche */
}
```

Pour obtenir le même arrondi à tous les angles, on ne donne qu'une seule valeur :

```css
border-radius: 5px;
```

On trouvera le détail sur [cette page de MDN](https://developer.mozilla.org/fr/docs/Web/CSS/border-radius).

## Exercice n°1

Compléter le code Css ci-dessous pour que :

1. L'`article` soit encadré d'une bordure en pointillés noirs d'épaisseur 1 pixel. 
2. Le titre `h3` se présente sous la forme suivante :

![Exemple](bordures_ex_titre.png)

```html
<!-- Partie html à ne pas modifier -->
<article>
    <h3> Un titre </h3>
    <p>
        A méditer :
        <q>Une fois qu'on a donné son opinion, il serait logique
        qu'on ne l'ait plus.</q>
    </p>
    <p>
        L'élément en-ligne <code>&lt;q&gt; &lt;/q&gt;</code> sert
        à baliser une citation courte dans la ligne.
    </p>
</article>
```

```css
/* Partie css modifiable */
article {

}

h3 {

}
```

## Exercice n°2

![Angles](bordures_ex_angle.png){ align=right ; width=30% }

Un bloc HTML n'a pas besoin de contenir de texte pour _exister_. En fixant ses dimensions _(`width` et `height`)_ dans
la feuille CSS et en lui attribuant des bordures, on le voit apparaître.

Modifier le CSS pour obtenir une bordure ressemblant à la bordure entourant le bloc dans l'image ci-contre.

```html
<!-- Partie html à ne pas modifier -->
<p>
</p>
```

```css
/* Partie css modifiable */
p{
    width : 200px;                    /* On fixe la largeur du bloc */
    height: 100px;                    /* On fixe la hauteur du bloc */
    border: 5px ridge grey;            /* On fixe l'aspect de la bordure */
}
```

??? question "Une piste ?"

    Les dimensions des arrondis ne sont pas obligatoirement en pixels. Une dimension donnée en pixels donnera un coin
    de bordure en forme de quart de cercle. Une dimension donnée en pourcentage donnera un quart d'ellipse de
    dimensions proportionnelles aux dimensions de la boîte à encadrer.

_Travail originel CC-BY-NC-SA : Nicolas Buyle-Bodin, Jean-Manuel Meny et Mcikaël Bordonaro. Autres modifications par
Cédric Frayssinet et Maxime Chourré_