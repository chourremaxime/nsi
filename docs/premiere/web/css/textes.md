La mise en forme des textes permet de changer les polices de caractères, de mettre en gras, en couleur, en italique,
de souligner et beaucoup d'autres choses encore :

- `font-size` permet de préciser la taille de police utilisée. Il existe deux types d'unités :
  - les unités absolues : px _(pixels)_, cm _(centimètres)_, ...
  - les unités relatives : % _(par rapport à la taille de police du conteneur)_, em, etc.
- `font-family` permet de spécifier la famille de police pour le texte.<br>
  _Comme une police donnée n'est pas nécessairement présente sur le poste de l'utilisateur, on précise en général
  plusieurs choix séparés par des virgules._
- `font-weight` permet de définir la graisse de la police ;
- `font-style` permet d'afficher du texte en italique ;
- `text-decoration` permet de souligner l'élément sélectionné ;
- `font-variant` permet d'afficher du texte en petites majuscules.

## Bac à sable : Mettre en forme les textes

Dans ce bac à sable, les dimensions sont en pixels.

<p id="boiteIllustrative">
Ci-contre, on peut modifier à loisir l'apparence du texte du paragraphe.
</p>

<form id="choixAttributsTextes">
    <label for="choixCouleur">Couleur : </label>
    <input type="color" id="choixCouleur" name="choixCouleur" onchange="definirTexte()">
    <br>
    <label for="choixTaille">Taille : </label>
    <input type="range" name="choixTaille" id="choixTaille" min="6" max="50" value="18" step="1" onchange="definirTexte() ; rangevalue.value=value">
    <output id="rangevalue">18</output>
    <br>
    <label for="choixPolice">Police de Caractère : </label>
    <select name="choixPolice" id="choixPolice" onchange="definirTexte()">
        <option value="Times New Roman">Times New Roman</option>
        <option value="Impact">Impact</option>
        <option value="Arial">Arial</option>
        <option value="Verdana">Verdana</option>
        <option value="Helvetica">Helvetica</option>
        <option value="Courier New">Courier New</option>
        <option value="cursive">cursive</option>
    </select>
    <br>
    <label for="choixGraisse">Épaisseur : </label>
    <select id="choixGraisse" name="choixGraisse" onchange="definirTexte()">
        <option value="normal">normal</option>
        <option value="bold">gras</option>
        <option value="bolder">très gras</option>
        <option value="lighter">fin</option>
    </select>
    <br>
    <label for="choixItalique">Style : </label>
    <select id="choixItalique" name="choixItalique" onchange="definirTexte()">
        <option value="normal">normal</option>
        <option value="italic">italique</option>
    </select>
    <br>
    <label for="choixDecoration">Decoration : </label>
    <select id="choixDecoration" name="choixDecoration" onchange="definirTexte()">
        <option value="none">normal</option>
        <option value="underline">souligné</option>
        <option value="overline">surligné</option>
        <option value="line-through">barré</option>
    </select>
    <br>
    <label for="choixMajuscules">Affichage : </label>
    <select id="choixMajuscules" name="choixMajuscules" onchange="definirTexte()">
        <option value="normal">normal</option>
        <option value="small-caps">petites majuscules</option>
    </select>
</form>

```css title="Code CSS"
p {                                 /* Mise en forme des paragraphes */
    color : #000000;                /* Couleur du texte */
    font-size : 11px;               /* Taille de la police en pixels */
    font-family : Times New Roman;  /* Police de caractère */
    font-weight : normal;           /* Graisse de la police */
    font-style : normal;            /* Apparence de la police */
    text-decoration : none;         /* Décoration de la police */
    font-variant : normal;          /* Affichage de la police */
}
```

## Bac à sable : Les ombres

Dans ce bac à sable, on peut attribuer une ombre au texte ou bien au conteneur du texte _(ici le paragraphe)_.

<p id="boiteIllustrative2" style="text-shadow: rgb(0, 0, 0) 6px -6px 2px; line-height: 1.5; box-shadow: rgb(0, 0, 255) -4px 7px 5px;">
L'homme qui tire plus vite que son ombre !
</p>

<form id="choixAttributsOmbres">
    <label><strong>Ombrer le texte</strong></label><br>
    <label for="decalageHorizontalTexte">Décalage horizontal (<em>px</em>) :</label>
    <input type="number" id="decalageHorizontalTexte" value="6" min="-20" max="20" onchange="ombrer()">
    <br>
    <label for="decalageVerticalTexte">Décalage vertical (<em>px</em>) :</label>
    <input type="number" id="decalageVerticalTexte" value="-6" min="-20" max="20" onchange="ombrer()">
    <br>
    <label for="flouTexte">Floutage (<em>px</em>) :</label>
    <input type="number" id="flouTexte" value="2" min="0" max="10" onchange="ombrer()">
    <br>
    <label for="couleurOmbreTexte">Couleur : </label>
    <input type="color" id="couleurOmbreTexte" name="couleurOmbreTexte" onchange="ombrer()">
    <br>
    <label for="interligne">Interligne :</label>
    <input type="number" id="interligne" value="1.5" step="0.1" min="0.5" max="5" onchange="ombrer()">
    <br>
    <br>
    <label><strong>Ombrer le conteneur</strong></label><br>
    <label for="decalageHorizontalBoite">Décalage horizontal (<em>px</em>) :</label>
    <input type="number" id="decalageHorizontalBoite" value="-4" min="-20" max="20" onchange="ombrer()">
    <br>
    <label for="decalageVerticalBoite">Décalage vertical (<em>px</em>) :</label>
    <input type="number" id="decalageVerticalBoite" value="7" min="-20" max="20" onchange="ombrer()">
    <br>
    <label for="flouBoite">Floutage (<em>px</em>) :</label>
    <input type="number" id="flouBoite" value="5" min="0" max="10" onchange="ombrer()">
    <br>
    <label for="couleurOmbreBoite">Couleur : </label>
    <input type="color" id="couleurOmbreBoite" name="couleurOmbreBoite" value="#0000FF" onchange="ombrer()">
</form>

```css title="Code CSS"
p {                                     /* Mise en forme des paragraphes */
    text-shadow: 6px -6px 2px #000000;
    line-height: 1.5;
    box-shadow: -4px 7px 5px #0000ff;
}
```

## Exercice n°1

Dans le fichier qui suit, sans toucher au code HTML :

1. Encadrer par une bordure verte chacune des listes.
2. Mettre sous fond jaune le dernier item de chaque liste.
3. Écrire en italique le premier item de chaque liste.
4. Écrire en petite majuscule l'élément d'identifiant `id="important"`.
5. Écrire en gras les éléments d'attribut `class="coucou"`.

```html
<!-- Partie html à ne pas modifier -->
<p id="important" class="coucou">
    La liste ci-dessous est vraiment importante :
</p>
<ul>
    <li>élément 1</li>
    <li class="coucou">élément 2</li>
    <li>élément 3</li>
    <li>élément 4</li>
</ul>

<p  class="coucou">
    Cette seconde liste l'est un peu moins.
</p>
<ul>
    <li>élément 1</li>
    <li>élément 2</li>
    <li class="coucou">élément 3</li>
</ul>

<p>
    Cette dernière liste semble peu intéressante.
</p>       

<ul>
    <li class="coucou">élément a</li>
    <li>élément b</li>
    <li>élément c</li>
</ul>
```

```css
/* Partie css modifiable */
ul {
    
}
```

??? question "Une piste ?"

    En plus du pseudo-élément `:nth-of-type()`, on peut utiliser les sélecteurs
    [`:first-of-type`](https://developer.mozilla.org/fr/docs/Web/CSS/:first-of-type) et
    [`:last-of-type`](https://developer.mozilla.org/fr/docs/Web/CSS/:last-of-type) pour
    cibler respectivement le premier et le dernier élément d'un conteneur parent.

_Travail originel CC-BY-NC-SA : Nicolas Buyle-Bodin, Jean-Manuel Meny et Mcikaël Bordonaro. Autres modifications par
Cédric Frayssinet et Maxime Chourré_