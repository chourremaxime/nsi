Chaque élément HTML est contenu dans une boîte _(un bloc)_. Pour comprendre le comportement des boîtes,
il faut savoir ce que désigne chaque dimension utilisée.

![Boîte](boite.svg){ align=right ; width=30% }

Par défaut, les propriétés `width` et `height` désignent la largeur et la hauteur du contenu de la boîte.
Pour avoir les dimensions de la boîte complète, il faut donc ajouter la valeur de :

- `padding`, dimensions entre le contenu et les bordures ;
- `border-width`, taille des bordures de la boîte ;
- `margin`, dimensions extérieures de la boîte.

Les propriétés précédentes peuvent être différenciées entre le haut (`-top`), le bas (`-bottom`), la gauche (`-left`)
et la droite (`-right`).

Il est aussi possible de définir des dimensions minimum et maximum pour la largeur et la hauteur d'une boîte grâce aux
propriétés `min-width`, `max-width`, `min-height` et `max-height` (détails à retrouver sur MDN).

!!! info "Voir les différents éléments"

En utilisant l'inspecteur du navigateur, on peut visualiser les marges des différents éléments. Essayez avec le bac à
sable, en faisant _clic droit_ puis _Inspecter_ et sélectionnez l'onglet _Mise en page_ sur la droite.

## Bac à sable : Visualiser les différentes dimensions

Dans ce bac à sable, les dimensions sont en pixels et les valeurs haut-bas-gauche-droite sont identiques. Ne pas
hésiter à tester des valeurs négatives pour les marges intérieures (`padding`) et/ou extérieures (`margin`).

<p id="boiteIllustrative">
Ci-contre, commencez par choisir les dimensions du contenu de la boîte. Ajoutez ensuite une bordure, un espace entre
le contenu et la bordure puis un espace extérieur à la boîte.
</p>

<form id="choixAttributsDimensions">
    <label for="choixLargeur">Largeur du contenu : </label>
    <input type="number" id="choixLargeur" name="choixLargeur" value="360" min="150" max="450" onchange="definirDimensions()">
    <br>
    <label for="choixHauteur">Hauteur du contenu : </label>
    <input type="number" id="choixHauteur" name="choixHauteur" value="100" min="40" max="300" onchange="definirDimensions()">
    <br>
    <label>Bordure :</label><br>
    <input type="number" id="choixEpaisseur" name="choixEpaisseur" value="0" min="0" max="1000" onchange="definirDimensions()">
    <select name="choixStyle" id="choixStyle" onchange="definirDimensions()">
        <option value="none">aucune</option>
        <option value="hidden">masquée</option>
        <option value="solid">trait</option>
        <option value="dashed">pointillée</option>
        <option value="dotted">point par point</option>
        <option value="double">double</option>
        <option value="groove">sous-épaisseur</option>
        <option value="ridge">sur-épaisseur</option>
        <option value="inset">sous-élevée</option>
        <option value="outset">sur-elevée</option>
    </select>
    <input type="color" id="choixCouleur" name="choixCouleur" onchange="definirDimensions()">
    <br>
    <label for="choixPadding">Padding : </label>
    <input type="number" id="choixPadding" name="choixPadding" value="0" min="-50" max="50" onchange="definirDimensions()">
    <br>
    <label for="choixMargin">Margin : </label>
    <input type="number" id="choixMargin" name="choixMargin" value="0" min="-50" max="50" onchange="definirDimensions()">
</form>

```css title="Code CSS"
p {                                /* Mise en forme des paragraphes */
    width: 360px;                  /* Largeur du contenu */
    height: 100px;                 /* Hauteur du contenu */
    border: 0px none #000000;      /* Épaisseur - Apparence - Couleur */
    padding: 0px;                  /* Espace "intérieur" */
    margin: 0px;                   /* Espace "extérieur" */
}
```

## Exercice n°1

Compléter le code Css ci-dessous pour que :

1. L'`article` ait une marge extérieure de 50 pixels et une marge intérieure de 20 pixels.
2. Le titre `h3` ait une marge intérieure gauche de 10 pixels et une marge extérieure basse de 10 pixels
3. Les paragraphes aient une marge extérieure haute de 25 pixels

```html
<!-- Partie html à ne pas modifier -->
<article>
    <h3> Un titre </h3>
    <p>
        A méditer :
        <q>Une fois qu'on a donné son opinion, il serait logique
        qu'on ne l'ait plus.</q>
    </p>
    <p>
        L'élément en-ligne <code>&lt;q&gt; &lt;/q&gt;</code> sert
        à baliser une citation courte dans la ligne.
    </p>
</article>
```

```css
/* Partie css modifiable */
article {
    border: 1px dotted black;
}

h3 {
    border-left: 5px ridge;
    border-bottom: 5px ridge;
    color: grey;
}
```

_Travail originel CC-BY-NC-SA : Nicolas Buyle-Bodin, Jean-Manuel Meny et Mcikaël Bordonaro. Autres modifications par
Cédric Frayssinet et Maxime Chourré_