## Cours

Pour appliquer des règles de style CSS aux éléments d'une page `.html`, il faut pouvoir sélectionner ces éléments
_(et uniquement ceux que l'on veut styler)_. Il existe pour cela différentes sortes de sélecteurs. Voici une
description des sélecteurs les plus utilisés :

### Sélecteurs d'éléments

Une règle Css telle que :

```css
p {
    color: darkblue;
}
```

concernera tous les éléments de type paragraphe.

On peut ainsi facilement définir des comportements _par défaut_ pour les éléments essentiels d'une page _(ce qui a
déjà été fait dans les exercices précédents)_.

### Sélecteurs de classe

Il peut exister plusieurs types de paragraphes dans une page web. Pour les distinguer, il faut définir des **groupes
de balises** à l'aide de l'attribut `class`.

Voici comment écrire en vert des paragraphes correspondants aux énoncés d'exercices :

```html
<!-- Partie html -->
<p class="enonce">
    Un énoncé d'exercice.
</p>
```

```css
/* Partie css */
.enonce {                /* ATTENTION : ne pas oublier le "." devant le nom de la classe */
    color: darkgreen;
}
```

### Sélecteurs d'identifiant

On peut aussi désigner un élément unique dans une page avec l'attribut `id`. Cet élément sera unique dans la page,
mais il pourra être présent dans plusieurs pages du site.

Voici comment écrire en rouge le paragraphe de conclusion de la page :

```html
<!-- Partie html -->
<p id="conclusion">
    Conclusion de la page.
</p>
```

```css
/* Partie css */
#conclusion {            /* ATTENTION : ne pas oublier le "#" devant le nom de l'identifiant */
    color: darkred;
}
```

### Conclusion sur les sélecteurs

Les attributs de classe et/ou d'identifiant s'utilisent aussi avec les balises génériques `<div>` _(de type block)_
et `<span>` _(de type en-ligne)_. Une façon sympathique et efficace de faire le tour des sélecteurs est de traiter
[les situations proposées à cette adresse](https://cssdiner.com). Prenez le temps de lire le cours qui se trouve à
droite de la fenêtre dans chaque situation.

On trouvera également une liste exhaustive des sélections possibles sur
[cette page de MDN](https://developer.mozilla.org/fr/docs/Learn/CSS/Building_blocks/Selectors).

## Exercices

### Exercice n°1

Dans le fichier `.html` ci-dessous, les trois paragraphes sont encadrés par une bordure gauche et une bordure basse.
Modifier les parties HTML et CSS en utilisant des sélecteurs et des bordures de couleur `transparent` pour obtenir
l'affichage ci-dessous.

![Aperçu](selecteurs_ex1.png)

On pourra augmenter l'épaisseur des bordures pour mieux voir les effets...

```html
<!-- Partie html à modifier -->
<p>
    Ce paragraphe a une bordure basse et une bordure gauche visibles.
</p>

<p>
    Pour ce paragraphe, bordures basse et gauche sont les mêmes que
    précédemment. Les autres bordures sont invisibles mais leur effet
    est visible.
</p>

<p>
    <!-- Un paragraphe vide... -->
</p>
```

```css
/* Partie css modifiable */
p {
    color: purple;
    border-left: 15px ridge;
    border-bottom: 15px ridge;
}
```

### Exercice n°2

En CSS, il existe un sélecteur permettant de repérer le **numéro de présence** d'un élément dans une page .html. Ce
sélecteur est le **pseudo-élément** `:nth-of-type(n)`. Voici comment écrire en violet le 3ᵉ titre de niveau `<h1>` présent
dans la page `.html` : 

```css
h1:nth-of-type(3) {        /* ne pas oublier le ":" !!! */
    color: purple;
}
```

Ce pseudo-élément permet aussi de repérer les numéros pairs et impairs à partir d'expressions de la forme `an+b`,
où `a` et `b` sont les nombres qui définissent la règle de calcul. En utilisant cette information, définir le CSS
permettant d'obtenir l'affichage ci-dessous.

![Aperçu](selecteurs_ex2.png)

```html
<!-- Partie html à ne pas modifier -->
<p> zigzag </p>
<p> circonvolution </p>
<p> détour </p>
<p> crochet </p>
<p> courbe </p>
<p> tordu </p>
<p> biscornu </p>
<p> zigvolution</p>
<p> circonzag</p>
```

```css
/* Partie css modifiable */
p {
    border-bottom: 3px solid red;
    width: 250px;
    margin: 0;                        /* Pas de marge extérieur */
}
```

_Travail originel CC-BY-NC-SA : Nicolas Buyle-Bodin, Jean-Manuel Meny et Mcikaël Bordonaro. Autres modifications par
Cédric Frayssinet et Maxime Chourré_