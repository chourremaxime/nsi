## Cours

Centrer est parfois difficile. On abordera quelques méthodes dans cette page, mais on trouvera un article bien plus
exhaustif (et en anglais) à ce sujet sur le [site css-tricks](https://css-tricks.com/centering-css-complete-guide/).

### Centrer du texte horizontalement

Les valeurs possibles pour la propriété `text-align` sont `left`, `right`, `center` et `justify`. La largeur de la
boîte contenant le texte doit être définie préalablement.

- Par exemple, les deux fichiers suivants :
```css
/* Feuille css */
p {
    text-align: center;
    border: 1px solid brown; 
}
```
```html
<!-- Fichier html -->
<p>
    Avec la règle précédente, le texte est centré horizontalement dans la
    boîte <code>&lt;p&gt;</code>.
</p>
```
donnent l'affichage ci-dessous :
<p style="text-align: center; border: 1px solid brown;">
Avec la règle précédente, le texte est centré horizontalement dans la boîte <code>&lt;p&gt;</code>.
</p>

### Astuce sur le centrage horizontal

On peut centrer horizontalement un bloc _(de largeur `width` définie explicitement)_ dans un autre bloc en déclarant
les marges gauche et droite à la valeur `auto`.

- Appliquer la propriété :
```css
margin: 0 auto;
```
donne une marge extérieure haute et basse de 0 pixels et centre horizontalement le bloc selectionné dans le bloc
conteneur.

### Hauteur d'une ligne de texte

`line-height` est la hauteur totale d'une ligne _(c'est donc la hauteur entre deux lignes)_. Il sera intéressant de
jouer sur cette propriété pour **centrer verticalement un élément de type en-ligne** grâce à la propriété
`vertical-align`. Parmi les valeurs possibles, on trouvera `middle`, `top`, `bottom`...

- Les deux fichiers suivants :
```css
/* Feuille css */
p {
    border: 1px solid brown;
    height: 40px;
    line-height: 40px;       /* égale à height pour centrage vertical */
}
```
```html
<!-- Fichier html -->
<p>
    Texte
</p>
```
donnent l'affichage ci-dessous :
<p style="border: 1px solid brown; height: 40px; line-height: 40px">texte</p>
- Les deux fichiers suivants :
```css
/* Feuille css */
p {
    border: 1px solid brown;
    height: 40px;
    line-height: 20px;
}
```
```html
<!-- Fichier html -->
<p>
    Texte
</p>
```
donnent l'affichage ci-dessous :
<p style="border: 1px solid brown; height: 40px; line-height: 20px">texte</p>
- Les deux fichiers suivants :
```css
/* Feuille css */
p {
    border: 1px solid brown;
    line-height: 40px;
}
img {
    vertical-align: bottom;
}
```
```html
<!-- Fichier html -->
<p>
    Une image bottom <img src="pthtml.png"> dans une boîte
    <code>&lt;p&gt;</code>.
</p>
```
donnent l'affichage ci-dessous :
<p style="border: 1px solid brown; line-height: 40px;">
    Une image bottom <img src="../pthtml.png" style="vertical-align: bottom;"> dans une boîte
    <code>&lt;p&gt;</code>.
</p>
- Les deux fichiers suivants :
```css
/* Feuille css */
p {
    border: 1px solid brown;
    line-height: 40px;
}
img {
    vertical-align: middle;
}
```
```html
<!-- Fichier html -->
<p>
    Une image middle <img src="pthtml.png"> dans une boîte
    <code>&lt;p&gt;</code>.
</p>
```
donnent l'affichage ci-dessous :
<p style="border: 1px solid brown; line-height: 40px;">
    Une image middle <img src="../pthtml.png" style="vertical-align: middle;"> dans une boîte
    <code>&lt;p&gt;</code>.
</p>


### Bac à sable : Boîtes _flexibles_

La propriété CSS `display: flex;` permet de définir un _conteneur flexible_. Ses enfants _(les éléments contenus
dans cette boîte)_ deviennent alors automatiquement _(sans déclarer quoi que ce soit)_ flexibles.

La boîte encadrée d'une bordure noire peut devenir _flexible_, les blocs enfants sont encadrés de pointillés.
On trouvera un article plus complet sur le site
[alsacréations](http://www.alsacreations.com/tuto/lire/1493-css3-flexbox-layout-module.html).

<div id="boiteIllustrative" style="border: 1px solid; display: flex; flex-flow: row; justify-content: flex-start; align-items: flex-start;">
    <div class="fils"> fils 1 </div>
    <div class="fils"> fils 2 </div>
    <div class="fils"> fils 3 </div>
    <div class="fils"> fils 4 </div>
    <div class="fils"> fils 5 </div>
    <div class="fils"> fils 6 </div>
    <div class="fils"> fils 7 </div>
    <div class="fils"> fils 8 </div>
    <div class="fils"> fils 9 </div>
    <div class="fils"> fils 10 </div>
</div>

<form id="choixAttributsFlex">
    <label>Direction :</label>
    <select name="choixDirection" id="choixDirection" onchange="definirFlex()">
        <option value="row">horizontale</option>
        <option value="row-reverse">horizontale-inversée</option>        
        <option value="column">verticale</option> 
        <option value="column-reverse">verticale-inversée</option> 
    </select>
    <br>
    <label>Passage à la ligne :</label>
    <select name="choixWrap" id="choixWrap" onchange="definirFlex()">
        <option value="nowrap">non</option>
        <option value="wrap">oui</option>        
        <option value="wrap-reverse">inversé</option>
    </select>    
    <br>
    <label>Alignement dir. horizontale :</label>
    <select name="choixAlign1" id="choixAlign1" onchange="definirFlex()">
        <option value="flex-start">début</option>
        <option value="flex-end">fin</option>
        <option value="center">centré</option>
        <option value="space-between">justifié_1</option>
        <option value="space-around">justifié_2</option>
    </select>
    <br>
    <label>Alignement dir. verticale :</label>
    <select name="choixAlign2" id="choixAlign2" onchange="definirFlex()">
        <option value="flex-start">début</option>
        <option value="flex-end">fin</option>
        <option value="center">centré</option>
        <option value="stretch">étiré</option>
    </select>
</form>

```css title="Code CSS"
p {     /* Mise en forme des paragraphes */
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    justify-content: flex-start;
    align-items: flex-start;
}
```

## Exercices

### Exercice n°1

Centrer horizontalement et verticalement le texte du paragraphe `<p>` dans l'article.

```html
<!-- Partie html à ne pas modifier -->
<article>
    <p>
        Les petits poissons rouges, les petits pois sont verts.
        A force de taper sur des clous, il était devenu marteau.
    </p>
</article>
```

```css
/* Partie css modifiable */
article {
    width: 450px;
    height: 180px;
    border: 2px solid blue;
}

p {
    width: 50%;
}
```