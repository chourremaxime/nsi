Vous devez modifier le minisite web en téléchargement ci-dessous. Vous avez le droit de modifier le code HTML et le
code CSS, laissez parler votre imagination !

Il vous faudra toutefois respecter les contraintes imposées. Vous serez évalué sur les critères suivants :

- Respect des contraintes imposées.
- Contenu des pages.
- Travail sur le code HTML, CSS.

[Télécharger le minisite](Projet_HTML_CSS.zip){ .md-button download="Projet_HTML_CSS.zip" }

Comme écrit dans les consignes, ce projet est **à rendre en ligne sur Éléa**.