Pour inviter un utilisateur à prendre contact avec l'administrateur du site, on peut mettre en place un formulaire,
plus convivial à rédiger qu'un simple lien vers une adresse de courriel.

Tous les formulaires doivent être encadrés par la balise bloc `<form>` contenant les attributs suivants :
- l'attribut `method="post"` indique le type de méthode utilisée pour l'envoi des données ;
- l'attribut `action="..."` indique l'URL de traitement des données.

Nous ne verrons pas dans ce cours comment traiter les résultats d'un formulaire.

!!! syntaxe "Regrouper les parties"

    Afin de séparer les parties d'un long formulaire, on peut utiliser la balise bloc `<fieldset>` suivie d'une autre
    balise `<legend>` contenant le texte à afficher au début du bloc.

!!! syntaxe "Étiqueter les champs"

    Avant ou après les champs de formulaire, on pourra ajouter des étiquettes pour indiquer à quoi servira le champ.
    Par exemple, on peut indiquer `Prénom` devant une zone de texte.
    
    On utilisera alors la balise en-ligne `<label>` avec l'attribut `for="..."` contenant l'identifiant du champ que
    l'on souhaite étiqueter.

Voici une liste non-exhaustive de champs utilisables dans un formulaire. Cliquez dessus pour consulter leur
documentation complète :
- 