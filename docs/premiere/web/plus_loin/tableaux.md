L'élément `<table>` sert à représenter des tableaux.
Les balises indispensables pour définir un tableau sont :

- les balises `<table>...</table>`, de type bloc, qui déclarent la création du tableau ;
- les balises `<tr>...</tr>` _(table row)_, de type bloc, qui définissent une nouvelle ligne du tableau ;
- les balises `<td>...</td>` _(table data)_, de type en-ligne, qui définissent une nouvelle cellule du tableau dans
  la ligne.

Il existe d'autres balises, mais elles ne seront pas forcément décrites ici.

!!! example "Un premier exemple"

    ![Tableau](tableau01.png){ align=right ; width=30% }
    
    Le code entré ci-dessous sert à obtenir le tableau ci-contre.
    
    Par défaut, les cellules ne sont pas encadrées, les colonnes sont alignées et il y a un retour à la ligne à chaque
    nouvelle ligne du tableau.
    
    ```html
    <!DOCTYPE html>
    <html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title> Tableaux </title>
    </head>
    <body>
        <table>
            <tr>    <td> Elément 1.1 </td>
                    <td> Elément 1.2 </td>
                    <td> Elément 1.3 </td> </tr>
            <tr>    <td> Elément 2.1 </td>
                    <td> Elément 2.2 </td>
                    <td> Elément 2.3 </td> </tr>
        </table>
    </body>
    </html>
    ```
    
    ![Tableau avec bordures](tableau02.png){ align=right ; width=30% }
    
    En ajoutant le fichier `.css` ci-dessous _(et en créant le lien adéquat dans la partie `<head>` du fichier HTML)_,
    on peut facilement afficher les bordures autour des cellules et du tableau :
    
    ```css
    table, td {                        /* encadrement du tableau */
        border: 1px solid black;    /* et de chaque cellule */
    }
    ```

Lorsqu'une cellule doit servir de titre (en première colonne ou première ligne), on peut utiliser les balises
`<th>...</th>` au lieu de `<td>...</td>`.

!!! example "Un second exemple - La fusion de cellules"

    ![Tableau avec span](tableau03.png)
    
    On peut regrouper _(fusionner)_ des cellules, c'est-à-dire faire en sorte qu'une cellule occupe plusieurs lignes ou
    plusieurs colonnes. Pour cela, on affecte les éléments cellule `<td>` ou `<th>` avec les attributs :
    
    - `rowspan=""`, à compléter par le nombre entier _(y compris la cellule portant cet attribut)_ de cellules à
      fusionner **en-dessous**.
    - `colspan=""`, à compléter par le nombre entier _(y compris la cellule portant cet attribut)_ de cellules à
      fusionner **à droite**.
    
    ```html
    <!DOCTYPE html>
    <html lang="fr">
    <head>
        <meta charset="ITF-8">
        <title>Fusionner</title>
    </head>
    <body>
        <table>
            <thead>
                <tr>    <!-- Fusion de 3 cellules sur la ligne -->
                        <th colspan="3">Enseignement obligatoire</th> 
                        <th>Spécialité</th> </tr>
            </thead>
        
            <tbody>
                <tr>    <!-- Fusion de 4 cellules sur la colonne -->
                        <td rowspan="4"> Mathématiques </td>
                        <td rowspan="4"> Sc. Physiques </td>
                        <td rowspan="4"> S.V.T. </td>
                        <td> I. S. N. </td> </tr>
                <tr>    <td> Maths </td> </tr>
                <tr>    <td> Sc. Physiques </td> </tr>
                <tr>    <td> S.V.T. </td> </tr>
            </tbody>
        </table>
    </body>
    </html>
    ```
    
    ```css
    /* Fichier CSS */
    table{
        border : 2px solid black;
        }
        
    th, td {
        border : 1px solid green;
        padding : 5px;
        }
    ```

## Exercice n°1 : Mon premier tableau

Donner un code permettant de réaliser le tableau ci-dessous. La mise en forme est effectuée par un fichier `.css`
importé grâce à la balise `<link>`.

![Tableau à obtenir](tableau05.png)

[Télécharger le fichier CSS](table_01.css){ .md-button ; download="table_01.css"}

```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Comparaison Html/Css</title>
    <link rel="stylesheet" href="table_01.css">
</head>
<body>
    <table>
        <!-- code à compléter ici -->
    </table>
</body>
</html>
```

## Exercice n°2 : Un tableau en morceaux

Donner le code nécessaire pour réaliser le tableau ci-dessous :

![Tableau avec spans](tableau06.png)

[Télécharger le fichier CSS](table_02.css){ .md-button ; download="table_02.css }
```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Fusion</title>
    <link rel="stylesheet" href="table_02.css">
</head>
<body>
     <table>
       <!-- code à compléter ici -->
     </table>
</body>
</html>
```

## Exercice n°3 : Un ancien mix

Écrire un **programme Python** qui crée un fichier HTML contenant une table de multiplication de 1 à 10 d'un nombre
donné par un utilisateur.

!!! info "Créer un fichier avec Python"

    La syntaxe pour créer et remplir un fichier à l'aide de Python est la suivante :
    
    ```py
    fichier = open("nom_fichier.html", "w") # On crée (w) un fichier nom_fichier.html
    
    fichier.write("Bonjour !")  # On écrit dans le fichier
    
    fichier.close()  # On ferme et enregistre le fichier 
    ```

_Travail originel CC-BY-NC-SA : Nicolas Buyle-Bodin, Jean-Manuel Meny et Mcikaël Bordonaro. Autres modifications par
Cédric Frayssinet et Maxime Chourré_