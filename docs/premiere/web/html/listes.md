Les listes permettent de faire apparaître plusieurs lignes de texte sur des lignes séparées.

!!! syntaxe "Listes non ordonnées"

    ![Listes non ordonnées](ul.png){ align=right }
    
    Structure du code Html :
    
    ```html
    <ul>                    <!-- ul : unordered list -->
        <li> Item 1 </li>   <!-- li : list item -->
        <li> Item 2 </li>
        <li> Item 3 </li>
    </ul>
    ```

!!! syntaxe "Listes ordonnées"

    ![Listes ordonnées](ol.png){ align=right }
    
    Structure du code Html :
    
    ```html
    <ol>                    <!-- ol : ordered list -->
        <li> Item 1 </li>   <!-- li : list item -->
        <li> Item 2 </li>
        <li> Item 3 </li>
    </ol>
    ```

!!! note "Remarque"

    Il est aussi possible de changer la numérotation grâce à l'attribut `type` :

    - `type="1"` pour un compteur 1, 2, 3...,
    - `type="a"` pour un compteur a, b, c...,
    - `type="A"` pour un compteur A, B, C,...,
    - `type="I"` pour un compteur I, II, III...,

    Cependant, ce sera plutôt au fichier .css de s'occuper de ce type de mise en forme...

## Exercice n°1 : Listes personnalisées

![Liste](liste_exercice.png){ align=right }

En s'inspirant du code ci-dessous, écrire le code HTML permettant d'afficher la liste ci-contre.

```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Liste d'éléments html</title>
</head>
<body>
<ol type="A">
    <li> Item 1</li>
    <li> Item 2</li>
    <li> Item 3</li>
</ol>

</body>
</html>
```

## Exercice n°2 : Suite des favoris

Reprendre le fichier `favoris.html` avec un éditeur de texte.

1. Dans ce fichier, ajouter les trois sites suivants sous forme d'une liste non ordonnée après le second titre :
    - Espace Numérique de Travail
    - Site du cours et des exercices
    - Mon site préféré
2. Ouvrir `favoris.html` à l'aide du navigateur pour constater le rendu. Ne pas hésiter à appeler le professeur pour
   vérification.

Pour l'instant, on ne fait pas de liens hypertextes vers ces sites.

_Travail originel CC-BY-NC-SA : Nicolas Buyle-Bodin, Jean-Manuel Meny et Mcikaël Bordonaro. Autres modifications par
Cédric Frayssinet et Maxime Chourré_