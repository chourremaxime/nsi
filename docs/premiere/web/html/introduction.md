Un site web est un ensemble de dossiers et de fichiers, reliés entre eux grâce à
des [liens hypertextes](https://fr.wikipedia.org/wiki/Lien_hypertexte). De manière simplifiée, on aura un dossier
principal qui contient la page d'accueil du site, au format HTML (HyperText Markup Language) et plusieurs
sous-dossiers :

![Sous-dossiers d'un site](sous_dossiers.png)

On organise les dossiers pour qu'ils contiennent le même type de fichiers. Par exemple,

- un dossier contient les autres pages du site, au format [HTML](principes.md).
- un dossier contient la mise en forme de toutes les pages du site, au moyen d'un (ou plusieurs) fichiers au
  format [CSS](../css/introduction.md) (Cascading Style Sheets).
- un dossier contient les images affichées par le site, reliées aux fichiers `.html` et `.css` grâce aux liens
  hypertextes
- d'autres dossiers peuvent contenir des vidéos, des documents à télécharger ou des fichiers javascript permettant de
  rendre l'affichage plus dynamique. Ce format de fichier ne sera pas abordé ici.

![Contenu de chaque dossier](detail_dossiers.png)

## Deux Langages pour une page Web

Lorsqu'un appareil numérique (ordinateur, smartphone…) se connecte à un site web, c'est le **navigateur internet** (
Firefox, Safari, Chrome, Edge, etc.) qui remplit le rôle de *décodeur*, de «passerelle» entre le contenu de la page et
sa mise en forme.

![Logo HTML](logo_HTML5.png){ width=30px ; align=left } Le **contenu** de la page est stocké dans un fichier `.html`.

C'est en fait un **simple fichier texte** avec...

- ... une structure de lecture non linéaire _(HyperText)_.<br>
  *On peut sauter d'une partie du document à une autre, voire à un autre document...*
- ... des indications sur l'articulation logique du document _(Markup)_.<br>
  *Quel élément est un titre, quelle partie constitue un paragraphe, etc.*

![Logo CSS](logo_CSS3.png){ width=30px ; align=left } La **mise en forme** de la page est stockée dans un fichier
`.css`.

Lui aussi est un **simple fichier texte**...

- ... qui gère la mise en **forme** de chacune des pages du site web...
- ... en faisant référence aux articulations logiques du fichier `.html` _(titres, paragraphes…)_ pour personnaliser
  leur apparence _(couleur, positionnement…)_.
  *Ce titre doit être de couleur verte, ce paragraphe doit être en italique, etc.*

!!! info "Comment supprimer la mise en forme d'une page web ?"

    Pour comprendre l'importance du fichier `.css`, on peut désactiver son impact sous Firefox avec le menu
    [Affichage] → [Style de la page] → [Aucun style].

## Concevoir une page Web

Un simple bloc-note suffit pour écrire les fichiers `.html` et `.css`. Toutefois, utiliser un éditeur de texte avec
coloration syntaxique permet de travailler plus confortablement.

Pour illustrer ce cours, on utilisera :

![Encodage en UTF-8 (sans BOM)](encodage.png){ align=right }

![Logo Notepad++](logo_notepad_plus_plus.png){ width=30px ; align=left } L'éditeur de texte **Notepad++** pour Windows
ou **Geany** pour Linux.

Tout nouveau document avec cet éditeur est un fichier texte *classique* au format `.txt`. Pour bénéficier de la
coloration syntaxique, il faut l'enregistrer sous un nouveau nom en lui attribuant le format `.html` _(ou `.css` selon
les besoins)_ :

![Animation enregistrement](notepad_anim.gif){ align=left }

**Attention**, pour que les accents soient bien pris en charge, il est impératif de configurer **Notepad++** en mode *
*UTF-8 (sans BOM)** dans le menu **[Encodage]**.

Enfin, il faut vérifier que **Firefox** est bien le navigateur par défaut en double-cliquant sur un fichier `.html`
nouvellement créé ; sinon, clic droit sur le fichier et **[Ouvrir avec...]**...

_Travail originel CC-BY-NC-SA : Nicolas Buyle-Bodin, Jean-Manuel Meny et Mcikaël Bordonaro. Autres modifications par
Cédric Frayssinet et Maxime Chourré_