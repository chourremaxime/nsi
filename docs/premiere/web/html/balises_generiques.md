Les **balises génériques** et les **sélecteurs de balises** sont les notions les plus importantes lorsqu'on écrit du
code `.html`. Ces éléments permettent de distinguer des balises de même nature _(des paragraphes par exemple)_ qui n'ont
pas la même _finalité_. Par exemple, un paragraphe pour les définitions et un paragraphe pour les exemples.

## Définir un groupe de balises avec l'attribut `class`

L'attribut `class="nom_de_la_classe"` permet de distinguer et mettre en valeur de la même manière des **balises ayant
les mêmes caractéristiques**. Par exemple, dans une page formée de plusieurs paragraphes, on pourra distinguer :

- des paragraphes contenant du _texte simple_ qui seront délimités par `<p>` et `</p>` :<br>
  ```html
  <p>
    Un paragraphe classique.
  </p>
  ```
- des paragraphes servant d'énoncé d'exercices (par exemple).<br>
  On les regroupe dans une classe distincte avec `<p class="enonce">` et `</p>` :
  ```html
  <p class="enonce">
    Un paragraphe qui fait partie du groupe "enonce".
  </p>
  ```

## Balises universelles `div` et `span`

Parfois, il n'existe pas de balise spécifique qui corresponde à ce dont on a besoin. On utilise alors une **balise
générique** dotée de l'attribut `class=""` (à nommer...) :

- La balise `<div class=" ">...</div>` est de **type bloc**.
- La balise `<span class=" ">...</span>` est de **type en-ligne**.

Comme dans le cas des groupes de balises, c'est l'appel au nom de la classe dans le fichier `.css` qui permettra de
mettre en valeur le contenu de ces balises.

## Définir une balise de manière unique avec l'attribut `id`

Attribuer à une balise un **identifiant unique** permet de _naviguer_ facilement à l'intérieur d'une page _(d'un site)_
web et / ou de **distinguer un élément en particulier** pour une mise en page spécifique.

[//]: # (## Compléments)

[//]: # ()

[//]: # (On verra une utilisation très précise de ces balises, des classes et des identifiants...)

[//]: # ()

[//]: # (- dans le paragraphe Liens intra-page de l'onglet Arborescence ;)

[//]: # (- dans le paragraphe Sélecteurs de l'onglet Css.)

## Exercice n°1 : Le span, c'est la classe !

Pour visualiser l'imbrication et la dénomination des balises, on importe _(à nouveau)_ un fichier `.css`. Ce fichier
met en forme les **groupes de balises ayant pour nom de classe** `rouge`, `vert`, `bleu`, `gras`, `souligne` ou
`italique` _(les mises en forme obtenues sont évidentes)_.

En attribuant ces attributs à différentes balises `<span>`, essayez d'obtenir l'affichage ci-dessous :

![Texte](span_classes.png)

[Télécharger le fichier CSS](html_generique_01.css){ .md-button ; download="html_generique_01.css" }

```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Groupes de balises</title>
    <link rel="stylesheet" href="html_generique_01.css">
</head>
<body>
<p>
    Un exemple de texte coloré, chamarré, surchargé par une
    mise en forme bien trop abondante. Cela permet d'explorer
    l'imbrication des balises ainsi que la notion d'héritage
    des styles. Le plus dur étant toujours de faire un texte
    assez long pour pouvoir être intéressant.
</p>
</body>
</html>
```

## Exercice n°2 : Un beau menu déroulant !

Par défaut, chaque nouvel item d'une liste doit être affiché sous le précédent. Pour établir un menu déroulant, il
faut donc distinguer la liste formant les onglets du menu, les listes formant les items de chaque onglet et les
listes usuelles utilisées dans la page web.

![Menu déroulant](menu_deroulant.png)

Modifiez le code HTML ci-dessous pour obtenir un menu déroulant fonctionnel grâce au fichier `.css` importé.

Vous devrez uniquement rajouter les attributs suivants :
- `id="menu"`
- `class="onglet"`
- `class="item"`

[Télécharger le fichier CSS](html_generique_02.css){ .md-button ; download="html_generique_02.css" }

```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Classes et identifiants</title>
    <link rel="stylesheet" href="html_generique_02.css">
</head>
<body>
<ul>
    <li>Onglet n°1
        <ul>
            <li>Item 1 de l'onglet 1</li>
            <li>Item 2 de l'onglet 1</li>
            <li>Item 3 de l'onglet 1</li>
        </ul>
    </li>
    <li>Onglet n°2
        <ul>
            <li>Item 1 de l'onglet 2</li>
            <li>Item 2 de l'onglet 2</li>
        </ul>
    </li>
    <li>Onglet n°3
        <ul>
            <li>Item 1 de l'onglet 3</li>
            <li>Item 2 de l'onglet 3</li>
            <li>Item 3 de l'onglet 3</li>
            <li>Item 4 de l'onglet 3</li>
        </ul>
    </li>
    <li>Onglet n°4
        <ul>
            <li>Item 1 de l'onglet 4</li>
        </ul>
    </li>
</ul>
</body>
</html>
```

_Travail originel CC-BY-NC-SA : Nicolas Buyle-Bodin, Jean-Manuel Meny et Mcikaël Bordonaro. Autres modifications par
Cédric Frayssinet et Maxime Chourré_