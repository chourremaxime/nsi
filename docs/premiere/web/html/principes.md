Avant de concevoir un site internet dans son ensemble, il faut apprendre à rédiger / composer une page au format
`.html`. On **vérifie que son fichier est conforme** en le validant sur le [site du W3C](http://validator.w3.org/).

## Rédiger un fichier au format .html

!!! abstract "Balises"

    Un fichier `.html` a une structure d'éléments emboîtés appelés **balises**.
    
    - Ces balises ont des **noms différents** selon les informations qu'elles définissent / délimitent.
    - Une balise nommée `<balise>` marque le début d'une information. Cette partie doit être fermée avec `</balise>`.
    - Les balises s'imbriquent dans l'ordre, comme les parenthèses (ou les crochets) en mathématiques :
    
    ```html
    <a> ... <b> ... </b> ... </a>
    ```
    
    - Certaines balises comportent des attributs qui permettent de préciser davantage le contenu des balises.
      La valeur de chaque attribut est à indiquer entre guillemets :
    
    ```html
    <balise attribut="valeur">
    ```

## Page Html conforme

Un fichier .html conforme est composé comme ci-dessous. Les indentations ainsi que les commentaires ne sont pas
obligatoires *(ils ne sont pas interprétés par le navigateur)*, mais ils permettent de mieux se repérer dans le fichier
source.

```html
<!-- Un commentaire s'écrit entre ces deux symboles -->
<!DOCTYPE html>                        <!-- Codage du fichier, ici en HTML 5 -->

<html lang="fr">                    <!-- On ouvre le document rédigé en html -->

<head>                            <!-- Indications sur la page - non visible à l'écran -->
    <meta name="auteur" content="Mon identité">
    <title>Titre de la page - Lisible sur l'onglet du navigateur</title>
</head>                            <!-- Fermeture de l'en-tête -->

<body>                            <!-- Contenu de la page - visible à l'écran -->
<p>                            <!-- Un paragraphe de bla bla... -->
    <strong>Bla</strong> bla bla
</p>                        <!-- Fin du paragraphe -->
</body>                            <!-- Fermeture du corps de page -->

</html>                                <!-- Fermeture du document -->
```

Le fichier écrit ci-dessus donnera l'affichage :

![Résultat obtenu dans Firefox](exemple_navigateur.png)

On peut constater que trois types de balises se détachent :

- celles qui définissent des **blocs** comme `<html>`, `<head>`, `<body>` et `<p>` ;
- les balises **en-ligne** comme `<strong>`, qui s'appliquent à une phrase ou à une partie de phrase ;
- et les balises de type **marqueur** comme `<meta>`, qui sont *auto-fermantes*.

## Explorer le code-source avec le navigateur

Tous les navigateurs internet permettent d'afficher directement le code source du fichier .html consulté. Dans le cas du
navigateur Firefox, il suffit d'aller dans le menu :

- **[Outils]** → **[Développement web]** → **[Code source de la page]** (ou **[Ctrl]+[U]**).

Pourquoi ne pas essayer en explorant le code-source de cette page web ?

_Travail originel CC-BY-NC-SA : Nicolas Buyle-Bodin, Jean-Manuel Meny et Mcikaël Bordonaro. Autres modifications par
Cédric Frayssinet et Maxime Chourré_