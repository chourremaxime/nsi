Il y a six niveaux possibles de titre dans un fichier .html :

```html
<h1> Titre niveau 1 </h1>
<h2> Titre niveau 2 </h2>
<h3> Titre niveau 3 </h3>
<h4> Titre niveau 4 </h4>
<h5> Titre niveau 5 </h5>
<h6> Titre niveau 6 </h6>
```

Les navigateurs prédéfinissent un rendu par défaut de chaque niveau de titre. Les titres sont déclarés dans les
navigateurs comme éléments de type bloc.

## Exercice n°1 : Tester l'affichage

Testez le rendu par défaut de chaque niveau de titre entre les balises `<body>` :

```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title> La titraille </title>
</head>
<body>
<h1> Titre niveau 1 </h1>
<!-- À compléter -->
</body>
</html>
```

## Exercice n°2 : Suite des favoris

Reprendre le fichier `favoris.html`.

1. Dans ce fichier, remplacer :
    - son nom et son prénom en titre 1 ;
    - *Mes sites utiles en NSI* en titre 2.
2. Ouvrir `favoris.html` à l'aide du navigateur pour constater le rendu. Ne pas hésiter à appeler le professeur pour
   vérification.

## Exercice n°3 : Exemple de personalisation

Pour personnaliser son site, on redéfinit en général le rendu des titres grâce à un fichier `.css` importé par la
balise marqueur `<link>` _(inutile de s'attarder sur la syntaxe pour l'instant)_.

[Télécharger le fichier CSS](exemple.css){ download="exemple.css" }, puis tester à nouveau le rendu de chaque niveau de
titre dans le cas ci-dessous.

```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title> Exemples de titres </title>
    <link rel="stylesheet" href="exemple.css">
</head>
<body>

</body>
</html>
```

_Travail originel CC-BY-NC-SA : Nicolas Buyle-Bodin, Jean-Manuel Meny et Mcikaël Bordonaro. Autres modifications par
Cédric Frayssinet et Maxime Chourré_