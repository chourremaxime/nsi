## Cours

![Construction d'une page HTML](balises_HTML.png){ align=right }

Un fichier `.html` est un fichier texte dont les informations décrivent et organisent le contenu des pages. Les *
*balises** du fichier sont interprétées *(ceci est un titre, ceci est un paragraphe, ceci est une image, etc.)* puis la
page est affichée par le navigateur internet.

!!! syntaxe "Balises de type bloc"

    Il faut que le fichier contienne au minimum la balise bloc `<html>`.
    
    Les éléments de blocs sont, par défaut, affichés **les uns en-dessous des autres**. Ils peuvent contenir
    d'autres blocs, des éléments en-ligne et / ou des marqueurs.

!!! syntaxe "Balises de type en-ligne"

    Les balises de type en-ligne doivent être **à l'intérieur d'un bloc**.
    
    Les éléments en-ligne sont, par défaut, **affichés les uns à côté des autres**. Ils peuvent contenir d'autres
    éléments en-ligne.

!!! syntaxe "Balises de type marqueur"

    Ces balises **se suffisent à elle-mêmes**, elles n'ont **ni contenu, ni balise fermante**. Elles indiquent, par
    exemple, un saut à la ligne ou l'affichage d'une image.

!!! syntaxe "Balise `<head>`"

    Cette balise *bloc* contient de nombreuses informations qui ne sont **pas affichées à l'écran** mais qui permettent
    de préciser, entre autres, le nom de l'auteur, le titre à afficher dans l'onglet du navigateur et / ou le lien
    vers le fichier `.css` qui indiquera la mise en page/mise en forme générale de la page.

     Voici un exemple correct d'en-tête pour un fichier `.html` :

    ```html
    <head>                            <!-- Ouverture de l'en-tête -->
        <meta charset="UTF-8">        <!-- Pour les accents -->
        <meta name="author" lang="fr" content="Identité à personnaliser" />
        
        <title>Titre de la page - Lisible sur l'onglet du navigateur</title>
        
        <!-- Mise en page générale - Feuille de style CSS -->
        <link rel="stylesheet" href="lien_vers_la_feuille_de_style.css">
    
    </head>                            <!-- Fermeture de l'en-tête -->
    ```

!!! syntaxe "Balise `<body>`"

    Ce bloc se place sous le bloc `<head>`, il contient les éléments affichés par le navigateur internet.

## Exercices d'application

Avant tout, commencez par créer un répertoire _(un dossier)_ nommé HTML dans lequel vous pourrez stocker les différents
fichiers conçus au cours de cet enseignement.

1. Lancez un éditeur de texte (comme Notepad++).
2. Créez un **[Nouveau]** fichier.<br>![Nouveau fichier Notepad++](notepad_new.png)
3. L'**[Enregistrer sous...]** le nom `essai01.html` dans votre répertoire HTML.<br>
   ![Enregistrer sous Notepad++](notepad_saveas.png)
4. Vérifiez que le type de ce fichier est **[Hyper Text Markup Language]**.<br>
   ![Type de fichier Notepad++](notepad_html.png)
5. Allez dans le répertoire dans lequel vous avez sauvegardé ce fichier puis double-cliquer sur `essai01.html`.
   Normalement le navigateur internet _(Firefox)_ doit s'ouvrir… sur une page blanche. Si oui, fermez cette page, sinon,
   recommencez depuis la question 1.

### Exercice n°1 : Ma présentation

Recopier puis compléter le code nécessaire entre les deux balises `<body>` et `</body>` afin d'avoir une
prévisualisation qui ressemble à l'image ci-dessous (les pointillés sont à remplacer par son nom). Votre fichier sera
nommé `ma_presentation.html`.

![Code à créer](presentation.png)

```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Premières balises</title>
</head>
<body>
Bonjour...
</body>
</html>
```

??? question "Une piste ?"

    - Un paragraphe s'écrit entre les balises `<p>` et `</p>` ;
    - Un passage à la ligne est spécifié par la balise marqueur `<br>`.

    Attention, il vaut mieux distinguer plusieurs paragraphes plutôt que multiplier les retours à la ligne...

    Enfin, plusieurs parties du texte sont mises en valeurs grâce à des balises en-ligne telles que :
    `<strong>...</strong>` ; `<em>...</em>` ; `<code>...</code>` ; `<sup>...</sup>`.

### Exercice n°2 : Corriger les erreurs

Repérer et corriger toutes les erreurs de syntaxe dans le code ci-dessous.

Cliquer sur le bouton *Affichage à obtenir ?* pour voir l'affichage final qu'il faudra obtenir.

```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Corriger les erreurs</title>
</head>
<body>
<p>Créer des exemples de fichier <code>.html<code> est souvent
    <strong>fastidieux.</p></strong>
<p>En effet, outre les </em>problèmes de balises<em>, il faut
    aussi inventer un 1<sup>er</sup>, voire un 2<sub>d</sup> texte
        à insérer dans les balises.
</p>
</body>
</html>
```

??? info "Affichage à obtenir ?"

    Créer des exemples de fichier `.html` est souvent **fastidieux.**

    En effet, outre les _problèmes de balises_, il faut aussi inventer un 1<sup>er</sup> voire un 2<sup>nd</sup> texte
    à insérer dans les balises.

Cet exemple montre qu'un éditeur avec coloration syntaxique permet de repérer quelques-unes des erreurs que peut
contenir un fichier `.html`.

Pour être encore plus rigoureux, prenez le réflexe d'utiliser [le site de validation du W3C](http://validator.w3.org/).

### Exercice n°3 : Favoris

Toujours à l'aide d'un éditeur de texte, créer un fichier simple `.html`, nommé `favoris.html` avec pour contenu les
informations suivantes :

- Nom et Prénom en gras
- Le texte suivant : _Sites utiles en NSI_

_Travail originel CC-BY-NC-SA : Nicolas Buyle-Bodin, Jean-Manuel Meny et Mcikaël Bordonaro. Autres modifications par
Cédric Frayssinet et Maxime Chourré_