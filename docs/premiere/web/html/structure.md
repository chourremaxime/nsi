Le HTML5 a apporté de nouvelles balises *(de type bloc)* qui permettent d'harmoniser la structure des pages web. En
particulier, il permet de définir pour la partie visible de chaque page :

- un **en-tête** grâce à la balise `<header>` *(à ne pas confondre avec la balise `<head>`...)* ;
- un **menu de navigation** dans le site grâce à la balise `<nav>` ;
- la **partie rédactionnelle** de la page grâce à la balise `<section>` qui peut être partagée avec une ou plusieurs
  balises `<article>` ;
- un **pied de page** grâce à la balise `<footer>`.

Il existe d'autres balises et de nombreuses discussions sur le web quant à l'utilisation correcte des balises `<main>`
*(non décrite ici)*, `<section>` et `<article>.` Nous n'entrerons pas dans ces polémiques pour simplifier le contenu des
exercices.

En pratique, **on ne se lance pas au hasard pour concevoir une page web !** On prépare un croquis qui permet d'anticiper
la structure de la page. Cette structure sera évidemment améliorée au fur et à mesure de la construction de la page _(et
du site)_ web, mais elle sera un bon point de départ.

Ci-dessous se trouve un exemple de **croquis préparatoire**.

![Croquis](croquis.png)

Ce croquis montre aussi la necessité d'utiliser des balises universelles `<span>` et `<div>` ainsi que des attributs
`class` et `id` pour préciser encore mieux les éléments constitutifs de la page web.

Enfin, pour la mise en forme (couleurs, bordures, etc.), ce sera au fichier `.css` de la définir.

## Exercice n°1 : Un premier exemple

Le but de l'exercice est d'afficher la page représentée ci-dessous.

![Body dans lequel se trouve header, nav, section, footer](exemple_imbrication_01.png)

La bordure et la couleur des blocs sont définies par un fichier `.css` importé grâce à la balise marqueur `<link>` _(on
rappelle qu'il est inutile de s'attarder sur la syntaxe pour l'instant)_.

De plus, chaque bloc devra être nommé. Pour pouvoir afficher les caractères spéciaux tels que "&", "<" ou ">", on
utilise des **entités HTML** _(on pourra copier / coller / modifier l'écriture de `<body>`)_.

!!! info "Entités HTML"

    Les **entités** qui servent à afficher les caractères spéciaux sont sous la forme `&truc;` _(ne pas oublier le `&`
    ni le point virgule final). Ainsi, pour afficher `<`, on écrit `&lt;` et pour afficher `&`, il a fallu écrire 
    `&amp;`.
    
    Une liste complète des entités est disponible sur le
    [site de Doc'Alex](https://alexandre.alapetite.fr/doc-alex/alx_special.html).

[Télécharger le fichier CSS](html_structure_01.css){ .md-button ; download="html_structure_01.css" }

```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Structurer sa page</title>
    <link rel="stylesheet" href="html_structure_01.css">
</head>
<body>
<code>&lt;body&gt;</code>
<header>
    <code>&lt;header&gt;</code>
</header>

</body>
</html>
```

## Exercice n°2 : Une belle organisation

Compléter le code de l'exercice précédent, en utilisant un autre fichier CSS, pour obtenir le résultat ci-dessous :

![Autre imbrication](exemple_imbrication_02.png)

[Télécharger le fichier CSS](html_structure_02.css){ .md-button ; download="html_structure_02.css" }

```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Structurer sa page</title>
    <link rel="stylesheet" href="html_structure_02.css">
</head>
<body>
<code>&lt;body&gt;</code>
<header>
    <code>&lt;header&gt;</code>
</header>

</body>
</html>
```

_Travail originel CC-BY-NC-SA : Nicolas Buyle-Bodin, Jean-Manuel Meny et Mcikaël Bordonaro. Autres modifications par
Cédric Frayssinet et Maxime Chourré_