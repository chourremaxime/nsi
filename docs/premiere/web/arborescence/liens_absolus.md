Un **chemin absolu** décrit l'emplacement exact d'un fichier dans une arborescence. Ce chemin est affiché dans un
navigateur. Cependant, il suffit que le dossier contenant le fichier soit déplacé pour que ce fichier ne soit plus
accessible.

Par exemple, l'adresse absolue de cette page sur mon ordinateur personnel est affichée ci-dessous. Lit-on le même chemin
dans la barre d'adresse _(en haut)_ du navigateur ?

![Lien vers http://127.0.0.1:8000/nsi/premiere/web/arborescence/liens_absolus/](lien_absolu.png)

On utilisera donc une URL absolue pour accéder à un fichier qui se trouve à **l'extérieur de son site**. Pour les
fichiers de son propre site, il faudra utiliser des [liens relatifs](liens_relatifs.md).

## Exercice n°1 : Notre première image

Compléter le code Html ci-dessous _(en remplaçant le point d'interrogation)_ pour que le logo de l'académie de Lyon
apparaisse dans la fenêtre d'affichage.

Il faudra se rendre sur le site de l'académie de Lyon **pour récupérer l'URL** de cette image.

```html
<!DOCTYPE html>

<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Liens externes</title>
</head>

<body>
    <p>Voici le logo de l'académie de Lyon :</p>
    <img src="?" />
</body>
</html>
```

## Exercice n°2 : Notre premier lien

Modifier le code précédent afin que l'image serve d'ancre pour un lien externe vers le site de l'académie de Lyon. Ce
site doit s'afficher dans une nouvelle fenêtre.

!!! info "Et comment ?"

    Vous aurez besoin de rajouter un attribut à la balise `<a>`. Pour cela, consultez la doc
    [sur le site MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/a#target).

## Exercice n°3 : Les favoris

Reprendre le fichier `favoris.html`, et y ajouter les liens hypertextes vers les sites que vous avez indiqués.

_Travail originel CC-BY-NC-SA : Nicolas Buyle-Bodin, Jean-Manuel Meny et Mcikaël Bordonaro. Autres modifications par
Cédric Frayssinet et Maxime Chourré_