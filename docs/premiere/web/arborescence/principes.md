De manière générale, un site internet _(voire le web dans son intégralité)_ peut être représenté comme un répertoire _(
un dossier)_ contenant des fichiers et/ou des dossiers :

![Arborescence](arborescence.png)

Pour _passer_ d'une page web à une autre, pour afficher des images sur ces pages ou pour télécharger des documents à
partir d'un site, il faut créer des **liens hypertextes** entre ces fichiers en déclarant leur **URL** _(Uniform
Resource Locator)_.

## Différents liens pour différents usages

Ces _adresses web_ construisent une arborescence qui indique comment ces fichiers et ces dossiers sont reliés entre eux.
Trois types de liens vont alors se dégager :

- Un [lien absolu](liens_absolus.md) (lien externe) renvoie vers un fichier contenu dans un autre site ;
- Un [lien relatif](liens_relatifs.md) (lien interne) renvoie vers un fichier contenu dans son propre site ;
- Un [lien intra-page](liens_intrapages.md) renvoie vers une autre partie de la même page web.

## Déclarer les liens dans un fichier `.html`

Voici trois balises très utiles en Html qui prennent pour attribut des URL :

!!! syntaxe "Afficher une image"

    On utilise `<img>` qui est une balise marqueur de **type bloc**. La syntaxe est :
    
    ```html
    <img src="url_du_lien_vers_l'image_a_afficher">
    ```

!!! syntaxe "Lien hypertexte pour se déplacer vers une autre page"

    `<a>` désigne une **ancre**, c'est-à-dire un lien vers une cible de destination et/ou la cible nommée d'un autre
    lien. Cette balise entoure généralement un mot _(une image, un paragraphe, un titre…)_ qui devient le support du
    lien hypertexte. La syntaxe est :
    
    ```html
    <a href="url_du_lien_hypertexte">support_du_lien</a>
    ```

!!! syntaxe "Lien vers le fichier `.css`"

    On importe un fichier _(de mise en forme)_ `.css` en se plaçant entre les balises `<head>` et `</head>`. Pour cela,
    on utilise la balise marqueur `<link>` en respectant la syntaxe :
    
    ```html
    <link href="url_du_lien_vers_le_fichier_css">
    ```

_Travail originel CC-BY-NC-SA : Nicolas Buyle-Bodin, Jean-Manuel Meny et Mcikaël Bordonaro. Autres modifications par
Cédric Frayssinet et Maxime Chourré_