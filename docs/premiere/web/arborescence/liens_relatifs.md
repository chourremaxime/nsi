Un **chemin relatif** est une description de l'emplacement d'un fichier à atteindre **depuis** l'emplacement du fichier actuel. En pratique :

- Utiliser un lien absolu, c'est comme indiquer une adresse sur une lettre postale. Le facteur va à cette adresse _(peu importe comment)_ et il dépose le courrier lorsque cette adresse est habitée _(ou renvoie le courrier s'il n'y a personne)_.
- Utiliser un lien relatif, c'est indiquer à une personne dans la rue comment se rendre à une adresse. On peut lui dire d'aller tout droit, puis de tourner à gauche dans cette rue, à droite dans une autre rue, etc.

Pour naviguer **dans son propre site**, il faut utiliser une **URL relative**. En **organisant correctement ses dossiers et ses fichiers**, on peut être certain que les liens hypertextes créés **resteront valables quel que soit le serveur** sur lequel le site internet est hébergé.

## Exercice n°1 : Bien se repérer pour bien naviguer

![Arborescence](arborescence_vertical.png){ align=right }

Cet exercice est à réaliser **avec un papier et un crayon** !

Il faudra utiliser l'arborescence du site affiché ci-contre pour visualiser les _passages_ d'un fichier à l'autre, les remontées ou descentes d'un dossier à un autre...

Remplacer chaque point d'interrogation par le lien relatif qui convient.

!!! question "Fichier-cible stocké dans le même répertoire que le fichier-source"

    ![Lien vers le même dossier](lien_meme_dossier.gif){ align=right }
    
    Je suis en train de consulter le fichier `page1.html`.

    Quelle balise et quel lien permettent d'afficher la page contenue dans le fichier `page2.html` _(qui est dans le
    même dossier que `page1.html`)_ ?
    
    ```html
    <?>lien vers la page 2</?>
    ```

!!! question "Fichier-cible stocké dans un sous-répertoire du répertoire contenant le fichier-source"

    ![Lien vers un sous-dossier](lien_sous_dossier.gif){ align=right }

    Je suis en train de consulter le fichier `accueil.html`.
    
    Quel lien permet d'afficher la page contenue dans le fichier `page1.html` qui appartient au dossier
    `autres_pages_du_site` _(qui est dans le même répertoire que `accueil.html`)_ ?
    
    ```html
    <?>lien vers la page 1</?>
    ```
    
    ??? bug "Solution ?"
    
        Pour _descendre_ dans un dossier, on utilise le symbole `/` après le nom du dossier.

!!! question "Fichier-cible stocké dans un autre répertoire que le fichier-source"

    ![Lien avec retour arrière](lien_arriere.gif){ align=right } 

    Je suis en train de consulter le fichier `page2.html`.
    Quel lien permet d'afficher l'image `Une_image.png` dans cette page ? En d'autres termes, comment remonter d'un
    répertoire puis aller dans le dossier [images_du_site] afin d'atteindre ce fichier image ?
    
    ```html
    <img src="liens_internes.html?">
    ```

    ??? bug "Solution ?"

         Pour _remonter_ d'un dossier, on utilise le symbole `../`.

## Exercice n°2 : Plusieurs images

![Arborescence](exercice_arborescence.png){ align=right }

L'image ci-contre présente un extrait d'une arborescence de site.

[Télécharger la structure](exercice_arborescence.zip){ download ; .md-button }

Compléter le code de la page `exercice_02.html` afin que les images dont le nom commence par `Liens_internes` s'affichent les unes à côté des autres.

**Attention à la casse (minuscules/majuscules).**

[//]: # (## Exercice 3 : Importer un CSS)

[//]: # ()
[//]: # (La mise en page d'un site est assurée par un fichier `.css`. Ce fichier s'importe entre les balises `<head>`.)

[//]: # ()
[//]: # (Remplacer le _point d'interrogation_ du fichier `exercice_03.html` afin d'importer le fichier `liens_internes.css` qui se trouve dans le répertoire `css_exo`.)

## Exercice n°4 : Encore des favoris !

1. Chercher sur internet une image _(libre de droits !)_ puis enregistrer cette image dans son répertoire personnel.
2. Reprendre le fichier `favoris.html`, et afficher l'image téléchargée précédemment après la liste des sites utiles.

_Travail originel CC-BY-NC-SA : Nicolas Buyle-Bodin, Jean-Manuel Meny et Mcikaël Bordonaro. Autres modifications par
Cédric Frayssinet et Maxime Chourré_