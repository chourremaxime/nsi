Une **URL intra-page** permet de _naviguer_ facilement à l'intérieur d'un fichier `.html`. C'est le cas du menu présent
à droite de cette page (sur ordinateur) dont le lien conduit directement à la partie de la page correspondante.

Ces URL peuvent être associées aux URL absolues afin d'atteindre **un emplacement précis d'une autre page**. Par
exemple, en cliquant [ici](liens_relatifs.md#exercice-n1-bien-se-reperer-pour-bien-naviguer), on ouvre la page des
liens relatifs directement au niveau de l'exercice n°1.

!!! syntaxe "Méthode"

    1. On donne un identifiant unique à la balise HTML de l'élément à cibler avec l'attribut `id="..."` :
    ```html
    <h1 id="presentation">Présentation</h1>
    ```
    2. On crée le lien vers ces identifiants (précédés du symbole `#`) dans une balise _ancre_ `<a>` :
    ```html
    <a href="liens_intra_page.html#presentation">Lien vers le titre</a>.
    ```
    
    L'attribut `href="#"` conduit le lien en haut de la page HTML consultée.

## Exercice n°1 : Faire des liens sur une page

Le code `.html` ci-dessous présente les compétences issues du bulletin officiel.

Compléter ce code afin que :

- chaque lien _« Décrire »_, _« Concevoir »_, _« Collaborer »_, _« Communiquer »_ et _« Responsabiliser »_ renvoie vers
  le paragraphe correspondant ;
- chaque lien _« Haut de page »_ renvoie vers le haut de la page.

```html
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Liens intra-page</title>
    </head>

    <body>
        <h1>Compétences à évaluer en NSI</h1>
        <p>Décrire - Concevoir - Collaborer - Communiquer - Responsabiliser</p>

        <h2>Décrire et expliquer une situation, un système ou un programme</h2>
            <ul><li>Justifier dans une situation donnée, un codage
                    numérique ou l'usage d'un format approprié, qu'un
                    programme réalise l'action attendue...</li>

                <li>Détailler le déroulement d'une communication numérique,
                    le rôle des constituants d'un système numérique, le rôle
                    des    éléments constitutifs d'une page web, ce qu'effectue
                    tout ou    partie d'un programme ou de l'algorithme associé,
                    l'enchaînement des événements qui réalisent la fonction
                    attendue par un programme...</li>
            </ul>
            <p>Haut de page</p>


        <h2>Concevoir et réaliser une solution informatique en réponse à un problème</h2>
            <ul><li>Analyser un besoin dans un système d'information, le
                    fonctionnement d'un algorithme...</li>

                <li>Structurer une formule logique, des données, une
                    arborescence, une page web, une approche fonctionnelle
                    en réponse à un besoin...</li>

                <li>Développer une interface logicielle ou une interface
                    homme-machine, un algorithme, un programme, un
                    document ou fichier numérique...</li>
            </ul>
            <p>Haut de page</p>


        <h2>Collaborer efficacement au sein d'une équipe dans le cadre d'un projet</h2>
            <ul><li>Agir au sein d'une équipe dans des rôles bien définis,
                    en interaction avec le professeur.</li>

                <li>Rechercher et partager une information, une documentation,
                    une explication.</li>

                <li>Maîtriser l'utilisation d'outils numériques collaboratifs
                    du type ENT, système de gestion de contenu (CMS),
                    groupe de travail, forums...</li>
            </ul>
            <p>Haut de page</p>


        <h2>Communiquer à l'écrit et à l'oral</h2>
            <ul><li>Documenter un projet numérique pour en permettre la
                    communication en cours de réalisation et à l'achèvement,
                    tout en précisant le déroulement et la finalité du
                    projet.</li>

                <li>Présenter le cahier des charges relatif à un projet ou
                    un mini-projet, la répartition des tâches au sein de
                    l'équipe, les phases successives mises en œuvre, le
                    déroulement de l'ensemble des opérations...</li>

                <li>Argumenter les choix relatifs à une solution (choix
                    d'un format, d'un algorithme, d'une interface...).</li>
            </ul>
            <p>Haut de page</p>


        <h2>Faire un usage responsable des sciences du numérique en ayant conscience des problèmes sociétaux induits</h2>
            <ul><li>Avoir conscience de l'impact du numérique dans la
                société notamment de la persistance de l'information
                numérique, de la non-rivalité des biens immatériels, du
                caractère supranational des réseaux, de l'importance des
                licences et du droit.</li>

                <li>Mesurer les limites et les conséquences de la
                    persistance de l'information numérique, des lois
                    régissant les échanges numériques, du caractère
                    supranational des réseaux.</li>
            </ul>
            <p>Haut de page</p>

    </body>
</html>
```

## Exercice n°2 : Lien intra-page interne

On peut bien évidemment _mélanger_ un lien interne au site avec un lien intra-page. Il faut alors connaître
l'arborescence de son site pour atteindre la page désirée ainsi que l'identifiant de la partie de la page à cibler.

Remplacer le point d'interrogation par un lien qui permettra d'afficher le paragraphe « Collaborer » de la page de
l'exercice n°1.

```html
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Mix entre lien interne et lien intra-page</title>
    </head>

    <body>
        <p>
            En cliquant <a href="?">ici</a>, on affiche la page du programme de NSI au niveau
            du paragraphe sur la collaboration en équipe.
        </p>
    </body>
</html>
```

_Travail originel CC-BY-NC-SA : Nicolas Buyle-Bodin, Jean-Manuel Meny et Mcikaël Bordonaro. Autres modifications par
Cédric Frayssinet et Maxime Chourré_