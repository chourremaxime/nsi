## Les limites de l'ASCII

![Détournement d'un album de Martine](martine_utf8.jpg){ align=right }

Même si 128 caractères suffisaient pour coder les textes de la langue anglaise, cela ne suffisait évidemment pas pour
coder tous les caractères de toutes les langues.

Par exemple, dans le code ASCII initial (page précédente), les lettres é, è, ê ne sont pas codées.

Pour pallier ce problème, on a utilisé des extensions de l'ASCII en utilisant le 8ᵉ bit des octets du code ASCII.
Ces extensions sont encore utilisées.

Le problème est que sur 8 bits, on ne code que $2^8$ = **256 caractères**, ce qui est encore insuffisant pour coder tous
les caractères de toutes les langues. Il a donc fallu définir **plusieurs extensions** de la norme ASCII, incompatibles 
entre elles.

Dès 1960, plusieurs propositions de codage coexistent et ont été mises en place sans concertation par différents 
constructeurs d'ordinateurs qui voulaient profiter du dernier bit (le 8ᵉ) inutilisé par le codage ASCII initial :

- Le codage des caractères présents dans la table ASCII est conservé.
- Le principe du codage de chacun des caractères sur 1 octet est conservé, avec utilisation complète des 8 bits
  de l'octet. Cela permet donc de coder $2^8$, soit 256 caractères (128 en plus).

!!! example "Exemple de norme : ISO 8859-1"

    La norme **ISO 8859-1** souvent appelée **Latin-1**.
    
    Dans cette norme la lettre `é` est prévue : elle correspond au code $(E9)_{16}$, c'est-à-dire 233 en décimal (donc
    codé $1110\ 1001$ en machine).

## Incompatibilité

L'incompatibilité de ces normes entre elles posent de nombreux problèmes. Avant l'Unicode (que l'on va étudier 
en pages suivantes), aucun jeu de caractères ne fournissait toutes les lettres, symboles techniques...

Produire un logiciel obligeait alors à écrire plusieurs versions du logiciel (pour chaque jeu de caractères) ou 
produire un code beaucoup plus complexe pour traiter tous les jeux de caractères.