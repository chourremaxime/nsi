En machine, tout n'est que 0 et 1. Ainsi, **les lettres ne sont finalement que des nombres** !

Pour coder autre chose qu'un entier naturel (image, son, texte...), il faut donc nécessairement décider d'une
**convention associant un nombre et l'objet à représenter**. Dans le langage informatique, cela s'appelle un
**codage**.

Bien entendu, un même nombre pourra représenter dans un contexte donné un entier naturel, dans un autre
contexte une lettre, dans un autre contexte un son, etc.

Dans cette partie, nous allons voir quels sont les nombres associés usuellement aux lettres (et autres 
symboles du texte).