Python intègre nativement des méthodes et fonctions permettant de convertir un caractère en représentation 
ASCII décimale (entre 0 et 127) et vice-versa.

La fonction `ord()` donne la valeur décimale ASCII d'un caractère :

```py
ord('B')
```

affiche `66`.

La fonction `chr()` renvoie le caractère de valeur décimale ASCII donnée :

```py
chr(65)
```

affiche `A`.