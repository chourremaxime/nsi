Le **codage ASCII** est l'acronyme de **A**merican **S**tandard **C**ode for **I**nformation **I**nterchange.

En 1960, il a été défini et utilisé pour **écrire des textes en anglais**. Cette norme ne définissait que 128
codes _(sur une longueur 7 bits, codes de 0000 0000 à 0111 1111)_.

La table ASCII ci-dessous fournit la correspondance entre $128 = 2^7$ caractères et leur représentation binaire.

95 caractères sont imprimables :

- les **chiffres** de 0 à 9,
- les **lettres minuscules** de a à z et les **majuscules** de A à Z,
- et des **symboles mathématiques** et de **ponctuation**.

| Base 16 |            | 0        | 1        | 2        | 3        | 4        | 5        | 6        | 7        | 8        | 9        | A        | B        | C        | D        | E        | F        |
|---------|------------|----------|----------|----------|----------|----------|----------|----------|----------|----------|----------|----------|----------|----------|----------|----------|----------|
|         | **Base 2** | **0000** | **0001** | **0010** | **0011** | **0100** | **0101** | **0110** | **0111** | **1000** | **1001** | **1010** | **1011** | **1100** | **1101** | **1110** | **1111** |
| **000** | **0000**   | NUL      | SOH      | STX      | ETX      | EOT      | ENQ      | ACK      | BEL      | BS       | HT       | LF       | VT       | FF       | CR       | SO       | SI       |
| **001** | **0001**   | DLE      | DC1      | DC2      | DC3      | DC4      | NAK      | SYN      | ETB      | CAN      | EM       | SUB      | ESC      | FS       | GS       | RS       | US       |
| **002** | **0010**   | SP       | !        | "        | #        | $        | %        | &amp;    | '        | (        | )        | *        | +        | ,        | -        | .        | /        |
| **003** | **0011**   | 0        | 1        | 2        | 3        | 4        | 5        | 6        | 7        | 8        | 9        | :        | ;        | &lt;     | =        | &gt;     | ?        |
| **004** | **0100**   | @        | A        | B        | C        | D        | E        | F        | G        | H        | I        | J        | K        | L        | M        | N        | O        |
| **005** | **0101**   | P        | Q        | R        | S        | T        | U        | V        | W        | X        | Y        | Z        | [        | \        | }        | ^        | _        |
| **006** | **0110**   | `        | a        | b        | c        | d        | e        | f        | g        | h        | i        | j        | k        | l        | m        | n        | o        |
| **007** | **0111**   | p        | q        | r        | s        | t        | u        | v        | w        | x        | y        | z        | {        | \|       | }        | ~        | DEL      |

!!! example "Exemple"

    - La représentation binaire du caractère `Z` est $(0101\ 1010)_2$ et sa représentation hexadécimale est 
      $(5A)_{16}$ ; lecture de la ligne puis de la colonne.
    - `SP` correspond à un espace (SPace) et `DEL` à un effacement (DELete)

!!! note "Caractères de contrôle"
    
    Les deux premières lignes (soit 32 caractères) correspondent à des caractères de contrôle dont on trouvera la 
    signification sur cet [article](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange#Table_des_128_caract.C3.A8res_ASCII)
    de Wikipedia. On pourra quand même repérer les éléments suivants :
    
    - `STX` : Start of Text (début de texte)
    - `ETX` : End of Text (fin de texte)
    - `HT` : Horizontal Tab (tabulation horizontale)
    - `LF` : Line Feed (saut de ligne)
    - `ESC` : Escape...