## Exercices avec Python

### Exercice n°1

Écrire un script Python qui affiche les lettres de l'alphabet en minuscules puis en majuscules avec leur code
ASCII associé.

Pour vous faire gagner du temps, on vous donnera cela :

```py
lettres = 'abcdefghijklmnopqrstuvwxyz'
```

??? question "Indice"

    Une **méthode** bien utile en Python : `upper()`, à utiliser...

### Exercice n°2

Écrire un script Python qui affiche les 256 caractères de la table ASCII étendue. De quel type sont ces caractères ?

??? question "Indice"

    On remarquera que certains caractères bloquent l'affichage des caractères jusqu'au 256 caractères, excluez-les
    de votre boucle for.

## Exercices avec un éditeur hexadécimal

Si le logiciel HxD est installé, utilisez-le. Sinon, utilisez le site web [HexEd](https://hexed.it/).

### Exercice 1 : lecture d'un simple fichier texte

- Créer un fichier `a.txt` dans lequel vous entrez simplement la lettre `a`.
- Ouvrir avec l'éditeur hexadécimal, ce fichier texte.
- Qu'observez-vous ?
- Comment interprétez-vous le code affiché ?

### Exercice 2

Recommencer le travail, mais avec la lettre `é`.

- Avec un premier fichier encodé en utf8.
- Avec un second fichier encodé en iso-8859-1.