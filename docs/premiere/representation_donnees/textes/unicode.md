Le bit supplémentaire utilisé par les tables ASCII étendues ne permet pas de prendre en charge tous les caractères
de toutes les langues humaines, notamment, en première initiative, le caractère français `œ` a été oublié... Il faut
donc mettre en place une nouvelle norme : ce sera l'**UNICODE**.

La norme Unicode (`UTF-8`, `UTF-16`, `UTF-32` et variantes...) définit des méthodes standardisées pour coder et
stocker un index de caractères sous forme de séquence d'octets.

La principale caractéristique de la **norme UTF-8 est qu'elle est rétrocompatible avec la norme ASCII** : c'est 
le standard UNICODE le plus courant sur internet.

En UNICODE, chaque caractère est représenté sous la forme d'un **bloc U+xxxx** (où xxxx est un hexadécimal de 4 à 6
chiffres, entre U+0000 et U+10FFFF ). La plage ainsi définie permet d'attribuer **jusqu'à 1 114 112 caractères**. La
table ASCII est codée en UTF-8 de U+0000 à U+007F.

On recense **environ 130 000 caractères** dans UNICODE.

!!! example "Quelques exemples de codage UTF-8"

    Sur le tableau ci-dessous, on pourra repérer le codage de la lettre `A` ou des caractères `é` ou `€` :
    
    ![Exemple de codage UTF-8](ex_codage_utf8.png)

!!! info "Usage d'UTF-8 sur Internet"

    ![Evolution UTF-8](evolution_UTF8.png){ align=right ; width=300px }
    
    L'UTF-8 est utilisé par 82,2 % des sites web en décembre 2014, puis 87.6% en 2016 et enfin 95,2% en octobre 2020.
    
    Par sa nature, UTF-8 est d'un usage de plus en plus courant sur Internet, et dans les systèmes devant échanger
    de l'information.
    
    Il s'agit également du codage le plus utilisé dans les systèmes GNU / Linux et compatibles pour gérer le plus 
    simplement possible des textes et leurs traductions dans tous les systèmes d'écritures et tous les alphabets du monde.

Concrètement, UTF-8 est utilisé par quasi tous les serveurs Web. Aujourd'hui, il n'y a plus de questions à se poser :
**choisissez systématiquement l'encodage utf-8 pour vos travaux**.

UTF-8 est en effet un codage de caractères conçu pour coder l'ensemble des caractères Unicode, en restant compatible
avec la norme ASCII.

??? plus-loin "Aller plus loin..."

    On trouvera plus de précision sur cet [excellent article](https://fr.wikipedia.org/wiki/UTF-8) de Wikipédia
    sur l'encodage UTF-8.