Dans une machine, on utilise l'écriture binaire pour représenter un entier naturel.

Avec des octets, soit 8 bits, on peut représenter les entiers naturels **de 0** ($0000 0000$ en base 2) **à 255**
($1111 1111$ en base 2). Donc 45 est représenté par $0010 1101$.

Si on utilise 16 bits, soit 2 octets, on peut représenter les entiers naturels jusqu'à **65 535** ($1111 1111
1111 1111$ en base 2) et dans ce cas, 45 est représenté par $0000 0000 0010 1101$.

Avec $n$ bits, on peut représenter les nombres entre $0$ et $2^n-1$, c'est-à-dire tout nombre $k$ qui s'écrit :

$$k=\sum_{i=0}^{n-1}b_i \times 2^{i}\space\space avec\space\space b_i \in \{0,1\}$$

!!! info "Remarque"

    Un entier est en général codé sur 4 octets (soit 32 bits) ou sur 8 octets (soit 64 bits).