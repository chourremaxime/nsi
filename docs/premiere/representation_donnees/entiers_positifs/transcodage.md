## Transcodage : depuis toutes les bases vers le décimal

!!! methode "Méthode"

    Passer de l'écriture en base $b$ d'un nombre à son écriture décimale est aisé, il suffit de faire la somme
    de la multiplication de chaque chiffre d'indice $k$ par son poids correspondant $b^k$ _($b$ étant la$
    base d'origine)_.

!!! example "Hexadécimal vers Décimal"

    Au préalable, on prendra soin de transcoder les lettres A à F en décimal _(de 10 à 15)_ :
    
    - A vaut 10
    - B vaut 11
    - C vaut 12
    - D vaut 13
    - E vaut 14
    - F vaut 15.
    
    $$(FA9)_{16} = 15 \times 16^2 + 10 \times 16^1 + 9 \times 16^0 = 3840 + 160 + 9 = (4009)_{10}$$

## Transcodage : depuis le décimal vers les autres bases

!!! methode "Méthode"

    Pour convertir un nombre $(N)_{10}$ écrit en base $10$ dans la base $b$, il faut effectuer des **divisions 
    euclidiennes** successives, d'abord de $N$ par $b$, puis des quotients obtenus par $b$ jusqu'à ce que le
    quotient soit $0$. Les restes successifs sont ensuite écrits du dernier au premier de gauche à droite.

!!! info "Rappel : Division euclidienne"

    $a$ et $b$ sont deux entiers naturels, avec $b$ non nul. On appelle **division euclidienne de $a$ par $b$** 
    la division donnant **un quotient $q$ et un reste $r$ entiers** tels que **$0 \le r < b$**.
    
    Cette division _entière_ conduit à l'égalité : **$a = b \times q + r$**
    
    Ainsi, on aura un reste égal à 0 ou 1 si on transcode en binaire, ou un reste compris entre 0 et 15 si on
    transcode en hexadécimal.
    
    En Python, on obtient :
    
    - le quotient de a par b avec l'instruction : `q = a // b`
    - le reste de a par b avec l'instruction : `r = a % b`

!!! example "Décimal vers Binaire"

    Si on veut trouver un résultat en binaire, on va devoir diviser par 2. Si on veut de l'hexadécimal,
    on va diviser par 16.
    
    Dans l'exemple ci-dessous, on souhaite écrire 77 en binaire.
    
    ![Division euclidienne](bin_div_euclidienne.png)
    
    Le résultat sera donc $(77)_{10} = (1001101)_2$ _(lecture de bas vers le haut des restes)_.