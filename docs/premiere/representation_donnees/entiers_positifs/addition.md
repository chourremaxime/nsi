Les ordinateurs ne connaissent pratiquement qu'une opération : l'**addition**. Pour additionner deux nombres 
en binaire, **on procède comme en base 10**.

Ainsi, on utilise ces 4 opérations références :

- $0 + 0 = 0$
- $1 + 0 = 1$
- $0 + 1 = 1$
- $1 + 1 = 0$ plus $1$ de retenue, soit $10$<br>
  puis, on pose l'addition comme en base 10, avec le système de retenue.

!!! example "Exemple"

    Additionnons par exemple $9$ à $13$ :
    
    $$
    \begin{array}{llllllc}
    & 1 & & & 1 & & retenues\\
    & & 1 & 1 & 0 & 1 & treize\\
    + & & 1 & 0 & 0 & 1 & neuf\\
    \hline\\
    & 1 & 0 & 1 & 1 & 0 & vingt-deux\\
    \end{array}
    $$

!!! warning "Attention"

    Si la taille des entiers est limitée, par exemple avec 4 bits, alors dans l'addition ci-dessus, le bit à gauche
    est **perdu** ! Donc pour la machine, la somme de $1101$ et de $1001$ vaut $0110$. Autrement dit, si nous
    demandons à la machine de calculer 13 + 9, elle nous répond 6 !