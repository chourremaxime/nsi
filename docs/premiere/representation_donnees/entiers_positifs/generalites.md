!!! abstract "Définition"

    On appelle **base d'un système de numération** de position, le **nombre de chiffres distincts** permettant 
    l'écriture de n'importe quel nombre entier naturel.
    
    Dans un système de base $b$, un nombre sera noté :
    
    $$(a_n a_{n−1} \dots a_1 a_0)_b = 
    a_0 \times b^0 + a_1 \times b^1 + \dots + a_{n−1} \times b^{n−1} + a_n \times b^n$$
    
    où les $a_i$ sont des chiffres compris entre $0$ et $b-1$.
    
    Exemple pour le nombre $(1234)_{10}$ _(le $_{10}$ signifie que ce nombre est écrit en base 10)_ :
    
    $$(1234)_{10} = 4 \times 10^0 + 3 \times 10^1 + 2 \times 10^2 + 1 \times 10^3 = 4 + 30 + 200 + 1000$$

En informatique, nous utilisons principalement 3 bases :

1. Le système **binaire** utilisant la base **2**.<br>
   Les processeurs des ordinateurs actuels sont composés de transistors ne gérant chacun que deux états. Un calcul
   informatique n'est donc qu'une suite d'opérations sur des nombres composés de 0 et de 1, appelés **octets** 
   lorsqu'ils sont regroupés par huit _(1 octet = 8 bits)_.
2. Le système **décimal** utilisant la base **10**.<br>
   Ce système est le plus utilisé pour représenter des nombres. Il découle d'un choix naturel, dicté par le nombre
   de doigts des deux mains.
3. Le système **hexadécimal** utilisant la base **16**.<br>
   Ce système est fréquemment utilisé en informatique, car il permet un compromis entre le code binaire des machines 
   et une base de numération pratique à utiliser _(chaque chiffre correspond exactement à 4 chiffres binaires, soit
   4 bits)_.

![Bande dessinée hexadécimal](hexa_bd.png)