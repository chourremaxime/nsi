## Numération décimale de position

C'est le système de base 10 que nous utilisons tous les jours. Les 10 chiffres sont :

**0, 1, 2, 3, 4, 5, 6, 7 ,8, 9**

!!! methode "Décomposition d'un nombre décimal"

    Le nombre $3284$ s'écrira $(3284)_{10}$ dans cette base et signifie, en utilisant la définition générique :
    
    $$(3284)_{10} = 4 \times 10^0 + 8 \times 10^1 + 2 \times 10^2 + 3 \times 10^3$$
    
    | Nombre | 3             | 2            | 8           | 4          |
    |--------|---------------|--------------|-------------|------------|
    | Rang   | 3             | 2            | 1           | 0          |
    | Poids  | $10^3 = 1000$ | $10^2 = 100$ | $10^1 = 10$ | $10^0 = 1$ |

## Numération binaire de position

C'est donc le système de base 2 qu'utilisent les ordinateurs. Les chiffres sont au nombre de 2 : **0 et 1**.

Ainsi le nombre $13$ s'écrit $(1101)_2$ et est stocké dans l'ordinateur par quatre circuits mémoires respectivement
dans les états 1, 0, 1, 1.

!!! abstract "Définition"

    Un nombre binaire de **8 bits** _(huit chiffres)_ est appelé un **octet**.
    
    | Rang            | 7     | 6     | 5     | 4     | 3     | 2     | 1     | 0     |
    |-----------------|-------|-------|-------|-------|-------|-------|-------|-------|
    | Poids           | $2^7$ | $2^6$ | $2^5$ | $2^4$ | $2^3$ | $2^2$ | $2^1$ | $2^0$ |
    | Valeur décimale | 128   | 64    | 32    | 16    | 8     | 4     | 2     | 1     |
    
!!! example "Nombre $(1101)_2$"

    $$(1101)_2 = 1 \times 2^0 + 0 \times 2^1 + 1 \times 2^2 + 1 \times 2^3 = (13)_{10}$$

!!! plus-loin "Poids des bits"

    ![Poids des bits](bin_lsb.png){ align=right }
    
    Dans un mot binaire, on parle de **bit de poids faible** et de **bit de poids fort** selon leur valeur.
    
    En écriture conventionnelle binaire, le **bit de poids faible** _(**LSB** pour Least Significant Bit)_ est celui
    qui se trouve le plus à **droite** et le **bit de poids fort** le plus à **gauche** _(**MSB** pour Most 
    Significant Bit)_

## Numération hexadécimale de position

Les circuits mémoires d'un ordinateur sont **groupés par octets** _(8 bits)_. Une architecture 32 bits est
constituée de quatre octets pouvant représenter jusqu'à 4 294 967 296 valeurs. A vous de faire le calcul
pour 64 bits...

Utiliser la base 2 devient vite trop fastidieux à calculer. Il faut une autre base qui permette de simplifier
ces calculs tout en ayant une **correspondance rapide avec la base 2**. Ce sera le **système hexadécimal**,
de **base 16**, qui admet pour chiffres :

**0, 1, 2, 3, 4, 5, 6, 7 ,8, 9** et **A, B, C, D, E, F**

La base 16 est donc une **représentation abrégée** du binaire.

!!! abstract "Octet et Hexadécimal"

    Puisque $2^8 = 16^2 = 256$, un **octet est codé sur 2 chiffres en hexadécimal**.

Voici les 4 écritures que l'on trouve pour la notation en base hexadécimale :

- (BC5F)$_16$
- (BC5F)$_h$
- 0xBC5F
- $BC5F

!!! example "Adresse matérielle des cartes réseaux"

    Les adresses MAC, matérielle ou encore physique des cartes réseaux RJ45 ou Wifi utilisent le codage
    hexadécimal : `68:f7:28:00:0d:c9`.
    
    On peut l'obtenir sous GNU/Linux via la commande `ifconfig` ou `ipconfig/all` sur un système Windows.