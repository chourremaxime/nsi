## Table de vérité

La **porte ET** ou **AND** à deux entrées, notées ici $a$ et $b$, peut être décrite comme suit : « la sortie
notée $S$ est à 1 lorsque les deux entrées sont à 1 ».

Cette fonction, ou porte, est ainsi **définie en langue naturelle**. Il est donc aisé d'écrire sa table de vérité :

| $a$ | $b$ | $S$ |
|-----|-----|-----|
| 0   | 0   | 0   |
| 0   | 1   | 0   |
| 1   | 0   | 0   |
| 1   | 1   | 1   |

## Équation logique et illustration du principe

$$S = {a . b}$$

On peut illustrer son fonctionnement avec une lampe $L$ ($S$ dans notre cas) et 2 interrupteurs nommés $a$ et $b$ :

![Illustration du principe de la fonction ET](porte_et_illustration.png)

## Représentation graphique

La norme **internationale** et **européenne** en vigueur :

![Porte ET euro](porte_et_euro.png)

La norme américaine :

![Porte ET USA](porte_et_usa.png)