## Table de vérité

La **porte NON**, appelée aussi porte **inverseuse** ou simplement **inverseur**, possède une **entrée unique**,
notée ici $a$. La sortie, notée $S$, est conforme à la table de vérité ci-dessous :

| $a$ | $S$ |
|-----|-----|
| 0   | 1   |
| 1   | 0   |

On dit aussi que la sortie est **complémentée**. La porte NON est la fonction de complémentation. Cette porte, 
pourtant très simple et basique, est cruciale puisque nous verrons plus loin que dans les expressions algébriques 
qui décrivent toute fonction combinatoire, cette fonction ou opération de complémentation est omniprésente.

## Équation logique et illustration du principe

$$S = \bar{a}$$

On peut illustrer son fonctionnement avec une lampe $L$ ($S$ dans notre cas) et un interrupteur nommé $a$ :

![Illustration du principe de la fonction NON](porte_non_illustration.png)

## Représentation graphique

La représentation symbolique graphique de cette porte existe selon deux normes qu'il faut connaître :

La norme **internationale** et **européenne** en vigueur :

![Porte NON avec rond](porte_non1.png) ou ![Porte NON avec triangle](porte_non2.png)

La norme américaine, théoriquement désuète, mais encore beaucoup utilisée par les praticiens et les logiciels :

![Porte NON américaine](porte_non3.png)