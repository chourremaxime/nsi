## Portes NAND et NOR

Il existe deux grands types d'autres portes, mais qui ne sont pas au programme : NAND et NOR.

Il s'agit de combiner :

- la porte ET (AND) et la porte NON pour NAND,
- la porte OU (OR) et la porte NON pour NOR.

**Travail à réaliser** : trouver la table de vérité, l'équation logique ainsi que les représentations graphiques
de ces deux nouvelles portes.

## Porte XOR ou OU exclusif

La porte XOR est définie en langue naturelle par la définition suivante : « la sortie est à 1 si une entrée et une
seule est à 1 ». On obtient facilement la table de vérité :

| $a$ | $b$ | $S$ |
|-----|-----|-----|
| 0   | 0   | 0   |
| 0   | 1   | 1   |
| 1   | 0   | 1   |
| 1   | 1   | 0   |

Cette nouvelle fonction ou porte peut être écrite sous forme d'expression booléenne comme combinaison des
fonctions ET, OU et NON. L'équation logique étant :

$S = a\oplus{}b$

La norme **internationale** et **européenne** en vigueur :

![Porte XOR euro](porte_xor_eu.png)

La norme américaine :

![Porte XOR USA](porte_xor_usa.png)