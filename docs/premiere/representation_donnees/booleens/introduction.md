En début d'année, nous avons rapidement [présenté le type booléen en Python](../../python/introduction/types.md#types).
Nous allons approfondir ce type puisque vous savez maintenant qu'**un processeur d'ordinateur est composé de plusieurs 
milliards de transistors** qui réalisent des **fonctions logiques**.

Les fonctions ou portes élémentaires qui suivent, définissent des fonctions qui **élaborent une sortie en fonction
des entrées comprises au sens de ces variables logiques**. Une **variable logique** est une variable qui ne peut
prendre **que 2 états** : 0 ou 1 par exemple.

Pour simplifier et parce que dans un premier temps, on n'abordera que des portes élémentaires à 2 entrées,
ces 2 entrées (ou variables logiques) seront nommées $a$ et $b$.