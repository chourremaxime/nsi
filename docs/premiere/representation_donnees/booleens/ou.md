## Table de vérité

La **porte OU** ou **OR** à deux entrées peut se traduire de façon naturelle ainsi : « la sortie est à 1 si une
entrée au moins est à 1 ». Là encore, la table de vérité est immédiate selon :

| $a$ | $b$ | $S$ |
|-----|-----|-----|
| 0   | 0   | 0   |
| 0   | 1   | 1   |
| 1   | 0   | 1   |
| 1   | 1   | 1   |

## Équation logique et illustration du principe

$$S = {a + b}$$

On peut illustrer son fonctionnement avec une lampe $L$ ($S$ dans notre cas) et 2 interrupteurs nommés $a$ et $b$ :

![Illustration du principe de la fonction OU](porte_ou_illustration.png)

## Représentation graphique

La norme **internationale** et **européenne** en vigueur :

![Porte OU euro](porte_ou_eu.png)

La norme américaine :

![Porte OU USA](porte_ou_usa.png)