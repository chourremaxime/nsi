## La représentation des données en binaire

!!! quote "Le binaire, et ce qui en découle"
    
    Au commencement étaient le 0 et le 1, puis nous avons créé les nombres, les textes, les images et les sons.

Pour représenter l’information dans un ordinateur, nous utilisons uniquement deux symboles : **0 et 1**. Une des raisons 
est liée aux **circuits électroniques** : il est plus simple et fiable de distinguer deux états, comme _ouvert_ ou
_fermé_, plutôt que plusieurs. _Nous avons abordé cette notion lors du [cours sur les 
transistors](../architecture_os/constitution_ordinateur/echelle_microscopique.md)_

Ces deux symboles, appelés **bits** (pour _**binary digit**_), permettent de représenter des informations variées. 
Par exemple, une variable pouvant prendre deux valeurs (comme _vrai_ ou _faux_) est appelée un **booléen**, en
hommage au mathématicien britannique **George Boole**, inventeur de l’algèbre de Boole.

Grâce à ces petits circuits électroniques ne pouvant être que dans deux états, une machine peut recevoir, mémoriser,
modifier et transmettre des informations.

## Évolution des méthodes de dénombrement

Depuis l’Antiquité, les façons de compter ont beaucoup évolué !

Au départ, on distinguait seulement le _un_ et le _beaucoup_, ce qui manquait de précision.

Ensuite, les bâtons **|** ont permis de dénombrer, mais cette méthode devient vite peu pratique pour de grands nombres.

Des symboles comme les **chiffres romains** ont été inventés, mais ils deviennent difficiles à utiliser pour
les nombres complexes.

Au Moyen Âge, la **numération de position** a été créée. Avec un petit nombre de symboles, **les chiffres**, il 
devient possible de représenter tous les nombres de manière efficace.