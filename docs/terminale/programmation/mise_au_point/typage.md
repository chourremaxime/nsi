Les objets manipulés en programmation sont tous définis par un type. Le type est donc un moyen de classifier les objets.

En Python, on rappelle les types de base :

| Type       | Description           |
|------------|-----------------------|
| `NoneType` | Valeur indéfinie      |
| `bool`     | Booléens              |
| `int`      | Nombres entiers       |
| `float`    | Nombres décimaux      |
| `str`      | Chaînes de caractères |
| `tuple`    | Tuples                |
| `list`     | Listes                |
| `set`      | Ensembles             |
| `dict`     | Dictionnaires         |

De même pour les classes que l'on peut créer, leurs instances seront de type de même nom.

## Connaître le type d'un objet

En utilisant la fonction prédéfinie `#!python type()`, on peut connaître le type de n'importe quel objet.

```python
>>> type({})
<class 'dict'>
>>> type(uneFonction)
<class 'function'>
>>> type(uneClasse)
<class 'type'>
```

!!! info "Pourquoi des types ?"

    Les types permettent de préciser les **conditions d'exécution** des opérandes ou des paramètres. Ainsi, l'opération
    `1 + [2, 3]` lève une exception `TypeError` car cette opération n'a pas de sens.

## Gestion dynamique

La différence de Python avec d'autres languages est que la gestion des types est dynamique. Cela signifie que le type
d'une variable est **évalué lors de l'exécution**. Ainsi, le programme ci-dessous ne produira jamais d'erreurs et
s'exécutera normalement :

```python
if False:
    print(1 + [2, 3])
else:
    print("Ça marche !")
```

En tant que développeur, on souhaite **éviter de changer le type d'une variable** en cours de route (et donc limiter ce
risque de types incompatibles). Comment faire alors pour éviter des problèmes comme celui-ci :

```python
def res(x):
    y = x + 2
    res = y * 3  # Notez le nom choisi : res passe de function à int
    return res
```

Dans l'exemple ci-dessus, si la fonction `res` était récursive, on ne pourrait plus l'utiliser.

## Typer les variables et les fonctions

Quand on programme une fonction, on a pris l'habitude de rédiger les docstrings. Pour rappel, ces docstrings
contiennent les entrées et sorties, ce que fait la fonction, et des tests. Mais ce docstring reste adapté à la
langue de l'auteur, et son format n'est également pas défini.

On peut donc annoter directement le code Python pour préciser les types de tout objet, à commencer par les variables :

```python
# C'est ainsi que l'on déclare le type d'une variable
age: int = 1

# Il n'est pas nécessaire d'initialiser une variable pour l'annoter
a: int  # Ok (pas de valeur à l'exécution jusqu'à ce qu'elle soit assignée)

# Cela peut être utile dans les branches conditionnelles
enfant: bool
if age < 18:
    enfant = True
else:
    enfant = False
```

Pour les types construits (listes, tuples, etc.) la syntaxe depuis Python 3.9 est la suivante :

```python
# Pour les listes et ensembles, le type de l'élément de la liste ou de l'ensemble est entre parenthèses
x: list[int] = [1]
x: set[int] = {6, 7}

# Pour les dictionnaires, nous avons besoin des types des clés et des valeurs
x: dict[str, float] = {"field": 2.0}

# Pour les tuples de taille fixe, nous spécifions les types de tous les éléments
x: tuple[int, str, float] = (3, "yes", 7.5)

# Pour les tuples de taille variable, nous utilisons un type et des points de suspension
x: tuple[int, ...] = (1, 2, 3)
```

Enfin, pour les fonctions, la syntaxe est la suivante :

```python
def plus(num1: int, num2: int) -> int:
    return num1 + num2
```

!!! warning "Python ne suivra pas cette notation"

    Toutefois, comme précisé dans le [PEP 484](https://peps.python.org/pep-0484/) qui dirige ces règles de typage,
    *« Python restera un langage dynamiquement typé, et les auteurs n'ont aucun désir de rendre les indications de
    type obligatoires, même par convention »*. Ainsi, le code suivant fonctionne sans erreurs :
    
    ```python
    age: str = 3
    print(age + 7)
    ```

## Et alors, mais qu'est-ce-que ça fait ?

Par défaut, ça ne fait rien ! Cela ne sert qu'avec des **analyseurs de codes**.

L'analyseur historique est **mypy**. Il s'agit d'une commande à taper dans le terminal pour analyser un ou plusieurs
fichiers Python. Le programme affichera alors des erreurs qu'il a pu détecter, notamment de typage.

``` python title="exemple.py" linenums="1"
age: str = 3
print(age + 7)
print(bonjour)
```

```
❯ mypy exemple.py
exemple.py:1: error: Incompatible types in assignment (expression has type "int", variable has type "str")  [assignment]
exemple.py:2: error: Unsupported operand types for + ("str" and "int")  [operator]
exemple.py:3: error: Name "bonjour" is not defined  [name-defined]
Found 3 errors in 1 file (checked 1 source file)
```

Enfin, typer vos programmes vous permettra de vous préparer à d'autres langages, notamment le C et l'OCaml. Dans ces
langages, le typage est obligatoire.