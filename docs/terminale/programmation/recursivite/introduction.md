!!! quote "Récursivité"

    La **récursivité** est une démarche qui fait référence à l'objet même de la démarche à un moment du processus. En
    d'autres termes, c'est une démarche dont la description mène à la répétition d'une même règle.[^1]

[^1]:
    [Contenu soumis à la licence CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr). Source :
    Article *[Récursivité](http://fr.wikipedia.org/wiki/R%C3%A9cursivit%C3%A9)* de
    [Wikipédia en français](https://fr.wikipedia.org/)
    ([auteurs](http://fr.wikipedia.org/w/index.php?title=R%C3%A9cursivit%C3%A9&action=history))

On trouvera donc des cas concrets de récursivité dans de nombreux domaines :

## En biologie

La récursivité est présente dans les motifs végétaux et les processus de développement.

![Chou Romanesco](chou.jpg){ align=left ; width=35% }
![Coupe d'une coquille de Nautile](nautile.jpeg){ align=right ; width=58% }

## Dans les arts

![Boite de Cacao](cacao.jpg){ align=right ; width=30% }

Dans le domaine des arts, le procédé récursif se retrouve dans le principe de la
[mise en abyme](https://fr.wikipedia.org/wiki/Mise_en_abyme).

## Dans les mathématiques

Et bien sûr en **mathématiques**, notamment avec les fractales, et l'**algorithmie** que nous allons étudier dans
les pages suivantes.