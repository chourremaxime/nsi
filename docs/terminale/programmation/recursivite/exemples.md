## Exemple 1 : Factorielle

On va étudier ici un des exemples les plus classiques de fonction pouvant être définie de manière récursive.

Si $n$ est un entier naturel non nul, $n!$ (factorielle n) est le produit des $n$ premiers entiers naturels non nuls.

Ainsi, $4! = 4 \times 3 \times 2 \times 1$, donc $4!=24$.

1. Écrire une version Python non récursive de la fonction `factorielle` utilisant une **boucle for**.
2. Écrire une version Python non récursive de la fonction `factorielle` utilisant une **boucle while**.
3. Écrire une version Python **récursive** de la fonction `factorielle`.

## Exemple 2 : Suite de Fibonacci

Dans cet exemple, on va étudier une fonction qui nécessite deux appels récursifs dans une même fonction.

La suite de Fibonacci est une suite numérique définie ($n$ appartenant aux naturels) :

$$fibonacci(n) =
\begin{cases}
0 & \quad \text{si $n = 0$}\\
1 & \quad \text{si $n = 1$}\\
fibonacci(n - 2) + fibonacci(n - 1) & \quad \text{si $n > 1$}
\end{cases}$$

1. Calculer *à la main* $fibonacci(6)$.
2. Programmer une version Python non récursive de la fonction `fibonacci` utilisant une **boucle for**.
3. Programmer une version Python non récursive de la fonction `fibonacci` utilisant une **boucle while**.
4. Programmer une version Python **récursive** de la fonction `fibonacci`.
5. Appeler la fonction `fibonacci(50)` avec la méthode for et la méthode récursive. **Conclure**.