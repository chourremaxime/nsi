## Exercice 1

Écrire une fonction **récursive** `puissance` qui prend en argument un flottant $x$ non nul et un entier naturel $n$ et
qui renvoie $x^n$.

Pour écrire cette fonction, on réfléchira au cas de base puis, on pourra écrire `puissance(n)` en fonction de
`puissance(n-1)`.

## Exercice 2 : Secret de protection

On appelle **SECRET de protection `n`**, une chaîne de caractères composée de `n` parenthèses ouvrantes, puis du mot
`SECRET` suivi de `n` parenthèses fermantes. Par exemple :

- un SECRET de protection 4 est la chaîne de caractères : `"((((SECRET))))"`
- un SECRET de protection 0 est la chaîne de caractères : `"SECRET"`

Le but de cet exercice est de programmer une **version récursive** de la fabrication d'un SECRET de protection `n`.

1. Déterminez le (ou les) cas de base.
2. Exprimer `protection(n)` en fonction de `protection(n-1)`.
3. En déduire la définition récursive de la fonction `protection()` :

## Exercice 3 : Suite de Syracuse

La suite de Syracuse d'un nombre entier $N > 0$ est définie par récurrence, de la manière suivante :

$$U_0 = N$$

$$U_{n+1} =
\begin{cases}
\frac{U_n}{2} & \quad \text{si $U_n$ est pair}\\
3 U_n + 1 & \quad \text{si $U_n$ est impair}
\end{cases}$$

La conjecture de Syracuse *(non résolue à ce jour)* affirme que $\forall N > 0, \exists n \text{ tel que } U_n = 1$.

Programmer une **fonction récursive** `syracuse` qui **affiche** les termes de la suite jusqu'à ce qu'un terme soit
égal à 1.