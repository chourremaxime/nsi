## Définition

!!! abstract "Méthode"

    Une **méthode** est une fonction définie à l’intérieur d’une classe. Elle définit les comportements des objets
    créés à partir de cette classe. Une méthode peut accéder et modifier les attributs de l’objet, ainsi que prendre
    des paramètres et renvoyer des valeurs comme toute fonction en Python.

Pour définir une méthode dans une classe, nous utilisons la syntaxe suivante :

```python
class MaClasse:
    def ma_methode(self, param1, param2):
        # Corps de la méthode
```

Ici, nous avons défini une méthode `ma_methode` dans la classe `MaClasse`. Cette méthode prend trois arguments : `self`,
`param1` et `param2`. L’argument `self` est une référence à l’objet sur lequel la méthode est appelée. Il est
automatiquement passé par Python lors de l’appel de la méthode et doit toujours être le premier argument d’une méthode.

## Exemple

```python
class Velo:
    """Représente un vélo avec une marque, un modèle et une couleur."""

    def __init__(self, marque, modele, couleur):
        """Initialise les attributs du vélo avec les valeurs données."""
        self.marque = marque
        self.modele = modele
        self.couleur = couleur

    def rouler(self, distance):
        """Fait rouler le vélo sur la distance donnée."""
        print(f"Le vélo {self.marque} {self.modele} roule sur {distance} km.")

# Création d'un objet Velo
mon_velo = Velo('Pinarello', 'Dogma F', 'Rouge')

# Appel de la méthode rouler sur l'objet
mon_velo.rouler(10)  # Le vélo Pinarello Dogma F roule sur 10 km.
```

Dans cet exemple, nous avons défini une méthode `rouler` dans la classe `Velo`. Cette méthode prend deux paramètres :
`self` et `distance`. Elle utilise le paramètre `self` pour accéder aux attributs `marque` et `modele` de l’objet et
affiche un message indiquant que le vélo roule sur la distance donnée.

Ensuite, nous avons créé un objet `mon_velo` à partir de la classe `Velo` et avons appelé la méthode `rouler` sur cet
objet en utilisant la syntaxe `#!python mon_velo.rouler(10)`. Comme on peut le voir, la méthode a bien été exécutée et
a affiché le message attendu.

## Les méthodes spéciales

Les méthodes spéciales sont des méthodes qui ont des noms spécifiques commençants et se terminant par deux underscores
`_`, comme `__init__`, `__str__` et `__repr__`. Ces méthodes sont appelées automatiquement par Python dans certaines
situations, comme lors de la création d’un objet ou lors de l’affichage d’un objet en tant que chaîne de caractères.

!!! example "`__str__` et `__repr__`"

    ```python
    class Velo:
        """Représente un vélo avec une marque, un modèle et une couleur."""
    
        def __init__(self, marque, modele, couleur):
            """Initialise les attributs du vélo avec les valeurs données."""
            self.marque = marque
            self.modele = modele
            self.couleur = couleur
    
        def __str__(self):
            """Retourne une représentation en chaîne de caractères du vélo."""
            return f"Velo {self.marque} {self.modele} de couleur {self.couleur}"
    
        def __repr__(self):
            """Retourne une représentation formelle du vélo."""
            return f"Velo('{self.marque}', '{self.modele}', '{self.couleur}')"
    
    # Création d'un objet Velo
    mon_velo = Velo('Pinarello', 'Dogma F', 'Rouge')
    
    # Affichage de l'objet en utilisant la méthode __str__
    print(mon_velo)  # Velo Pinarello Dogma F de couleur Rouge
    
    # Affichage de l'objet en utilisant la méthode __repr__
    print(repr(mon_velo))  # Velo('Pinarello', 'Dogma F', 'Rouge')
    ```

    Dans cet exemple, nous avons défini les méthodes spéciales `__str__` et `__repr__` dans la classe `Velo`. La
    méthode `__str__` retourne une représentation en chaîne de caractères du vélo, tandis que la méthode `__repr__`
    retourne une représentation formelle du vélo. Ces méthodes sont appelées automatiquement par Python lorsque nous
    affichons l’objet en utilisant la fonction `print` ou la fonction `repr`.

## Les méthodes de classe et les méthodes statiques

Les méthodes de classe et les méthodes statiques sont des méthodes qui sont liées à la classe elle-même plutôt qu’à ses
instances. Les méthodes de classe prennent en premier paramètre une référence à la classe, tandis que les méthodes
statiques ne prennent aucun paramètre spécial. Ces méthodes sont généralement utilisées pour créer des fonctions
utilitaires liées à la classe.

!!! example "`@classmethod` et `@staticmethod`"

    ```python
    class Velo:
        """Représente un vélo avec une marque, un modèle et une couleur."""
    
        nb_velos = 0  # Attribut de classe pour compter le nombre de vélos
    
        def __init__(self, marque, modele, couleur):
            """Initialise les attributs du vélo avec les valeurs données."""
            self.marque = marque
            self.modele = modele
            self.couleur = couleur
            Velo.nb_velos += 1  # Incrémente le compteur de vélos
    
        @classmethod
        def get_nb_velos(cls):
            """Retourne le nombre total de vélos créés."""
            return cls.nb_velos
    
        @staticmethod
        def get_types():
            """Retourne une liste des types de vélos disponibles."""
            return ['VTT', 'VTC', 'Vélo de route', 'Vélo électrique']
    
    # Affichage du nombre total de vélos (avant création d'objets)
    print(Velo.get_nb_velos())  # 0
    
    # Création d'objets Velo
    velo1 = Velo('Pinarello', 'Dogma F', 'Rouge')
    velo2 = Velo('Bianchi', 'Oltre XR4', 'Noir')
    
    # Affichage du nombre total de vélos (après création d'objets)
    print(Velo.get_nb_velos())  # 2
    
    # Affichage des types de vélos disponibles
    print(Velo.get_types())  # ['VTT', 'VTC', 'Vélo de route', 'Vélo électrique']
    ```

    Dans cet exemple, nous avons défini une méthode de classe `get_nb_velos` et une méthode statique `get_types` dans
    la classe `Velo`. La méthode de classe `get_nb_velos` utilise le paramètre `cls` pour accéder à l’attribut de classe
    `nb_velos` et retourner sa valeur. La méthode statique `get_types` ne prend aucun paramètre spécial et retourne
    simplement une liste des types de vélos disponibles.

    **Il est important d'utiliser `@classmethod` et `@staticmethod`** pour indiquer à Python de ne pas passer `self` en
    argument.

## La surcharge d’opérateurs

La surcharge d’opérateurs permet de définir des comportements personnalisés pour les opérateurs tels que `+`, `-`, `*`,
`/`, etc. en utilisant des méthodes spéciales. Par exemple, nous pouvons définir une méthode spéciale `__add__` dans
notre classe pour spécifier comment les objets de cette classe doivent se comporter lorsqu’ils sont ajoutés avec
l’opérateur `+`.

!!! example "`__add__`"

    ```python
    class Velo:
        """Représente un vélo avec une marque, un modèle et une couleur."""
    
        def __init__(self, marque, modele, couleur):
            """Initialise les attributs du vélo avec les valeurs données."""
            self.marque = marque
            self.modele = modele
            self.couleur = couleur
    
        def __add__(self, autre_velo):
            """Combine deux vélos en un seul vélo tandem."""
            nouvelle_marque = f"{self.marque}-{autre_velo.marque}"
            nouveau_modele = f"{self.modele}-{autre_velo.modele}"
            nouvelle_couleur = f"{self.couleur}/{autre_velo.couleur}"
            return Velo(nouvelle_marque, nouveau_modele, nouvelle_couleur)
    
    # Création de deux objets Velo
    velo1 = Velo('Pinarello', 'Dogma F', 'Rouge')
    velo2 = Velo('Bianchi', 'Oltre XR4', 'Noir')
    
    # Combinaison des deux vélos en un seul vélo tandem
    tandem = velo1 + velo2
    
    # Affichage des attributs du vélo tandem
    print(tandem.marque)  # Pinarello-Bianchi
    print(tandem.modele)  # Dogma F-Oltre XR4
    print(tandem.couleur) # Rouge/Noir
    ```

    Dans cet exemple, nous avons défini une méthode spéciale `__add__` dans la classe `Velo` pour surcharger l’opérateur
    `+`. Cette méthode prend en paramètre un autre objet `Velo` et crée un nouvel objet `Velo` qui représente un vélo
    tandem combinant les marques, les modèles et les couleurs des deux vélos originaux.

    Nous avons ensuite créé deux objets `Velo` et avons utilisé l’opérateur `+` pour les combiner en un seul objet
    `tandem`. Comme on peut le voir, les attributs du nouvel objet `tandem` combinent les valeurs des attributs des
    deux objets `Velo` originaux.