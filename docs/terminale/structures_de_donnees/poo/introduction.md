La **programmation orientée objet (POO)** est un **paradigme de programmation** qui permet de structurer le code
en utilisant des **objets** représentant des entités du monde réel. En utilisant la POO, cela permet de créer des
programmes plus modulaires, plus faciles à maintenir et à réutiliser.

Un exemple concret d’utilisation de la POO dans la vie réelle est la simulation de systèmes de fabrication.
En utilisant la POO, on peut créer des objets représentant les différentes parties d’un système de fabrication, comme
les machines, les produits et les employés. Ces objets peuvent interagir entre eux pour simuler le fonctionnement
du système.

Nous utilisons déjà des objets en Python sans le remarquer. Par exemple une **liste est un objet**, puisqu'on peut
appeler des fonctions ***(méthodes)*** sur cet objet.

```python
m = [4,5,2]
type(m) # list
m.reverse() # [2,5,4]
```

Et on peut obtenir la liste des méthodes disponibles grâce à `#!python dir()`.

```python
dir(m)
# ['__add__',
# '__class__',
# '__contains__',
# '__delattr__',
# ...
# 'clear',
# 'copy',
# 'count',
# 'extend',
# 'index',
# 'insert',
# 'pop',
# 'remove',
# 'reverse',
# 'sort']
```