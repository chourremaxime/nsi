!!! danger "Hors-programme"

L'héritage est explicitement hors-programme. Ce sujet est toutefois abordé ici, car il s'agit d'un élément très
important de la programmation orientée objet.

L'héritage ne sera donc pas évalué lors des évaluations en classe ou au baccalauréat.

!!! abstract "Héritage"

    L’**héritage** est un mécanisme en POO qui permet à une classe d'hériter des attributs et des méthodes d’une autre
    classe. La classe qui hérite est appelée la **sous-classe**, tandis que la classe dont elle hérite est appelée la
    **super-classe**. L’héritage permet de réutiliser du code et de créer des hiérarchies de classes pour représenter
    des concepts liés.

En Python, l’héritage est réalisé en définissant une nouvelle classe et en spécifiant la super-classe entre parenthèses
après le nom de la nouvelle classe. La nouvelle classe hérite alors des attributs et des méthodes de la super-classe.

On utilise la méthode `super()` qui permet de faire référence à la super-classe.

```py
class Velo:
    """Représente un vélo avec une marque, un modèle et une couleur."""

    def __init__(self, marque, modele, couleur):
        """Initialise les attributs du vélo avec les valeurs données."""
        self.marque = marque
        self.modele = modele
        self.couleur = couleur

    def rouler(self, distance):
        """Fait rouler le vélo sur la distance donnée."""
        print(f"Le vélo {self.marque} {self.modele} roule sur {distance} km.")

class VeloElectrique(Velo):
    """Représente un vélo électrique avec une autonomie en plus."""

    def __init__(self, marque, modele, couleur, autonomie):
        """Initialise les attributs du vélo électrique avec les valeurs données."""
        super().__init__(marque, modele, couleur)
        self.autonomie = autonomie

    def rouler(self, distance):
        """Fait rouler le vélo électrique sur la distance donnée."""
        if distance > self.autonomie:
            print(f"Le vélo électrique {self.marque} {self.modele} ne peut pas rouler sur {distance} km.")
            return
        super().rouler(distance)
        self.autonomie -= distance

# Création d'un objet VeloElectrique
mon_velo = VeloElectrique('Pinarello', 'Dogma F', 'Rouge', 150)

# Appel de la méthode rouler sur l'objet
mon_velo.rouler(40)  # Le vélo Pinarello Dogma F roule sur 40 km.
mon_velo.rouler(200)  # Le vélo électrique Pinarello Dogma F ne peut pas rouler sur 200 km.
```