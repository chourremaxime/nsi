L'exercice est à rédiger à la suite des exercices précédents.

## Exercice 9 : Utiliser l’encapsulation

- Modifier la classe `Chien` pour rendre les attributs `nom`, `race` et `age` privés en ajoutant un préfixe `__`
  devant leurs noms.
- Modifier les méthodes `get_nom`, `get_race`, `get_age`, `set_nom`, `set_race` et `set_age` pour utiliser les
  attributs privés au lieu des attributs publics.
- Créer un objet `chien4` à partir de la classe `Chien` et utiliser les méthodes publiques pour accéder et modifier
  les attributs privés du chien.