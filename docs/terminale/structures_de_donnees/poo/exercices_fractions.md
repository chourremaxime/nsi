**Python s’Objette aux Fractions : Un Voyage dans le Monde des Fractions avec Python**

!!! quote "Synopsis"

    Vous devez **créer un module de logiciel éducatif** pour aider les élèves à comprendre les **fractions** en
    utilisant Python et la programmation orientée objet. Vous allez **développer une classe `Fraction`** qui représente
    une fraction avec deux parties : le numérateur (le nombre au-dessus) et le dénominateur (le nombre en bas).
    Cette classe devra permettre aux utilisateurs de faire des **opérations arithmétiques simples** comme l'addition,
    la soustraction, la multiplication et la division. Elle devra également permettre de **comparer les fractions**
    entre elles et garantir que chaque fraction est **simplifiée** à sa forme la plus simple.

N'oubliez pas de **tester vos fonctions** ! Cet exercice propose une implémentation des fractions en Python, mais **vous
pouvez ne pas suivre l'énoncé** et proposer votre propre version.

## Classes et attributs

Commencez par définir une classe `Fraction`. Cette classe doit avoir deux attributs : `num` (numérateur) et `denom`
(dénominateur). Vérifiez que les propriétés nécessaires à nos fractions soient respectées à l'aide de `assert`.

??? question "Indice"

    Vous pouvez initialiser ces attributs dans la méthode `__init__`. Assurez-vous que le dénominateur ne soit jamais
    égal à zéro et que le numérateur et le dénomiateur soient de type `int` en ajoutant un `assert` dans la
    méthode `__init__`.

## Méthodes

Ajoutez une méthode `simplifier` à la classe. Cette méthode doit simplifier la fraction en réduisant le numérateur et
le dénominateur à leur plus petit commun diviseur.

Ajoutez ensuite une méthode `__str__` pour pouvoir afficher convenablement les fractions sur une seule ligne *(par
exemple `3/4`)*.

??? question "Indice"

    Vous pouvez utiliser l’algorithme d’Euclide pour trouver le plus grand commun diviseur. [Consultez l’article
    Wikipédia sur l’algorithme d’Euclide](https://fr.wikipedia.org/wiki/Algorithme_d%27Euclide) pour plus de détails.

## Surcharge d’opérateurs :

Vous devez maintenant surcharger les opérateurs `+`, `-`, `*`, `/`. Ces méthodes doivent retourner une nouvelle
instance de `Fraction` qui est le résultat de l’opération. N’oubliez pas de simplifier la fraction avant de retourner
le résultat.

Rajoutez ensuite la surcharge du `==` et de la comparaison.

??? question "Indice"

    Pour cela, vous devez définir les méthodes spéciales `__add__`, `__sub__`, `__mul__` et `__div__` qui prennent
    en argument une autre instance de `Fraction`. Consultez la [documentation Python sur les méthodes
    spéciales](https://docs.python.org/3/reference/datamodel.html#emulating-numeric-types) pour plus d’informations.

    Pour la surcharge du `==` et de la comparaison, vous devez utiliser les [opérateurs présents sur la documentation
    Python](https://docs.python.org/3/reference/datamodel.html#object.__lt__) (de `__lt__` à `__ge__`)