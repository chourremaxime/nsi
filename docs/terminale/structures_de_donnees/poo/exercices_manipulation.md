Les exercices ci-dessous peuvent être réalisés dans **un seul et unique fichier**. Vous mettrez donc votre classe à jour
à chaque exercice.

## Exercice 1 : Définir une classe

- Définir une classe Chien qui représente un chien avec un nom, une race et un âge.
- La classe doit avoir une méthode `__init__` pour initialiser les attributs du chien avec les valeurs données.
- La classe doit également avoir des méthodes `get_nom`, `get_race` et `get_age` pour retourner les valeurs des
  attributs du chien.

## Exercice 2 : Créer des objets

- Créer deux objets `chien1` et `chien2` à partir de la classe `Chien` en utilisant les valeurs de votre choix pour les
  attributs.
- Utiliser les méthodes `get_nom`, `get_race` et `get_age` pour afficher les valeurs des attributs des deux chiens.

## Exercice 3 : Définir des méthodes

- Ajouter une méthode `aboyer` à la classe `Chien` qui affiche un message indiquant que le chien aboie.
- Ajouter également une méthode `dormir` qui prend en argument le nombre d’heures de sommeil et affiche un message
  indiquant que le chien dort pendant ce temps.
- Appeler les méthodes `aboyer` et `dormir` sur les deux objets `chien1` et `chien2`.

## Exercice 4 : Modifier des attributs

- Ajouter des méthodes `set_nom`, `set_race` et `set_age` à la classe `Chien` pour modifier les valeurs des attributs
  du chien.
- Utiliser ces méthodes pour modifier les valeurs des attributs des deux objets `chien1` et `chien2`.
- Afficher à nouveau les valeurs des attributs des deux chiens pour vérifier que les modifications ont bien été prises
  en compte.

## Exercice 5 : Utiliser des méthodes spéciales

- Modifier la classe `Chien` pour ajouter une méthode spéciale `__str__` qui retourne une chaîne de caractères
  représentant le chien.
- Ajouter également une méthode spéciale `__repr__` qui retourne une représentation formelle du chien.
- Créer un objet `chien3` à partir de la classe Chien et utiliser les fonctions `print` et `repr` pour afficher les
  représentations en chaîne de caractères et formelle du chien.

## Exercice 6 : Utiliser des méthodes de classe

- Ajouter une méthode statique `creer_chiot` à la classe `Chien` qui prend en argument un nom et une race et crée un
  nouvel objet `Chien` avec l’âge `0`.
- Utiliser cette méthode pour créer un nouvel objet `chiot1` à partir de la classe `Chien`.
- Afficher les attributs du nouvel objet `chiot1` pour vérifier qu’il a bien été créé avec l’âge `0`.

## Exercice 7 : Surcharge d’opérateurs

- Ajouter une méthode spéciale `__add__` à la classe `Chien` pour surcharger l’opérateur `+`.
- Cette méthode doit prendre en argument un autre objet `Chien` et retourner un nouvel objet `Chien` représentant un
  chiot issu des deux chiens parents.
- Utiliser l’opérateur `+` pour combiner les chiens `chien1` et `chien2` en un seul objet `chiot2`.
- Afficher les attributs du nouvel objet `chiot2` pour vérifier qu’il a bien été créé.