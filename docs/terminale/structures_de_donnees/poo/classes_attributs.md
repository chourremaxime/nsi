## Définitions

!!! abstract "Classe"

    Une **classe** est un modèle pour créer des objets (aussi appelés instances). Elle définit les attributs et les
    méthodes communs à tous les objets créés à partir de cette classe. On peut considérer une classe comme un plan
    pour construire des objets.

!!! abstract "Attribut"

    Un **attribut** est une variable associée à un objet. Les attributs définissent les caractéristiques de l’objet,
    comme sa couleur, sa taille ou son état. Chaque objet créé à partir d’une classe a ses propres attributs, dont les
    valeurs peuvent être différentes des autres objets de la même classe.

Pour définir une classe, nous utilisons la syntaxe suivante :

```python
class MaClasse:
    """Docstring pour documenter la classe."""

    def __init__(self, param1, param2):
        """Méthode d'initialisation des attributs de la classe."""
        self.attribut1 = param1
        self.attribut2 = param2
```

Dans cet exemple, nous avons défini une classe `MaClasse` avec une docstring pour documenter la classe. Nous avons
également défini une méthode d’initialisation `__init__` pour initialiser les attributs de la classe avec les valeurs
passées en arguments lors de la création d'un objet. Le paramètre `self` correspond à l'objet créé par la méthode
d'initialisation. Il est passé par Python automatiquement à chaque appel de méthode. Nous en reparlerons un peu plus
tard.

Pour créer un objet à partir de cette classe, nous utilisons la syntaxe suivante :

```python
mon_objet = MaClasse(val1, val2)
```

Dans cet exemple, nous avons créé un objet `mon_objet` à partir de la classe `MaClasse` en passant les valeurs `val1`
et `val2` en arguments. Ces valeurs sont utilisées pour initialiser les attributs `attribut1` et `attribut2` de l’objet.

!!! info "CamelCase"

    ![CamelCase illustré avec un chameau](camel_case.png){ align=right : width=20% }

    En Python, il est d’usage de nommer les classes en utilisant la convention de nommage **CamelCase**, où chaque mot
    commence par une majuscule et les mots sont collés les uns aux autres sans espaces. Cette convention permet de
    distinguer facilement les noms de classes des autres identificateurs, tels que les variables, les fonctions et
    les méthodes, qui suivent généralement d’autres conventions de nommage.

    L’utilisation de la majuscule pour le premier mot du nom d’une classe permet également de suivre la convention
    générale en programmation orientée objet, où les classes représentent des concepts abstraits et sont souvent
    considérées comme des entités importantes dans le code. La majuscule donne une certaine importance visuelle au
    nom de la classe et aide à le distinguer des autres identificateurs.

## Exemple

Imaginons que nous voulons créer une classe `Velo` pour représenter des vélos dans notre programme. Nous pouvons
définir cette classe en Python comme suit :

```python
class Velo:
    """Représente un vélo avec une marque, un modèle et une couleur."""

    def __init__(self, marque, modele, couleur):
        """Initialise les attributs du vélo avec les valeurs données."""
        self.marque = marque
        self.modele = modele
        self.couleur = couleur
```

Dans cet exemple, nous avons défini une classe `Velo` avec trois attributs : `marque`, `modele` et `couleur`. Ces
attributs sont initialisés dans la méthode `__init__`, qui est **appelée automatiquement** lorsqu’un objet est créé
à partir de cette classe.

Nous pouvons maintenant créer des objets à partir de cette classe en utilisant la syntaxe suivante :

```python
# Création d'un objet Velo
mon_velo = Velo('Pinarello', 'Dogma F', 'Rouge')

# Affichage des attributs de l'objet
print(mon_velo.marque)  # Pinarello
print(mon_velo.modele)  # Dogma F
print(mon_velo.couleur) # Rouge
```

Dans cet exemple, nous avons créé un objet `mon_velo` à partir de la classe `Velo` en utilisant la syntaxe `Velo(...)`.
Nous avons passé les valeurs `'Pinarello'`, `'Dogma F'` et `'Rouge'` en arguments pour initialiser les attributs
`marque`, `modele` et `couleur` de l’objet.

Ensuite, nous avons utilisé la syntaxe `mon_velo.attribut` pour accéder aux attributs de l’objet et afficher leurs
valeurs. Comme on peut le voir, les attributs ont bien été initialisés avec les valeurs que nous avons passées lors de
la création de l’objet.

## Modifier les attributs

En Python, il est possible de modifier les attributs d’un objet après sa création, car les attributs sont des variables
associées à l’objet. Comme toute variable en Python, la valeur d’un attribut peut être modifiée en utilisant une
affectation. On dit que **les attributs sont mutables**.

Lorsque nous créons un objet à partir d’une classe, les attributs de l’objet sont initialisés avec les valeurs
spécifiées dans la méthode `__init__` de la classe. Cependant, ces valeurs ne sont pas figées et peuvent être modifiées
ultérieurement en utilisant une affectation.

La possibilité de modifier les attributs d’un objet est très utile, car elle permet de mettre à jour l’état de l’objet
au fur et à mesure que le programme s’exécute. Par exemple, dans notre exemple précédent avec la classe `Velo`, si nous
voulons changer la couleur de ce vélo, nous pouvons simplement modifier l’attribut couleur de l’objet `mon_velo` pour
refléter ce changement.

!!! warning "Attention"

Il est important de noter que la modification des attributs d’un objet doit être effectuée avec précaution. Dans
certains cas, il peut être préférable de **définir des méthodes pour modifier les attributs de manière contrôlée**
plutôt que de les modifier directement. Cela permet de **s’assurer que les modifications sont valides et cohérentes**
avec les autres attributs de l’objet, et avec les autres instances de la classe.

!!! quote "Métaphore"

    - Une classe, c'est le plan d'une maison (abstrait).
    - Un objet, c'est la maison issue du plan (concret).
    - Une autre maison, issue du même plan, est aussi un objet qui peut avoir des caractéristiques (attributs)
    différentes (peinture, mobilier, etc...) de la première maison.

??? plus-loin "Objets & Instances"

    En programmation orientée objet, les termes *objet* et *instance* sont souvent utilisés de manière interchangeable
    pour désigner une entité créée à partir d'une classe. Une **instance** est un objet qui a été créé à partir d’une
    classe et qui possède les attributs et les méthodes définis par cette classe.
    
    Cependant, il existe une nuance entre ces deux termes. Un objet est un bloc de mémoire contigu qui stocke les
    informations réelles qui distinguent cet objet des autres objets, tandis qu'une instance est une référence à un
    objet. C'est un bloc de mémoire, qui pointe vers l'adresse de départ de l'endroit où l'objet est stocké.
    
    En résumé, **un objet est une entité concrète** qui existe en mémoire, tandis qu'**une instance est une référence à
    cet objet**. Dans la pratique, ces deux termes sont souvent utilisés de manière interchangeable pour désigner une
    entité créée à partir d’une classe.