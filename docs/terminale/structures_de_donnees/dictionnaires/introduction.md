Les **dictionnaires** sont une **structure de données** qui permet de stocker et de récupérer des informations **de
manière efficace**. Ils sont implémentés en utilisant une structure appelée **table de hachage**, qui est ce qui permet
aux dictionnaires d’avoir une **complexité temporelle moyenne de $\O(1)$** pour les opérations d’insertion, de recherche
et de suppression.

Un dictionnaire est une **collection non ordonnée de paires clé-valeur**. Chaque clé dans le dictionnaire est **unique**
et est associée à une valeur spécifique. Les dictionnaires sont **mutables**, ce qui signifie que nous pouvons ajouter,
supprimer et modifier les éléments d’un dictionnaire après sa création.

Une compréhension de l’implémentation des dictionnaires peut nous aider à écrire un code plus performant. Par exemple,
si nous savons que les opérations d’insertion, de recherche et de suppression ont une complexité temporelle moyenne de
$\O(1)$ dans un dictionnaire, nous pouvons **choisir d’utiliser un dictionnaire au lieu d’une liste** lorsque nous avons
besoin d’effectuer un grand nombre de ces opérations.