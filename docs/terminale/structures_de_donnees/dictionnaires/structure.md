## Étape 1 : Concevoir le cahier des charges (TAD)

Un dictionnaire est un type abstrait de données contenant un ensemble de paires `(clé, valeur)`, dont chaque clé est
unique, et l'accès à une valeur depuis une clé se fait en accès constant.

Nous nommons notre structure de dictionnaire avec le nom `Dict[K,V]` dont les clés sont de type `K` et les valeurs sont
de type `V`.

!!! syntaxe "Les Primitives"

    Nous avons dans le dictionnaire plusieurs primitives importantes qui vont nous permettre de créer et manipuler les
    valeurs de notre dictionnaire :

    - `creer_dictionnaire()` : _Dict[K,V]_ - crée un dictionnaire vide et renvoie ce nouveau dictionnaire.
    - `inserer()` : _Dict[K,V], K, V -> Dict[K,V]_ - associe une nouvelle valeur à une nouvelle clé.
    - `modifier()` : _Dict[K,V], K, V -> Dict[K,V]_ - associe une nouvelle valeur à une ancienne clé.
    - `supprimer()` : _Dict[K,V], K -> Dict[K,V]_ - supprime la valeur associée à une clé, et la clé.
    - `rechercher()` : _Dict[K,V], K -> V_ - renvoie la valeur associée à une clé.

## Étape 2 : Proposer une implémentation

### Table de hachage

!!! abstract "Fonction de hachage"

    Une **fonction de hachage** est une fonction qui prend une entrée _(ou une clé)_ et retourne un nombre fixe de
    caractères, appelé _**hash**_.
    
    Les fonctions de hachage sont conçues de manière que **chaque clé unique produise un hash unique**, bien que dans
    la pratique, deux clés différentes peuvent produire le même hash, ce qu’on appelle une **collision de hachage**.
    
    Dans le cas des dictionnaires, la fonction de hachage doit renvoyer un nombre entier positif.

Les dictionnaires utilisent un tableau _(appelé table de hachage)_ de taille finie pour stocker les paires clé-valeur.
La clé est hachée à l’aide d’une fonction de hachage, et le résultat est utilisé comme index pour stocker la valeur
correspondante dans la table. **Si le hash est plus grand que la taille du tableau, on calcule son modulo à la taille
du tableau pour se ramener à une valeur bornée.**

Lorsqu’on cherche une valeur dans le dictionnaire, on utilise la fonction de hachage pour hacher la clé et trouver
l’index de la valeur.

!!! example "Exemple de fonctions"

    ```py
    def simple_hash(s):
        """Cette fonction prend une chaîne de caractères et retourne un nombre qui représente cette chaîne."""
        return sum(ord(char) for char in s)
    ```
    
    Dans cette fonction, `ord(char)` retourne la valeur ASCII du caractère, et `sum()` additionne ces valeurs pour tous
    les caractères de la chaîne.
    
    C’est une fonction de hachage très basique qui n’est pas idéale pour une utilisation réelle, car elle peut
    facilement conduire à des collisions (deux anagrammes produisent le même hash). Cependant, elle sert à illustrer le
    concept.
    
    ```py
    def djb2(s):
        """Cette fonction prend une chaîne de caractères et retourne un nombre qui représente cette chaîne."""
        hash = 5381
        for x in s.encode():
            hash = ((hash << 5) + hash) + x
        return hash & 0xFFFFFFFF_FFFFFFFF  # Retourne un hash de 64 bits
    ```
    
    Cette fonction est une implémentation simple de l’algorithme de hachage DJB2, qui est un algorithme de hachage
    couramment utilisé pour les chaînes de caractères.
    
    Dans cette fonction, nous commençons par un nombre initial (5381), puis pour chaque caractère dans la chaîne, nous
    décalons le hash actuel de 5 bits vers la gauche (ce qui est équivalent à le multiplier par 32), ajoutons le hash
    actuel, ensuite ajoutons la valeur ASCII du caractère. Le résultat est un nombre qui est relativement unique pour
    chaque chaîne de caractères unique.
    
    Comme toutes les fonctions de hachage, il est possible que deux chaînes différentes produisent le même hash (une
    collision). Cependant, l’algorithme DJB2 est conçu de manière à minimiser la probabilité de telles collisions.

!!! bug "Mise en pratique"

    On souhaite ajouter les couples `(clé, valeur)` suivants en utilisant la fonction de hachage `len()` dans un
    tableau de taille 5 :
    
    - `("NSI", "Chourré")`
    - `("Histoire & Geographie", "Charlemagne")`
    - `("Anglais","Biden")`
    - `("Sport", "Marchand")`
    
    Questions :
    
    1. Représenter le dictionnaire contenant ces clés/valeurs
    2. Que se passe-t-il si on souhaite ajouter `("Mathématiques", "Poincaré")` ?
    3. Que se passe-t-il si on souhaite rechercher la valeur ayant pour clé `"SNT"` ?

### Collision

!!! abstract "Définition"

    Une collision de hachage se produit lorsque **deux clés différentes produisent le même hash**. Cela peut poser un
    problème, car notre table de hachage dépend de l’**unicité des hashes** pour retrouver rapidement les valeurs
    associées à une clé donnée. Si deux clés différentes ont le même hash, alors la deuxième clé insérée écraserait la
    première.

??? plus-loin "Comment Python gère les collisions de hachage ?"

    Python utilise une méthode appelée **linear probing** pour gérer les collisions de hachage. _Plus précisément, il
    utilise une variante du linear probing appelée random probing._
    
    Lorsqu’une collision se produit, Python essaie de **placer la deuxième clé à la position suivante** dans la table
    de hachage. Si cette position est également occupée, il essaie la position suivante, et ainsi de suite, jusqu’à ce
    qu’il trouve une position libre.
    
    Cela signifie que lors de la recherche d’une clé, Python peut avoir à parcourir plusieurs positions dans la table
    de hachage jusqu’à ce qu’il trouve la clé recherchée. Cependant, en pratique, les collisions sont relativement
    rares et la taille de la table de hachage est généralement beaucoup plus grande que le nombre d’éléments qu’elle
    contient, de sorte que le nombre moyen de positions que Python doit vérifier reste très faible.

!!! info "Comment nous, on gère les collisions de hachage ?"

    Une autre méthode pour gérer ces collisions existe et s'appelle **chaînage**.

    Avec cette méthode, chaque position dans la table de hachage est associée à une **liste chaînée de paires
    clé-valeur**. Lorsqu’une collision se produit _(c’est-à-dire que deux clés différentes produisent le même hash)_,
    la nouvelle paire clé-valeur est simplement ajoutée à la fin de la liste chaînée.

### Redimensionnement

Lorsque nous ajoutons des éléments à un dictionnaire, la table de hachage peut finir par **manquer d’espace**. Si nous
continuons à ajouter des éléments sans augmenter la taille de la table de hachage, nous finirons par avoir de
nombreuses collisions de hachage, ce qui ralentira les opérations de recherche, d’insertion et de suppression.

Pour éviter cela, on augmente donc la taille de la table de hachage lorsque le nombre d’éléments atteint un certain
seuil. Cela permet de maintenir un **faible taux de remplissage** de la table de hachage, ce qui **minimise le nombre
de collisions** de hachage et permet d’obtenir des performances optimales.

!!! methode "Comment faire ?"

    Lorsque le nombre d’éléments dans un dictionnaire dépasse un certain seuil, on crée une **nouvelle table de
    hachage** de taille plus grande. On parcourt ensuite tous les éléments de l’ancienne table de hachage et les
    **insère** dans la nouvelle table de hachage. C'est tout.

!!! warning "Mais c'est long à faire !"

    Il est important de noter que le redimensionnement d’une table de hachage est une **opération coûteuse**, car elle
    nécessite la création d’une nouvelle table de hachage et le **repositionnement** de tous les éléments. Cependant,
    puisque le redimensionnement double généralement la taille de la table de hachage, **le nombre de redimensionnements
    nécessaires est logarithmique par rapport au nombre d’éléments insérés**. Cela signifie que le coût du
    redimensionnement est amorti sur un grand nombre d’opérations d’insertion, de sorte que le coût moyen par opération
    reste constant.

### Complexité temporelle

- Insertion : L’insertion d’un nouvel élément dans un dictionnaire a une complexité temporelle moyenne de $\O(1)$. Cela
  signifie que le temps nécessaire pour insérer un nouvel élément reste **constant, quel que soit le nombre d’éléments**
  déjà présents dans le dictionnaire. C’est parce que l’insertion implique simplement de **calculer le hash de la clé**,
  d’utiliser ce hash pour trouver un emplacement dans la table de hachage, et d’**insérer la paire clé-valeur** à cet
  emplacement. Même si une collision de hachage se produit, on peut gérer cela efficacement grâce à notre liste chaînée.
- Recherche : La recherche d’un élément dans un dictionnaire a également une complexité temporelle moyenne de $\O(1)$.
  Comme pour l’insertion, la recherche implique de **calculer le hash de la clé** et d’utiliser ce hash pour trouver
  l’emplacement de la paire clé-valeur dans la table de hachage.
- Suppression : La suppression d’un élément a une complexité temporelle moyenne de $\O(1)$ pour les mêmes raisons que
  l’insertion et la recherche.