Dans cette unique application des dictionnaires, on souhaite trouver les mots les plus fréquents dans un texte.

Commencer par rédiger une fonction `occurrences(l)` qui prend en paramètre une liste `l` de chaînes de caractères et
renvoie le dictionnaire d'occurrences de chaque élément de `l`. _Par exemple, si `l=["a", "oui", "b", "a", "b", "a"]`
, alors le dictionnaire renvoyé est sous la forme `{"a": 3, "b": 2, "oui": 1}`._

On utilisera bien évidemment la classe `Dict` créée à l'aide de l'implémentation vue en cours.

Rédiger ensuite une nouvelle fonction `plus_frequent(d, i)` qui prend en paramètre un dictionnaire `Dict[str,int]` et
un entier `i` et renvoie la chaîne de caractères contenant `i` lettres la plus fréquente. _Avec l'exemple précédent,
et la valeur `i = 1`, la fonction doit renvoyer `a`._

Récupérer le fichier [chaperon_rouge.txt](chaperon_rouge.txt){ download="chaperon_rouge.txt" } et extraire chaque mot
du conte dans une liste `conte`. Attention à ne pas inclure les virgules, points, etc.

Afficher ensuite, pour chaque nombre de lettres, le mot le plus fréquent et son occurrence.