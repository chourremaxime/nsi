!!! info "Rappel"

    Les [listes chaînées](../listes/introduction.md) vues précédemment permettent d'accéder **rapidement** à un élément
    situé en **début ou fin de liste**, tout comme les [piles](../piles_files/structure_pile.md) et les
    [files](../piles_files/structure_file.md).
    
    Ces structures de données ne permettent toutefois pas d'avoir un accès rapide à un **élément qui ne serait pas
    situé en bout de liste**. En effet, à chaque recherche ou ajout, il faut parcourir tous les éléments de la
    structure de données.

Les arbres permettent avant tout de représenter une structure de données arborescente.

Il s'agit d'une structure qui permet d'arriver à n'importe quel endroit en partant de la racine, où chaque élément se
découpe en plusieurs branches.

On les retrouve notamment dans les arborescences de fichiers.

![Arborescence simple](arborescence_simple.png)

Dans le cadre programme de terminale, nous nous concentrerons sur une sous-partie des arbres : les **arbres binaires**.