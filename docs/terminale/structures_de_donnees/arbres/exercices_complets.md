Passons aux choses sérieuses ! Il est temps de rédiger de nouvelles fonctions pour nos arbres !

## Exercice n°1 : Les parcours

Rédiger en Python une fonction pour chaque type de [parcours](parcours.md). La fonction devra afficher la valeur du
nœud traité par le parcours.

_**Bonus (pour les plus rapides) :** Les fonctions de parcours prennent en paramètre une fonction qui réalise une action
pour chaque nœud. Cette fonction prendra en paramètre la valeur stockée dans le nœud parcouru._

## Exercice n°2 : Arbre parfait

Rédiger une fonction `parfait(h)` qui prend en paramètre un entier `h` et renvoie un arbre parfait de hauteur `h` avec
des valeurs aléatoires.

## Exercice n°3 : Peigne

Rédiger deux fonctions `est_peigne_gauche(a)` et `est_peigne_droit(a)` qui prennent en paramètre un arbre `a` et
renvoient `True` si `a` est un peigne gauche (ou droit) et `False` sinon.