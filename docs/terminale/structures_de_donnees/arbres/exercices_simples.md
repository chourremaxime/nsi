Avant de s'attaquer au plus dur, commençons par mettre en place des fonctions sur les points abordés dans le cours.

## Exercice n°1 : Calcul de la taille

Rédiger une fonction récursive `taille(a)` qui prend en paramètre un arbre binaire `a` et qui renvoie la taille de cet
arbre.

## Exercice n°2 : Calul de la hauteur

Rédiger une fonction récursive `hauteur(a)` qui prend en paramètre un arbre binaire `a` et qui renvoie la hauteur de cet
arbre.