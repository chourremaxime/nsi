## Exercice n°1 : Application directe

![Graphe](graphe_exercice.png)

En utilisant les trois méthodes vues dans le cours, représenter le graphe ci-dessus en Python.

## Exercice n°2 : Une classe, c'est la classe !

Créer une classe, en utilisant la méthode de votre choix, qui permet d'intéragir avec un graphe **orienté**. On doit
pouvoir nommer les sommets comme souhaité.

- Création de graphe vide
- Ajout de sommet
- Suppression de sommet
- Ajout d'arête
- Suppression d'arête
- Savoir si une arête se trouve entre deux sommets donnés
- Connaître la liste des voisins d'un sommet

**Pour les plus rapides :** En utilisant l'[héritage _(hors-programme)_](../poo/heritage.md), créer une classe dédiée
aux graphes non orientés ayant les mêmes fonctionnalités.

On souhaite ensuite représenter un chemin dans un graphe _(cela nous sera très utile pour les futurs algorithmes)_.

## Exercice n°3 : Les chemins

Comment représenter un chemin dans un graphe ? Quels sont les limites de votre choix ?

Implémentez ensuite une méthode dans la classe `Graphe` qui permet de vérifier qu'un chemin donné est bien un chemin
dans le graphe.