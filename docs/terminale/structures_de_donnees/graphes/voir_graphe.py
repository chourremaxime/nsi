Q='LST'
P=print
N=list
M=type
J='MAT'
I='DCT'
H=range
E=len
import webbrowser as O
def visualiser_graphe(graphe):
	B=graphe;K=I
	if M(B)==N:
		K=J
		for A in B:
			if E(A)!=E(B):K=Q;break
	D=set();F='graph{'
	if K==I:
		for G in B:
			for L in B[G]:
				if not(G,L)in D:F+=f"{G}--{L};";D.add((G,L));D.add((L,G))
	else:
		for A in H(E(B)):
			for C in H(E(B[A])):
				if K==J:
					if not(A,C)in D:
						if B[A][C]==1:F+=f"{A}--{C};"
						elif B[A][C]!=0:F+=f"{A}--{C}[label={B[A][C]}];"
						D.add((A,C));D.add((C,A))
				elif not(A,B[A][C])in D:F+=f"{A}--{B[A][C]};";D.add((A,B[A][C]));D.add((B[A][C],A))
	F+='}';R=f"https://dreampuf.github.io/GraphvizOnline/#{F}";P(f"Si votre navigateur ne s'ouvre pas, copiez/collez ce lien : {R}");O.open(R)
def visualiser_digraphe(graphe):
	A=graphe;F=I
	if M(A)==N:
		F=J
		for B in A:
			if M(B)!=N or E(B)!=E(A):F=Q;break
	C='digraph{'
	if F==I:
		for G in A:
			for L in A[G]:C+=f"{G}->{L};"
	else:
		for B in H(E(A)):
			for D in H(E(A[B])):
				if F==J:
					if A[B][D]==1:C+=f"{B}->{D};"
					elif A[B][D]!=0:C+=f"{B}->{D}[label={A[B][D]}];"
				else:C+=f"{B}->{A[B][D]};"
	C+='}';K=f"https://dreampuf.github.io/GraphvizOnline/#{C}";P(f"Si votre navigateur ne s'ouvre pas, copiez/collez ce lien : {K}");O.open(K)