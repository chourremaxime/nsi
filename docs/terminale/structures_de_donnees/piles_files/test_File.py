import unittest
from file import File


class TestFile(unittest.TestCase):
    """
    Cette classe permet d'introduire des tests pour tester nos différentes méthodes.
    Pour créer des tests, il suffit de créer une méthode avec le nom du test et de
    tester différentes choses à l'intérieur.
    Le mot clef self permet d'avoir accès à des méthodes de test poussées.
    """

    def test_creation(self):
        une_file = File()
        self.assertIsInstance(une_file, File)  # Vérifie qu'une file créée est bien de type File
        self.assertTrue(une_file.est_vide())  # Vérifie qu'une file créée est bien vide

    def test_fonctionnement_1(self):
        """
        Différents tests pour enfiler (et défiler) des éléments de même type
        """
        une_file = File()

        une_file.enfiler(3)
        une_file.enfiler(7)
        une_file.enfiler(8)
        self.assertEqual(une_file.defiler(), 3)
        self.assertEqual(une_file.defiler(), 7)
        self.assertEqual(une_file.defiler(), 8)

    def test_fonctionnement_2(self):
        """
        Différents tests pour enfiler (et défiler) des éléments de même type
        """
        une_file = File()

        une_file.enfiler(3)
        self.assertEqual(une_file.defiler(), 3)
        une_file.enfiler(7)
        self.assertEqual(une_file.defiler(), 7)
        une_file.enfiler(8)
        self.assertEqual(une_file.defiler(), 8)

    def test_fonctionnement_3(self):
        """
        Différents tests pour enfiler (et défiler) des éléments de même type
        """
        une_file = File()

        une_file.enfiler(3)
        une_file.enfiler(7)
        self.assertEqual(une_file.defiler(), 3)
        une_file.enfiler(8)
        self.assertEqual(une_file.defiler(), 7)
        self.assertEqual(une_file.defiler(), 8)

    def test_est_vide(self):
        """
        Vérifie le fonctionnement de la méthode est_vide() et du défilement d'une file vide
        """
        une_file = File()

        self.assertTrue(une_file.est_vide())
        une_file.enfiler(3)
        self.assertFalse(une_file.est_vide())
        une_file.enfiler(7)
        self.assertFalse(une_file.est_vide())
        une_file.defiler()
        self.assertFalse(une_file.est_vide())
        une_file.defiler()
        self.assertTrue(une_file.est_vide())

        with self.assertRaises(Exception):
            une_file.defiler()

    def test_homogeneite(self):
        """
        Vérifie que l'homogénéité est bien respectée
        """
        une_file = File()

        une_file.enfiler(3)
        with self.assertRaises(Exception):
            une_file.enfiler("A")
        une_file.enfiler(7)
        with self.assertRaises(Exception):
            une_file.enfiler([])
        une_file.defiler()
        une_file.defiler()
        self.assertTrue(une_file.est_vide())


if __name__ == '__main__':
    unittest.main()
