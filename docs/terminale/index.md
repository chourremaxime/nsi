# Terminale NSI

Vous êtes au bon endroit pour consulter les cours de Terminale NSI !

## Programme

Au programme cette année :

- [ ] Histoire de l'informatique.
    - [ ] Événements clés de l'histoire de l'informatique.
- [x] Structures de données.
    - [x] Structures de données, interface et implémentation.
    - [x] Vocabulaire de la programmation objet : classes, attributs, méthodes, objets.
    - [x] Listes, piles, files : structures linéaires.
    - [x] Dictionnaires, index et clé.
    - [x] Arbres : structures hiérarchiques.
    - [x] Arbres binaires : nœuds, racines, feuilles, sous-arbres gauches, sous-arbres droits.
    - [x] Graphes : structures relationnelles.
    - [x] Sommets, arcs, arêtes, graphes orientés ou non orientés.
- [x] Bases de données.
    - [x] Modèle relationnel : relation, attribut, domaine, clef primaire, clef étrangère, schéma relationnel.
    - [x] Base de données relationnelle.
    - [x] Système de gestion de bases de données relationnelles.
    - [x] Langage SQL : requêtes d’interrogation et de mise à jour d’une base de données.
- [ ] Architectures matérielles, systèmes d’exploitation et réseaux.
    - [ ] Composants intégrés d'un système sur puce.
    - [ ] Gestion des processus et des ressources par un système d'exploitation.
    - [ ] Protocoles de routage.
    - [ ] Sécurisation des communications.
- [ ] Langages et programmation.
    - [ ] Notion de programme en tant que donnée.
    - [ ] Calculabilité, décidabilité.
    - [x] Récursivité.
    - [ ] Modularité.
    - [ ] Paradigmes de programmation.
    - [x] Mise au point des programmes.
    - [ ] Gestion des bugs.
- [ ] Algorithmique.
    - [x] Algorithmes sur les arbres binaires et sur les arbres binaires de recherche.
    - [x] Algorithmes sur les graphes.
    - [ ] Méthode « diviser pour régner ».
    - [x] Programmation dynamique.
    - [ ] Recherche textuelle.