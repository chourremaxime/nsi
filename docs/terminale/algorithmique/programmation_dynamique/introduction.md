On souhaite trouver, dans la pyramide de nombres ci-dessous, le chemin descendant dont la somme des nombres 
est la plus élevée :

```
   4
  1 8
 9 2 1
5 1 6 2
```

Le meilleur chemin est le chemin `4 8 2 6` pour un total de 20 points.

Quel est le meilleur chemin dans la pyramide ci-dessous ?

```
    4
   8 9
  7 2 1
 5 7 2 8
9 1 3 7 6
```

!!! abstract "Définition"

    La **programmation dynamique** est un **paradigme de programmation** qui consiste à éviter de recalculer 
    plusieurs fois la même chose en **stockant les résultats des calculs intermédiaires**.

Ce type de programmation a été inventé en 1950 par Richard Bellman, un mathématicien qu'on aura l'occasion
de voir dans un autre cours.

Il permet donc d'accélérer les algorithmes en stockant les résultats intermédiaires. On parle de **mémoïsation**
lorsque les **valeurs de retour d'une fonction** sont enregistrées.