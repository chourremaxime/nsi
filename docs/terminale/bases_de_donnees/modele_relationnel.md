## Introduction

En 1970, Edgar Frank Codd, chercheur chez IBM, a publié un article intitulé *"A Relational Model of Data for Large
Shared Data Banks"*. Cet article a introduit le **modèle relationnel**, une approche conçue pour organiser et gérer les
données, qui a transformé le domaine des bases de données.

Avant cette publication, les systèmes de gestion de bases de données reposaient principalement sur des modèles
hiérarchiques ou en réseau, souvent complexes et rigides. Le modèle relationnel de Codd, basé sur des concepts
mathématiques tels que la théorie des ensembles et la logique, a permis de simplifier la manière dont les données
sont structurées et interrogées, offrant une plus grande flexibilité et une meilleure indépendance des données par
rapport aux applications.

Cette innovation a jeté les bases des systèmes de gestion de bases de données relationnelles (SGBDR) modernes, largement
utilisés aujourd'hui. Pour sa contribution majeure à l'informatique, Edgar Frank Codd a reçu le prix Turing en 1981.

## Principe du modèle relationnel

Dans un fichier au format odt (LibreOffice Writer), répondez aux questions ci-dessous après avoir consulté la vidéo.
Vous ferez attention à la mise en forme dans votre document.

<iframe title="BDD - Principe du modèle relationnel" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/5c13ad07-5cb0-472a-8086-8f24b7946af9?start=16s&amp;stop=13m45s&amp;title=0&amp;warningTitle=0&amp;peertubeLink=0" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

- **Question 1** : Quel ordre de grandeur peuvent atteindre les bases de données actuelles ?
- **Question 2** : Dans la vidéo, on parle de RGPD, précisez l'acronyme et préciser quel est ce règlement ?
- **Question 3** : Où sont stockées les bases de données ?
- **Question 4** : Quels seront les moyens techniques pour accéder à ces données ?
- **Question 5** : Résumer sous forme graphique le vocabulaire utilisé pour décrire une relation. Vous ferez apparaître sur votre schéma les termes :
    - Relation
    - Attribut
    - t-uplet
- **Question 6** : Expliquer ce qu'est un domaine. Quel(s) avantage(s) présente celui-ci ?
- **Question 7** : Donner la définition de clé primaire et de clé étrangère.
- **Question 8** : Qu'est-ce qu'un schéma relationnel ?
- **Question 9** : Pourquoi faut-il éviter les doublons ?
- **Question 10** : Comment vérifier l'intégrité d'une base de données ?

## Adaptation d'une base de données existante

Toujours dans le même document texte, répondre aux exercices suivants, permettant de mettre en place le schéma
relationnel de la table **film** ci-dessous :

![Table Film](table_films.png)

### Exercice n°1 : Schéma relationnel

Nous souhaitons scinder la table ci-dessus _(qui pourrait être un fichier .csv)_, en 2 tables qui permettront une
meilleure lecture et, à terme, une exploitation bien plus aisée.

Nous modéliserons cette table en se restreignant à ses attributs. Cette modélisation est appelée schéma relationnel.

**Réaliser** le schéma relationnel de la table *Film* et *Réalisateur*.

!!! danger "Attention"

    - Dans vos schémas relationnels, n'oubliez pas de **préciser le type et le domaine** de chaque attribut.
    - Indiquez systématiquement les attributs servant de **clé primaire** ou de **clé étrangère**.

### Exercice n°2 : Table *pays*

Plusieurs attributs de la table d'origine font références à des pays. Nous décidons donc de créer une table des pays
que nous mettrons en relation avec les tables *film* et *réalisateur*. Cette table aura également l'attribut `capitale`.

**Compléter** la table des pays ci-dessous en laissant la première colonne vide _(au cas où)_.

|   | `nom_pays` | `capitale` |
|---|------------|------------|
|   |            |            |
|   |            |            |
|   |            |            |

### Exercice n°3 : Schéma relationnel de la table *pays*

**Réaliser** le schéma relationnel de la table *pays* permettant de stocker le nom des pays, ainsi que l'attribut
`capitale`. Bien préciser le domaine de chaque attribut de cette table.

### Exercice n°4

**Adapter** la relation *film* à la création de la nouvelle relation *pays*.

### Exercice n°5

Adapter la relation *réalisateur* à la création de la nouvelle relation *pays*.

### Exercice n°6

À partir des données contenues dans la table *film*, **donner** le schéma relationnel d'une table *genre*, contenant
les genres des films.

**Ajoutez** à cette relation, l'attribut `ordre_affichage` contenant un entier permettant de connaître l'ordre
d'affichage des genres des films lors d'une recherche sur l'espace utilisateur d'une plateforme de diffusion de médias.
Préciser le domaine de chaque attribut de cette table.

### Exercice n°7 : Schéma relationnel global

Donner le schéma relationnel global présentant les liens entre les tables :

- film
- réalisateur
- pays
- genre

Vous ne manquerez pas de **faire apparaître** sur celui-ci, de manière visible, les différentes **clés primaires** et
les différentes **clés étrangères**.