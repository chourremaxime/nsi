<link rel="stylesheet" href="codemirror.css">
<link rel="stylesheet" href="style.css">
<script type="text/javascript" src="codemirror.js"></script>

## Synopsis

Vous venez de finir brillamment vos études en Bretagne, et, confiant dans vos capacités, vous décidez de monter votre
petite entreprise. Vous avez choisi de vous lancer dans la location de véhicules et en à peine six mois, vous êtes
déjà à la tête d'un réseau d'agences.

Malheureusement, il apparaît que les rentrées financières ne décollent pas vraiment et il est temps d'analyser 
en détail votre historique des locations, le déplacement de vos véhicules et la synergie entre vos différentes agences.

## Le schéma relationnel en image

![Schéma](tp_location.png)

## Analyse des différentes relations

<div style="display: none" id="tp">
    <div id="content" class="content">
    </div>
    <textarea id="commands"></textarea>
    <br>
    <button onclick=test(this)>Tester</button>
    <button onclick=validate(this)>Valider</button>
    <button onclick=giveUp(this)>Abandonner</button>
    <button id="print"  style="display:none" onclick="print()">Générer un PDF</button>
    <div id="error" class="error"></div>
    <pre id="output">Les résultats apparaitront ici</pre>
    <br>
    <br>
    <br>
    <br>
    <script type="text/javascript" src="sql.min.js"></script>
    <script type="text/javascript" src="gestion_agence_location.js"></script>
</div>

Pour chaque analyse, on vous informe des différentes commandes SQL abordées, ainsi que les fonctions à utiliser.

### Analyse 1

**La relation Agences :** vous allez utiliser `SELECT` associé à des `WHERE`, `WHERE LIKE` et `WHERE IN`. Vous
utiliserez également les fonctions `COUNT` et `SUBSTR`.

[Charger le TP](#analyse-des-differentes-relations){ .md-button ; onclick="load('locations_1', 'locations')" }

### Analyse 2

**La relation Vehicules :** vous allez utiliser `SELECT` avec `AS` et `ORDER BY`. Vous utiliserez également les
fonctions `MAX`, `MIN`, `AVG`.

[Charger le TP](#analyse-des-differentes-relations){ .md-button ; onclick="load('locations_2', 'locations')" }


### Analyse 3


**La relation Locations :** vous définirez des jointures entre les trois relations avec des `JOIN AS ON`.


[Charger le TP](#analyse-des-differentes-relations){ .md-button ; onclick="load('locations_3', 'locations')" }


### Analyse 4


**Gestion du réseau :** vous utiliserez les commandes `UPDATE`, `INSERT` et `DELETE`.


[Charger le TP](#analyse-des-differentes-relations){ .md-button ; onclick="load('locations_4', 'locations')" }

_Ce TP est propulsé par [SQLite compiled to JavaScript](https://sql.js.org/)._

_Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution 4.0 International,
adapté du TP de Jacques Le Coupanec._