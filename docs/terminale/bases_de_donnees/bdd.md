Avec l'explosion des **volumes de données à traiter**, l'organisation et le stockage efficace de ces informations sont
devenus des **enjeux majeurs** pour garantir la performance des systèmes informatiques.

Les **bases de données relationnelles** constituent aujourd'hui une des solutions pour répondre à ces besoins. Elles
permettent de structurer, stocker, mettre à jour et interroger de grandes quantités de données organisées. Ces données,
souvent utilisées simultanément par plusieurs programmes ou utilisateurs, dépassent largement les capacités des simples
représentations sous forme de tableau (csv, tableur) abordées en classe de première.

Les **Systèmes de Gestion de Bases de Données (SGBD)**, pouvant atteindre des tailles gigantesques, jouent un rôle
central dans les dispositifs modernes de collecte, de stockage et de production d'informations.

L'accès aux données dans une base relationnelle repose sur des requêtes conçues pour interroger ou modifier ces
informations. Le langage **SQL (Structured Query Language)** est fréquemment utilisé à cette fin, souvent en complément
d'un langage de programmation pour créer des traitements complexes et automatisés.