# Programme de la NSI - De la Première à la Terminale

Bienvenue dans ce *petit* site pour découvrir la NSI, présentée par un enseignant au Lycée Charles Mérieux à Lyon.

**Vous trouverez les différents accès via le menu supérieur**, allant de la première à la terminale.

## Ressources officielles

- Site web [Eduscol](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-g)
- Programme officiel à télécharger :fontawesome-regular-file-pdf:
    - [Programme de première](https://eduscol.education.fr/document/30007/download)
    - [Programme de terminale](https://eduscol.education.fr/document/30010/download)

## Autres sites web

- [PythonTutor](https://pythontutor.com/python-compiler.html#mode=edit) - Visualiser l'exécution d'un programme Python
- [CodEx](https://codex.forge.apps.education.fr/) - Exercices Python corrigés
- [ZoneNSI](https://www.zonensi.fr/) - Cours de NSI
- [Lycée Diderot](https://portail.lyc-la-martiniere-diderot.ac-lyon.fr/srv1/) - Cours de NSI d'un lycée lyonnais