function definirCouleur() {
    /* récupération des choix : */
    const choixFondParagraphe = document.getElementById("FondParagraphe");
    const choixCouleurParagraphe = document.getElementById("CouleurParagraphe");
    const choixCouleurImportant = document.getElementById("CouleurImportant");

    /* application des choix à la boite d'illustration : */
    const boiteIllustrative = document.getElementById("boiteIllustrative");
    boiteIllustrative.style.backgroundColor = choixFondParagraphe.value;
    boiteIllustrative.style.color = choixCouleurParagraphe.value;

    const important = document.getElementById("important");
    important.style.color = choixCouleurImportant.value;

    /*  affichage des choix :  */
    const le_code = document.getElementById("choixAttributs").nextElementSibling.getElementsByTagName("code")[0];

    le_code.innerHTML = `<span class="nt">p</span><span class="w"> </span><span class="p">{</span><span class="w">                                 </span><span class="c">/* Mise en forme des paragraphes */</span>
<span class="w">    </span><span class="k">background-color</span><span class="p">:</span><span class="w"> </span><span class="mh">${choixFondParagraphe.value}</span><span class="p">;</span><span class="w"> </span>
<span class="w">    </span><span class="k">color</span><span class="p">:</span><span class="w"> </span><span class="mh">${choixCouleurParagraphe.value}</span><span class="p">;</span><span class="w"> </span>
<span class="p">}</span><span class="w"> </span>

<span class="nt">strong</span><span class="w"> </span><span class="p">{</span><span class="w">                            </span><span class="c">/* Mise en forme des mots importants */</span>
<span class="w">    </span><span class="k">color</span><span class="p">:</span><span class="w"> </span><span class="mh">${choixCouleurImportant.value}</span><span class="p">;</span><span class="w"> </span>
<span class="p">}</span>`;
}


function definirBordure() {
    /* récupération des choix : */
    const choixEpaisseur = document.getElementById("choixEpaisseur");
    const choixStyle = document.getElementById("choixStyle");
    const choixCouleur = document.getElementById("choixCouleur");


    /* application des choix à la boite d'illustration : */
    const boiteIllustrative = document.getElementById("boiteIllustrative");
    boiteIllustrative.style.borderWidth = choixEpaisseur.value + 'px';
    boiteIllustrative.style.borderColor = choixCouleur.value;
    boiteIllustrative.style.borderStyle = choixStyle.value;

    /*  affichage des choix :  */
    const le_code = document.getElementById("choixAttributsBordures").nextElementSibling.getElementsByTagName("code")[0];

    le_code.innerHTML = `<span class="nt">p</span><span class="w"> </span><span class="p">{</span><span class="w">                                 </span><span class="c">/* Mise en forme des paragraphes */</span>
<span class="w">    </span><span class="k">border-width</span><span class="p">:</span><span class="w"> </span><span class="mi">${choixEpaisseur.value}</span><span class="kt">px</span><span class="p">;</span><span class="w"> </span>
<span class="w">    </span><span class="k">border-style</span><span class="p">:</span><span class="w"> </span><span class="kc">${choixStyle.value}</span><span class="p">;</span><span class="w"> </span>
<span class="w">    </span><span class="k">border-color</span><span class="p">:</span><span class="w"> </span><span class="mh">${choixCouleur.value}</span><span class="p">;</span><span class="w"> </span>
<span class="p">}</span>`;

    /*  affichage du raccourci :  */
    const le_code2 = document.getElementById("choixAttributsBordures").nextElementSibling.nextElementSibling.getElementsByTagName("code")[0];

    le_code2.innerHTML = `<span class="nt">p</span><span class="w"> </span><span class="p">{</span><span class="w"> </span>
<span class="w">    </span><span class="k">border</span><span class="p">:</span><span class="w"> </span><span class="mi">${choixEpaisseur.value}</span><span class="kt">px</span><span class="w"> </span><span class="kc">${choixStyle.value}</span><span class="w"> </span><span class="mh">${choixCouleur.value}</span><span class="p">;</span><span class="w"> </span>
<span class="p">}</span>`;
}


function definirBordure2() {
    /* récupération des choix pour le haut : */
    const choixEpaisseurHaut = document.getElementById("choixEpaisseurHaut");
    const choixStyleHaut = document.getElementById("choixStyleHaut");
    const choixCouleurHaut = document.getElementById("choixCouleurHaut");

    /* récupération des choix pour la droite : */
    const choixEpaisseurDroit = document.getElementById("choixEpaisseurDroit");
    const choixStyleDroit = document.getElementById("choixStyleDroit");
    const choixCouleurDroit = document.getElementById("choixCouleurDroit");

    /* récupération des choix pour le bas : */
    const choixEpaisseurBas = document.getElementById("choixEpaisseurBas");
    const choixStyleBas = document.getElementById("choixStyleBas");
    const choixCouleurBas = document.getElementById("choixCouleurBas");

    /* récupération des choix pour la gauche : */
    const choixEpaisseurGauche = document.getElementById("choixEpaisseurGauche");
    const choixStyleGauche = document.getElementById("choixStyleGauche");
    const choixCouleurGauche = document.getElementById("choixCouleurGauche");


    /* application des choix à la boite d'illustration : */
    const boiteIllustrative = document.getElementById("boiteIllustrative2");

    boiteIllustrative.style.borderTopWidth = choixEpaisseurHaut.value + 'px';
    boiteIllustrative.style.borderTopColor = choixCouleurHaut.value;
    boiteIllustrative.style.borderTopStyle = choixStyleHaut.value;

    boiteIllustrative.style.borderRightWidth = choixEpaisseurDroit.value + 'px';
    boiteIllustrative.style.borderRightColor = choixCouleurDroit.value;
    boiteIllustrative.style.borderRightStyle = choixStyleDroit.value;

    boiteIllustrative.style.borderBottomWidth = choixEpaisseurBas.value + 'px';
    boiteIllustrative.style.borderBottomColor = choixCouleurBas.value;
    boiteIllustrative.style.borderBottomStyle = choixStyleBas.value;

    boiteIllustrative.style.borderLeftWidth = choixEpaisseurGauche.value + 'px';
    boiteIllustrative.style.borderLeftColor = choixCouleurGauche.value;
    boiteIllustrative.style.borderLeftStyle = choixStyleGauche.value;

    /*  affichage des choix :  */
    const le_code = document.getElementById("choixAttributsIdentifier").nextElementSibling.getElementsByTagName("code")[0];

    le_code.innerHTML = `<span class="nt">p</span><span class="w"> </span><span class="p">{</span><span class="w">                                     </span><span class="c">/* Mise en forme des paragraphes */</span>
<span class="w">    </span><span class="k">border-top</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="mi">${choixEpaisseurHaut.value}</span><span class="kt">px</span><span class="w"> </span><span class="kc">${choixStyleHaut.value}</span><span class="w"> </span><span class="mh">${choixCouleurHaut.value}</span><span class="p">;</span><span class="w"> </span>
<span class="w">    </span><span class="k">border-right</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="mi">${choixEpaisseurDroit.value}</span><span class="kt">px</span><span class="w"> </span><span class="kc">${choixStyleDroit.value}</span><span class="w"> </span><span class="mh">${choixCouleurDroit.value}</span><span class="p">;</span><span class="w"> </span>
<span class="w">    </span><span class="k">border-bottom</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="mi">${choixEpaisseurBas.value}</span><span class="kt">px</span><span class="w"> </span><span class="kc">${choixStyleBas.value}</span><span class="w"> </span><span class="mh">${choixCouleurBas.value}</span><span class="p">;</span><span class="w"> </span>
<span class="w">    </span><span class="k">border-left</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="mi">${choixEpaisseurGauche.value}</span><span class="kt">px</span><span class="w"> </span><span class="kc">${choixStyleGauche.value}</span><span class="w"> </span><span class="mh">${choixCouleurGauche.value}</span><span class="p">;</span><span class="w"> </span>
<span class="p">}</span>`;
}


function definirBordure3() {
    /* récupération des choix : */
    const choixEpaisseur = document.getElementById("choixEpaisseurArrondi");
    const choixStyle = document.getElementById("choixStyleArrondi");
    const choixCouleur = document.getElementById("choixCouleurArrondi");
    const BBLR = document.getElementById("choixBBLR");
    const BBRR = document.getElementById("choixBBRR");
    const BTLR = document.getElementById("choixBTLR");
    const BTRR = document.getElementById("choixBTRR");


    /* application des choix à la boite d'illustration : */
    const boiteIllustrative = document.getElementById("boiteIllustrative3");
    boiteIllustrative.style.borderWidth = choixEpaisseur.value + 'px';
    boiteIllustrative.style.borderColor = choixCouleur.value;
    boiteIllustrative.style.borderStyle = choixStyle.value;
    boiteIllustrative.style.borderRadius = BTLR.value + 'px ' + BTRR.value + 'px ' + BBRR.value + 'px ' + BBLR.value + 'px ';

    /*  affichage des choix :  */
    const le_code = document.getElementById("choixAttributsAngles").nextElementSibling.getElementsByTagName("code")[0];

    le_code.innerHTML = `<span class="nt">p</span><span class="w"> </span><span class="p">{</span><span class="w">                                 </span><span class="c">/* Mise en forme des paragraphes */</span>
<span class="w">    </span><span class="k">border</span><span class="p">:</span><span class="w"> </span><span class="mi">${choixEpaisseur.value}</span><span class="kt">px</span><span class="w"> </span><span class="kc">${choixStyle.value}</span><span class="w"> </span><span class="mh">${choixCouleur.value}</span><span class="p">;</span><span class="w">       </span><span class="c">/* Épaisseur - Apparence - Couleur */</span>
<span class="w">    </span><span class="k">border-radius</span><span class="p">:</span><span class="w"> </span><span class="mi">${BTLR.value}</span><span class="kt">px</span><span class="w"> </span><span class="mi">${BTRR.value}</span><span class="kt">px</span><span class="w"> </span><span class="mi">${BBRR.value}</span><span class="kt">px</span><span class="w"> </span><span class="mi">${BBLR.value}</span><span class="kt">px</span><span class="p">;</span><span class="w"> </span><span class="c">/* Haut-Gauche Haut-Droit Bas-Droit Bas-Gauche */</span>
<span class="p">}</span>`;
}


function definirDimensions() {
    /* récupération des choix : */
    const choixLargeur = document.getElementById("choixLargeur");
    const choixHauteur = document.getElementById("choixHauteur");
    const choixEpaisseur = document.getElementById("choixEpaisseur");
    const choixStyle = document.getElementById("choixStyle");
    const choixCouleur = document.getElementById("choixCouleur");
    const choixPadding = document.getElementById("choixPadding");
    const choixMargin = document.getElementById("choixMargin");


    /* application des choix à la boite d'illustration : */
    const boiteIllustrative = document.getElementById("boiteIllustrative");
    boiteIllustrative.style.width = choixLargeur.value + 'px';
    boiteIllustrative.style.height = choixHauteur.value + 'px';
    boiteIllustrative.style.borderWidth = choixEpaisseur.value + 'px';
    boiteIllustrative.style.borderStyle = choixStyle.value;
    boiteIllustrative.style.borderColor = choixCouleur.value;
    boiteIllustrative.style.padding = choixPadding.value + 'px';
    boiteIllustrative.style.margin = choixMargin.value + 'px';

    /*  affichage des choix :  */
    const le_code = document.getElementById("choixAttributsDimensions").nextElementSibling.getElementsByTagName("code")[0];

    le_code.innerHTML = `<span class="nt">p</span><span class="w"> </span><span class="p">{</span><span class="w">                                </span><span class="c">/* Mise en forme des paragraphes */</span>
<span class="w">    </span><span class="k">width</span><span class="p">:</span><span class="w"> </span><span class="mi">${choixLargeur.value}</span><span class="kt">px</span><span class="p">;</span><span class="w">                  </span><span class="c">/* Largeur du contenu */</span>
<span class="w">    </span><span class="k">height</span><span class="p">:</span><span class="w"> </span><span class="mi">${choixHauteur.value}</span><span class="kt">px</span><span class="p">;</span><span class="w">                 </span><span class="c">/* Hauteur du contenu */</span>
<span class="w">    </span><span class="k">border</span><span class="p">:</span><span class="w"> </span><span class="mi">${choixEpaisseur.value}</span><span class="kt">px</span><span class="w"> </span><span class="kc">${choixStyle.value}</span><span class="w"> </span><span class="mh">${choixCouleur.value}</span><span class="p">;</span><span class="w">      </span><span class="c">/* Épaisseur - Apparence - Couleur */</span>
<span class="w">    </span><span class="k">padding</span><span class="p">:</span><span class="w"> </span><span class="mi"${choixPadding.value}</span><span class="kt">px</span><span class="p">;</span><span class="w">                  </span><span class="c">/* Espace "intérieur" */</span>
<span class="w">    </span><span class="k">margin</span><span class="p">:</span><span class="w"> </span><span class="mi">${choixMargin.value}</span><span class="kt">px</span><span class="p">;</span><span class="w">                   </span><span class="c">/* Espace "extérieur" */</span>
<span class="p">}</span>`;
}


function definirTexte() {
    /* récupération des choix : */
    const choixCouleur = document.getElementById("choixCouleur");
    const choixTaille = document.getElementById("choixTaille");
    const choixPolice = document.getElementById("choixPolice");
    const choixGraisse = document.getElementById("choixGraisse");
    const choixItalique = document.getElementById("choixItalique");
    const choixDecoration = document.getElementById("choixDecoration");
    const choixMajuscules = document.getElementById("choixMajuscules");


    /* application des choix à la boite d'illustration : */
    const boiteIllustrative = document.getElementById("boiteIllustrative");
    boiteIllustrative.style.color = choixCouleur.value;
    boiteIllustrative.style.fontSize = choixTaille.value + 'px';
    boiteIllustrative.style.fontFamily = choixPolice.value;
    boiteIllustrative.style.fontWeight = choixGraisse.value;
    boiteIllustrative.style.fontStyle = choixItalique.value;
    boiteIllustrative.style.textDecoration = choixDecoration.value;
    boiteIllustrative.style.fontVariant = choixMajuscules.value;

    /*  affichage des choix :  */
    const le_code = document.getElementById("choixAttributsTextes").nextElementSibling.getElementsByTagName("code")[0];

    le_code.innerHTML = `<span class="nt">p</span><span class="w"> </span><span class="p">{</span><span class="w">                                 </span><span class="c">/* Mise en forme des paragraphes */</span>
<span class="w">    </span><span class="k">color</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="mh">${choixCouleur.value}</span><span class="p">;</span><span class="w">                </span><span class="c">/* Couleur du texte */</span>
<span class="w">    </span><span class="k">font-size</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="mi">${choixTaille.value}</span><span class="kt">px</span><span class="p">;</span><span class="w">               </span><span class="c">/* Taille de la police en pixels */</span>
<span class="w">    </span><span class="k">font-family</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="n">${choixPolice.value}</span><span class="p">;</span><span class="w">  </span><span class="c">/* Police de caractère */</span>
<span class="w">    </span><span class="k">font-weight</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="kc">${choixGraisse.value}</span><span class="p">;</span><span class="w">           </span><span class="c">/* Graisse de la police */</span>
<span class="w">    </span><span class="k">font-style</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="kc">${choixItalique.value}</span><span class="p">;</span><span class="w">            </span><span class="c">/* Apparence de la police */</span>
<span class="w">    </span><span class="k">text-decoration</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="kc">${choixDecoration.value}</span><span class="p">;</span><span class="w">         </span><span class="c">/* Décoration de la police */</span>
<span class="w">    </span><span class="k">font-variant</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="kc">${choixMajuscules.value}</span><span class="p">;</span><span class="w">          </span><span class="c">/* Affichage de la police */</span>
<span class="p">}</span>`;
}


function ombrer() {
    /* récupération des choix : */
    const decalageHorizontalTexte = document.getElementById("decalageHorizontalTexte");
    const decalageVerticalTexte = document.getElementById("decalageVerticalTexte");
    const flouTexte = document.getElementById("flouTexte");
    const couleurOmbreTexte = document.getElementById("couleurOmbreTexte");
    const interligne = document.getElementById("interligne");

    const decalageHorizontalBoite = document.getElementById("decalageHorizontalBoite");
    const decalageVerticalBoite = document.getElementById("decalageVerticalBoite");
    const flouBoite = document.getElementById("flouBoite");
    const couleurOmbreBoite = document.getElementById("couleurOmbreBoite");


    /* application des choix à la boite d'illustration : */
    const boiteIllustrative = document.getElementById("boiteIllustrative2");
    boiteIllustrative.style.textShadow = decalageHorizontalTexte.value + 'px ' + decalageVerticalTexte.value + 'px ' + flouTexte.value + 'px ' + couleurOmbreTexte.value;
    boiteIllustrative.style.lineHeight = interligne.value;
    boiteIllustrative.style.boxShadow = decalageHorizontalBoite.value + 'px ' + decalageVerticalBoite.value + 'px ' + flouBoite.value + 'px ' + couleurOmbreBoite.value;


    /*  affichage des choix :  */
    const le_code = document.getElementById("choixAttributsOmbres").nextElementSibling.getElementsByTagName("code")[0];

    le_code.innerHTML = `<span class="nt">p</span><span class="w"> </span><span class="p">{</span><span class="w">                                     </span><span class="c">/* Mise en forme des paragraphes */</span>
<span class="w">    </span><span class="k">text-shadow</span><span class="p">:</span><span class="w"> </span><span class="mi">${decalageHorizontalTexte.value}</span><span class="kt">px</span><span class="w"> </span><span class="mi">${decalageVerticalTexte.value}</span><span class="kt">px</span><span class="w"> </span><span class="mi">${flouTexte.value}</span><span class="kt">px</span><span class="w"> </span><span class="mh">${couleurOmbreTexte.value}</span><span class="p">;</span>
<span class="w">    </span><span class="k">line-height</span><span class="p">:</span><span class="w"> </span><span class="mf">${interligne.value}</span><span class="p">;</span>
<span class="w">    </span><span class="k">box-shadow</span><span class="p">:</span><span class="w"> </span><span class="mi">${decalageHorizontalBoite.value}</span><span class="kt">px</span><span class="w"> </span><span class="mi">${decalageVerticalBoite.value}</span><span class="kt">px</span><span class="w"> </span><span class="mi">${flouBoite.value}</span><span class="kt">px</span><span class="w"> </span><span class="mh">${couleurOmbreBoite.value}</span><span class="p">;</span>
<span class="p">}</span>`;
}




function definirFlex() {
    /* récupération des choix : */
    const choixDirection = document.getElementById("choixDirection").value;
    const choixWrap = document.getElementById("choixWrap").value;
    const choixAlign1 = document.getElementById("choixAlign1").value;
    const choixAlign2 = document.getElementById("choixAlign2").value;


    /* application des choix à la boite d'illustration : */
    const boiteIllustrative = document.getElementById("boiteIllustrative");
    boiteIllustrative.style.display = 'flex';
    boiteIllustrative.style.flexDirection = choixDirection;
    boiteIllustrative.style.flexWrap = choixWrap;
    boiteIllustrative.style.justifyContent = choixAlign1;
    boiteIllustrative.style.alignItems = choixAlign2;

    /*  affichage des choix :  */
    const le_code = document.getElementById("choixAttributsFlex").nextElementSibling.getElementsByTagName("code")[0];

    le_code.innerHTML = `<span class="nt">p</span><span class="w"> </span><span class="p">{</span><span class="w">     </span><span class="c">/* Mise en forme des paragraphes */</span>
<span class="w">    </span><span class="k">display</span><span class="p">:</span><span class="w"> </span><span class="kc">flex</span><span class="p">;</span>
<span class="w">    </span><span class="k">flex-direction</span><span class="p">:</span><span class="w"> </span><span class="kc">${choixDirection}</span><span class="p">;</span>
<span class="w">    </span><span class="k">flex-wrap</span><span class="p">:</span><span class="w"> </span><span class="kc">${choixWrap}</span><span class="p">;</span>
<span class="w">    </span><span class="k">justify-content</span><span class="p">:</span><span class="w"> </span><span class="kc">${choixAlign1}</span><span class="p">;</span>
<span class="w">    </span><span class="k">align-items</span><span class="p">:</span><span class="w"> </span><span class="kc">${choixAlign2}</span><span class="p">;</span>
<span class="p">}</span>`;
}


function definirHover() {
    /* récupération des choix : */
    const choixArrierePlan = document.getElementById("choixArrierePlan").value;
    const choixCouleur = document.getElementById("choixCouleur").value;
    const choixEpaisseur = document.getElementById("choixEpaisseur").value;
    const choixStyleBordure = 'solid';
    const choixGraisse = document.getElementById("choixGraisse").value;
    const choixDecoration = document.getElementById("choixDecoration").value;

    /* application des choix à la boite d'illustration : */
    const boiteIllustrative = document.getElementById("boiteIllustrative");
    boiteIllustrative.style.backgroundImage = 'url("../' + choixArrierePlan + '")';
    boiteIllustrative.style.borderColor = choixCouleur;
    boiteIllustrative.style.borderWidth = choixEpaisseur + 'px';
    boiteIllustrative.style.borderStyle = choixStyleBordure;
    boiteIllustrative.style.fontWeight = choixGraisse;
    boiteIllustrative.style.textDecoration = choixDecoration;
}


function definirNormal() {
    const boiteIllustrative = document.getElementById("boiteIllustrative");
    boiteIllustrative.style.backgroundImage = "";
    boiteIllustrative.style.borderColor = 'black';
    boiteIllustrative.style.borderWidth = '0';
    boiteIllustrative.style.borderStyle = 'solid';
    boiteIllustrative.style.fontWeight = 'normal';
    boiteIllustrative.style.textDecoration = 'none';
}


function affichageHover() {
    /* récupération des choix : */
    const choixArrierePlan = document.getElementById("choixArrierePlan").value;
    const choixCouleur = document.getElementById("choixCouleur").value;
    const choixEpaisseur = document.getElementById("choixEpaisseur").value;
    const choixStyleBordure = 'solid';
    const choixGraisse = document.getElementById("choixGraisse").value;
    const choixDecoration = document.getElementById("choixDecoration").value;

    /*  affichage des choix :  */
    const le_code = document.getElementById("choixAttributsHover").nextElementSibling.getElementsByTagName("code")[0];

    le_code.innerHTML = `<span class="nt">p</span><span class="p">:</span><span class="nd">hover</span><span class="w"> </span><span class="p">{</span><span class="w">      </span><span class="c">/* Mise en forme lors du survol */</span>
<span class="w">    </span><span class="k">background-image</span><span class="p">:</span><span class="w"> </span><span class="nb">url</span><span class="p">(</span><span class="s2">"${choixArrierePlan}"</span><span class="p">);</span><span class="w"> </span>
<span class="w">    </span><span class="k">border</span><span class="p">:</span><span class="w"> </span><span class="mi">${choixEpaisseur}</span><span class="kt">px</span><span class="w"> </span><span class="kc">${choixStyleBordure}</span><span class="w"> </span><span class="mh">${choixCouleur}</span><span class="p">;</span><span class="w"> </span>
<span class="w">    </span><span class="k">font-weight</span><span class="p">:</span><span class="w"> </span><span class="kc">${choixGraisse}</span><span class="p">;</span><span class="w"> </span>
<span class="w">    </span><span class="k">text-decoration</span><span class="p">:</span><span class="w"> </span><span class="kc">${choixDecoration}</span><span class="p">;</span><span class="w"> </span>
<span class="p">}</span>`;
}